<?php

namespace App\Policies;

use App\Models\Advert;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class AdvertPolicy
{
    use HandlesAuthorization;

    const UPDATE = 'update';

    public function update(User $user, Advert $advert)
    {
        return $user->id === $advert->user_id || $user->role === 'admin';
    }
}
