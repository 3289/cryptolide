<?php

namespace App\Policies;

use App\Models\Deal;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class DealPolicy
{
    use HandlesAuthorization;

    const ACCESS = 'access';

    public function access(User $user, Deal $deal)
    {
        return $user->role === 'admin' || $user->id === $deal->user_id || $user->id === $deal->creator_id;
    }
}
