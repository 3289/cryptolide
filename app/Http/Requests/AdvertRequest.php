<?php

namespace App\Http\Requests;

use App\Models\AdditionalCryptoCurrency;
use App\Models\CryptoCurrency;
use App\Models\Currency;
use Illuminate\Validation\Rule;

class AdvertRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->getPathInfo() == '/validate-advert-copy') {
            return [];
        }
        $crypto = CryptoCurrency::findOrFail($this->get('crypto_id'));

        $rules = [
            'crypto_id' => 'bail|required|numeric|exists:crypto_currencies,id',
            'type' => 'bail|required|string|in:buy_for_cash,buy_online,sell_for_cash,sell_online,trade',
        ];

        if (in_array($this->get('type'), ['buy_online', 'sell_online'])) {
            $rules['payment_type_id'] = 'bail|required|numeric|exists:payment_types,id';
        }

        if ($this->get('type') != 'trade') {
            $rules['country_id'] = 'bail|required|numeric|exists:countries,id';
            $rules['currency_id'] = 'bail|required|numeric|exists:currencies,id';
        } else {
            $rules['crypto_trade_id'] = 'bail|required|numeric|exists:additional_crypto_currencies,id';
        }

        if (in_array($this->get('type'), ['buy_for_cash', 'sell_for_cash'])) {
            $rules['city_id'] = [
                'bail', 'required', Rule::exists('cities', 'id')->where(function ($query) {
                    return $query->where('country_id', $this->get('country_id'));
                }),
            ];
        }

        $rules = array_merge($rules, [
            'description' => 'nullable|string|max:500',
            'rate' => 'required|numeric|min:0',
            'achievement_id' => 'nullable|exists:achievements,id',
            'time' => 'required|numeric|min:15|max:4320',
        ]);

        if ($this->get('constraints-1')) {
            $rules['min_crypto'] = 'nullable|numeric|min:0|max:' . (float) $this->get('sum_crypto', 0);
            $rules['min_currency'] = 'nullable|numeric|min:1';
        }

        if (in_array($this->get('type'), ['buy_online', 'sell_online']) && $this->get('payment_type_id') == 2) {
            $rules['bank_ids'] = 'required';
        }

        $rules['sum_crypto'] = 'required|numeric|min:0.0005';
        if ($this->get('sum_crypto')>25){
            $rules['sum_crypto'] .= '|max:25';
        }
        elseif ($this->get('type') != 'buy_online' && $this->get('type') != 'buy_for_cash') {
            $total_amount = (auth()->user()->balance[$crypto->short] / 100 * (100-auth()->user()->commission)) + 0.00000001;
            $rules['sum_crypto'] .= '|max:' . $total_amount;
        }


        $rules['keep_up'] = 'nullable|numeric';

        $rules['requisites'] = 'nullable|string|max:500';

        if ($this->has('constraints')) {
            $rules['constraints'] = 'required|numeric';
            $rules['constraints_min'] = 'nullable|numeric|min:0';
            $rules['constraints_max'] = 'nullable|numeric|min:0';
        }

        return $rules;
    }

    public function messages()
    {
        $name = '';
        if (isset($this->currency_id)) {
            $name = Currency::findOrFail($this->currency_id)
                ->name;
        } else if ($this->type == 'trade') {
            $name = AdditionalCryptoCurrency::findOrFail($this->crypto_trade_id)
                ->short;
        }

        return [
            'crypto_id.required' => 'Не выбрана криптовалюта.',
            'crypto_id.numeric' => 'Некорректный ID криптовалюты.',
            'crypto_id.exists' => 'Такая криптовалюта не найдена.',

            'type.required' => 'Не выбран тип объявления.',
            'type.in' => 'Некорректный тип объявления.',

            'description.required' => 'Не указано описание.',
            'description.max' => 'Максимальная длина описания :max символов.',

            'rate.required' => 'Не указан курс.',
            'rate.numeric' => 'Курс должен быть числом.',
            'rate.min' => 'Минимальный курс :min.',

            'min_crypto.required' => 'Укажите сумму покупки криптовалюты.',
            'min_crypto.numeric' => 'Минимальная сумма криптовалюты должна быть числом.',
            'min_crypto.min' => 'Минимальная сумма криптовалюты :min.',
            'min_crypto.max' => 'Минимальная сумма для открытия сделки не может превышать сумму сделки.',

            'min_currency.min' => 'Минимальная сумма сделки должна быть больше: 1.00 '.$name.'.',

            'sum_crypto.required' => 'Не указана сумма криптовалюты.',
            'sum_crypto.numeric' => 'Сумма криптовалюты должна быть числом.',
            'sum_crypto.min' => 'Сумма криптовалюты не может быть меньше :min.',
            'sum_crypto.max' => $this->get('sum_crypto')>25 ? 'Максимальная сумма криптовалюты не может быть больше :max BTC.' : 'На вашем балансе недостаточно криптовалюты.',

            'payment_type_id.required' => 'Не указан способ оплаты.',
            'payment_type_id.numeric' => 'Некорректный способ оплаты',
            'payment_type_id.exists' => 'Такой способ оплаты не найден.',

            'country_id.*' => 'Укажите вашу страну.',

            'currency_id.*' => 'Укажите вашу валюту.',

            'crypto_trade_id.required' => 'Не указана криптовалюта к обмену.',
            'crypto_trade_id.numeric' => 'Некорректный ID криптовалюты.',
            'crypto_trade_id.exists' => 'Такая криптовалюта не найдена.',

            'city_id.*' => 'Укажите город, в котором хотите провести сделку за наличные.',

            'bank_id.required' => 'Не указан банк.',
            'bank_id.numeric' => 'Некорректный ID банка.',

            'keep_up.required' => '1...',
            'keep_up.numeric' => '2...',

            'time.required' => 'Не указано время сделки.',
            'time.numeric' => 'Время сделки должно быть числом.',
            'time.min' => 'Минимальное время сделки :min минут.',
            'time.max' => 'Время на оплату сделки не должно превышать 4320 минут (72 часа).',

            'requisites.required' => 'Не указаны реквизиты.',
            'requisites.max' => 'Максимальная длина реквизитов :max символов.',

            'constraints.required' => '...',
            'constraints.numeric' => '...',

            'constraints_min.required' => 'Не указана минимальная сумма ограничения.',
            'constraints_min.numeric' => 'Минимальная сумма ограничения должна быть числом.',
            'constraints_min.min' => 'Минимальная сумма ограничения не может быть меньше :min.',

            'constraints_max.required' => 'Не указана максимальная сумма ограничения.',
            'constraints_max.numeric' => 'Максимальная сумма ограничения должна быть числом.',
            'constraints_max.min' => 'Максимальная сумма ограничения не может быть меньше :min.',

            'achievement_id.exists' => 'Такое достижение не найдено.',
        ];
    }
}
