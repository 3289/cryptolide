<?php

namespace App\Http\Requests;

class WithdrawalRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'address' => 'required|string|min:31|max:36',
            'amount' => 'required|numeric|min:0.00005',
            'random_hash' => 'required|string|size:16',
        ];
    }

    public function messages()
    {
        return [
            'amount.required' => 'Не указана сумма вывода.',
            'amount.numeric' => 'Сумма вывода должна быть числом.',
            'amount.min' => 'Сумма вывода не может быть меньше :min.',

            'address.required' => 'Не указан адрес.',
            'address.min' => 'Введен некорректный адрес.',
            'address.max' => 'Введен некорректный адрес.',
        ];
    }
}
