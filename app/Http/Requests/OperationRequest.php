<?php

namespace App\Http\Requests;

class OperationRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'amount' => 'required|numeric',
            'commission' => 'required|numeric',
            'crypto_id' => 'required|numeric|exists:crypto_currencies,id',
            'address' => 'required|string|exists:addresses,address',
            'tx' => 'required|string',
        ];
    }

    public function messages()
    {
        return [
            'amount.required' => 'Не указана сумма.',
            'amount.numeric' => 'Сумма должна быть числом.',

            'commission.required' => 'Не указана комиссия.',
            'commission.numeric' => 'Комиссия должна быть числом.',

            'crypto_id.required' => 'Не указана криптовалюта.',
            'crypto_id.exists' => 'Такая криптовалюта не найдена в базе.',

            'address.required' => 'Не указан адрес.',
            'address.exists' => 'Такой адрес не найден в базе.',

            'tx.required' => 'Не указан tx.',
        ];
    }
}
