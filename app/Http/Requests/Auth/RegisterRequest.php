<?php

namespace App\Http\Requests\Auth;

use App\Http\Requests\Request;

class RegisterRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:15|regex:/(^([a-zA-Zа-яА-ЯёЁ\-]+)?$)/u',
            'login' => 'required|string|min:3|max:10|unique:users|regex:/(^([a-zA-Z0-9\-]+)?$)/',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'phone' => 'required|digits_between:10,12',
            'g-recaptcha-response' => 'required|captcha',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Не указано имя.',
            'name.max' => 'Максимальная длина имени 15 символов.',
            'name.regex' => 'Некорректное имя.',

            'login.required' => 'Не указан логин.',
            'login.min' => 'Минимальная длина логина 3 символа.',
            'login.max' => 'Максимальная длина логина 10 символов.',
            'login.unique' => 'Данный логин уже используется.',
            'login.regex' => 'Логин должен содержать только латинские буквы и цифры.',

            'email.required' => 'Не указан e-mail.',
            'email.email' => 'Некорректный e-mail.',
            'email.max' => 'Максимальная длина e-mail 255 символов.',
            'email.unique' => 'Данный e-mail уже используется в системе.',

            'password.required' => 'Не указан пароль.',
            'password.min' => 'Минимальная длина пароля 6 символов.',
            'password.confirmed' => 'Пароли не совпадают.',

            'phone.required' => 'Не указан номер телефона.',
            'phone.numeric' => 'Телефонный номер не может содержать различные символы.',
            'phone.digits_between' => 'Телефонный номер должен состоять из не менее чем 10 и не более чем 12 цифр.',

            'g-recaptcha-response.*' => 'Пожалуйста, подтвердите что вы реальный пользователь.',
        ];
    }
}
