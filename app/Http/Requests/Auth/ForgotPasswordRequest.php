<?php

namespace App\Http\Requests\Auth;

use App\Http\Requests\Request;

class ForgotPasswordRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|string|email',
        ];
    }

    public function messages()
    {
        return [
            'email.required' => 'Не указан e-mail.',
            'email.email' => 'Некорректный e-mail.',
        ];
    }
}
