<?php

namespace App\Http\Requests\Auth;

use App\Http\Requests\Request;

class LoginRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email|exists:users,email',
            'password' => 'required|string',
            'g-recaptcha-response' => 'required|captcha',
        ];
    }

    public function messages()
    {
        return [
            'email.required' => 'Не указан e-mail.',
            'email.email' => 'E-mail введен с ошибкой.',
            'email.exists' => 'Аккаунт с таким e-mail не найден.',

            'password.required' => 'Не указан пароль.',

            'g-recaptcha-response.*' => 'Пожалуйста, подтвердите что вы реальный пользователь.',
        ];
    }
}
