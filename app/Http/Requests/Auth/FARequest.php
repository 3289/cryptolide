<?php

namespace App\Http\Requests\Auth;

use App\Http\Requests\Request;
use PragmaRX\Google2FA\Google2FA;
use Illuminate\Validation\Factory;

class FARequest extends Request
{
    public function __construct(Factory $factory, Google2FA $google2FA)
    {
        $factory->extend('valid_token', function ($attribute, $value, $parameters, $validator) use($google2FA) {
            $secret = decrypt(session()->get('2fa:user:id')->google_fa);

            return $google2FA->verifyKey($secret, $value);
        }, 'Not a valid token');

        $factory->extend('used_token', function ($attribute, $value, $parameters, $validator) {
            $key = session()->get('2fa:user:id')->id . ':' . $value;

            return !cache()->has($key);
        }, 'Cannot reuse token');

        parent::__construct();
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return session()->has('2fa:user:id');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'totp' => 'required|digits:6|valid_token|used_token',
        ];
    }

    public function messages()
    {
        return [
            'totp.required' => 'Не указан код подтверждения.',
            'totp.digits' => 'Длина кода должна быть 6 символов.',
            'totp.valid_token' => 'Неверный код подтверждения.',
            'totp.used_token' => 'Данный код уже был использован.',
        ];
    }
}
