<?php

namespace App\Http\Requests;

class MessageRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'deal_id' => 'required|numeric|exists:deals,id',
            'text' => 'required|string|max:500',
        ];
    }

    public function messages()
    {
        return [
            'text.required' => 'Вы не указали сообщение.',
            'text.max' => 'Максимальная длина сообщения 50 символов.',
        ];
    }
}
