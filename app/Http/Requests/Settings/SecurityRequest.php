<?php

namespace App\Http\Requests\Settings;

use App\Http\Requests\Request;

class SecurityRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone' => 'required|numeric|digits_between:10,12',
        ];
    }

    public function messages()
    {
        return [
            'phone.required' => 'Не указан пароль.',
            'phone.numeric' => 'Телефонный номер не может содержать различные символы.',
            'phone.digits_between' => 'Телефонный номер должен состоять из не менее чем 10 и не более чем 12 цифр.',
        ];
    }
}
