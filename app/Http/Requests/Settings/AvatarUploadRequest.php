<?php

namespace App\Http\Requests\Settings;

use App\Http\Requests\Request;

class AvatarUploadRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'image' => 'required|file|image|max:5120',
            'start.x' => 'required|numeric|min:0',
            'start.y' => 'required|numeric|min:0',
            'end.x' => 'required|numeric|min:50',
            'end.y' => 'required|numeric|min:50',
        ];
    }

    public function messages()
    {
        return [
            'image.required' => 'Вы не выбрали изображение.',
            'image.file' => 'Не удалось загрузить изображение.',
            'image.image' => 'Вы должны были загрузить изображение.',
            'image.max' => 'Максимальный размер файла 5мб.',
        ];
    }
}
