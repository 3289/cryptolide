<?php

namespace App\Http\Requests\Settings;

use App\Http\Requests\Request;

class EmailRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|string|email|max:255|unique:users',
        ];
    }

    public function messages()
    {
        return [
            'email.required' => 'Не указан e-mail.',
            'email.email' => 'Некорректный e-mail.',
            'email.max' => 'Максимальная длина e-mail 255 символов.',
            'email.unique' => 'Данный e-mail уже используется в системе.',
        ];
    }
}
