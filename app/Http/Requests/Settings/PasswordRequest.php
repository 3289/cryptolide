<?php

namespace App\Http\Requests\Settings;

use App\Http\Requests\Request;

class PasswordRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'current_password' => 'required|string',
            'password' => 'required|string|min:6|confirmed',
        ];
    }

    public function messages()
    {
        return [
            'current_password.required' => 'Не указан текущий пароль.',

            'password.required' => 'Не указан пароль.',
            'password.min' => 'Минимальная длина пароля 6 символов.',
            'password.confirmed' => 'Новые пароли не совпадают.',
        ];
    }
}
