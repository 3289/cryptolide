<?php

namespace App\Http\Requests\Settings;

use App\Http\Requests\Request;

class GeneralRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $keys = join(',', array_keys(config('custom.contacts')));

        return [
            'name' => 'required|string|max:15|regex:/(^([a-zA-Zа-яА-ЯёЁ\-]+)?$)/u',
            'last_name' => 'nullable|string|max:15|regex:/(^([a-zA-Zа-яА-ЯёЁ\-]+)?$)/u',
            'middle_name' => 'nullable|string|max:15|regex:/(^([a-zA-Zа-яА-ЯёЁ\-]+)?$)/u',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Не указано имя.',
            'name.max' => 'Максимальная длина имени 15 символов.',
            'name.regex' => 'Некорректное имя.',

            'middle_name.required' => 'Не указано отчество.',
            'middle_name.max' => 'Максимальная длина отчества 15 символов.',
            'middle_name.regex' => 'Допущена ошибка при заполнении отчества.',

            'last_name.required' => 'Не указано фамилия.',
            'last_name.max' => 'Максимальная длина фамилии 15 символов.',
            'last_name.regex' => 'Допущена ошибка при заполнении фамилии.',
        ];
    }
}
