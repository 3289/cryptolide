<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class MessageResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user' => [
                'login' => $this->user->login,
                'image' => $this->user->image,
            ],
            'from_me' => auth()->user()->id === $this->user_id,
            'text' => $this->text,
            'created_at' => $this->created_at->format('d.m H:i'),
        ];
    }
}
