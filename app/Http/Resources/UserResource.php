<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\Resource;

class UserResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request);
    }

    public function with($request)
    {
        return [
            'commission' => $this->commission,
            'achievements' => $this->achievements,
            'is_commission_active' => $this->created_at->addDays(7)->lt(Carbon::now()),
            'contacts' => $this->contacts->map(function($contact) {
                return [
                    'name' => config('custom.contacts')[$contact->contact_id],
                    'value' => $contact->value,
                    'type' => $contact->type,
                ];
            }),
        ];
    }
}
