<?php

namespace App\Http\Middleware;

use App\Models\Session;
use Closure;

class LastActivity
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!\Session::has('user_id')) {
            $user = \Auth::user();
            $session = Session::where('user_id', $user->id)->first();
            $user->last_activity_timestamp = $session->last_activity;
            $user->save();
        }
        return $next($request);
    }
}
