<?php

namespace App\Http\Middleware;

use Closure;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!auth()->check()) {
            return redirect('/login');
        }

        if (auth()->user()->role == 'user' && !(\Session::get('user_id') && \Session::get('user_id') == auth()->user()->id)) {
            return abort(403);
        }

        return $next($request);
    }
}
