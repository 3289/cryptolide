<?php

namespace App\Http\Controllers;

use App\Http\Requests\Settings\AvatarUploadRequest;
use Intervention\Image\ImageManagerStatic as Image;

class AvatarController extends Controller
{
    public function update(AvatarUploadRequest $request)
    {
        $start = $request->get('start');
        $end = $request->get('end');

        $img = Image::make($request->file('image')->path());

        $img->crop($end['x'] - $start['x'], $end['y'] - $start['y'], $start['x'], $start['y']);

        $img->resize(150, 150);

        $path = base_path('storage/app/public/' . $request->user()->id . '.png');

        $img->save($path);

        return redirect('/general-information');
    }
}
