<?php

namespace App\Http\Controllers;

use App\Mail\ActivationMail;
use App\Models\UserActivation;

class ActivationController
{
    public function handle(UserActivation $userActivation)
    {
        if (!auth()->check() || (auth()->check() && $userActivation->user->id != auth()->user()->id)) {
            return abort(404);
        }

        if (!$userActivation->user->activated) {
            \Mail::to($userActivation->user)->send(new ActivationMail);
            $userActivation->user()->update(['activated' => true]);
        }

        return redirect('/p2p?crypto_id=1&sum_currency=&currency_id=2&payment_type_id=0&country_id=162&city_id=');
    }
}