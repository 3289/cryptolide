<?php

namespace App\Http\Controllers;

use App\Models\Address;
use App\Models\Advert;
use App\Models\Operation;
use App\Models\User;
use App\Models\Withdrawal;
use App\Models\CryptoCurrency;
use PragmaRX\Google2FA\Google2FA;
use App\Http\Requests\WithdrawalRequest;
use App\Notifications\WithdrawalConfirmationCodeNotification;

class WithdrawalController extends Controller
{
    public function show(CryptoCurrency $crypto_currency)
    {
        if ($crypto_currency->id != 1) {
            abort(404);
        }

        $random_hash = str_random();

        session()->put('commission:' . $random_hash, $crypto_currency->commission_output);
        session()->put('withdrawal:' . $random_hash, 'hi');
        session()->put('withdrawal:' . $random_hash . ':count', 0);

        return view('withdrawal-funds-bitcoin', compact('crypto_currency', 'random_hash'));
    }

    public function store(WithdrawalRequest $request, CryptoCurrency $crypto_currency)
    {
        if ($crypto_currency->id != 1) {
            abort(404);
        }

        $hash = $request->get('random_hash');

        $session_commission = session()->get('commission:' . $hash, false);
        $count = session()->get('withdrawal:' . $hash . ':count', 0);

        if ($count == 5) {
            return $this->sendResponseWithError('Слишком много неудачных попыток. В целях безопасности, создайте заявку на вывод заново.');
        }

        $authUserAddress = Address::where('address', $request->get('address'))->where('user_id', auth()->user()->id)->first();
        if (isset($authUserAddress)) {
            return $this->sendResponseWithError('Вы не можете перевести средства самому себе внутри сервиса.');
        }

        $addresses = Address::where('address', $request->get('address'))->whereNotNull('user_id');
        $commission = $addresses->count() == 1 ? 0 : ($session_commission !== false ? $session_commission : $crypto_currency->commission_output);
        $amount = number_format($request->get('amount'), 8, '.', '');
        $total_amount = $amount - $commission;
        $confirmation_code = $request->get('confirmation_code', '');
        if ($this->user->balance[$crypto_currency->short] < $amount) {
            return $this->sendResponseWithError('Недостаточно средств на балансе.');
        }

        if ($total_amount <= 0) {
            return $this->sendResponseWithError('Некорректная сумма вывода.');
        }

        if (!$this->user->google_fa) {
            $send_code = $this->user->sendWithdrawalConfirmationCode($hash, $request->get('address'), $crypto_currency->short, $addresses->count() == 0 ? $total_amount : $amount);
            if ($send_code) {
                return ['status' => 'need_code', 'message' => 'На Ваш E-mail был отправлен код подтверждения.'];
            }
        } elseif ($confirmation_code == '') {
            return ['status' => 'need_code', 'message' => 'Введите код подтверждения с Вашего Google Authenticator.'];
        }

        session()->put('withdrawal:' . $hash . ':count', $count + 1);

        if (!$this->validateCode($hash, $confirmation_code)) {
            return ['status' => 'incorrect_code', 'message' => 'Введен неправильный код подтверждения.'];
        }

        session()->pull('withdrawal:' . $request->get('random_hash'));

        if ($addresses->count() == 1) {
            $address = $addresses->first();
            $this->makeTransfer($address, $crypto_currency, $amount);
        } else {
            $this->makeWithdrawal($crypto_currency, $total_amount, $amount, $commission, $request->get('address'));
        }
        $this->user->keepUpAdverts($crypto_currency);

        return ['status' => true];
    }

    protected function validateCode($hash, $code)
    {
        $google2FA = new Google2FA();
        if ($this->user->google_fa) {
            if (!$google2FA->verifyKey(decrypt($this->user->google_fa), $code)) {
                return false;
            }
        } else {
            if (session()->get('withdrawal:' . $hash) != $code) {
                return false;
            }
        }

        return true;
    }

    protected function makeTransfer(Address $address, CryptoCurrency $crypto_currency, $amount)
    {
        $amount = rtrim(number_format($amount, 20), '0');
        $description_from = 'Вы успешно перевели <strong>'.$amount.' '.$crypto_currency->short.'</strong> пользователю <a href="'.url('/profile/'.$address->user->login).'">'.$address->user->login.'</a>';
        $notification_from = 'Вы успешно перевели '.$amount.' '.$crypto_currency->short.' пользователю <a href="'.url('/profile/'.$address->user->login).'">'.$address->user->login.'</a>';
        Operation::createOperation($crypto_currency->id, $this->user->id, -$amount, 0, $description_from, Operation::TYPE_TRANSFER_FROM, Operation::READED, $notification_from);

        $description_to = 'Пользователь <a href="'.url('/profile/'.$this->user->login).'">'.$this->user->login.'</a> перевёл на ваш баланс '.$amount.' '.$crypto_currency->short.'.';
        $notification_to = 'Перевод средств от пользователя <a href="'.url('/profile/'.$this->user->login).'">'.$this->user->login.'</a>';
        Operation::createOperation($crypto_currency->id, $address->user->id, $amount, 0, $description_to, Operation::TYPE_TRANSFER_TO, Operation::NOT_READED, $notification_to);
    }

    protected function makeWithdrawal(CryptoCurrency $crypto_currency, $total_amount, $amount, $commission, $address)
    {
        $amount = rtrim(number_format($amount, 8), '0');
        $total_amount= rtrim(number_format($total_amount, 8), '0');
        $description = 'Вы успешно вывели '.$total_amount.' '.$crypto_currency->short.' на адрес: '.$address;
        $notification = 'Перевод '.$total_amount.' '.$crypto_currency->short.' на адрес: '.$address;
        $operation = Operation::createOperation($crypto_currency->id, $this->user->id, -$amount, $commission, $description, Operation::TYPE_WITHDRAW, Operation::READED, $notification);

        Withdrawal::createWithdrawal($operation->id, $address);
    }
}
