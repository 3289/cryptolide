<?php

namespace App\Http\Controllers;

use App\Notifications\ConfirmEmailNotification;

class EmailController extends Controller
{
    public function confirm()
    {
        $user = \Auth::user();

        if ($user->activations_count >= 3 && $user->last_activation_time > (time()-60*5)) {
            return redirect('/p2p')->with('email-status', 'Попробуйте снова через 5 минут.');
        } else if ($user->activations_count > 0 && $user->last_activation_time > (time()-60)) {
            return redirect('/p2p')->with('email-status', 'Попробуйте снова через минуту.');
        }

        $confirmation = new ConfirmEmailNotification(request()->user());

        request()->user()->notify($confirmation);

        return redirect('/p2p')->with('email-status', 'Письмо было отправлено снова.');
    }
}
