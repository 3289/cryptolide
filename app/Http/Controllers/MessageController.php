<?php

namespace App\Http\Controllers;

use App\Models\DealHistory;
use App\Http\Requests\MessageRequest;
use App\Http\Resources\MessageResource;
use App\Models\Deal;
use App\Models\Message;
use App\Policies\DealPolicy;
use Illuminate\Http\Request;

class MessageController extends Controller
{
    public function store(MessageRequest $request)
    {
        $deal_id = $request->get('deal_id');

        $message = Message::create([
            'user_id' => $request->user()->id,
            'deal_id' => $deal_id,
            'text' => $request->get('text'),
        ]);

        $deal = Deal::find($deal_id);

        DealHistory::create([
            'user_id' => $deal->partner->id,
            'deal_id' => $deal_id,
            'text' => 'Новое сообщение в чате по сделке <a href="' . url('/deals/'.$deal_id) . '">№' . $deal_id . '.</a><br>Пользователь ' . auth()->user()->login . ' написал вам сообщение в онлайн чат:<br><br>'.$message->text,
            'type' => 'new_message',
            'topic' => 'Уведомление о новом сообщении'
        ]);

        return MessageResource::make($message);
    }

    public function show(Deal $deal)
    {
        $this->authorize(DealPolicy::ACCESS, $deal);

        return MessageResource::collection($deal->messages);
    }
}
