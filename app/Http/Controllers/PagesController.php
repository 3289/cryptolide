<?php

namespace App\Http\Controllers;

use App\Models\User;
use DB;
use Cookie;
use App\Models\Advert;
use App\Models\Country;
use App\Models\Currency;
use App\Models\PaymentType;
use App\Models\CryptoCurrency;
use App\Models\AdditionalCryptoCurrency;

class PagesController extends Controller
{
    public function __construct()
    {
        $userCountry = request()->cookie('user-country', 162);
        $userCurrency = request()->cookie('user-currency', 2);

        if (!request()->cookie('user-country')) {
            $response = file_get_contents('http://api.sypexgeo.net/json/' . request()->server('REMOTE_ADDR'));
            $json = json_decode($response, true);
            $countries = Country::whereName($json['country']['name_ru']);
            if ($countries->count() == 1) {
                $country = $countries->first();
                $userCountry = $country->id;
                $userCurrency = $country->currency_id;
            }
        }

        if (request()->has('country_id')) {
            $country = Country::find(request('country_id'));
            if ($country != null) {
                $userCountry = $country->id;
                $userCurrency = $country->currency_id;
            }
        }

        Cookie::queue(Cookie::make('user-country', $userCountry, 360));
        Cookie::queue(Cookie::make('user-currency', $userCurrency, 360));

        Advert::$userCountry = $userCountry;
        Advert::$userCurrency = $userCurrency;

        view()->composer(['index', 'cryptosell', 'cryptoexchange', 'p2p'], function($view) {
            $view->with('crypto_currencies', CryptoCurrency::all());
            $view->with('crypto', CryptoCurrency::findOrFail(request('crypto_id', 1)));
        });

        view()->composer(['index', 'cryptosell'], function($view) use($userCountry, $userCurrency) {
            $view->with('countries', Country::all());
            $view->with('currencies', Currency::all());
            $view->with('payment_types',  PaymentType::orderBy('priority', 'desc')->get());
            $view->with('paymentType', PaymentType::find(request('payment_type_id')));
            $view->with('selectedCountry', Country::find($userCountry));
            $view->with('selectedCurrency', $userCurrency);
            $view->with('selected_currency', Currency::find(request('currency_id')));
        });

        view()->composer(['cryptoexchange', 'p2p'], function($view) {
            $view->with('crypto_trade_currencies', AdditionalCryptoCurrency::all());
            $view->with('cryptoTrade', AdditionalCryptoCurrency::find(request('crypto_trade_id', 1)) ?? AdditionalCryptoCurrency::all()->first());
        });

        parent::__construct();
    }

    protected function fetchAdverts($type, $perPage = 500, $sort = 'asc')
    {
        return Advert::select(DB::raw('adverts.*, users.last_activity_timestamp, COALESCE((SELECT (UNIX_TIMESTAMP() - sessions.last_activity)<600 FROM sessions WHERE adverts.user_id = sessions.user_id order by sessions.last_activity desc limit 1), 0) as last_activity'))
            ->join('users', 'adverts.user_id', '=', 'users.id')
            ->filter(request()->all())->where('type', $type)
            ->where('status', 'active')
            ->where('sum_crypto', '>', 0.0035)
            ->where('last_activity_timestamp', '>', time()-60*60*24)
            ->orderBy('rate', $sort)
            ->paginate($perPage);
    }

    public function buy()
    {
        $adverts_for_cash = $this->fetchAdverts('sell_for_cash');
        $adverts_online = $this->fetchAdverts('sell_online');

        return view('index', compact('adverts_online', 'adverts_for_cash'));
    }

    public function sell()
    {
        $adverts_for_cash = $this->fetchAdverts('buy_for_cash', 500, 'desc');
        $adverts_online = $this->fetchAdverts('buy_online', 500, 'desc');

        return view('cryptosell', compact('adverts_online', 'adverts_for_cash'));
    }

    public function trade()
    {
        $adverts = $this->fetchAdverts('trade', 500, 'desc');

        return view('cryptoexchange', compact('adverts'));
    }

    public function paginate($type)
    {
        if (!in_array($type, ['buy_for_cash', 'buy_online', 'sell_for_cash', 'sell_online', 'trade'])) {
            abort(404);
        }	
			
				$currencies = Currency::all();
				$payment_types = PaymentType::all();
				$countries = Country::all();
				$adverts = $this->fetchAdverts($type, 20);

        return view('p2p', compact('adverts', 'type', 'currencies', 'payment_types', 'countries'));
    }
}
