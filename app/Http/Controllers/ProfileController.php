<?php

namespace App\Http\Controllers;

use App\Models\Advert;
use App\Models\DealHistory;
use App\Models\Operation;
use App\Models\OperationPartner;
use App\Models\PartnerHistory;
use App\Models\User;

class ProfileController extends Controller
{
    public function show(User $user)
    {
        return view('user-information', compact('user'));
    }

    public function adverts(User $user)
    {
        $catalog = $user->getActiveAdverts();

        return view('active-deals', [
            'user' => $user,
            'catalog' => $catalog,
        ]);
    }

    public function auth(User $user)
    {
        if (session('admin_id') == $user->id) {
            \Auth::loginUsingId($user->id);
//            session()->forget('admin_id');
            session()->forget('user_id');
            return redirect('/admin/users');
        }
        return $this->sendResponseWithError('Вы не являетесь админом.');
    }

    public function refreshNotifications()
    {
        return response()->json([
            'operations' => Operation::getNewOperations(),
            'partnerHistories' => PartnerHistory::getNewPartnerHistories(),
            'dealHistories' => DealHistory::getNewDealHistories(),
        ]);
    }
}
