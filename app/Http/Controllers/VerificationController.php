<?php

namespace App\Http\Controllers;

use App\Models\Verification;
use Illuminate\Http\Request;

class VerificationController extends Controller
{
    public function index(Request $request)
    {
        $user = auth()->user();
        $old_verification = Verification::codeForAuthUser($request->get('type'))->first();
        if (isset($old_verification)) {
            return response()->json([
                'code' => $old_verification->code
            ]);
        }

        $code = rand(100000, 999999);
        $verification = new Verification();
        $verification->user_id = $user->id;
        $verification->type = $request->get('type');
        $verification->code = $code;
        $verification->phone = $user->phone;
        $verification->save();

        return response()->json([
            'code' => $verification->code
        ]);
    }
}
