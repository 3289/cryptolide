<?php

namespace App\Http\Controllers;

use App\Http\Requests\Settings\EmailRequest;
use App\Http\Requests\Settings\GeneralRequest;
use App\Http\Requests\Settings\PasswordRequest;
use App\Http\Requests\Settings\SecurityRequest;
use App\Models\User;
use App\Models\UserContact;
use App\Notifications\ChangeEmailNotification;

class SettingController extends Controller
{
    public function update_general(GeneralRequest $request)
    {
        $contacts = $request->get('contacts');
        $values = $request->get('values');

        if ($contacts && $values) {
            $this->addContacts($request->get('contacts'), $request->get('values'), 'partner');
        }
        else{
            $this->deleteContacts('partner');
        }

        auth()->user()->update([
            'name' => $request->name,
            'last_name' => $request->last_name,
            'middle_name' => $request->middle_name,
            'show_name' => (boolean)$request->show_name,
        ]);

        return redirect('/general-information')->with('status', 'Данные успешно сохранены.');
    }

    public function security()
    {
        $email = explode('@', auth()->user()->email);
        $email = substr_replace($email[0], '***', -3) . '@' . $email[1];

        return view('my-security', compact('email'));
    }

    public function update_security(SecurityRequest $request)
    {
        auth()->user()->update(['phone' => $request->get('phone')]);

        $contacts = $request->get('contacts');
        $values = $request->get('values');

        if ($contacts && $values) {
            $this->addContacts($contacts, $values, 'admin');
        }
        else{
            $this->deleteContacts('admin');
        }

        return redirect('/my-security')->with('status', 'Данные успешно сохранены.');
    }

    private function addContacts(array $contacts, array $values, $type)
    {
        auth()->user()->contacts()->type($type)->delete();

        $available = config('custom.contacts');

        if (count($contacts) == 0) {
            return;
        }

        foreach ($contacts as $key => $contact) {
            if (!array_key_exists($contact, $available) || empty($values[$key])) {
                continue;
            }

            $this->addContact($contact, $values[$key], $type);
        }
    }

    private function addContact($key, $value, $type)
    {
        UserContact::create([
            'user_id' => auth()->user()->id,
            'type' => $type,
            'contact_id' => $key,
            'value' => $value,
        ]);
    }

    private function deleteContacts($type){
        $contracts = UserContact::where('user_id',auth()->user()->id)
            ->where('type', $type)
            ->get();
        foreach ($contracts as $contract){
            $contract->delete();
        }
    }

    public function change_email(EmailRequest $request)
    {
        cache()->set('email', $request->email, 4);
        $subject = 'Изменение почты';
        /** @var User $user */
        $user = auth()->user();
        $user->notify(new ChangeEmailNotification($user, $subject));

        return ['status' => true];
    }

    public function confirm_change_email(EmailRequest $request)
    {
        if (cache()->has('activation_code') && request()->activation_code == cache()->get('activation_code')) {
            auth()->user()->update([
                'email' => $request->email
            ]);

            return ['status' => true, 'message' => 'E-mail успешно изменен.'];
        }

        return ['status' => false, 'message' => 'Введен неправильный код подтверждения.' . session('count')];
    }

    public function change_password(PasswordRequest $request)
    {
        /** @var User $user */
        $user = auth()->user();
        if (password_verify($request->current_password, $user->password)) {
            $user->update([
                'password' => bcrypt($request->password),
            ]);

            return ['status' => true, 'message' => 'Пароль успешно изменен.'];
        }

        return ['status' => false, 'message' => 'Старый пароль указан не верно.'];
    }
}
