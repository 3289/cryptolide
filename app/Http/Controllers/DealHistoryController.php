<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DealHistoryController extends Controller
{
    public function update()
    {
        auth()->user()->deal_histories()->update(['read' => 1]);

        return ['status' => true];
    }
}
