<?php

namespace App\Http\Controllers;

use App\Models\CryptoCurrency;
use Illuminate\Http\Request;

class PartnerController extends Controller
{
    public function index()
    {
        $user = auth()->user();
        $partners = $user->partners()->paginate(10);
        $balance = $user->partner_balance;

        return view('my-partners', compact('partners', 'balance'));
    }

    public function transfer_income(CryptoCurrency $cryptoCurrency)
    {
        $user = auth()->user();
        $balance = $user->partner_balance[$cryptoCurrency->short];
        if ($balance == 0) {
            return ['status' => false, 'message' => 'На кошельке нет средств.'];
        }

        $user->operations_partner()->where('crypto_id', $cryptoCurrency->id)->update(['transferred' => true]);

        $user->operations()->create([
            'crypto_id' => $cryptoCurrency->id,
            'amount' => $balance,
            'description' => 'Перевод с партнёрского баланса.',
        ]);

        return ['status' => true, 'message' => 'Средства успешно переведены.'];
    }

    public function read_partner_history()
    {
        return auth()->user()->partner_histories()->update(['read' => 1]);
    }
}
