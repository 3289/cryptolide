<?php

namespace App\Http\Controllers;

use App\Models\Advert;
use App\Models\Deal;
use App\Models\DealHistory;
use App\Models\Operation;
use App\Models\OperationPartner;
use App\Models\User;
use App\Notifications\ChangeDealStatusConfirmCodeNotification;
use App\Policies\DealPolicy;
use App\Providers\DealServiceProvider;
use Illuminate\Http\Request;
use PragmaRX\Google2FA\Google2FA;

class DealController extends Controller
{
    public function index()
    {
        DealServiceProvider::closeDeals();
        /** @var User $user */
        $user = auth()->user();
        $deals = $user->getHistoryDeals();
        $activeDeals = $user->getActiveDeals();

        return view('my-deals', [
            'deals' => $deals,
            'activeDeals' => $activeDeals,
        ]);
    }

    public function store(Request $request)
    {
        /** @var Advert $advert */
        $advert = Advert::findOrFail($request->get('advert_id'));

        /** @var User $user */
        $user = auth()->user();
        if (!is_null($advert->achievement_id) && !in_array($advert->achievement_id, $user->achievements->pluck('id')->toArray())) {
            abort(403, 'Только пользователи с отметкой «' . $advert->achievement->name . '» могут открыть сделку.');
        }

        $sum_crypto = $request->get('sum_crypto', 0);
        $commission = number_format($sum_crypto * ($advert->user->commission / 100), 8);
        if ($advert->short_type == 'buy') {
            $validMinCrypto = $advert->min_crypto;
            $validSumCrypto = $advert->sum_crypto;
        } else {
            $validMinCrypto = $advert->min_crypto;
            if ($advert->min_crypto >= $advert->sum_crypto) {
                $validMinCrypto = $advert->min_crypto - number_format($advert->min_crypto * ($advert->user->commission / 100), 8);
            }
            $validSumCrypto = $advert->sum_crypto - number_format($advert->sum_crypto * ($advert->user->commission / 100), 8);
            //$commission = number_format($sum_crypto*number_format(($advert->user->commission/100)/(1-($advert->user->commission/100)), 4), 8);
        }

        $rules = [
            'sum_crypto' => 'required|numeric|min:' . $validMinCrypto . '|max:' . $validSumCrypto,
            'sum_currency' => 'required|numeric|min:1.00',
        ];
        if ($advert->type == 'trade') {
            $rules['sum_currency'] = 'required|numeric|min:0.00000001';
        } else {
            $rules['sum_currency'] = 'required|numeric|min:1.00';
        }

        $request->validate($rules, [
            'sum_currency.required' => 'Не указана сумма сделки.',
            'sum_currency.numeric' => 'Сумма сделки должна быть числом.',
            'sum_currency.min' => 'Сумма сделки не может быть меньше :min ' . $advert->currency_name,

            'sum_crypto.required' => 'Не указана сумма криптовалюты.',
            'sum_crypto.numeric' => 'Сумма криптовалюты должна быть числом.',
            'sum_crypto.min' => 'Сумма криптовалюты не может меньше :min.',
            'sum_crypto.max' => 'Сумма криптовалюты не может быть больше :max.',
        ]);

        if ($advert->type == 'trade') {
            $sum_currency = $request->get('sum_currency');
            $trade_rate = $sum_currency/$sum_crypto;
        } else {
            $sum_currency = $sum_crypto * $advert->rate;
        }

        $sum_currency = explode('.', $sum_currency);
        if ($advert->type == 'trade') {
            $sum_currency = $sum_currency[0] . '.' . substr($sum_currency[1] ?? 0, 0, 8);
        } else {
            $sum_currency = $sum_currency[0] . '.' . substr($sum_currency[1] ?? 0, 0, 2);
        }

        if ($sum_currency < $advert->min_currency) {
            return back()->withErrors(['sum_currency' => 'Минимальная сумма ' . $advert->min_currency . ' ' . $advert->currency_name]);
        }

        if ($sum_currency > $advert->sum_currency) {
            return back()->withErrors(['sum_currency' => 'Сумма криптовалюты не должна превышать сумму сделки.']);
        }

        if ($advert->short_type == 'buy' && $user->balance[$advert->crypto->short] < ($sum_crypto + $commission)) {
            return back()->withErrors(['sum_crypto' => 'Продажа криптовалюты осуществляется с помощью депонирования (защищенного временного хранения) поэтому, у вас должны быть средства на балансе. Сейчас у вас недостаточно средств, чтобы открыть сделку на введенную сумму. Укажите меньшую сумму или пополните кошелёк.']);
        }

        if ($advert->short_type != 'buy' && $advert->user->balance[$advert->crypto->short] < ($sum_crypto + $commission)) {
            return back()->withErrors(['sum_crypto' => 'У продавца недостаточно средств на балансе']);
        }

        if ($advert->description == '') {
            $advert->description = 'Описание сделки отсутствует. Вы можете уточнить все необходимые детали через онлайн чат.';
        }

        if ($advert->type == 'trade') {
            /** @var Deal $deal */
            $deal = $advert->makeDeal($sum_crypto, $commission, $trade_rate);
        } else {
            /** @var Deal $deal */
            $deal = $advert->makeDeal($sum_crypto, $commission);
        }

        Operation::create([
            'amount' => -$deal->sum_crypto,
            'user_id' => $deal->seller->id,
            'description' => '',
            'commission' => $commission,
            'crypto_id' => $deal->crypto_id,
            'type' => Operation::TYPE_DEPOSIT,
            'notification' => 'Депонирование средств по сделке <a href="' . url('/deals/' . $deal->id) . '">#' . $deal->id . '</a>.',
        ]);

        if ($advert->keep_up == Advert::KEEP_UP) {
            $advert->changeSumCrypto($sum_crypto);
        }

        $message = $deal->makeDealCreateMsg();
        $notification = $deal->makeDealCreateNotification();

        $deal->notify($message, 'opened', $advert->user_id, null, $notification);

        return redirect('/deals/' . $deal->id);
    }

    public function show(Deal $deal)
    {
        $this->authorize(DealPolicy::ACCESS, $deal);

        if ($deal->time_left < 0 && $deal->status == 'pending') {
            $deal->cancelDeal(null);
        }

        return view('make-deal', compact('deal'));
    }

    public function send_confirmation_code(Deal $deal)
    {
        $this->authorize(DealPolicy::ACCESS, $deal);
        /** @var User $user */
        $user = auth()->user();
        if (($user->role == 'admin' || session()->has('admin_id')) && \request('change')) {
            $admin = session()->has('admin_id') ? User::find(session()->get('admin_id')) : $user;
            if (isset($admin)) {
                $admin->notify(new ChangeDealStatusConfirmCodeNotification($deal));
            }
            return;
        }

        return $deal->saveConfirmationCode();
    }

    public function update(Deal $deal, Google2FA $google2FA)
    {
        $this->authorize(DealPolicy::ACCESS, $deal);

        if ($deal->isCompleted() || $deal->isCanceled()) {
            return ['status' => true];
        }

        $status = request()->get('status');
        $user = auth()->user();
        if ($status == 'completed' && $deal->isSeller($user)) {
            if ($deal->seller->google_fa) {
                if (!$google2FA->verifyKey(decrypt($deal->seller->google_fa), request('confirmation_code'))) {
                    return ['status' => false, 'message' => 'Введен неправильный код подтверждения.'];
                }
            } else if (request('confirmation_code') == null || (request('confirmation_code') != session()->get('deal:' . $deal->id))) {
                return ['status' => false, 'message' => 'Введен неправильный код подтверждения. Убедитесь, что вы вводите последний полученный код подтверждения.'];
            }

            $amount = ($deal->sum_crypto - ($deal->commission ? $deal->commission : ($deal->creator->commission / 100 * $deal->sum_crypto)));

            if ($deal->creator->referrer_id != null && $deal->creator->commission_partner != 0) {
                $bonus = ($deal->sum_crypto - $amount) / 100 * $deal->creator->commission_partner;


                $bonus2 = 0;
//                if ($deal->creator->referrer->referrer_id != null && $deal->creator->referrer->commission_partner != 0) {
//
//                    $bonus2 = $bonus / 100 * $deal->creator->referrer->commission_partner;
//
//                    if ($bonus2 > 0.000000001) {
//                        OperationPartner::create([
//                            'partner_id' => $deal->creator->referrer->id,
//                            'user_id' => $deal->creator->referrer->referrer_id,
//                            'crypto_id' => $deal->crypto_id,
//                            'amount' => $bonus2,
//                        ]);
//                    }
//                }

                if ($bonus > 0.000000001) {
                    OperationPartner::create([
                        'partner_id' => $deal->creator->id,
                        'user_id' => $deal->creator->referrer_id,
                        'crypto_id' => $deal->crypto_id,
                        'amount' => $bonus - $bonus2,
                    ]);
                }
            }
            $message = '<a href="' . url('/deals/' . $deal->id) . '">Сделка №' . $deal->id . '</a> успешно завершена!<br> Вы получили + ' . $deal->addRate($deal->buyer) . ' TS к вашему рейтингу.';
            $deal->notify($message, 'completed', $deal->buyer->id);

            Operation::create([
                'user_id' => $deal->buyer->id,
                'crypto_id' => $deal->crypto_id,
                'commission' => ($deal->commission ? $deal->commission : ($deal->creator->commission / 100 * $deal->sum_crypto)),
                'amount' => $amount,
                'description' => 'Сделка <a href="' . url('/deals/' . $deal->id) . '">№' . $deal->id . '</a> успешно завершена!<br><br>Денежные средства в размере <strong>' . number_format($amount, 8) . ' ' . $deal->crypto->short . '</strong> зачислены на ваш баланс.',
                'deal_id' => $deal->id,
                'commission_partner' => $bonus ?? null,
                'type' => Operation::TYPE_ENDED_DEAL,
                'read' => Operation::NOT_READED,
                'notification' => 'Зачисление средств из депонирования по успешно завершенной сделке <a href="' . url('/deals/' . $deal->id) . '">#' . $deal->id . '</a>.',
            ]);
        }

        if ($status == 'cancelled' && $deal->buyer->id == $user->id) {
            $deal->cancelDeal('user');
        }

        $buyer = ['cancelled', 'paid'];
        $seller = ['cancelled', 'completed'];

        if (in_array($status, $deal->isSeller($user) ? $seller : $buyer)) {
            if ($status == 'completed') {
                $seller_user = $deal->seller()->first();
                $buyer_user = $deal->buyer()->first();

                $seller_user->rating = $deal->seller->rating + $deal->addRate($deal->seller);
                $buyer_user->rating = $deal->buyer->rating + $deal->addRate($deal->buyer);

                $seller_user->save();
                $buyer_user->save();

                if (!is_null($deal->advert)) {
                    /*
                    $deal->advert()->update(['sum_crypto' => $deal->advert->sum_crypto - $deal->sum_crypto]);

                    if ($deal->advert->sum_crypto == 0) {
                        $deal->advert()->update(['status' => 'hidden']);
                    }
                    */
                }
            }
            if ($status == 'paid') {
                $message = 'Пользователь <a href="' . url('/profile/' . $deal->buyer->login) . '">' . $deal->buyer->login . '</a> отметил, что перевел вам средства в размере: <strong>' . $deal->sumCurrency() . ' ' . $deal->currency_name . '</strong> по <a href="' . url('/deals/' . $deal->id) . '">сделке №' . $deal->id . '</a>.<br> Пожалуйста, проверьте поступление средств.';
                $notification = 'Отправлены средства по <a href="' . url('/deals/' . $deal->id) . '">сделке №' . $deal->id . '</a>.<br> Пользователь ' . $deal->buyer->login . ' уведомил, что отправил вам средства.';
                $topic = 'Уведомление об оплате по сделке №' . $deal->id;
                $deal->notify($message, 'paid', $deal->partner->id, $topic, $notification);
            }

            $deal->update(['status' => $status]);
        }

        return ['status' => true];
    }
}
