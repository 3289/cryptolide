<?php

namespace App\Http\Controllers;

use App\Models\CryptoCurrency;
use App\Http\Controllers\Controller;

class StaticController extends Controller
{
    public function commission()
    {
        $commissions = CryptoCurrency::all();
        return view('commissions', ['commissions'=>$commissions]);
    }
}
