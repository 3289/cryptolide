<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Achievement;

class AchievementController extends Controller
{
    public function index()
    {
        $achievements = Achievement::all();

        return view('admin.achievements', compact('achievements'));
    }

    public function show(Achievement $achievement)
    {
        return $achievement;
    }

    public function store()
    {
        $achievement = Achievement::create(request()->only('name'));

        request()->file('image')->storeAs('public/achievements', $achievement->id . '.png');

        return redirect('/admin/achievements');
    }

    public function update(Achievement $achievement)
    {
        if (request()->hasFile('image')) {
            request()->file('image')->storeAs('public/achievements', $achievement->id . '.png');
        }

        $achievement->update(request()->only('name'));

        return $achievement;
    }

    public function destroy(Achievement $achievement)
    {
        $achievement->delete();

        return [];
    }
}
