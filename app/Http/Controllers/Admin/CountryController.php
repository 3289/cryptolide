<?php

namespace App\Http\Controllers\Admin;

use App\Models\Country;
use App\Http\Controllers\Controller;
use App\Models\Currency;

class CountryController extends Controller
{
    public function index()
    {
        $countries = Country::all();
        $currencies = Currency::all();

        return view('admin.countries_and_currencies', compact('countries', 'currencies'));
    }

    public function show(Country $country)
    {
        return [
            'id' => $country->id,
            'name' => $country->name,
            'currency_id' => $country->currency_id
        ];
    }

    public function store()
    {
        Country::create(request()->all());

        return redirect('/admin/countries_and_currencies');
    }

    public function update(Country $country)
    {
        $country->update(request()->all());

        return $country;
    }

    public function destroy(Country $country)
    {
        $country->delete();

        return [];
    }
}
