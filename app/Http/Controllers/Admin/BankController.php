<?php

namespace App\Http\Controllers\Admin;

use App\Models\Bank;
use App\Http\Controllers\Controller;
use App\Models\Country;

class BankController extends Controller
{
    public function index()
    {
        $banks = Bank::all();
        $countries = Country::all();

        return view('admin.banks', compact('banks', 'countries'));
    }

    public function store()
    {
        Bank::create(request()->all());

        return redirect('/admin/banks');
    }

    public function show(Bank $bank)
    {
        return $bank;
    }

    public function update(Bank $bank)
    {
        $bank->update(request()->all());

        return $bank;
    }

    public function destroy(Bank $bank)
    {
        $bank->delete();

        return [];
    }
}
