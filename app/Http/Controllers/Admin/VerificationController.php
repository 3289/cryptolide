<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Verification;

class VerificationController extends Controller
{
    public function index()
    {
        $verifications = Verification::orderBy('id', 'desc')
            ->paginate(50);
        return view('admin.verification', [
            'verifications' => $verifications,
        ]);
    }
}
