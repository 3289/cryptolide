<?php

namespace App\Http\Controllers\Admin;

use App\Models\CryptoCurrency;
use App\Http\Controllers\Controller;

class CryptoCurrencyController extends Controller
{
    public function index()
    {
        $crypto_currencies = CryptoCurrency::all();

        return view('admin.commissions', compact('crypto_currencies'));
    }

    public function show(CryptoCurrency $cryptoCurrency)
    {
        return $cryptoCurrency;
    }

    public function update(CryptoCurrency $cryptoCurrency)
    {
        $cryptoCurrency->update([
            'commission_input' => request()->commission_input,
            'commission_output' => request()->commission_output,
        ]);

        return $cryptoCurrency;
    }
}
