<?php

namespace App\Http\Controllers\Admin;

use App\Models\Currency;
use App\Models\Risk;
use App\Models\PaymentType;
use App\Http\Controllers\Controller;

class PaymentTypeController extends Controller
{
    public function index()
    {
        $payment_types = PaymentType::orderBy('priority', 'desc')->get();
        $risks = Risk::all();
        $currencies = Currency::all();

        return view('admin.payment_types_and_risks', compact('payment_types', 'risks', 'currencies'));
    }

    public function store()
    {
        PaymentType::create(request()->all());

        return redirect('/admin/payment_types_and_risks');
    }

    public function show(PaymentType $paymentType)
    {
        return $paymentType;
    }

    public function update(PaymentType $paymentType)
    {
        $paymentType->update(request()->all());

        return $paymentType;
    }

    public function destroy(PaymentType $paymentType)
    {
        $paymentType->delete();

        return [];
    }
}
