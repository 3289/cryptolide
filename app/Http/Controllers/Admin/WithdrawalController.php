<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Request;
use App\Models\Withdrawal;
use App\Models\Operation;
use App\Models\Address;

class WithdrawalController extends Controller
{
    public function index()
    {
        $withdrawals = Withdrawal::paginate(50);

        return view('admin.withdrawals', compact('withdrawals'));
    }

    public function destroy(Withdrawal $withdrawal)
    {
        //$withdrawal->delete();

        return [];
    }

    public function update(Withdrawal $withdrawal)
    {
        $txid = \request()->get('txid');
        $status = \request()->get('status');

        $withdrawal->update(['status' => $status, 'txid' => $txid]);
        //<a href="https://www.blocktrail.com/BTC/tx/{{ $withdrawal->txid }}" target="_blank">TXID</a>
        if ($status == 'completed') {

            /** @var Operation $operation */
            $operation = $withdrawal->operation;
            $operation->description .= ' / <a href="https://www.blocktrail.com/BTC/tx/' . $withdrawal->txid . '" target="_blank">TXID</a>';
            $operation->notification .= ' / <a href="https://www.blocktrail.com/BTC/tx/' . $withdrawal->txid . '" target="_blank">TXID</a>';
            $operation->save();
            $withdrawal->operation->user->sendNotification('Успешный вывод денежных средств', $withdrawal->operation->description, 'Уведомление об успешном выводе денежных средств');

        }
        if ($status == 'cancelled') {
            $this->cashReturn($withdrawal);
        }
        return [];
    }

    public function cashReturn(Withdrawal $withdrawal)
    {
        $address = $withdrawal->address;


        $description = 'Возврат денежных средств. Причина: Отмена заявки на перевод. ' . $address;
        $notification = 'Возврат денежных средств. Причина: Отмена заявки на перевод. ' . $address;

        Operation::create([
            'amount' =>  - $withdrawal->operation->amount,
            'crypto_id' => $withdrawal->operation->crypto_id,
            'user_id' => $withdrawal->operation->user_id,
            'description' => $description,
            'type' => Operation::TYPE_CASH_RETURN,
            'read' => Operation::NOT_READED,
            'notification' => $notification,
            'commission' => 0
        ]);

    }
}
