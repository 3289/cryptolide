<?php

namespace App\Http\Controllers\Admin;

use App\Models\Address;
use App\Http\Controllers\Controller;
use App\Models\CryptoCurrency;
use App\Notifications\ConfirmAddAdress;
use PragmaRX\Google2FA\Google2FA;

class AddressController extends Controller
{
    public function index()
    {
        $addresses = Address::filter(request()->all())->paginate(10);

        $crypto_currencies = CryptoCurrency::all();

        return view('admin.addresses', compact('addresses', 'crypto_currencies'));
    }

    public function store(Google2FA $google2FA)
    {
        if (auth()->user()->google_fa) {
            if (!$google2FA->verifyKey(decrypt(auth()->user()->google_fa), request('confirmation_code'))) {
                return back()->with(['status' => 'error', 'message' => 'Введен неправильный код подтверждения.']);
            }
        } else if (request('confirmation_code') == null || (request('confirmation_code') != session()->get('code:' . auth()->user()->id))) {
            return back()->with(['status' => 'error', 'message' => 'Введен неправильный код подтверждения. Убедитесь, что вы вводите последний полученный код подтверждения.']);
        }
        session()->forget('code:' . auth()->user()->id);

        foreach (['public', 'private'] as $type) {
            if (empty(request($type))) {
                continue;
            }

            preg_match_all('#[a-zA-Z0-9]{34}#', request($type), $addresses);

            foreach ($addresses[0] as $address) {
                if (Address::where('address', $address)->count() == 0) {
                    Address::create([
                        'address' => $address,
                        'crypto_id' => request('crypto_id'),
                        'type' => $type
                    ]);
                }
            }
        }

        return back()->with(['status' => 'success', 'message' => 'Адреса криптовалют добавлены.']);
    }

    public function destroy(Address $address)
    {
        $address->delete();

        return [];
    }


    public function sendConfirmationCode()
    {
        $user = auth()->user();
        if ($user->role != 'admin'){
            return 'error';
        }
        $user->notify(new ConfirmAddAdress());
        return 'success';
    }
}
