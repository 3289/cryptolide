<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Risk;

class RiskController extends Controller
{
    public function store()
    {
        Risk::create(request()->all());

        return redirect('/admin/payment_types_and_risks');
    }

    public function destroy(Risk $risk)
    {
        $risk->delete();

        return [];
    }
}
