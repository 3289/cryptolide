<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Operation;

class ProfitController extends Controller
{
    public function index()
    {
        $operations = Operation::filter(request()->all())
            ->where('type', '!=', Operation::TYPE_DEPOSIT)
            ->where('commission', '>', 0)
            ->paginate(100);

        return view('admin.profit', compact('operations'));
    }
}
