<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Notice;
use App\Models\User;

class NoticeController extends Controller
{
    public function index()
    {
        $notices = Notice::orderBy('id', 'desc')->paginate(50);
        $users = User::all()->pluck('login', 'id')->toArray();

        return view('admin.notices', [
            'notices' => $notices,
            'users' => $users
        ]);
    }
    public function store()
    {
        Notice::create(request()->all());

        return redirect('/admin/notices');
    }

    public function show(Notice $notice)
    {
        return $notice;
    }

    public function update(Notice $notice)
    {
        $notice->update(request()->all());

        return $notice;
    }

    public function destroy(Notice $notice)
    {
        $notice->delete();

        return [];
    }
}
