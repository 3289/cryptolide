<?php

namespace App\Http\Controllers\Admin;

use App\Models\Address;
use App\Models\Operation;
use App\Models\CryptoCurrency;
use App\Http\Controllers\Controller;
use App\Http\Requests\OperationRequest;

class OperationController extends Controller
{
    public function index()
    {
        $operations = Operation::filter(request()->all())->paginate(100);

        return view('admin.operations', compact('operations'));
    }

    public function create()
    {
        $crypto_currencies = CryptoCurrency::all();

        return view('admin.replenishment', compact('crypto_currencies'));
    }

    public function store(OperationRequest $request)
    {
        $address = Address::where('address', $request->address)->first();
        if ($address->user != null) {
            $address->update(['used' => true]);

            $description = 'Сумма пополнения: '.number_format($request->amount-$request->commission, 8, '.', '').' BTC
                <br>
                <br>
                Пополнение BTC адреса: ' . substr($request->address, 0, 10) . '...
                <br>
                TXID: <a href="https://www.blocktrail.com/BTC/tx/' . $request->tx . '">' . substr($request->tx, 0, 20) . '...</a>';
            $notification = 'Пополнение BTC адреса: ' . substr($request->address, 0, 10) . '...
                <br>
                TXID: <a href="https://www.blocktrail.com/BTC/tx/' . $request->tx . '">' . substr($request->tx, 0, 20) . '...</a>';

            Operation::create([
                'amount' => $request->amount-$request->commission,
                'crypto_id' => $request->crypto_id,
                'user_id' => $address->user_id,
                'description' => $description,
                'type' => 1,
                'read' => Operation::NOT_READED,
                'notification' => $notification,
                'commission' => $request->commission
            ]);
        }

        return redirect('/admin/operations')->with('status', 'Средства успешно зачислены.');
    }
}
