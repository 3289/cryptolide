<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use App\Models\Achievement;
use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;

class UserController extends Controller
{
    public function index()
    {
        $users = User::filter(request()->all())->paginate(50);

        $achievements = Achievement::all();

        return view('admin.users', compact('users','achievements'));
    }

    public function show(User $user)
    {
        return UserResource::make($user);
    }

    public function update(User $user)
    {
        if ($user->commission != request()->get('commission_deal')) {
            $params = array_merge(request()->except('achievements'), [
                'change_commission' => 1,
            ]);
        } else {
            $params = request()->except('achievements');
        }

        $user->achievements()->detach();

        if (count(request()->get('achievements'))) {
            $user->achievements()->attach(Achievement::find(request()->get('achievements')));
        }

        $user->update($params);

        return $user;
    }

    public function destroy(User $user)
    {
        $user->delete();

        return [];
    }

    public function auth(User $user)
    {
        $admin = \Auth::user();
        $status = \Auth::loginUsingId($user->id);
        if ($status) {
            if (!session()->get('admin_id') && $admin->role == 'admin') {
                session()->put('admin_id', $admin->id);
            }
            session()->put('user_id', $user->id);
            return redirect('/p2p');
        }

        return $this->sendResponseWithError('Не удалось авторизовать пользователя.');
    }
}
