<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\AdvertRequest;
use App\Mail\NotificationMail;
use App\Models\Advert;
use App\Models\Deal;
use App\Models\Operation;
use App\Models\OperationPartner;
use App\Policies\DealPolicy;
use PragmaRX\Google2FA\Google2FA;

class DealController extends Controller
{
    public function index()
    {
        if (request()->get('status') && request()->get('status') == 'active') {
            $deals = Deal::active()->paginate(50);
        } else {
            $deals = Deal::archive()->paginate(50);
        }

        return view('admin.deals', [
            'deals' => $deals
        ]);
    }

    public function changeStatus(Deal $deal, Google2FA $google2FA)
    {
        $this->authorize(DealPolicy::ACCESS, $deal);

        if ($deal->isCompleted() || $deal->isCanceled()) {
            return ['status' => true];
        }

        if (auth()->user()->google_fa) {
            if (!$google2FA->verifyKey(decrypt(auth()->user()->google_fa), request('confirmation_code'))) {
                return ['status' => false, 'message' => 'Введен неправильный код подтверждения.'];
            }
        } else if (request('confirmation_code') == null || (request('confirmation_code') != session()->get('changeStatus:' . $deal->id))) {
            return ['status' => false, 'message' => 'Введен неправильный код подтверждения. Убедитесь, что вы вводите последний полученный код подтверждения.'];
        }

        $deal->admin_change = 1;
        $deal->save();

        $status = request()->get('status');

        if ($status == 'completed') {
            $amount = ($deal->sum_crypto - ($deal->commission ? $deal->commission : ($deal->creator->commission / 100 * $deal->sum_crypto)));

            if ($deal->creator->referrer_id != null && $deal->creator->partner_commission != 0) {
                $bonus = ($deal->sum_crypto - $amount) / 100 * $deal->creator->partner_commission;


                $bonus2 = 0;
//                if ($deal->creator->referrer->referrer_id != null && $deal->creator->referrer->partner_commission != 0) {
//
//                    $bonus2 = $bonus / 100 * $deal->creator->referrer->partner_commission;
//
//                    if ($bonus2 > 0.000000001) {
//                        OperationPartner::create([
//                            'partner_id' => $deal->creator->referrer->id,
//                            'user_id' => $deal->creator->referrer->referrer_id,
//                            'crypto_id' => $deal->crypto_id,
//                            'amount' => $bonus2,
//                        ]);
//                    }
//                }

                if ($bonus > 0.000000001) {
                    OperationPartner::create([
                        'partner_id' => $deal->creator->id,
                        'user_id' => $deal->creator->referrer_id,
                        'crypto_id' => $deal->crypto_id,
                        'amount' => $bonus - $bonus2,
                    ]);
                }
            }

            Operation::create([
                'user_id' => $deal->buyer->id,
                'crypto_id' => $deal->crypto_id,
                'commission' => $deal->creator->commission,
                'amount' => $amount,
                'description' => 'Зачисление средств из депонирования по успешно завершенной сделке <a href="' . url('/deals/' . $deal->id) . '">#' . $deal->id . '</a>.',
                'deal_id' => $deal->id,
                'commission_partner' => $bonus ?? null,
                'type' => 0,
            ]);
        }

        if ($status == 'cancelled') {
            $deal->cancelDeal('user');
        }

        $buyer = ['cancelled', 'paid'];
        $seller = ['cancelled', 'completed'];

        if (in_array($status, $seller)) {
            if ($status == 'completed') {
                $seller_user = $deal->seller()->first();
                $buyer_user = $deal->buyer()->first();

                $seller_user->rating = $deal->seller->rating + $deal->addRate($deal->seller);
                $buyer_user->rating = $deal->buyer->rating + $deal->addRate($deal->buyer);

                $seller_user->save();
                $buyer_user->save();
            }

            $deal->update(['status' => $status]);
        }

        return ['status' => true];
    }

    public function auth(Deal $deal)
    {
        $admin = \Auth::user();
        $status = \Auth::loginUsingId($deal->buyer->id);
        if ($status) {
            if (!session()->get('admin_id')) {
                session()->put('admin_id', $admin->id);
            }
            session()->put('user_id', $deal->buyer->id);
            return redirect('/deals/' . $deal->id);
        }

        return $this->sendResponseWithError('Не удалось авторизовать пользователя.');
    }
}
