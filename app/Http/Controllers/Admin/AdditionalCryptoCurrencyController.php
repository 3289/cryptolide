<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\AdditionalCryptoCurrency;

class AdditionalCryptoCurrencyController extends Controller
{
    public function index()
    {
        $additionalCryptoCurrencies = AdditionalCryptoCurrency::all();

        return view('admin.additional_crypto_currencies', compact('additionalCryptoCurrencies'));
    }

    public function store()
    {
        AdditionalCryptoCurrency::create([
            'name' => request()->get('name'),
            'short' => request()->get('short'),
        ]);

        return redirect('/admin/additional_crypto_currencies');
    }

    public function destroy(AdditionalCryptoCurrency $additionalCryptoCurrency)
    {
        $additionalCryptoCurrency->delete();

        return [];
    }
}
