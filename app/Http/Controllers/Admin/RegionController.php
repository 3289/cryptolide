<?php

namespace App\Http\Controllers\Admin;

use App\Models\Region;
use App\Http\Controllers\Controller;

class RegionController extends Controller
{
    public function store()
    {
        //
    }

    public function update(Region $region)
    {
        //
    }

    public function destroy(Region $region)
    {
        //
    }
}
