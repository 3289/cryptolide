<?php

namespace App\Http\Controllers\Admin;

use App\Models\City;
use App\Http\Controllers\Controller;

class CityController extends Controller
{
    public function store()
    {
        //
    }

    public function update(City $city)
    {
        //
    }

    public function destroy(City $city)
    {
        //
    }
}
