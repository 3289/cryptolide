<?php

namespace App\Http\Controllers\Auth;

use App\Models\Session;
use Illuminate\Http\Request;
use App\Mail\ActivationMail;
use App\Models\UserActivation;
use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\FARequest;
use App\Http\Requests\Auth\LoginRequest;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    protected $activationService;

    public function __construct()
    {
        $this->middleware('guest', ['except' => ['logout']]);
    }

    public function login(LoginRequest $request)
    {
        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        if ($this->attemptLogin($request)) {
            return $this->sendLoginResponse($request);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    public function authenticated(Request $request, $user)
    {
        session()->put('admin_id', $user->id);
        if ($user->google_fa) {
            auth()->logout();

            $request->session()->put('2fa:user:id', $user);

            return redirect('2fa/validate');
        }

        return redirect()->intended($this->redirectTo);
    }

    public function showConfirmationLogin()
    {
        return view('2fa/validate');
    }

    public function confirmationLogin(FARequest $request)
    {
        $user = $request->session()->pull('2fa:user:id');
        $key = $user->id . ':' . $request->totp;

        cache()->set($key, true, 4);

        auth()->login($user);

        return redirect('/p2p');
    }

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        return redirect('/');
    }
}
