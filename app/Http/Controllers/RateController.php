<?php

namespace App\Http\Controllers;

use App\Models\Advert;
use App\Models\Currency;
use Illuminate\Http\Request;
use App\Models\CryptoCurrency;
use App\Models\AdditionalCryptoCurrency;

class RateController extends Controller
{
    public function calculation(Request $request)
    {
        if (
            $request->get('crypto_id', 0) == 0 ||
            ($request->get('crypto_trade_id', 0) == 0 && $request->get('currency_id', 0) == 0)
        ) {
            return [];
        }

        $crypto = CryptoCurrency::findOrFail($request->get('crypto_id'));

        $data = [];

        if ($request->get('type') != 'buy_online' && $request->get('type') != 'buy_for_cash') {
            $data['balance'] = number_format($request->user()->balance[$crypto->short], 8);
            $data['commission'] = number_format($data['balance'] / 100 * $request->user()->commission, 8);
            $data['total'] = number_format($data['balance'] - $data['commission'], 8);
        }

        if (request('type') == 'trade') {
            $crypto_trade = AdditionalCryptoCurrency::findOrFail($request->get('crypto_trade_id'));

            $rate = Advert::where('crypto_trade_id', $crypto_trade->id)->where('crypto_id', $crypto->id)->min('rate');
            $data['rate'] = number_format($rate, 8, '.', ',');
        } else {
            $currency = Currency::findOrFail($request->get('currency_id'));
            $method = $request->get('type') == 'buy_online' || $request->get('buy_for_cash') ? 'max' : 'min';
            $rate = Advert::where('currency_id', $currency->id)->where('crypto_id', $crypto->id)->$method('rate');
            $data['rate'] = number_format($rate, 2, '.', ',');
        }
        $sort = 'desc';
        if ($request->get('type') != 'buy_online' && $request->get('type') != 'buy_for_cash' && $request->get('type') != 'trade') {
            $sort = 'asc';
        }
        $ad =  Advert::select(\DB::raw('adverts.*, users.last_activity_timestamp, COALESCE((SELECT (UNIX_TIMESTAMP() - sessions.last_activity)<600 FROM sessions WHERE adverts.user_id = sessions.user_id order by sessions.last_activity desc limit 1), 0) as last_activity'))
            ->join('users', 'adverts.user_id', '=', 'users.id')
            ->where('type', $request->get('type'))
            ->where('crypto_id', $request->get('crypto_id'))
            ->where('status', 'active')
            ->where('sum_crypto', '>', 0.0035)
            ->where('last_activity_timestamp', '>', time()-60*60*24);

        if (request('type') == 'trade') {
            $ad = $ad->where('crypto_trade_id', $request->get('crypto_trade_id'));
        } else {
            $ad = $ad->where('country_id', $request->get('country_id'));
            $ad = $ad->where('currency_id', $request->get('currency_id'));
        }
        if($request->get('payment_type_id')){
            $ad = $ad->where('payment_type_id', $request->get('payment_type_id'));
        }
        $ad = $ad->orderByDesc('last_activity')
        ->orderBy('rate', $sort)
        ->first();

        if ($request->get('type') == 'trade') {
            $data['rate'] = number_format(optional($ad)->rate, 8, '.', ',');
        } else {
            $data['rate'] = number_format(optional($ad)->rate, 2, '.', ',');
        }
        return $data;
    }
}
