<?php

namespace App\Http\Controllers;

use App\Http\Resources\CityResource;
use App\Models\City;
use Illuminate\Http\Request;

class CityController extends Controller
{
    public function search()
    {
        $cities = City::where('country_id', request()->get('country_id'))
            ->where('name', 'LIKE', request()->get('q') . '%')
            ->get();

        return CityResource::collection($cities);
    }
}
