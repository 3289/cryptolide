<?php

namespace App\Http\Controllers;

use App\Models\User;
use ParagonIE\ConstantTime\Base32;
use PragmaRX\Google2FA\Google2FA;

class GoogleFAController extends Controller
{
    public function enable(Google2FA $google2FA)
    {
        $randomBytes = random_bytes(10);

        $secret = Base32::encodeUpper($randomBytes);

        session()->put('google:fa', $secret);

        $imageDataUri = $google2FA->getQRCodeInline(
            request()->getHttpHost(),
            auth()->user()->email,
            $secret,
            200
        );

        return view('2fa/enable', [
            'image' => $imageDataUri,
            'secret' => $secret,
        ]);
    }

    public function confirm_enable(Google2FA $google2FA)
    {
        if (session()->has('google:fa')) {
            if ($google2FA->verifyKey(session('google:fa'), request('confirmation_code'))) {
                auth()->user()->update(['google_fa' => encrypt(session('google:fa'))]);
            } else {
                return back()->withErrors(['confirmation_code' => 'Неверный код подтверждения']);
            }
        }

        return redirect('/my-security');
    }

    public function disable()
    {
        return view('2fa/disable');
    }

    public function confirm_disable(Google2FA $google2FA)
    {
        /** @var User $user */
        $user = auth()->user();
        if ($user->google_fa) {
            if (!$google2FA->verifyKey(decrypt($user->google_fa), request('confirmation_code'))) {
                return back()->withErrors(['confirmation_code' => 'Неверный код подтверждения']);
            }

            $user->update(['google_fa' => null]);
        }

        return redirect('/my-security');
    }
}
