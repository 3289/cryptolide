<?php

namespace App\Http\Controllers;

use App\Models\Address;
use App\Models\CryptoCurrency;
use chillerlan\QRCode\Output\QRImage;
use chillerlan\QRCode\QRCode;

class AddressController extends Controller
{
    public function index(CryptoCurrency $crypto_currency)
    {
        if ($crypto_currency->id != 1) {
            return redirect('/my-wallet');
        }

        /** @var User $user */
        $user = auth()->user();
        $addresses = $user->addresses()->where('used' , 0)->where('crypto_id', $crypto_currency->id);

        if ($addresses->count() == 0) {
            $address = Address::where([['type', 'public'], ['crypto_id', $crypto_currency->id]])->get()->random();

            $address->update([
                'user_id' => $user->id,
            ]);
        } else {
            $address = $addresses->first();
        }

        $addresses = $user->addresses()->paginate(10);

        $image = new QRCode($address->address, new QRImage());
        $image_with_url = new QRCode(strtolower($address->crypto_currency->name) . ':' . $address->address, new QRImage());

        return view('replenishment-bitcoin-purse', compact('crypto_currency', 'address', 'addresses', 'image', 'image_with_url'));
    }

    public function isAddressHasAUser()
    {
        request()->validate([
            'address' => 'required|string|size:34',
        ]);

        $addresses = Address::where('address', request('address'));
        if ($addresses->count() > 0) {
            $address =  $addresses->first();
            if ($address->user_id) {
                return ['status' => true, 'username' => $address->user->login];
            }
        }

        return ['status' => false];
    }
}
