<?php

namespace App\Http\Controllers;

use App\Http\Resources\CityResource;
use App\Http\Resources\CountryResource;
use App\Models\City;
use App\Models\Country;
use Illuminate\Http\Request;

class CountryController extends Controller
{
    public function show(Country $country)
    {
        return [
            'currency' => $country->currency_id,
            'banks' => $country->banks,
        ];
    }

    public function cities(Country $country)
    {
        return response()->json(City::where('country_id', $country->id)->get());
    }
}
