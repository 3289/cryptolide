<?php

namespace App\Http\Controllers;

use App\Models\CryptoCurrency;

class OperationController extends Controller
{
    public function index()
    {
        $crypto = CryptoCurrency::find(request('id', 1));

        $operations = auth()->user()
            ->operations()
            ->where('crypto_id', $crypto->id)
            ->paginate(10);
        return view('my-wallet', compact('operations', 'crypto'));
    }

    public function update()
    {
        auth()->user()->operations()->update(['read' => 1]);

        return ['status' => true];
    }
}
