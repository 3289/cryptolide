<?php

namespace App\Http\Controllers;

use App\Http\Requests\Request;
use App\Models\Bank;
use App\Models\Advert;
use App\Models\Country;
use App\Models\Currency;
use App\Models\Achievement;
use App\Models\Deal;
use App\Models\PaymentType;
use App\Policies\AdvertPolicy;
use App\Models\CryptoCurrency;
use App\Http\Requests\AdvertRequest;
use App\Models\AdditionalCryptoCurrency;

class AdvertController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except('show');

        view()->composer(['create-ad', 'edit-ad'], function ($v) {
            $v->with('countries', Country::all());
            $v->with('currencies', Currency::all());
            $v->with('achievements', Achievement::all()->take(3));
            $v->with('paymentTypes', PaymentType::orderBy('priority', 'desc')->get());
            $v->with('cryptoCurrencies', CryptoCurrency::all());
            $v->with('additionalCryptoCurrencies', AdditionalCryptoCurrency::all());
            $v->with('banks', Bank::all());
        });
    }

    public function index()
    {
        $adverts = auth()->user()->adverts()->paginate(10);

        return view('my-ads', compact('adverts'));
    }

    public function create()
    {
        return view('create-ad');
    }

    protected function validateBanks($banks)
    {
        $list = json_decode($banks, true);

        if (count($list) == 0) {
            return 'Вы не выбрали банк.';
        }

        if (count($list) > 3) {
            return 'Вы можете выбрать только до 3-х банков включительно.';
        }

        if (in_array(0, $list) && count($list) > 1) {
            return 'Нельзя выбирать все банки страны и банки по-отдельности.';
        }

        if (!in_array(0, $list)) {
            foreach ($list as $bank) {
                if (!Bank::find($bank)) {
                    return 'Такой банк не найден.';
                }
            }
        }

        return false;
    }

    public function store(AdvertRequest $request)
    {
        if (in_array($request->get('type'), ['buy_online', 'sell_online']) && $request->get('payment_type_id') == 2) {
            if ($error = $this->validateBanks($request->get('bank_ids'))) {
                return $this->sendResponseWithError($error);
            }
        }

        $min_crypto = $request->get('min_crypto', 0);
        $min_currency = $request->get('min_currency', 1);
        if ($min_crypto == 0) {
            $min_crypto = $min_currency / $request->get('rate');
        }
        $params = array_merge($request->validated(), [
            'min_crypto' => $min_crypto,
        ]);

        if (!$request->has('keep_up')) {
            $params = array_merge($params, [
                'keep_up' => 0,
            ]);
        }

        unset($params['min_currency']);

        $advert = $request->user()->adverts()->create($params);

        return $advert;
    }

    public function show(Advert $advert)
    {
        $user = auth()->user();
        $_title = $advert->get_Title($user);
        $advert_type = $advert->getType();

        if (!isset($user)) {
            return view('buy-bitcoin', compact('advert', '_title', 'deal', 'advert_type'));
        }
        if ($advert->type == 'trade'){
            $deal = Deal::where('user_id', $user->id)
                ->whereIn('status', ['pending', 'paid'])
                ->first();
        }
        else{
            $deal = Deal::where('user_id', $user->id)
                ->where('type', $advert->type)
                ->where('currency_id', $advert->currency_id)
                ->where('payment_type_id', $advert->payment_type_id)
                ->where('country_id', $advert->country_id)
                ->whereIn('status', ['pending', 'paid'])
                ->first();
        }

        return view('buy-bitcoin', compact('advert', '_title', 'deal', 'advert_type'));
    }

    public function edit(Advert $advert)
    {
        $this->authorize(AdvertPolicy::UPDATE, $advert);

        return view('edit-ad', compact('advert'));
    }

    public function changeVisibility(Advert $advert)
    {
        $this->authorize(AdvertPolicy::UPDATE, $advert);

        $advert->update([
            'status' => request()->get('status'),
        ]);

        return ['status' => true];
    }

    public function update(AdvertRequest $request, Advert $advert)
    {

        $this->authorize(AdvertPolicy::UPDATE, $advert);

        if (in_array($request->get('type'), ['buy_online', 'sell_online']) && $request->get('payment_type_id') == 2) {
            if ($error = $this->validateBanks($request->get('bank_ids'))) {
                return $this->sendResponseWithError($error);
            }
        }

        $min_crypto = $request->get('min_crypto', 0);
        $min_currency = $request->get('min_currency', 1);
        if ($min_crypto == 0) {
            $min_crypto = $min_currency / $request->get('rate');
        }
        $keep_up = 1;
        if (!$request->has('keep_up')) {
            $keep_up = 0;
        }

        $params = array_merge($request->validated(), [
            'min_crypto' => $min_crypto,
            'keep_up' => $keep_up,
        ]);
        unset($params['min_currency']);

        $advert->update($params);

        return $advert;
    }

    public function destroy(Advert $advert)
    {
        $this->authorize(AdvertPolicy::UPDATE, $advert);

        $advert->delete();

        return [];
    }

    public function validateAdvertCopy(AdvertRequest $request)
    {
        $user = \Auth::user();
        $advert = Advert::where('user_id', $user->id);

        if ($request->get('type') == 'trade') {
            $advert = $advert->where('crypto_id', $request->get('crypto_id'))
                ->where('type', $request->get('type'))
                ->where('crypto_trade_id', $request->get('crypto_trade_id'))
                ->first();
        } else {
            $advert = $advert->where('country_id', $request->get('country_id'))
                ->where('type', $request->get('type'))
                ->where('payment_type_id', $request->get('payment_type_id'))
                ->where('currency_id', $request->get('currency_id'))
                ->first();
        }

        if (!$advert) {
            return [];
        }
        $result = '';
        if ($request->get('type') == 'trade') {
            $result = 'У вас уже есть объявление №' . $advert->id . ' с получаемой криптовалютой ' . $advert->crypto_trade->name . ' ' . $advert->crypto_trade->short . '
                и отдаваемой криптовалютой ' . $advert->crypto->name . ' ' . $advert->crypto->short . '
                Чтобы создать новое объявление - измените страну, валюту или способ оплаты. 
                Также вы можете <a href="' . url('/publications/' . $advert->id . '/edit') . '">отредактировать</a> уже существующее объявление №' . $advert->id . '.';
        } else {
            $result = 'У вас уже есть объявление №' . $advert->id . ' со способом оплаты: ' . $advert->payment_type->name
                . ' (' . $advert->currency->name . ') в стране: ' . $advert->country->name . '.
                Чтобы создать новое объявление - измените страну, валюту или способ оплаты. 
                Также вы можете <a href="' . url('/publications/' . $advert->id . '/edit') . '">отредактировать</a> уже существующее объявление №' . $advert->id . '.';
        }

        return $this->sendResponseWithError($result);
    }
}
