<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ChangeDealStatusConfirmCodeNotification extends Notification
{
    protected $deal;
    protected $confirmation_code;

    public function __construct($deal)
    {
        $this->deal = $deal;

        $this->confirmation_code = rand(100000, 999999);

        session()->put('changeStatus:' . $deal->id, $this->confirmation_code);
    }

    public function via($notifiable)
    {
        return ['mail'];
    }

    public function toMail($notifiable)
    {
        return (new MailMessage())->view('emails.change-status-confirm', [
            'deal' => $this->deal,
            'confirmation_code' => $this->confirmation_code,
        ]);
    }
}
