<?php

namespace App\Notifications;

use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ConfirmEmailNotification extends Notification
{
    protected $user;
    protected $url;

    public function __construct($user)
    {
        $this->user = $user;

        $token = hash_hmac('sha256', str_random(40), config('app.key'));

        $user->activations()->create(['token' => $token]);

        $this->url = route('user.activate', $token);
    }

    public function via($notifiable)
    {
        return ['mail'];
    }

    public function toMail($notifiable)
    {
        return (new MailMessage())->subject('Подтверждение адреса электронной почты')->view('emails.register', ['user' => $this->user, 'url' => $this->url]);
    }
}
