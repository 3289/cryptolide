<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class DefaultNotification extends Notification
{
    protected $topic;
    protected $subject;
    protected $content;

    public function __construct($topic = null, $subject, $content)
    {
        $this->topic = $topic;

        $this->subject = $subject;

        $this->content = $content;
    }

    public function via($notifiable)
    {
        return ['mail'];
    }

    public function toMail($notifiable)
    {
        return (new MailMessage())->subject($this->subject)->view('emails.notification', [
            'topic' => $this->topic,
            'subject' => $this->subject,
            'content' => $this->content,
        ]);
    }
}
