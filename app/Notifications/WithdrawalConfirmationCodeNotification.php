<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class WithdrawalConfirmationCodeNotification extends Notification
{
    protected $address;
    protected $confirmation_code;
    protected $currency;
    protected $amount;

    public function __construct($address, $confirmation_code, $currency, $amount)
    {
        $this->address = $address;
        $this->confirmation_code = $confirmation_code;
        $this->currency = $currency;
        //$this->amount = $amount;
        $this->amount = rtrim(number_format($amount, 8), '0');
    }

    public function via($notifiable)
    {
        return ['mail'];
    }

    public function toMail($notifiable)
    {
        return (new MailMessage())->subject('Подтверждение вывода денежных средств')->view('emails.withdrawal-confirm', [
            'address' => $this->address,
            'amount' => $this->amount,
            'currency' => $this->currency,
            'confirmation_code' => $this->confirmation_code,
        ]);
    }
}
