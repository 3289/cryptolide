<?php

namespace App\Notifications;

use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ChangeEmailNotification extends Notification
{
    protected $activation_code;
    protected $user;
    public $subject;

    public function __construct($user, $subject)
    {
        $this->user = $user;
        $this->subject = $subject;
        $this->activation_code = rand(100000, 999999);
        cache()->set('activation_code', $this->activation_code, 4);
    }

    public function via($notifiable)
    {
        return ['mail'];
    }

    public function toMail($notifiable)
    {
        return (new MailMessage())->subject($this->subject)->view('emails.change-email', ['login' => $this->user->login, 'activation_code' => $this->activation_code]);
    }
}
