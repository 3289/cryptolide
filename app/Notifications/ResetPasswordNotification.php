<?php

namespace App\Notifications;

use App\Mail\RecoveryMail;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ResetPasswordNotification extends Notification
{
    public $token;
    public $user;
    public $subject;

    public function __construct($token, $user,  $subject)
    {
        $this->token = $token;
        $this->user = $user;
        $this->subject = $subject;

    }

    public function via($notifiable)
    {
        return ['mail'];
    }

    public function toMail($notifiable)
    {
        return (new MailMessage())->subject($this->subject)->view('emails.recovery', ['token' => $this->token, 'login' => $this->user->login]);
    }
}
