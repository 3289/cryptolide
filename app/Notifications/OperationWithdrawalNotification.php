<?php

namespace App\Notifications;

use App\Models\Withdrawal;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class OperationWithdrawalNotification extends Notification
{
    protected $userLogin;
    protected $address;
    protected $currency;
    protected $amount;
    protected $subject;

    public function __construct($subject, Withdrawal $withdrawal)
    {
        $this->userLogin = $withdrawal->operation->user->login;
        $this->address = $withdrawal->address;
        $this->currency = $withdrawal->operation->crypto_currency->short;
        //$this->amount = abs($withdrawal->operation->amount)-$withdrawal->operation->commission;
        $this->amount = rtrim(number_format(abs($withdrawal->operation->amount)-$withdrawal->operation->commission, 8), '0');
        $this->subject = $subject;
    }

    public function via($notifiable)
    {
        return ['mail'];
    }

    public function toMail($notifiable)
    {
        return (new MailMessage())->subject($this->subject)->view('emails.operation-withdrawal', [
            'userLogin' => $this->userLogin,
            'address' => $this->address,
            'amount' => $this->amount,
            'currency' => $this->currency,
            'subject' => $this->subject,
        ]);
    }
}
