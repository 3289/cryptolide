<?php

namespace App\Observers;


use App\Models\User;

class UserObserver
{
    /**
     * Listen to the User created event.
     *
     * @param \App\Models\User $user
     * @return void
     */
    public function created(User $user)
    {
        if ($user->referrer_id) {
            $user->referrer->sendNotification('К вам присоединился новый партнёр',
                'Пользователь <a href="'.url('/profile/'.$user->login).'">'.$user->login.'</a> присоединился в вашу команду',
                'Уведомление о регистрации нового партнера');
        }
    }
}