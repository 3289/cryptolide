<?php

namespace App\Observers;

use App\Models\Withdrawal;

class WithdrawalObserver
{

    public function created(Withdrawal $withdrawal)
    {
        $withdrawal->sendNotification('Заявка на вывод денежных средств', $withdrawal);
    }
}