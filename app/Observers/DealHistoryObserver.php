<?php

namespace App\Observers;

use App\Models\DealHistory;

class DealHistoryObserver
{
    /**
     * Listen to the DealHistory created event.
     *
     * @param \App\Models\DealHistory $dealHistory
     * @return void
     */
    public function created(DealHistory $dealHistory)
    {
        if ($dealHistory->type == 'completed') {
            return;
        }
        $dealHistory->user->sendNotification(str_replace(
            ['completed', 'paid', 'new_message', 'opened', 'canceled_by_service', 'canceled_by_user'],
            ['Сделка завершена', 'Уведомление об оплате по сделке №'.$dealHistory->deal_id, 'Новое сообщение от '.auth()->user()->login.' по сделке №'.$dealHistory->deal_id, 'По вашему объявлению открыта сделка №'.$dealHistory->deal_id, 'Автоматическая отмена сделки №'.$dealHistory->deal_id.' по причине неуплаты', 'Сделка №'.$dealHistory->deal_id.' отменена пользователем '.auth()->user()->login],
            $dealHistory->type
        ), $dealHistory->text,
            $dealHistory->topic);
    }
}