<?php

namespace App\Observers;

use App\Models\Operation;

class OperationObserver
{
    /**
     * Listen to the DealHistory created event.
     *
     * @param \App\Models\Operation $operation
     * @return void
     */
    public function created(Operation $operation)
    {
        if ($operation->type == Operation::TYPE_REFILL) {
            $operation->user->sendNotification('Успешное пополнение BTC адреса', $operation->description);
        } else if ($operation->type == Operation::TYPE_ENDED_DEAL) {
            $operation->user->sendNotification('Сделка №'.$operation->deal_id.' успешно завершена!', $operation->description, 'Уведомление об успешно завершенной сделке');
        } else if ($operation->type == Operation::TYPE_WITHDRAW) {
            //$operation->user->sendNotification('Успешный вывод денежных средств', $operation->description, 'Уведомление об успешном выводе денежных средств');
        } else if ($operation->type == Operation::TYPE_TRANSFER_FROM) {
            $operation->user->sendNotification('Успешный вывод денежных средств', $operation->description, 'Уведомление об успешном выводе денежных средств');
        } else if ($operation->type == Operation::TYPE_TRANSFER_TO) {
            $operation->user->sendNotification('Вы получили внутренний денежный перевод от '.auth()->user()->login, $operation->description, 'Уведомление об успешном получении денежного перевода');
        } else if ($operation->description !== '') {
            $operation->user->sendNotification('Уведомление о новой операции', $operation->description);
        }
    }
}