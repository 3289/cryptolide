<?php

namespace App\Models;

class Risk extends Model
{
    public $timestamps = false;

    public function currency()
    {
        return $this->belongsTo(Currency::class);
    }

    public function payment_type()
    {
        return $this->belongsTo(PaymentType::class);
    }
}
