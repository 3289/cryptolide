<?php

namespace App\Models;

use App\Scopes\ScopeName;

class AdditionalCryptoCurrency extends Model
{
    public $timestamps = false;

    protected static function boot()
    {
        static::addGlobalScope(new ScopeName);

        parent::boot();
    }
}
