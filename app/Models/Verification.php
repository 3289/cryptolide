<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Verification extends Model
{
    const TYPES = [
        1 => 'Viber или WhatsApp',
        2 => 'СМС',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function scopeCodeForAuthUser($query, $type)
    {
        return $query->where('user_id', auth()->user()->id)
            ->where('type', $type)
            ->where('created_at', '>', date('Y-m-d H:i:s', time()-900));
    }
}
