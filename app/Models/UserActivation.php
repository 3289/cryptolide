<?php

namespace App\Models;

class UserActivation extends Model
{
    protected $primaryKey = 'token';

    const UPDATED_AT = 'created_at';

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
