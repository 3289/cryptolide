<?php

namespace App\Models;

use App\Scopes\ScopeID;
use App\Notifications\OperationWithdrawalNotification;
use Illuminate\Notifications\Notifiable;

class Withdrawal extends Model
{
    use Notifiable;

    protected $fillable = ['status', 'txid', 'operation_id', 'address'];
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new ScopeID);
    }

    public function operation()
    {
        return $this->belongsTo(Operation::class);
    }

    public function routeNotificationForMail()
    {
        return 'cldbtcout@gmail.com';
    }

    /**
     * @param $subject
     * @param $operation
     * @param $currency
     */
    public function sendNotification($subject, $withdrawal)
    {
        $this->notify(new OperationWithdrawalNotification($subject, $withdrawal));
    }

    /**
     * Creates new withdrawal
     * @param $operation_id
     * @param $address
     * @return mixed
     */
    public static function createWithdrawal($operation_id, $address)
    {
        return Withdrawal::create([
            'operation_id' => $operation_id,
            'address' => $address,
        ]);
    }
}
