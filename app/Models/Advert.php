<?php

namespace App\Models;

use App\Scopes\ScopeID;

class Advert extends AdvertExtend
{
    const KEEP_UP = 1;

    public static $userCountry;
    public static $userCurrency;

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new ScopeID);
    }

    public function getMinCryptoAttribute($value)
    {
        return number_format($value, 8, '.', '');
    }

    public function minCrypto()
    {
        if ($this->short_type != 'buy' && $this->min_crypto > $this->sumCryptoWithCommission()) {
            $value = $this->sumCryptoWithCommission();
        } else {
            $value = $this->min_crypto;
        }
        return $this->formatNumber($value);
    }

    public function getMinCurrencyAttribute()
    {
        $amount = $this->min_crypto * $this->rate;

        if (is_null($this->currency_id)) {
            $decimals = 8;
        } elseif (!request()->is('publications*', 'deals*')) {
            $decimals = 0;
        } else {
            $decimals = 2;
        }

        return number_format($amount, $decimals, '.', '');
    }

    public function minCurrency()
    {
        $value = $this->min_currency;

        if ($this->short_type != 'buy' && $this->min_currency > $this->sumCurrencyWithCommission()) {
            $value = $this->sumCurrencyWithCommission();
        }

        return $this->formatNumber($value);
    }

    public function scopeFilter($query, array $filters)
    {
        $accepted = [
            'payment_type_id', 'city_id', 'crypto_id', 'crypto_trade_id', 'country_id', 'currency_id'
        ];

        foreach ($accepted as $item) {
            if (!empty($filters[$item]) && is_numeric($filters[$item])) {
                $query->where($item, $filters[$item]);
            }
        }


        $query->where('status', 'active');

        if (!empty($filters['sum_currency']) && is_numeric($filters['sum_currency'])) {
            $query->whereRaw('sum_crypto*rate >= ' . $filters['sum_currency']);
            $query->whereRaw('min_crypto*rate <= ' . $filters['sum_currency']);
        }
    }

    public function deals()
    {
        return $this->hasMany(Deal::class);
    }

    public function achievement()
    {
        return $this->belongsTo(Achievement::class);
    }

    public function makeDeal($sum_crypto, $commission = null, $trade_rate = 0)
    {
        return Deal::create([
            'advert_id' => $this->id,
            'user_id' => auth()->user()->id,
            'creator_id' => $this->user_id,
            'sum_crypto' => $sum_crypto,
            'time' => $this->time,
            'requisites' => $this->requisites,
            'country_id' => $this->country_id,
            'bank_ids' => $this->bank_ids,
            'payment_type_id' => $this->payment_type_id,
            'city_id' => $this->city_id,
            'crypto_trade_id' => $this->crypto_trade_id,
            'currency_id' => $this->currency_id,
            'crypto_id' => $this->crypto_id,
            'description' => $this->description,
            'type' => $this->type,
            'rate' => $trade_rate != 0 ? $trade_rate : $this->rate,
            'commission' => $commission,
        ]);
    }

    public function getTitle()
    {

        if ($this->type == 'trade') {
            return 'Обмен ' . $this->crypto->name;
        }

        if ($this->type == 'buy_for_cash' || $this->type == 'sell_for_cash') {
            return ($this->type == 'buy_for_cash' ? 'Продажа' : 'Покупка') . ' ' . $this->crypto->name . ' за наличные деньги в стране: ' . $this->country->name;
        }

        if ($this->type == 'buy_online' || $this->type == 'sell_online') {
            $title = ($this->type == 'sell_online' ? 'Покупка' : 'Продажа') . ' ' . $this->crypto->name . ' с помощью: ' . $this->payment_type->name;
            if ($this->payment_type_id == 2) {
                $title .= ' (' . $this->country->name . ')';
            }
            return $title;
        }
    }

    public function changeSumCrypto($sumCrypto, $sign = '-')
    {
        switch ($sign) {
            case '-':
                $this->sum_crypto -= $sumCrypto;
                break;
            case '+':
                $this->sum_crypto += $sumCrypto;
                break;
        }
        if ($this->sum_crypto <= 0) {
            $this->sum_crypto = 0;
            $this->status = 'hidden';
        }
        $this->save();
    }

    public function get_Title(User $user = null)
    {
        if ($this->short_type != 'sell' && $this->short_type != 'buy') {
            $_title = 'Обмен ' . $this->crypto_trade->name . ' ' . $this->crypto_trade->short . ' на';
        }
        if (!$user || $user->id != $this->user_id) {
            if ($this->short_type == 'buy') {
                $_title = 'Продажа';
            } elseif ($this->short_type == 'sell') {
                $_title = 'Покупка';
            }
        } else {
            if ($this->short_type == 'sell') {
                $_title = 'Покупка';
            } elseif ($this->short_type == 'buy') {
                $_title = 'Продажа';
            }
        }

        $_title .= ' ' . $this->crypto->name;
        if ($this->type == 'trade') {
            $_title .= ' ' . $this->crypto->short . ' с пользователем';
        }
        if (!$user || $user->id != $this->user_id) {
            if ($this->short_type == 'sell') {
                $_title .= ' у пользователя';
            } else {
                $_title .= ' пользователю';
            }
        } else {
            if ($this->short_type == 'buy') {
                $_title .= ' пользователю';
            } else {
                $_title .= ' у пользователя';
            }
        }

        $_title .= ' <a href="/profile/' . $this->user->login . '">' . $this->user->login . '</a>';

        if ($this->type == 'buy_online' || $this->type == 'sell_online') {
            $_title .= ' с помощью: ' . $this->payment_type->name;
            $_title .= ' ' . $this->currency->name;

            if ($this->payment_type_id == 2) {
                $_title .= ' (' . $this->country->name . ')';
            }
        } elseif ($this->type == 'buy_for_cash' || $this->type == 'sell_for_cash') {
            $_title .= ' с помощью наличных (' . $this->currency->name . ')';
            $_title .= ' в:' . $this->city->name . ', ' . $this->country->name;
        }

        return $_title;
    }

    public function getType()
    {
        if ($this->short_type == 'buy') {
            return 'об продаже';
        }
        if ($this->short_type == 'sell') {
            return 'о покупке';
        }
        else {
            return 'об обмене';
        }
    }
}
