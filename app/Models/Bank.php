<?php

namespace App\Models;

class Bank extends Model
{
    public $timestamps = false;

    public function country()
    {
        return $this->belongsTo(Country::class);
    }
}
