<?php

namespace App\Models;

use App\Scopes\ScopeID;

class Address extends Model
{
    protected static function boot()
    {
        static::addGlobalScope(new ScopeID);

        parent::boot();
    }

    public function scopeFilter($query, array $filters = [])
    {
        if (!empty($filters['type'])) {
            $query->where('type', $filters['type']);
        }
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function crypto_currency()
    {
        return $this->belongsTo(CryptoCurrency::class, 'crypto_id', 'id');
    }
}
