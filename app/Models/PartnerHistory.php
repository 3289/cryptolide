<?php

namespace App\Models;

class PartnerHistory extends Model
{
    public $timestamps = false;

    public static function getNewPartnerHistories()
    {
        $partnerHistories = [];
        $count = auth()->user()
            ->partner_histories()
            ->where('read', 0)
            ->count();
        foreach (auth()->user()
                     ->partners()
                     ->orderBy('id', 'desc')
                     ->limit($count)
                     ->get() as $partnerHistory) {
            $partnerHistories[$partnerHistory->id]['date'] = $partnerHistory->created_at->format('d.m.Y H:i');
            $partnerHistories[$partnerHistory->id]['img'] = $partnerHistory->image;
            $partnerHistories[$partnerHistory->id]['login'] = $partnerHistory->login;
        }
        return $partnerHistories;
    }
}
