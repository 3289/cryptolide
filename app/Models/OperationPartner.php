<?php

namespace App\Models;

use App\Scopes\ScopeID;

class OperationPartner extends Model
{
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new ScopeID);
    }
}
