<?php

namespace App\Models;

use App\Mail\NotificationMail;
use App\Notifications\DealConfirmationCodeNotification;
use Carbon\Carbon;
use App\Scopes\ScopeID;

class Deal extends AdvertExtend
{
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new ScopeID);
    }

    public function getRateUpAttribute()
    {
        return floor(($this->sum_crypto * $this->crypto->coefficient * 0.12) + ($this->sum_crypto * $this->crypto->coefficient * auth()->user()->commission) + 1);
    }

    public function getTimeLeftAttribute()
    {
        return Carbon::now()->diffInMinutes($this->created_at->addMinutes($this->time), false);
    }

    public function getClosingTimeAttribute()
    {
        return $this->created_at->addMinutes($this->time)->format('H:i');
    }

    public function creator()
    {
        return $this->belongsTo(User::class, 'creator_id', 'id');
    }

    public function messages()
    {
        return $this->hasMany(Message::class);
    }

    public function operations()
    {
        return $this->hasMany(Operation::class);
    }

    public function advert()
    {
        return $this->belongsTo(Advert::class);
    }

    public function partner()
    {
        return auth()->user()->id == $this->user_id ? $this->creator() : $this->user();
    }

    public function seller()
    {
        if ($this->short_type == 'buy') {
            return $this->user();
        }

        return $this->creator();
    }

    public function buyer()
    {
        if ($this->short_type == 'buy') {
            return $this->creator();
        }
        return $this->user();
    }

    public function notify($message, $type, $user_id, $topic = null, $notification = null)
    {
        DealHistory::create([
            'user_id' => $user_id,
            'deal_id' => $this->id,
            'text' => $message,
            'type' => $type,
            'topic' => $topic,
            'notification' => $notification,
        ]);
    }

    public function scopeActive($query)
    {
        return $query->where('status', 'pending')
            ->orWhere('status', 'paid');
    }

    public function scopeArchive($query)
    {
        return $query->where('status', 'cancelled')
            ->orWhere('status', 'completed');
    }

    public function addRate($user)
    {
        return floor(($this->sum_crypto * $this->crypto->coefficient * 0.12)
            + ($this->sum_crypto * $this->crypto->coefficient * $user->commission) + 1);
    }

    public function makeDealCreateMsg()
    {
        $html = 'По вашему объявлению открыта сделка <a href="' . url('/deals/' . $this->id) . '">№' . $this->id
            . '</a>.<br>' . 'Пользователь <a href="' . url('/profile/' . auth()->user()->login) . '">' . auth()->user()->login
            . '</a> <span class="rating">(TS: ' . auth()->user()->rating . ')</span> хочет ';


        if ($this->type == 'trade') {
            $html .=
                'обменять <span class="notification-message__income">' . $this->sum_currency . ' '
                . $this->crypto_trade->short . ' (' . $this->crypto_trade->name . ')</span> на ' . $this->sum_crypto . ' '
                . $this->crypto->short . ' (' . $this->crypto->name . ')';
        } else {
            $html .= ($this->short_type == 'buy' ? 'продать' : ($this->short_type == 'sell' ? 'купить' : 'обменять'))
                . ' ' . $this->sum_crypto . ' ' . $this->crypto->short . ' за <span class="notification-message__income">'
                . $this->sum_currency . ' ' . $this->currency_name . '.</span>'
                . (($this->type == 'sell_for_cash' || $this->type == 'buy_for_cash') ? ' с помощью наличных ('
                    . $this->currency_name . ') в: ' . $this->city->name . ', '
                    . $this->country->name : ' через: ' . $this->payment_type->name . ' '
                    . ($this->payment_type_id == 2 ? '(' . $this->country->name . ') - ' . $this->banks : ''));
        }

        return $html;


    }

    public function makeDealCreateNotification()
    {
        if ($this->type == 'trade') {
            return 'По вашему объявлению открыта сделка <a href="' . url('/deals/' . $this->id) . '">№' . $this->id . '</a>.<br>'
            . 'Пользователь <a href="' . url('/profile/' . auth()->user()->login) . '">' . auth()->user()->login . '</a> <span class="rating">' . auth()->user()->rating . '</span> хочет обменять <span class="notification-message__income">' . $this->sum_currency . ' ' . $this->crypto_trade->short . ' (' . $this->crypto_trade->name . ')</span> на ' . $this->sum_crypto . ' ' . $this->crypto->short . ' (' . $this->crypto->name . ')';
        }

        return 'По вашему объявлению открыта сделка <a href="' . url('/deals/' . $this->id) . '">№' . $this->id . '.</a>.<br>'
        . 'Пользователь <a href="' . url('/profile/' . auth()->user()->login) . '">' . auth()->user()->login . '</a> <span class="rating">' . auth()->user()->rating . '</span> хочет ' . ($this->short_type == 'buy' ? 'продать' : ($this->short_type == 'sell' ? 'купить' : 'обменять')) . ' ' . $this->sum_crypto . ' ' . $this->crypto->short . ' за <span class="notification-message__income">' . $this->sum_currency . ' ' . $this->currency_name . '.</span>'
        . (($this->type == 'sell_for_cash' || $this->type == 'buy_for_cash') ? ' с помощью наличных (' . $this->currency_name . ') в: ' . $this->city->name . ', ' . $this->country->name : ' через: ' . $this->payment_type->name . ' ' . ($this->payment_type_id == 2 ? '(' . $this->country->name . ') - ' . $this->banks : ''));

    }

    public function saveConfirmationCode()
    {
        $response = [
            'status' => true,
            'message' => 'Код успешно отправлен.'
        ];

        if (auth()->user()->id != $this->seller->id) {
            return $response;
        }

        $error_message = $this->checkError();

        if ($error_message !== false) {
            $response['message'] = $error_message;
            return $response;
        }

        $this->sendNewNotify();

        return $response;

    }

    public function sendNewNotify()
    {
        $this->seller->notify(new DealConfirmationCodeNotification($this));
        $this->attempts_count += 1;
        $this->send_time = date("Y-m-d H:i:s");
        $this->save();
    }

    /**
     * @return bool|string
     */
    private function checkError()
    {
        if (($this->attempts_count % 3 == 0) && isset($this->send_time) && $this->send_time >= date("Y-m-d H:i:s", time() - 300)) {
            return 'Попробуйте снова через 5 минут.';
        }
        if (isset($this->send_time) && $this->send_time >= date("Y-m-d H:i:s", time() - 60)) {
            return 'Письмо можно отправлять не чаще чем один раз в 60 секунд.';
        }
        return false;
    }

    public function cancelDeal($by='service')
    {
        $this->update(['status' => 'cancelled']);

        Operation::create([
            'amount' => $this->sum_crypto,
            'user_id' => $this->seller->id,
            'description' => 'Возврат средств из депонирования по сделке <a href="' . url('/deals/' . $this->id) . '">#' . $this->id . '</a>.',
            'crypto_id' => $this->crypto_id,
            'type' => 0,
            'notification' => 'Возврат средств из депонирования по сделке <a href="' . url('/deals/' . $this->id)
                . '">#' . $this->id . '</a>.',
        ]);

        if ($this->advert->keep_up == Advert::KEEP_UP) {
            $this->advert->changeSumCrypto($this->sum_crypto, '+');
        }
        if ($by == 'service') {
            $this->cancelByService();
        } else {
            $this->cancelByUser();
        }
    }

    private function cancelByService()
    {
        $topic = 'Уведомление об автоматической отмене сделки';
        $message = 'Сделка <a href="' . url('/deals/' . $this->id) . '">№' . $this->id . '</a>. отменена и закрыта сервисом по причине неуплаты!';

        if ($this->creator->id == $this->seller->id) {
            $this->notify($message . '<br>' . $this->sum_crypto . ' ' . $this->crypto->short . ' возвращены на баланс из депонирования.', 'canceled_by_service', $this->creator->id, $topic);
        } else {
            $this->notify($message, 'canceled_by_service', $this->creator->id, $topic);
        }
        $this->notify($message, 'canceled_by_service', $this->user->id, $topic);
    }

    private function cancelByUser()
    {
        $topic = 'Уведомление об отмене сделки';
        $message = 'Сделка <a href="' . url('/deals/' . $this->id) . '">№' . $this->id
            . '</a>. отменена пользователем ' . auth()->user()->login . '. <br>' . $this->sumCrypto() . ' ' . $this->crypto->short . ' возвращены на баланс из депонирования.';
        $this->notify($message, 'canceled_by_user', $this->seller->id, $topic);
    }

    /**
     * @param User|null $user
     * @return bool
     */
    public function isSeller(User $user = null)
    {
        if ($user === null) {
            return false;
        }
        return $this->seller->id == $user->id;
    }

    public function isCanceled()
    {
        return $this->status == 'cancelled';
    }

    public function isCompleted()
    {
        return $this->status == 'completed';
    }

}
