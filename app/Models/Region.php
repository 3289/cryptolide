<?php

namespace App\Models;

class Region extends Model
{
    public $timestamps = false;

    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function cities()
    {
        return $this->hasMany(City::class);
    }
}
