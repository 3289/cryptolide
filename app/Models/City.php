<?php

namespace App\Models;

use App\Scopes\ScopeName;

class City extends Model
{
    public $timestamps = false;

    protected static function boot()
    {
        static::addGlobalScope(new ScopeName);

        parent::boot();
    }

    public function country()
    {
        return $this->belongsTo(Country::class);
    }
}
