<?php

namespace App\Models;

use App\Scopes\ScopeID;

class Session extends Model
{
    protected static function boot()
    {
        static::addGlobalScope(new ScopeID);

        parent::boot();
    }
}
