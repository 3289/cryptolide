<?php

namespace App\Models;

use Storage;

class Achievement extends Model
{
    public $timestamps = false;

    public function getImageAttribute($value)
    {
        if (is_null($value)) {
            return Storage::url('public/achievements/' . $this->id . '.png');
        }

        return url($value);
    }

    public function users()
    {
        return $this->belongsToMany(User::class)->withPivot('created_at');
    }

    public function delete()
    {
        $this->users()->detach();

        return parent::delete();
    }
}
