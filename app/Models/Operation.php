<?php

namespace App\Models;

use App\Scopes\ScopeID;

class Operation extends Model
{
    const TYPE_NEW = 0;
    const TYPE_REFILL = 1;
    const TYPE_ENDED_DEAL = 2;
    const TYPE_WITHDRAW = 3;
    const TYPE_TRANSFER_FROM = 4;
    const TYPE_TRANSFER_TO = 5;
    const TYPE_DEPOSIT = 6;
    const TYPE_CASH_RETURN = 7;

    const NOT_READED = 0;
    const READED = 1;

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new ScopeID);
    }

    public function scopeFilter($query, array $filters = [])
    {
        if (!empty($filters['id'])) {
            $query->where('id', $filters['id']);
        }
    }

    public function getAmountAttribute($value)
    {
        return number_format($value, 8, '.', '');
    }

    public function getBalanceBeforeAttribute()
    {
        $user = \Auth::user();
        $balance = self::where('id', '<=', $this->id)
            ->where('crypto_id', $this->crypto_id)
            ->where('user_id', $user->id)
            ->sum('amount');

        return number_format($balance, 8, '.', '');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function deal()
    {
        return $this->belongsTo(Deal::class);
    }

    public function crypto_currency()
    {
        return $this->belongsTo(CryptoCurrency::class, 'crypto_id', 'id');
    }

    public static function getNewOperations()
    {
        $operations = [];
        foreach (auth()->user()->operations()->where('read', 0)->get() as $operation) {
            $operations[$operation->id]['date'] = $operation->created_at->format('d.m.Y в H:i');
            if ($operation->amount > 0) {
                $operations[$operation->id]['img'] = '/images/icons/icon_income_transaction.png';
                $operations[$operation->id]['text'] = 'Приход <span class="notification-message__income">+';
            } else {
                $operations[$operation->id]['img'] = '/images/icons/icon_cost_transaction.png';
                $operations[$operation->id]['text'] = 'Расход <span class="notification-message__income">';
            }
            $operations[$operation->id]['text'] .= $operation->amount . ' ' . $operation->crypto_currency->short
                . '.</span><br>' . ($operation->notification ? $operation->notification : $operation->description);
        }
        return $operations;
    }

    public function getCommissionInCryptoAttribute()
    {
        return number_format(abs($this->amount/(1-$this->commission/100)-$this->amount), 8);
    }

    public function withdrawal(){
        return $this->hasOne(Withdrawal::class);
    }

    /**
     * Creates new operation
     * @param $crypto_id
     * @param $user_id
     * @param $amount
     * @param $commission
     * @param $description
     * @param int $type
     * @param int $read
     * @param null $notification
     * @return mixed
     */
    public static function createOperation($crypto_id, $user_id, $amount, $commission, $description, $type = 0, $read = 1, $notification = null)
    {
        return Operation::create([
            'crypto_id' => $crypto_id,
            'user_id' => $user_id,
            'amount' => $amount,
            'commission' => $commission,
            'description' => $description,
            'type' => $type,
            'read' => $read,
            'notification' => $notification,
        ]);
    }
}
