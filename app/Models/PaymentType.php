<?php

namespace App\Models;

use App\Scopes\ScopeName;

class PaymentType extends Model
{
    public $timestamps = false;

    protected static function boot()
    {
        static::addGlobalScope(new ScopeName);

        parent::boot();
    }
}
