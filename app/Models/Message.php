<?php

namespace App\Models;

use App\Scopes\ScopeID;

class Message extends Model
{
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
