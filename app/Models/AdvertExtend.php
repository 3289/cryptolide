<?php

namespace App\Models;

class AdvertExtend extends Model
{
    public function getBanksAttribute()
    {
        $banks = json_decode($this->bank_ids, true);
        if(in_array(0, $banks)) {
            return 'Все банки страны';
        }

        return Bank::find($banks)->implode('name', ', ');
    }

    public function getTimeAttribute($value)
    {
        return $value ?: 90;
    }

    public function getCurrencyNameAttribute()
    {
        if (is_null($this->currency_id)) {
            return $this->crypto_trade->short;
        }

        return $this->currency->name;
    }

    protected function formatNumber($number)
    {
        $ex = explode('.', $this->decimalNotation($number));
        $decimals = count($ex) == 2 ? strlen($ex[1]) : 0;

        return number_format($number, $decimals, '.', ',');
    }

    public function decimalNotation($num){
        $parts = explode('E', $num);
        if(count($parts) != 2){
            return $num;
        }

        $exp = abs(end($parts)) + 3;
        $decimal = number_format($num, $exp);
        $decimal = rtrim($decimal, '0');

        return rtrim($decimal, '.');
    }

    public function getRateAttribute($value)
    {
        $decimals = 2;

        if (is_null($this->currency_id)) {
            $decimals = 8;
        }

        return number_format($value, $decimals, '.', '');
    }

    public function rate()
    {
        return $this->formatNumber($this->rate);
    }

    public function getShortTypeAttribute()
    {
        if ($this->type == 'sell_for_cash' || $this->type == 'sell_online') {
            return 'sell';
        }

        if ($this->type == 'buy_for_cash' || $this->type == 'buy_online') {
            return 'buy';
        }

        return 'trade';
    }

    public function getSumCryptoAttribute($value)
    {
        return number_format($value, 8, '.', '');
    }

    public function sumCrypto()
    {
        return $this->formatNumber($this->sum_crypto);
    }

    public function sumCryptoWithCommission()
    {
        return number_format($this->sumCrypto()-($this->sumCrypto()*$this->user->commission/100), 8, '.', ',');
    }

    public function getSumCurrencyAttribute()
    {
        $amount = $this->sum_crypto * $this->rate;

        $decimals = 2;
        if (is_null($this->currency_id)) {
            $decimals = 8;
        } elseif (!request()->is('publications*', 'deals*')) {
            $decimals = 0;
        }

        return number_format($amount, $decimals, '.', '');
    }

    public function sumCurrency()
    {
        return $this->formatNumber($this->sum_currency);
    }

    public function sumCurrencyWithCommission()
    {
        $amount = ($this->sum_crypto * $this->rate)-(($this->sum_crypto * $this->rate)*$this->user->commission/100);
        $decimals = 2;
        if (is_null($this->currency_id)) {
            $decimals = 8;
        } elseif (!request()->is('publications*', 'deals*')) {
            $decimals = 0;
        }

        return number_format($amount, $decimals, '.', '');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function currency()
    {
        return $this->belongsTo(Currency::class);
    }

    public function crypto()
    {
        return $this->belongsTo(CryptoCurrency::class, 'crypto_id', 'id');
    }

    public function crypto_trade()
    {
        return $this->belongsTo(AdditionalCryptoCurrency::class, 'crypto_trade_id', 'id');
    }

    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function payment_type()
    {
        return $this->belongsTo(PaymentType::class);
    }
}
