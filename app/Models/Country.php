<?php

namespace App\Models;

use App\Scopes\ScopeName;

class Country extends Model
{
    public $timestamps = false;

    protected static function boot()
    {
        static::addGlobalScope(new ScopeName);

        parent::boot();
    }

    public function currency()
    {
        return $this->belongsTo(Currency::class)->withDefault(['id' => 0, 'name' => '']);
    }

    public function banks()
    {
        return $this->hasMany(Bank::class)->orderBy('name', 'asc');
    }

    public function cities()
    {
        return $this->hasMany(City::class);
    }
}
