<?php

namespace App\Models;

use App\Scopes\ScopeID;
use Illuminate\Database\Eloquent\Model as Eloquent;

class Model extends Eloquent
{
    protected $guarded = ['id'];
}