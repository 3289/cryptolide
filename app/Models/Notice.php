<?php

namespace App\Models;


class Notice extends Model
{
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
