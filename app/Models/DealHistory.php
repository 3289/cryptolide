<?php

namespace App\Models;

use App\Scopes\ScopeID;

class DealHistory extends Model
{
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new ScopeID);
    }

    public function deal()
    {
        return $this->belongsTo(Deal::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public static function getNewDealHistories()
    {
        $dealHistories = [];
        foreach (auth()->user()->deal_histories()->where('read', 0)->get() as $dealHistory) {
            $dealHistories[$dealHistory->id]['date'] = $dealHistory->created_at->format('d.m.Y H:i');
            $dealHistories[$dealHistory->id]['text'] = $dealHistory->notification ? $dealHistory->notification : $dealHistory->text;
            if ($dealHistory->type == 'completed') {
                $dealHistories[$dealHistory->id]['img'] = '/images/icons/icon_successfull_transaction.png';
            } else if ($dealHistory->type == 'paid') {
                $dealHistories[$dealHistory->id]['img'] = '/images/icons/icon_sending_money.png';
            } else if ($dealHistory->type == 'new_message') {
                $dealHistories[$dealHistory->id]['img'] = '/images/icons/icon_new_message.png';
            } else if ($dealHistory->type == 'opened') {
                $dealHistories[$dealHistory->id]['img'] = $dealHistory->deal->partner->image;
            } else if ($dealHistory->type == 'canceled_by_service') {
                $dealHistories[$dealHistory->id]['img'] = '/images/icons/icon_cancel_deal.png';
            } else if ($dealHistory->type == 'canceled_by_user') {
                $dealHistories[$dealHistory->id]['img'] = '/images/icons/icon_cancel_deal_user.png';
            }
        }
        return $dealHistories;
    }
}
