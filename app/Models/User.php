<?php

namespace App\Models;

use App\Notifications\DefaultNotification;
use App\Notifications\WithdrawalConfirmationCodeNotification;
use Storage;
use Carbon\Carbon;
use App\Scopes\ScopeID;
use Illuminate\Auth\Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Auth\Passwords\CanResetPassword;
use App\Notifications\ResetPasswordNotification;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, AuthorizableContract, CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword, Notifiable;

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected static function boot()
    {
        static::addGlobalScope(new ScopeID);

        parent::boot();
    }

    public function getRecommendedAttribute()
    {
        return $this->achievements->where('id', 3)->count() == 1;
    }

    public function getCommissionAttribute()
    {
        if (is_null($this->referrer_id) || $this->created_at->addDays(7)->lt(Carbon::now()) || $this->change_commission == true) {
            return $this->commission_deal;
        }

        return 0;
    }

    public function getIsOnlineAttribute()
    {
        $sessions = $this->sessions();
        if ($sessions->count() != 0) {
            $last_activity = Carbon::createFromTimestamp($sessions->orderByDesc('last_activity')->first()->last_activity);
        } else {
            $last_activity = Carbon::yesterday();
        }
        return $last_activity->addMinutes(10)->gt(Carbon::now());
    }

    public function getLastActivityAttribute()
    {
        $sessions = $this->sessions();
        if ($sessions->count() != 0) {
            return Carbon::createFromTimestamp($sessions->orderByDesc('last_activity')->first()->last_activity);
        }
        if ($this->last_activity_timestamp == null) {
            return Carbon::yesterday();
        }
        return Carbon::createFromTimestamp($this->last_activity_timestamp);
    }

    public function getLastActivityFormatAttribute()
    {
        return $this->last_activity->format('d.m.Y H:i');
    }

    public function getImageAttribute()
    {
        if (Storage::exists('public/' . $this->id . '.png')) {
            return Storage::url('public/' . $this->id . '.png');
        }

        return '/images/content/user.png';
    }

    public function getReservedBalanceAttribute()
    {
        $balance = [
            'BTC' => '0.00000000',
            'LTC' => '0.00000000',
            'DASH' => '0.00000000',
        ];

        foreach ($this->deals as $deal) {
            if ($deal->seller->id == $this->id && $deal->status != 'cancelled' && $deal->status != 'completed') {
                $balance[$deal->crypto->short] = number_format($deal->sum_crypto + $balance[$deal->crypto->short] ?: 0, 8, '.', '');
            }
        }

        return $balance;
    }

    public function fetchBalance($method, array $wheres = [])
    {
        $crypto_currencies = CryptoCurrency::all();
        $balances = [];

        foreach ($crypto_currencies as $crypto_currency) {
            $wheres[] = ['crypto_id', $crypto_currency->id];

            $amount = $this->$method()->where($wheres)->sum('amount');

            $balances[$crypto_currency->short] = number_format($amount, 8, '.', '');

            array_pop($wheres);
        }

        return $balances;
    }

    public function getBalanceAttribute()
    {
        return $this->fetchBalance('operations');
    }

    public function getPartnerBalanceAttribute()
    {
        return $this->fetchBalance('operations_partner', [['transferred', false]]);
    }

    public function getRevertPartnerBalanceAttribute()
    {
        return $this->fetchBalance('operations_partner_revert');
    }

    public function scopeFilter($query, $filters = [])
    {
        if (!empty($filters['login'])) {
            $query->where('login', $filters['login']);
        }

        if (!empty($filters['email'])) {
            $query->where('email', $filters['email']);
        }
    }

    public function getLastActivationAttribute()
    {
        return $this->activations()
            ->orderBy('created_at', 'desc')
            ->first();
    }

    public function getActivationsCountAttribute()
    {
        return $this->activations()->count();
    }

    public function getLastActivationTimeAttribute()
    {
        $last_activation = $this->last_activation;

        if ($last_activation) {
            return strtotime($last_activation->created_at);
        }
        return null;
    }

    public function referrer()
    {
        return $this->belongsTo(User::class, 'referrer_id', 'id');
    }

    public function partners()
    {
        return $this->hasMany(User::class, 'referrer_id', 'id');
    }

    public function achievements()
    {
        return $this->belongsToMany(Achievement::class)->withPivot('created_at');
    }

    public function operations()
    {
        return $this->hasMany(Operation::class);
    }

    public function operations_partner()
    {
        return $this->hasMany(OperationPartner::class);
    }

    public function operations_partner_revert()
    {
        return $this->hasMany(OperationPartner::class, 'partner_id', 'id');
    }

    public function activations()
    {
        return $this->hasMany(UserActivation::class);
    }

    public function contacts()
    {
        return $this->hasMany(UserContact::class);
    }

    public function adverts()
    {
        return $this->hasMany(Advert::class);
    }

    public function deals()
    {
        return $this->hasMany(Deal::class)->orWhere('creator_id', $this->id);
    }

    public function partner_histories()
    {
        return $this->hasMany(PartnerHistory::class);
    }

    public function deal_histories()
    {
        return $this->hasMany(DealHistory::class);
    }

    public function addresses()
    {
        return $this->hasMany(Address::class);
    }

    public function sessions()
    {
        return $this->hasMany(Session::class);
    }

    public function notices()
    {
        return $this->hasMany(Notice::class);
    }

    public function verifications()
    {
        return $this->hasMany(Verification::class);
    }

    public function sendPasswordResetNotification($token)
    {
        $subject = 'Смена пароля';
        $this->notify(new ResetPasswordNotification($token, $this, $subject));
    }

    public function sendWithdrawalConfirmationCode($hash, $address, $crypto_short, $amount)
    {
        if (session()->get('withdrawal:' . $hash) != 'hi') {
            return false;
        }

        $confirmation_code = rand(100000, 999999);
        session()->put('withdrawal:' . $hash, $confirmation_code);
        $this->notify(new WithdrawalConfirmationCodeNotification($address, $confirmation_code, $crypto_short, $amount));

        return true;
    }

    public function sendNotification($subject, $content, $topic = null)
    {
        $this->notify(new DefaultNotification($topic, $subject, $content));
    }

    public function delete()
    {
        $this->activations()->delete();

        $this->achievements()->detach();

        $this->partners()->update(['referrer_id' => null]);

        return parent::delete();
    }

    public function getActiveAdverts()
    {
        $catalog = [];
        /** @var Advert $advert */
        foreach ($this->getEnumerator() as $advert) {
            $title = $advert->getTitle();
            $catalog[$title][] = $advert;
        }
        ksort($catalog);

        return $catalog;
    }

    private function getEnumerator()
    {
        return $this->adverts()
            ->where('status', 'active')
            ->get();
    }

    public function getHistoryDeals()
    {
        return Deal::orWhere([
            'creator_id' => $this->id,
            'user_id' => $this->id,
        ])->where(function ($query) {
            $query->orWhere('status', 'cancelled');
            $query->orWhere('status', 'completed');
        })->paginate(10);
    }

    public function getActiveDeals()
    {
        return Deal::orWhere([
            'creator_id' => $this->id,
            'user_id' => $this->id,
        ])->where(function ($query) {
            $query->orWhere('status', 'paid');
            $query->orWhere('status', 'pending');
        })->paginate(10);
    }

    /**
     * Change sum_crypto of all adverts if it's needed
     * @param CryptoCurrency $crypto_currency
     */
    public function keepUpAdverts(CryptoCurrency $crypto_currency)
    {
        foreach ($this->adverts as $advert) {
            if ($advert->short_type == 'sell' && $advert->keep_up == Advert::KEEP_UP && $this->balance[$crypto_currency->short] < $advert->sum_crypto) {
                $advert->sum_crypto = number_format($this->balance[$crypto_currency->short]-($this->balance[$crypto_currency->short]*($this->commission/100)), 8);
                $advert->save();
            }
        }
    }
}
