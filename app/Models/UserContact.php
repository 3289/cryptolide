<?php

namespace App\Models;

class UserContact extends Model
{
    public $timestamps = false;

    public function getNameAttribute()
    {
        return config('custom.contacts')[$this->contact_id];
    }

    public function scopeType($query, $type)
    {
        return $query->where('type', $type);
    }
}
