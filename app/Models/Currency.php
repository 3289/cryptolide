<?php

namespace App\Models;

use App\Scopes\ScopeName;

class Currency extends Model
{
    public $timestamps = false;

    protected static function boot()
    {
        static::addGlobalScope(new ScopeName);

        parent::boot();
    }

    public function country()
    {
        return $this->belongsTo(Country::class, 'id', 'currency_id');
    }

    public function delete()
    {
        if (!is_null($this->country)) {
            $this->country()->update([
                'currency_id' => null,
            ]);
        }

        return parent::delete();
    }
}
