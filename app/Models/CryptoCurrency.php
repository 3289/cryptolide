<?php

namespace App\Models;

use App\Scopes\ScopeName;

class CryptoCurrency extends Model
{
    public $timestamps = false;

    public function getCoefficientAttribute()
    {
        return 956;
    }

    public function getCommissionInputAttribute($value)
    {
        return number_format($value, 8, '.', '');
    }

    public function getCommissionOutputAttribute($value)
    {
        return number_format($value, 8, '.', '');
    }
}
