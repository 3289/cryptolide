<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotificationMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $subject, $topic, $messageBody;

    public function __construct($subject, $topic, $message)
    {
        $this->subject = $subject;
        $this->topic = $topic;
        $this->messageBody = $message;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->subject($this->subject);
        $this->with('messageBody', $this->messageBody);
        $this->with('topic', $this->topic);

        return $this->view('emails.notificationMail');
    }
}
