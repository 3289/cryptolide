<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RecoveryMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $url;
    protected $login;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($url, $login)
    {
        $this->url = $url;
        $this->login = $login;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Восстановление пароля')
            ->view('emails.recovery', ['url' => $this->url, 'login' => $this->login]);
    }
}
