<?php

namespace App\Providers;

use App\Models\Withdrawal;
use App\Observers\UserObserver;
use Cookie;
use App\Models\User;
use App\Models\Operation;
use App\Models\DealHistory;
use App\Observers\OperationObserver;
use App\Observers\WithdrawalObserver;
use Illuminate\Support\Facades\Blade;
use App\Observers\DealHistoryObserver;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::directive('title', function($expression) {
           return "<?php \$title=$expression; ?>";
        });

        Blade::directive('className', function($expression) {
           return "<?php \$className=$expression; ?>";
        });

        $referrer = null;

        if (request()->cookie('referrer') != null) {
            $referrer = User::find(request()->cookie('referrer')) ?? null;
        }

        if (request()->has('r')) {
            Cookie::queue(Cookie::make('referrer', request()->get('r'), 60));
            $referrer = User::find(request()->get('r')) ?? null;
        }

        view()->composer(['auth.register', 'welcome'], function($view) use ($referrer) {
            $view->with('referrer', $referrer);
        });

        DealHistory::observe(DealHistoryObserver::class);
        Operation::observe(OperationObserver::class);
        Withdrawal::observe(WithdrawalObserver::class);
        User::observe(UserObserver::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
