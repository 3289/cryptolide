<?php

namespace App\Providers;

use App\Models\Advert;
use App\Models\Deal;
use App\Models\Operation;
use Illuminate\Support\ServiceProvider;

class DealServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    public static function closeDeals()
    {
        $deals = Deal::active()
            ->get();
        foreach ($deals as $deal) {
            if ($deal->time_left < 0 && $deal->status == 'pending') {
                $deal->cancelDeal('service');
            }
        }
    }
}
