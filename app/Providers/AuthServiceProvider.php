<?php

namespace App\Providers;

use App\Models\Advert;
use App\Models\Deal;
use App\Policies\AdvertPolicy;
use App\Policies\DealPolicy;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Deal::class => DealPolicy::class,
        Advert::class => AdvertPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}
