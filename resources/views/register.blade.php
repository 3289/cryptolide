@extends('layouts.app')

@title('Регистрация')

@section('content')
	<div class="content">
		<div class="container">
			<div class="row">
				<form action="/register" method="POST" class="authorization-form authorization-form_register">
					{{ csrf_field() }}
					<h2 class="authorization-form__title">Добро пожаловать в CRYPTOLIDE!</h2>
					<div class="authorization-form__fieldset">
						@if ($referrer != null)
							<div class="user-encouragement authorization-form__user-encouragement">
								<div class="user-encouragement__user-photo"><img src="{{ $referrer->image }}" alt="Фото пользователя"></div>
								<div class="user-encouragement__message">Отлично! Пользователь <span class="user-encouragement__user-login">{{ $referrer->login }}</span> уже отключил вам все комиссии и
наградил вас статусом <b>«Партнёр»</b>. Теперь, в течение первых <b>7 дней</b> после регистрации, вы сможете абсолютно <b>без комиссий</b> покупать, продавать и обменивать неограниченное количество криптовалюты. Так же, благодаря партнёрскому статусу, в будущем вы сможете участвовать в конкурсах и получать дополнительные бонусы.
								</div>
							</div>
						@endif
						<div class="form-item{{ $errors->has('name') ? ' form-item_has-error' : '' }}">
							<input type="text" name="name" id="name" placeholder="Имя" class="form-text" value="{{ old('name') }}">
							@if ($errors->has('name'))
								<div class="form-error authorization-form__form-error">{{ $errors->first('name') }}</div>
							@endif
						</div>
						<div class="form-item{{ $errors->has('login') ? ' form-item_has-error' : '' }}">
							<input type="text" name="login" id="login" placeholder="Логин" class="form-text" value="{{ old('login') }}">
							@if ($errors->has('login'))
								<div class="form-error authorization-form__form-error">{{ $errors->first('login') }}</div>
							@endif
						</div>
						<div class="form-item{{ $errors->has('email') ? ' form-item_has-error' : '' }}">
							<input type="text" name="email" id="email" placeholder="E-mail" class="form-text" value="{{ old('email') }}">
							@if ($errors->has('email'))
								<div class="form-error authorization-form__form-error">{{ $errors->first('email') }}</div>
							@endif
						</div>
						<div class="form-item{{ $errors->has('password') ? ' form-item_has-error' : '' }}">
							<input type="password" name="password" id="password" placeholder="Придумайте пароль" class="form-text">
							@if ($errors->has('password'))
								<div class="form-error authorization-form__form-error">{{ $errors->first('password') }}</div>
							@endif
						</div>
						<div class="form-item">
							<input type="password" name="password_confirmation" id="password_confirmation" placeholder="Повторите пароль" class="form-text">
						</div>
						<div class="form-item{{ $errors->has('phone') ? ' form-item_has-error' : '' }}">
							<input type="text" name="phone" id="phone" placeholder="Ваш номер телефона" class="form-text" value="{{ old('phone') }}">
							@if ($errors->has('phone'))
								<div class="form-error authorization-form__form-error">{{ $errors->first('phone') }}</div>
							@endif
						</div>
					</div>
					<div class="authorization-form__agreements">
						<div class="form-item-checkbox">
							<input type="checkbox" name="agreement" id="agreement1" value="agreement1" class="form-checkbox">
							<label for="agreement1">Принимаю <a href="#">пользовательское соглашение</a></label>
						</div>
						<div class="form-error authorization-form__form-error" id="agreement1-error" style="display: none;">
							Вы должны согласиться с пользовательским соглашением.
						</div>
						<div class="form-item-checkbox">
							<input type="checkbox" name="agreement" id="agreement2" value="agreement2" class="form-checkbox">
							<label for="agreement2">I'm not a citizen of USA (Я не являюсь жителем США)</label>
						</div>
						<div class="form-error authorization-form__form-error" id="agreement2-error" style="display: none;">
							Вы не поставили галочку.
						</div>
					</div>
					<div class="authorization-form__captcha">
						{!! app('captcha')->display(); !!}
						@if ($errors->has('g-recaptcha-response'))
							<div class="form-error authorization-form__form-error">
								{{ $errors->first('g-recaptcha-response') }}
							</div>
						@endif
					</div>
					<div class="authorization-form__actions">
						<button type="submit" class="form-submit">Зарегистрироваться</button>
					</div>
					<div class="authorization-form__login">
						<a href="/login" class="authorization-form__login-link">Уже зарегистрированы?</a>
					</div>
				</form>
			</div>
		</div>
	</div>
@endsection

@section('scripts')
	<script>
		$(function() {
			$('form').on('submit', function(event) {
                $('#agreement1-error').css('display', 'none');
                $('#agreement2-error').css('display', 'none');

			    if (!$('#agreement1').prop('checked')) {
			        $('#agreement1-error').css('display', '');

			        event.preventDefault();
				}

				if (!$('#agreement2').prop('checked')) {
                    $('#agreement2-error').css('display', '');

                    event.preventDefault();
				}
			});

			$('input#phone').on('click', function(event) {
        $(this).parent().addClass('phone-focus phone-focus_register');
      });
		});
	</script>
@endsection