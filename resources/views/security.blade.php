@extends('layouts.app')

@title('Безопасность')

@className('main_infopage')

@section('content')
	<div class="content">
		<div class="infopage">
			<div class="infopage__title-wrap">
				<div class="container">
					<div class="row">
						<div class="col-xs-12">
							<h1 class="infopage-title">Безопасность</h1>
						</div>
					</div>
				</div>
			</div>
			<div class="container">
				<div class="row">
					<div class="infopage__content">
						<div class="infopage-block infopage-block_security corner-bottom">
							<h2 class="infopage-block__title">Безопасность ваших аккаунтов и средств - основа и фундамент нашей компании, на котором построены все остальные функции.</h2>
							<div class="infopage-block__text">
								<p>Мы пониманием, что максимальная защита средств и аккаунтов - это первоочередная и наиболее важная задача для <br>любой уважающей себя компании. Именно поэтому, вопрос безопасности всегда будет для нас в приоритете.</p>
							</div>
						</div>
						<div class="infopage-block infopage-block_protection-funds corner-top corner-bottom">
							<h2 class="infopage-block__title">Для защиты ваших средств используется:</h2>
							<div class="infopage-block__text">
								<div class="protection-funds">
									<div class="protection-funds__item clearfix">
										<div class="protection-funds__left">
											<div class="protection-funds__img"><img src="/images/content/protection_funds1.png" alt=""></div>
										</div>
										<div class="protection-funds__right">
											<h3 class="protection-funds__name">Защита при выводе</h3>
											<div class="protection-funds__description">
												<p>Система безопасности отслеживает подозрительную активность и любые закономерности. Функция подтверждения вывода средств защищена и устойчива к вредоносным модулям в браузере, а белый список адресов предотвратит вывод средств на сторонние ресурсы.</p>
												<p>Так же мы используем еще несколько алгоритмов защиты, информацию о которых, в целях безопасности, раскрывать не будем.</p>
											</div>
										</div>
									</div>
									<div class="protection-funds__item clearfix">
										<div class="protection-funds__left">
											<div class="protection-funds__img"><img src="/images/content/protection_funds2.png" alt=""></div>
										</div>
										<div class="protection-funds__right">
											<h3 class="protection-funds__name">Холодное хранение</h3>
											<div class="protection-funds__description">
												<p>Большинство денежных средств в сервисе хранится в автономных «холодных» (или как их еще называют «офлайн») кошельках. Только около 0,5% - 5% от всего объема криптовалют находятся в «горячих» кошельках для регулярных ежедневных операций. В целях безопасности и предотвращения любых угроз «холодные» кошельки недоступны из самой платформы и серверов, на которых она расположена. Ручной доступ к денежным средствам в «офлайн» кошельках имеют только несколько главных администраторов.</p>
											</div>
										</div>
									</div>
									<div class="protection-funds__item clearfix">
										<div class="protection-funds__left">
											<div class="protection-funds__img"><img src="/images/content/protection_funds3.png" alt=""></div>
										</div>
										<div class="protection-funds__right">
											<h3 class="protection-funds__name">Защита систем и серверов</h3>
											<div class="protection-funds__description">
												<p>В защите наших серверов используется только самое современное и актуальное программное обеспечение, которое подкреплено наиболее эффективными методами в компьютерной безопасности. В обязательном порядке, несколько раз в день, создается резервная копия наиболее важных полей в базе данных сервиса, после чего она вручную перемещается в архив на «холодное» хранение.</p>
												<p>Так же мы используем еще несколько алгоритмов защиты, информацию о которых, в целях безопасности, раскрывать не будем.</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="infopage-block infopage-block_account-protection corner-top">
							<h2 class="infopage-block__title">Для защиты ваших аккаунтов используется:</h2>
							<div class="infopage-block__text">
								<div class="account-protection">
									<div class="account-protection__item clearfix">
										<div class="account-protection__left">
											<div class="account-protection__img"><img src="/images/content/account_protection1.png" alt=""></div>
										</div>
										<div class="account-protection__right">
											<h3 class="account-protection__name">Двухэтапная аутентификация и secure ip</h3>
											<div class="account-protection__description">
												<p>Двухэтапная аутентификация обеспечивает дополнительную защиту ваших данных. Даже если мошенник украдет ваш пароль, он не сможет войти в аккаунт, поскольку система потребует ввести специальный код, который будет отправлен на ваш регистрационный E-mail. Система «secure ip» отслеживает закономерности в сессиях авторизации и блокирует доступ с требованием ввести код подтверждения. Без ввода кода подтверждения доступ в аккаунт получить не удастся.</p>
											</div>
										</div>
									</div>
									<div class="account-protection__item clearfix">
										<div class="account-protection__left">
											<div class="account-protection__img"><img src="/images/content/account_protection2.png" alt=""></div>
										</div>
										<div class="account-protection__right">
											<h3 class="account-protection__name">Двухфакторная аутентификация (2FA)</h3>
											<div class="account-protection__description">
												<p>Двухфакторная аутентификация – второй уровень безопасности, который защитит ваш аккаунт даже тогда, когда злоумышленники узнают все ваши данные для авторизации на сервис CRYPTOLIDE и получат доступ к вашему E-mail, который был использован при регистрации. Войти в ваш аккаунт не получится без подтверждения из вашего устройства, например, мобильного телефона. Двухфакторную аутентификацию можно настроить с помощью бесплатного приложения Google Authenticator для популярных мобильных ОС.</p>
											</div>
										</div>
									</div>
									<div class="account-protection__item clearfix">
										<div class="account-protection__left">
											<div class="account-protection__img"><img src="/images/content/account_protection3.png" alt=""></div>
										</div>
										<div class="account-protection__right">
											<h3 class="account-protection__name">Система PIN кодов</h3>
											<div class="account-protection__description">
												<p>Каждый пользователь может дополнительно улучшить защиту аккаунта с помощью генерации уникального PIN кода, который состоит из 6 символов. Его сразу же необходимо переписать на обычный лист бумаги и сохранить в надежном месте, ведь на сайте, в целях безопасности, он никогда указываться не будет, а процедура его восстановления требует прохождения верификации. Это значит, что даже если злоумышленник получит доступ к вашему почтовому ящику и мобильному телефону, у него все равно не получится вывести денежные средства, так как PIN код никогда не отправляется на почту и его невозможно узнать.</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection