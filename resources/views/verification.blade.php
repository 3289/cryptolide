@extends('layouts.app')

@title('Верификация')

@className('main_verification')

@section('content')
<div class="content">
  <div class="title-wrap title-wrap_verification">
    <div class="container">
      <div class="row">
        <h1 class="page-title title-wrap__title">Мои настройки</h1>
        <a href="/" class="btn-back title-wrap__back">назад</a>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <ul class="settings-tabs">
          <li class="settings-tabs__item"><a href="/general-information" class="settings-tabs__link">ОБЩАЯ ИНФОРМАЦИЯ</a></li>
          <li class="settings-tabs__item"><a href="/my-security" class="settings-tabs__link">БЕЗОПАСНОСТЬ</a></li>
          <li class="settings-tabs__item settings-tabs__item_active"><a href="/verification" class="settings-tabs__link">ВЕРИФИКАЦИЯ</a></li>
        </ul>
        <div class="verification-note">Верификация необходима для повышения доверия к вашему аккаунту среди пользователей CRYPTOLIDE, а так же для участия в особых сделках, которые доступны только верифицированным пользователям. Процедура прохождения верификации полностью бесплатная.</div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <div class="verification-step">
          <div class="verification-step__header">
            <h2 class="verification-step__title">ШАГ 1: Верификация контактного телефона</h2>
            <div class="verification-step__status">Иконка успешной верификации:</div>
          </div>
          <div class="verification-step__content">
            <div class="verification-step__item verification-step__item_contact-phone">
              <div class="verification-step__item-label">Контактный телефон:</div>
              <div class="verification-step__item-value">
                +{{ substr_replace(auth()->user()->phone, '****', -4) }}
                <span class="verification-step__note">Для указания или изменения телефона перейдите во вкладку «Безопасность»</span>
              </div>
            </div>
            <div class="verification-step__item verification-step__item_verification-method">
              <div class="verification-step__item-label">Способ верификации:</div>
              <div class="verification-step__item-value">
                <select name="verification-method" id="verification-method" class="verification-step__select verification-step__select_verification-method jqstyler">
                  <option value="1">С помощью Viber или WhatsApp</option>
                  <option value="2">С помощью СМС сообщения</option>
                </select>
                <div class="verification-step__instruction">
                  <p>Зайдите под вышеуказанным контактным номером в Viber или WhatsApp и напишите нашему роботу на номер: <b>+37281736450</b> ваш персональный временный код безопасности: <a class="generate-btn" href="">сгенерировать код</a>. После генерации кода вы должны написать сообщение роботу не позже чем через 15 минут. Если вы не успеете - код нужно будет сгенерировать еще раз.</p>
                  <p>В течение нескольких часов система автоматически и в порядке очереди обработает ваш запрос.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="verification-step verification-step_fio">
          <div class="verification-step__header">
            <h2 class="verification-step__title">ШАГ 2: Верификация имени, фамилии и отчества</h2>
            <div class="verification-step__status">Иконка успешной верификации:</div>
          </div>
          <div class="verification-step__content">
            <div class="verification-step__item verification-step__item_fio">
              <div class="verification-step__item-label">Имя:</div>
              <div class="verification-step__item-value">
                {{ auth()->user()->name }}
                <span class="verification-step__note">Для указания или изменения ФИО перейдите во вкладку «Общая информация»</span></div>
              </div>
              <div class="verification-step__item verification-step__item_fio">
                <div class="verification-step__item-label">Фамилия:</div>
                <div class="verification-step__item-value">{{ auth()->user()->last_name ?? 'не указано' }}</div>
              </div>
              <div class="verification-step__item verification-step__item_fio">
                <div class="verification-step__item-label">Отчество:</div>
                <div class="verification-step__item-value">{{ auth()->user()->middle_name ?? 'не указано' }}</div>
              </div>
              <div class="verification-step__item verification-step__item_images">
                <div class="verification-step__item-label">Подготовьте 4 типа изображений:</div>
                <div class="verification-step__item-value">
                  <ul class="conditions-list">
                    <li class="conditions-list__item">1. Первую страницу паспорта или ту сторону водительского удостоверения, на которой видно ваше ФИО
                    и идентификационный номер документа.</li>
                    <li class="conditions-list__item">2. Страницу удостоверения, на которой видно, кем и когда выдан документ.</li>
                    <li class="conditions-list__item">3. Страницу паспорта, на которой видно вашу последнюю прописку.</li>
                    <li class="conditions-list__item">4. Фото, на котором вы в руке возле лица держите документ (паспорт или водительское удостоверение).
                    На документе должно быть четко видно ваше фото и ваше ФИО.</li>
                  </ul>
                </div>
              </div>
              <div class="verification-step__explanatory-text verification-step__explanatory-text_images">Перечисленные выше изображения отправьте на E-mail: <a href="mailto:admin@cryptolide.com">admin@cryptolide.com</a> с темой письма: <a href="#">VID{{auth()->user()->id}}</a></div>
              <div class="verification-step__item verification-step__item_conditions">
                <div class="verification-step__item-label">Обязательные условия:</div>
                <div class="verification-step__item-value">
                  <ul class="conditions-list">
                    <li class="conditions-list__item">1. На документах не должно быть скрытых, зарисованных или поврежденных областей, которые будут
                    затруднять проверку. При фотографировании вы не пользовались вспышкой и на фото нет бликов.</li>
                    <li class="conditions-list__item">2. Документы должны быть в полный размер и без обрезаний (на них должны быть видны все
                    четыре уголка вашего удостоверения личности).</li>
                    <li class="conditions-list__item">3. Ваше удостоверение сфотографировано в прямом ракурсе, искаженные изображения не
                    обрабатываются. Фотография обзяательно должна быть цветной и четкой.</li>
                  </ul>
                </div>
              </div>
              <div class="verification-step__explanatory-text verification-step__explanatory-text_conditions">Проверка документов, а так же присвоение вам соответствующего статуса и иконки произойдет в течение 72 часов с момента отправки письма на наш адрес. Верификация ФИО может быть произведена только при наличии верифицированного телефона.</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  @endsection

  @section('scripts')
  <script>
    function setDefaultText() {
      if ($('.jqstyler').find('.jq-selectbox__dropdown ul li.selected').index() == 0) {
        $('.verification-step__instruction').html('<p>Зайдите под вышеуказанным контактным номером в Viber или WhatsApp и напишите нашему роботу на номер: <b>+37281736450</b> ваш персональный временный код безопасности: <a class="generate-btn" href="">сгенерировать код</a>. После генерации кода вы должны написать сообщение нашему роботу не позже чем через 15 минут. Если вы не успеете - код нужно будет сгенерировать еще раз.</p><p>В течение нескольких часов система автоматически и в порядке очереди обработает ваш запрос.</p>');
      } else {
        $('.verification-step__instruction').html('<p>Отправьте с вышеуказанного личного номера СМС нашему роботу на номер: <b>+37281736450</b> с вашим персональным временный кодом безопасности: <a class="generate-btn" href="">сгенерировать код</a>. После генерации кода вы должны отправить СМС сообщение не позже чем через 15 минут. Если вы не успеете - код нужно будет сгенерировать еще раз. Стоимость СМС зависит от тарифа вашего мобильного оператора. Дополнительные сборы не взимаются.</p><p>В течение нескольких часов система автоматически и в порядке очереди обработает ваш запрос.</p>');
      }
    }

    $('.jqstyler').styler({
      onFormStyled: function () {
        $('.jq-selectbox').each(function (index, el) {
          var bgImg = $(this).find('.jq-selectbox__dropdown ul li.selected').css('background-image');
          $(this).find('.jq-selectbox__select-text').css('background-image', bgImg);
        });
      },
      onSelectClosed: function () {
        var bgImg = $(this).find('.jq-selectbox__dropdown ul li.selected').css('background-image');
        $(this).find('.jq-selectbox__select-text').css('background-image', bgImg);
        setDefaultText();
      }
    });

    $(".verification-step__instruction").on("click", 'a.generate-btn', function (event) {
      console.log('Bind!');
      console.log($('#verification-method').val());
      event.preventDefault(true);
      $.post({
        url: "/verification",
        data: {
          _token: "{{ csrf_token() }}",
          type: $('#verification-method').val()
        },
        success: function (data) {
          if ($('.jqstyler').find('.jq-selectbox__dropdown ul li.selected').index() == 0) {
            $('.verification-step__instruction').html('<p>Зайдите под вышеуказанным контактным номером в Viber или WhatsApp и напишите нашему роботу на номер: <b>+37281736450</b> ваш персональный временный код безопасности: <b>'+data.code+'</b>. После генерации кода вы должны написать сообщение нашему роботу не позже чем через 15 минут. Если вы не успеете - код нужно будет сгенерировать еще раз.</p><p>В течение нескольких часов система автоматически и в порядке очереди обработает ваш запрос.</p>');
          } else {
            $('.verification-step__instruction').html('<p>Отправьте с вышеуказанного личного номера СМС нашему роботу на номер: <b>+37281736450</b> с вашим персональным временный кодом безопасности: <b>'+data.code+'</b>. После генерации кода вы должны отправить СМС сообщение не позже чем через 15 минут. Если вы не успеете - код нужно будет сгенерировать еще раз. Стоимость СМС зависит от тарифа вашего мобильного оператора. Дополнительные сборы не взимаются.</p><p>В течение нескольких часов система автоматически и в порядке очереди обработает ваш запрос.</p>');
          }
          setTimeout(setDefaultText, 900000);
        }
      });
    });
  </script>
  @endsection