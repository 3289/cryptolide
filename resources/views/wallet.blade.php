@extends('layouts.app')

@title('Защищённый криптовалютный кошелёк')

@className('main_infopage')

@section('content')
	<div class="content">
		<div class="infopage infopage_wallet">
			<div class="infopage__title-wrap">
				<div class="container">
					<div class="row">
						<div class="col-xs-12">
							<h1 class="infopage-title infopage-title_wallet"><span class="infopage-title__label">CRYPTOLIDE</span> Wallet - Удобный и надёжный криптовалютный кошелёк</h1>
							<div class="labels labels_wallet">
								<div class="labels__item labels__item_users">Любят пользователи.</div>
								<div class="labels__item labels__item_traders">Одобряют трейдеры.</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="container">
				<div class="row">
					<div class="infopage__content">
						<div class="infopage-block infopage-block_brief corner-bottom">
							<h2 class="infopage-block__title">Если в двух словах</h2>
							<div class="infopage-block__text">
								<p>Криптолайд кошелёк создан, чтобы обеспечить надёжное хранение ваших криптовалютных активов и высокую скорость обработки транзакций, а устроен как уютный и привычный веб-сервис, где все свои. Мы верим, что помимо сложной технической реализации и высоких параметров безопасности, в кошельке должна быть своя атмосфера. В Криптолайде ей уделено особое внимание. Наши специалисты работают для вас круглосуточно и без выходных и всегда приветливо дадут оперативные и конкретные ответы на ваши вопросы. С нами удобно, комфортно и безопасно!</p>
							</div>
							<div class="buttons">
								<a href="/my-wallet" class="buttons__btn btn-green3" target="_blank">Открыть кошелёк</a>
								<span class="buttons__sep">или</span>
								<a href="#" class="buttons__btn btn-blue2" onclick="Chatra('openChat', true); return false;">Задать вопрос о кошельке</a>
							</div>
						</div>
						<div class="infopage-block infopage-block_purse-advantages corner-top">
							<h2 class="infopage-title infopage-title_wallet infopage-title_purse-advantages">Криптовалютный кошелёк <span class="infopage-title__label">CRYPTOLIDE</span> лучше других, потому что он:</h2>
							<div class="infopage-block__text">
								<div class="purse-advantages purse-advantages_wallet">
									<div class="purse-advantage">
										<div class="purse-advantage__left">
											<h3 class="purse-advantage__title">БЕЗОПАСНЫЙ И ГИБКИЙ</h3>
											<div class="purse-advantage__description">
												<p>Используются 2FA, Secure IP, Pin codes, Login Guard, Instant transfer to CS и другие методы защиты.</p>
												<p>Мы пониманием, что максимальная защита средств и аккаунтов - это первоочередная и наиболее важная задача для любой уважающей себя компании. Именно поэтому, вопрос безопасности всегда будет для нас в приоритете. Более подробно о безопасности в сервисе CRYPTOLIDE вы можете прочесть <a href="/security">здесь.</a></p>
											</div>
										</div>
										<div class="purse-advantage__right">
											<div class="purse-advantage__img">
												<img src="/images/content/security.png" alt="">
											</div>
										</div>
									</div>
									<div class="purse-advantage">
										<div class="purse-advantage__left">
											<div class="purse-advantage__img">
												<img src="/images/content/free.png" alt="">
											</div>
										</div>
										<div class="purse-advantage__right">
											<h3 class="purse-advantage__title">БЕСПЛАТНЫЙ И МГНОВЕННЫЙ</h3>
											<div class="purse-advantage__description">
												<p>Криптолайд кошелёк всегда был и будет бесплатным абсолютно для каждого пользователя. Никаких дополнительных платежей и взносов, кроме оплаты стандартных <a href="/commissions">комиссий</a> сети за перевод криптовалюты c отображением в Blockchain, с вас никогда взиматься не будет. А ещё, все переводы между пользователями Криптолайд кошелька полностью мгновенны и совершенно бесплатны.</p>
											</div>
										</div>
									</div>
									<div class="purse-advantage">
										<div class="purse-advantage__left">
											<h3 class="purse-advantage__title">УДОБНЫЙ И ПРИБЫЛЬНЫЙ</h3>
											<div class="purse-advantage__description">
												<p>Забудьте о постоянных и невыгодных конвертациях валют, посредниках и обменниках с их длительной обработкой транзакций и высокой комиссией за каждый перевод. Благодаря интеграции Криптолайд кошелька в Торговую платформу, вы в любой момент сможете пополнить баланс или вывести средства с помощью наиболее удобной для вас платёжной системы, онлайн банка или наличных денег. Все переводы осуществляются напрямую между такими же пользователями, как и вы, и поступают сразу на ваши реквизиты, минуя пополнения баланса! А ещё, вы можете получать пассивную <a href="/affiliate-program">прибыль.</a></p>
											</div>
										</div>
										<div class="purse-advantage__right">
											<div class="purse-advantage__img">
												<img src="/images/content/profitable.png" alt="">
											</div>
										</div>
									</div>
								</div>
								<div class="open-wallet">
									<h3 class="open-wallet__title">Всего 30 секунд и кошелёк В вашем распоряжении!</h3>
									<div class="open-wallet__description">Попробуйте прямо сейчас! Это бесплатно и без обязательств.</div>
									<div class="buttons">
										<a href="/my-wallet" class="buttons__btn btn-green3" target="_blank">Открыть кошелёк</a>
										<span class="buttons__sep">или</span>
										<a href="#" class="buttons__btn btn-blue2" onclick="Chatra('openChat', true); return false;">Задать вопрос о кошельке</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('scripts')
@endsection