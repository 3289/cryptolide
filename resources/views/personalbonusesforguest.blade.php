@extends('layouts.app')

@title('Персональные бонусы')

@className('main_infopage')

@section('content')
	<div class="content">
		<div class="infopage infopage_bonuses">
			<div class="infopage__title-wrap">
				<div class="container">
					<div class="row">
						<div class="col-xs-12">
							<h1 class="infopage-title">Персональные бонусы и предложения</h1>
						</div>
					</div>
				</div>
			</div>
			<div class="container">
				<div class="row">
					<div class="infopage__content">
						<div class="infopage-block infopage-block_brief corner-bottom">
							<h2 class="infopage-block__title">Дарим приятные бонусы. Просто так!</h2>
							<div class="infopage-block__text">
								<p>Сервис CRYPTOLIDE очень любит своих пользователей и постоянно стремится к тому, что бы и пользователи чувствовали себя незаменимой частичкой нашей большой команды. Именно поэтому был разработан специальный алгоритм, который анализирует действия каждого участника сервиса и на основе полученных данных формирует и совершенно бесплатно предоставляет как раз те бонусы и акционные предложения, которые действительно будут интересны конкретному пользователю. Например, для активных трейдеров сервис предлагает дополнительное отключение комиссии, расширенный функционал, индивидуальные условия отображения объявлений и множество других инструментов, которые значительно улучшат торговлю и взаимодействие с другими участниками сервиса. Для пользователей, которые занимаются привлечением партнёров в команду, сервис дарит временное увеличение партнёрской ставки, сертификаты на зачисление в команду тех пользователей, которые были зарегистрированы без пригласителя, создание уникальных лендингов и других рекламных материалов, а так же массу других дополнительных возможностей, которые отлично помогают совершить задуманное. Мы понимаем, что каждый пользователь - уникален и поэтому стараемся создать такие условия взаимодействия с сервисом, при которых каждый будет чувствовать себя особенным! <br><br>Помните, что именно в CRYPTOLIDE вы всегда получаете больше!
								</p>
							</div>							
						</div>
						<div class="infopage-block infopage-block_bonuses corner-top">
							<div class="buttons">
								<a href="/mybonuses" class="btn-green3 buttons__btn buttons__btn_view-bonuses" target="_blank">Посмотреть мои бонусы</a>
								<span class="buttons__sep">или</span>
								<a href="#" class="buttons__btn btn-blue2" onclick="Chatra('openChat', true); return false;">Задать вопрос оператору</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('scripts')
@endsection