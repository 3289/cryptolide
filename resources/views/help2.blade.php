@extends('layouts.app')

@title('Помощь')

@className('main_infopage')

@section('content')
	<div class="content">
		<div class="infopage infopage_help">
			<div class="infopage__title-wrap">
				<div class="container">
					<div class="row">
						<div class="col-xs-12">
							<h1 class="infopage-title">Помощь</h1>
							<div class="infopage__note">Наиболее часто задаваемые вопросы и ответы на них</div>
						</div>
					</div>
				</div>
			</div>
			<div class="container">
				<div class="row">
					<div class="infopage__content">
						<div class="help-block">
							<div class="help-block__left">
								<div class="category-help">
									<div class="category-help__heading infopage-block corner-bottom">
										<h2 class="category-help__title">Выберите категорию</h2>
									</div>
									<div class="category-help__content infopage-block corner-top">
										<ul class="category-help__list">
											<li class="category-help__item category-help__item_active"><a href="#" class="category-help__link">Общие вопросы</a></li>
											<li class="category-help__item"><a href="#" class="category-help__link">Торговая платформа</a></li>
											<li class="category-help__item"><a href="#" class="category-help__link">Биржа</a></li>
											<li class="category-help__item"><a href="#" class="category-help__link">Партнёрская программа</a></li>
											<li class="category-help__item"><a href="#" class="category-help__link">Удаленная работа</a></li>
										</ul>
									</div>
									<div class="category-help__bottom">Не нашли ответ? <a href="/contacts">Напишите</a> нам!</div>
								</div>
							</div>
							<div class="help-block__right">
								<div class="faq">
									<div class="faq__heading infopage-block corner-bottom">
										<h2 class="faq__title">Вопросы из раздела «Общие вопросы» и ответы на них</h2>
									</div>
									<div class="faq__item infopage-block corner-top">
										<h3 class="faq__question">Что такое BitCoin и криптовалюта? Зачем и как именно всем этим пользоваться?</h3>
										<div class="faq__answer">
											<p>Криптовалюта – это цифровая (виртуальная) валюта, единица которой – монета (англ. -coin). Монета защищена от подделки, т.к. представляет собой зашифрованную информацию, скопировать которую невозможно (пользование криптографии и определило приставку «крипто» в названии). А чем тогда электронная криптовалюта отличается от обычных денег в электронном виде? Для того, чтобы обычные деньги появились на счету в электронном виде, они должны быть сначала внесены</p>
										</div>
									</div>
									<div class="faq__item infopage-block">
										<h3 class="faq__question">Что такое BitCoin и криптовалюта? Зачем и как именно ими пользоваться?</h3>
										<div class="faq__answer">
											<p>Криптовалюта – это цифровая (виртуальная) валюта, единица которой – монета (англ. -coin). Монета защищена от подделки, т.к. представляет собой зашифрованную информацию, скопировать которую невозможно (пользование криптографии и определило приставку «крипто» в названии). А чем тогда электронная криптовалюта отличается от обычных денег в электронном виде? Для того, чтобы обычные деньги появились на счету в электронном виде, они должны быть сначала внесены</p>
										</div>
									</div>
									<div class="faq__item infopage-block">
										<h3 class="faq__question">Что такое BitCoin и криптовалюта? Зачем и как именно ими пользоваться?</h3>
										<div class="faq__answer">
											<p>Криптовалюта – это цифровая (виртуальная) валюта, единица которой – монета (англ. -coin). Монета защищена от подделки, т.к. представляет собой зашифрованную информацию, скопировать которую невозможно (пользование криптографии и определило приставку «крипто» в названии). А чем тогда электронная криптовалюта отличается от обычных денег в электронном виде? Для того, чтобы обычные деньги появились на счету в электронном виде, они должны быть сначала внесены</p>
										</div>
									</div>
									<div class="faq__item infopage-block">
										<h3 class="faq__question">Что такое BitCoin и криптовалюта? Зачем и как именно ими пользоваться?</h3>
										<div class="faq__answer">
											<p>Криптовалюта – это цифровая (виртуальная) валюта, единица которой – монета (англ. -coin). Монета защищена от подделки, т.к. представляет собой зашифрованную информацию, скопировать которую невозможно (пользование криптографии и определило приставку «крипто» в названии). А чем тогда электронная криптовалюта отличается от обычных денег в электронном виде? Для того, чтобы обычные деньги появились на счету в электронном виде, они должны быть сначала внесены</p>
										</div>
									</div>
									<div class="faq__item infopage-block">
										<h3 class="faq__question">Что такое BitCoin и криптовалюта? Зачем и как именно ими пользоваться?</h3>
										<div class="faq__answer">
											<p>Криптовалюта – это цифровая (виртуальная) валюта, единица которой – монета (англ. -coin). Монета защищена от подделки, т.к. представляет собой зашифрованную информацию, скопировать которую невозможно (пользование криптографии и определило приставку «крипто» в названии). А чем тогда электронная криптовалюта отличается от обычных денег в электронном виде? Для того, чтобы обычные деньги появились на счету в электронном виде, они должны быть сначала внесены</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection