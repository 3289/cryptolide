@extends('layouts.app')

@title('Редактирование объявления')

@className('main_create-ad')

@section('content')
    <div class="content">
        <div class="title-wrap title-wrap_create-ad">
            <div class="container">
                <div class="row">
                    <h1 class="page-title title-wrap__title">Редактирование объявления</h1>
                    <a href="/" class="btn-back title-wrap__back">назад</a>
                </div>
            </div>
        </div>
        <div class="form-create-ad-wrapper">
            <form action="" method="post" class="form-create-ad" novalidate>
                {{ csrf_field() }}
                {{ method_field('PATCH') }}
                <input type="hidden" name="id" value="{{ $advert->id ?? '' }}">
                <input type="hidden" name="currency_id" value="{{ $advert->currency_id ?? '' }}">
                <input type="hidden" name="bank_ids" value="{{ $advert->bank_ids ?? '' }}">
                <input type="hidden" name="country_id" value="{{ $advert->country_id ?? '' }}">
                <input type="hidden" name="city_id" value="{{ $advert->city_id ?? '' }}">
                <input type="hidden" name="payment_type_id" value="{{ $advert->payment_type_id ?? '' }}">

                <div class="form-create-ad__item">
                    <label for="operation-type" class="form-create-ad__label">Я хочу:</label>
                    <div class="form-create-ad__control-wrap">
                        <input type="hidden" name="type" value="{{ $advert->type }}">
                        @if ($advert->type == 'buy_online')
                            <img src="{{ asset('/images/icons/icon_buy_crypto_online.png') }}">
                            <span class="form-create-ad__control-suffix">Купить криптовалюту онлайн</span>
                        @elseif ($advert->type == 'buy_for_cash')
                            <img src="{{ asset('/images/icons/icon_buy_crypto_cash.png') }}">
                            <span class="form-create-ad__control-suffix">Купить криптовалюту за наличные</span>
                        @elseif ($advert->type == 'sell_online')
                            <img src="{{ asset('/images/icons/icon_sell_crypto_online.png') }}">
                            <span class="form-create-ad__control-suffix">Продать криптовалюту онлайн</span>
                        @elseif ($advert->type == 'sell_for_cash')
                            <img src="{{ asset('/images/icons/icon_sell_crypto_cash.png') }}">
                            <span class="form-create-ad__control-suffix">Продать криптовалюту за наличные</span>
                        @elseif ($advert->type == 'trade')
                            <img src="{{ asset('/images/icons/icon_exchange_crypto.png') }}">
                            <span class="form-create-ad__control-suffix">Обменять криптовалюту на другую</span>
                        @endif
                        {{--<select name="type" id="operation-type" onchange="changeTypeLabel(this); cleaner()"--}}
                                {{--class="form-create-ad__select form-create-ad__select_operation-type jqstyler">--}}
                            {{--<option value="buy_online"{{ $advert->type == 'buy_online' ? ' selected' : '' }}>Купить криптовалюту онлайн</option>--}}
                            {{--<option value="buy_for_cash"{{ $advert->type == 'buy_for_cash' ? ' selected' : '' }}>Купить криптовалюту за наличные</option>--}}
                            {{--<option value="sell_online"{{ $advert->type == 'sell_online' ? ' selected' : '' }}>Продать криптовалюту онлайн</option>--}}
                            {{--<option value="sell_for_cash"{{ $advert->type == 'sell_for_cash' ? ' selected' : '' }}>Продать криптовалюту за наличные</option>--}}
                            {{--<option value="trade"{{ $advert->type == 'trade' ? ' selected' : '' }}>Обменять криптовалюту на другую</option>--}}
                        {{--</select>--}}
                    </div>
                </div>
                <div class="form-create-ad__item">
                    <label id="type-label" for="crypto-type" class="form-create-ad__label">Тип криптовалюты:</label>
                    <div class="form-create-ad__control-wrap">
                        <input type="hidden" name="crypto_id" value="1">
                        <img src="{{ asset('/images/icons/icon_bitcoin_create_ad.png') }}">
                        <span class="form-create-ad__control-suffix">Bitcoin</span>
                        {{--<select name="crypto_id" class="form-create-ad__select form-create-ad__select_crypto-type jqstyler">--}}
                            {{--<option data-short="BTC" value="1" selected>--}}
                                {{--Bitcoin--}}
                            {{--</option>--}}
                            {{--@foreach($cryptoCurrencies as $cryptoCurrency)--}}
                                {{--<option data-short="{{ $cryptoCurrency->short }}" value="{{ $cryptoCurrency->id }}"{{ $advert->crypto_id == $cryptoCurrency->id ? ' selected' : '' }}>--}}
                                    {{--{{ $cryptoCurrency->name }}--}}
                                {{--</option>--}}
                            {{--@endforeach--}}
                        {{--</select>--}}
                    </div>
                </div>
                <div class="form-create-ad__item" id="crypto-type-purchase" style="display: none;">
                    <label for="crypto-type" class="form-create-ad__label">Получаете криптовалюту:</label>
                    <div class="form-create-ad__control-wrap">
                        <select class="form-create-ad__select form-create-ad__select_crypto-type jqstyler" name="crypto_trade_id">
                            @foreach($additionalCryptoCurrencies as $additionalCryptoCurrency)
                                <option data-short="{{ $additionalCryptoCurrency->short }}" value="{{ $additionalCryptoCurrency->id }}"{{ $advert->crypto_trade_id == $additionalCryptoCurrency->id ? ' selected' : '' }}>{{ $additionalCryptoCurrency->name }} {{ $additionalCryptoCurrency->short }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-create-ad__item" id="country-form" style="display: none">
                    <label for="your-country" class="form-create-ad__label">Ваша страна:</label>
                    <div class="form-create-ad__control-wrap">
                        <div class="select-wrapper">
                            <div class="select-wrapper__input-block">
                                <input type="text" data-target="#country-list" class="search__input form-create-ad__text country-name x-search" placeholder="Начните вводить название страны..." value="{{ $advert->type != 'trade' ? $advert->country->name : '' }}">
                                <div class="arrow-block">
                                    <i class="arrow down"></i>
                                </div>
                            </div>
                            <ul class="search__list" style="display: none;" id="country-list">
                                @foreach($countries as $country)
                                    <li data-id="{{ $country->id }}">{{ $country->name }}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="form-create-ad__item form-create-ad__item_your-city" id="city-form" style="display: none">
                    <label for="your-city" class="form-create-ad__label">Ваш город:</label>
                    <div class="form-create-ad__control-wrap">
                        <div class="select-wrapper">
                            <div class="select-wrapper__input-block">
                                <input type="text" data-target="#city-list" id="city-name" value="{{ $advert->type == 'sell_for_cash' || $advert->type == 'buy_for_cash' ? $advert->city->name : '' }}" class="form-create-ad__text search__input city-name city-search" autocomplete="off" placeholder="Начните вводить название города...">
                                <div class="arrow-block">
                                    <i class="arrow down"></i>
                                </div>
                            </div>
                            <ul style="display: none;" id="city-list">
                                @if ($advert->type == 'sell_for_cash' || $advert->type == 'buy_for_cash')
                                    @foreach($advert->country->cities as $city)
                                        <li data-id="{{ $city->id }}">{{ $city->name }}</li>
                                    @endforeach
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="form-create-ad__item form-create-ad__item_currency" id="payment-and-currency-form" style="display: none;">
                    <label for="payment-system" class="form-create-ad__label" id="payment-label">Система и валюта получения:</label>
                    <div class="form-create-ad__control-wrap">
                        <div class="form-create-ad__select-item" id="payment-type-form" style="display: none;">
                            <div class="select-wrapper select-wrapper_payment-system">
                                <div class="select-wrapper__input-block">
                                    <input type="text" class="search__input form-create-ad__text x-search" value="{{ $advert->type == 'sell_online' || $advert->type == 'buy_online' ? $advert->payment_type->name : '' }}" data-target="#payment-list" placeholder="Начните вводить систему" id="payment-type-name">
                                    <div class="arrow-block">
                                        <i class="arrow down"></i>
                                    </div>
                                </div>
                                <ul class="search__list" style="display: none;" id="payment-list">
                                    @foreach($paymentTypes as $paymentType)
                                        <li data-id="{{ $paymentType->id }}">{{ $paymentType->name }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        <div class="form-create-ad__select-item form-create-ad__select-item_bank-name" id="bank-form" style="display: {{ $advert->payment_type_id == 2 ? '' : 'none' }};">
                            <div class="select-ul-wrapper">
                                <ul class="select-ul">
                                    @if (!is_null($advert->bank_ids))
                                        @foreach(($bank_list = json_decode($advert->bank_ids, true)) as $bank_id)
                                            @if ($bank_id === 0)
                                                @break
                                            @endif
                                            <li class="select-ul__li" data-id="{{ $bank_id }}" style="order: 1;"><p>{{ \App\Models\Bank::find($bank_id)->name }}</p><img src="/img/exit.png"></li>
                                        @endforeach
                                    @endif
                                    <li class="select-ul__li--input">
                                        <input type="text" class="multi__input form-create-ad__text bank-search" data-target="#bank-list" placeholder="Название банка" value="{{ ($bank_id ?? -1) == 0 ? 'Все банки страны' : '' }}" id="bank-name">
                                    </li>
                                </ul>
                                <ul class="multi__list" style="display: none;" id="bank-list">
                                    <li data-id="0"{!! ($bank_id ?? -1) == 0 ? ' class="selected" style="color: rgb(213, 213, 213);"' : '' !!}>Все банки страны</li>
                                    @if (!is_null($advert->bank_ids))
                                        @foreach($advert->country->banks as $bank)
                                            <li data-id="{{ $bank->id }}"{!! in_array($bank->id, $bank_list ?? [-1]) ? ' class="selected" style="color: rgb(213, 213, 213);"' : '' !!}>
                                                {{ $bank->name }}
                                            </li>
                                        @endforeach
                                    @endif
                                </ul>
                            </div>
                            <div class="form-create-ad__hint">Укажите «Все банки страны» или <br>выберите не более 3 банков из списка.</div>
                        </div>
                        <div class="form-create-ad__select-item">
                            <div class="select-wrapper select-wrapper_currency">
                                <div class="select-wrapper__input-block">
                                    <input type="text" class="search__input form-create-ad__text x-search" value="{{ $advert->type != 'trade' ? $advert->currency_name : '' }}" data-target="#currency-list" placeholder="Валюта" id="currency-name">
                                    <div class="arrow-block">
                                        <i class="arrow down"></i>
                                    </div>
                                </div>
                                <ul class="search__list" style="display: none;" id="currency-list">
                                    @foreach($currencies as $currency)
                                        <li data-id="{{ $currency->id }}">{{ $currency->name }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        <div class="advertisement-note" hidden>Уровень риска при продаже <span class="crypto-short">BTC</span> онлайн, используя способ оплаты «Банковский перевод»: <span style="font-weight: 500;">Низкий.</span> Более подробно о возможных рисках после заключения сделки написано <a href="#">здесь.</a></div>
                    </div>
                </div>
                <div style="display: none;" id="additional-data">
                    <div class="form-create-ad__item form-create-ad__item_purchase-rate" id="rate-purchase" style="display: none;">
                        <label for="purchase-rate" class="form-create-ad__label">Ваш курс <span class="case-1">покупки</span>:</label>
                        <div class="form-create-ad__control-wrap">
                            <div class="form-create-ad__control">
                                <span class="form-create-ad__control-prefix">1 <span class="{{ $advert->type == 'trade' ? 'currency-short' : 'crypto-short' }}">BTC</span> =</span>
                                <input type="text" class="form-create-ad__text" name="rate" id="purchase-rate" 
                                       placeholder="0.00" value="{{ $advert->rate }}">
                                <span class="form-create-ad__control-suffix"><span class="{{ $advert->type == 'trade' ? 'crypto-short' : 'currency-short' }}">{{ $advert->type == 'trade' ? 'BTC' : 'UAH' }}</span></span>
                                <span class="form-create-ad__hint">Укажите курс, по которому вы хотите <span class="case-2">купить</span> <span class="crypto-short">BTC</span></span>
                            </div>
                            <div class="form-create-ad__note" id="rate-notice">Лучший курс сейчас: <span class="rate-sum">0</span> <span class="currency-short">UAH</span></div>
                        </div>
                    </div>
                    <div class="form-create-ad__item form-create-ad__item_purchase-amount" id="form-sum" style="display: none;">
                        <label for="bitcoins-number-purchase" class="form-create-ad__label number-purchase">Сумма продажи:</label>
                        <div class="form-create-ad__control-wrap">
                            <div class="form-create-ad__content" id="form-sum-description" style="display: none;">
                                На вашем балансе <span class="form-create-ad__balance-amount"><span class="balance-amount">0</span> <span class="crypto-short">BTC</span></span><br>
                                Комиссия сервиса ({{ auth()->user()->commission }}%): <span class="commission-sum">0</span> <span class="crypto-short">BTC</span><br>
                                Максимальная сумма, которую вы можете <span class="case-2">продать</span>: <a href="#" class="form-create-ad-buy"><span class="sum-with-commission">0</span> <span class="crypto-short">BTC</span></a>
                            </div>
                            <div class="form-create-ad__control">
                                <input type="text" 
                                       class="form-create-ad__text form-create-ad__text_bitcoins-number-purchase"
                                       name="sum_crypto" id="bitcoins-number-purchase"
                                       placeholder="0.00000000" value="{{ $advert->sum_crypto }}">
                                <span class="form-create-ad__control-suffix"><span class="crypto-short">BTC</span></span>
                                <span class="form-create-ad__hint">Укажите, сколько <span class="crypto-short">BTC</span> вы хотите <span class="case-2">продать</span></span>
                            </div>
                            <div class="form-create-ad__control">
                                <input type="text" class="form-create-ad__text form-create-ad__text_amount-uah-purchase" 
                                       name="sum_currency" id="amount-uah-purchase" placeholder="0.00000000" value="{{ $advert->sum_currency }}">
                                <span class="form-create-ad__control-suffix"><span class="currency-short">UAH</span></span>
                                <span class="form-create-ad__hint">Укажите, на какую сумму в <span class="currency-short">UAH</span> вы хотите <span class="case-2">продать</span> <span class="crypto-short">BTC</span></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-create-ad__item form-create-ad__item_minimum-amount">

                         <div class="restriction-beginners-create-ad form-create-ad_block form-create-ad_block_left">
                           <label for="restriction-beginners-1" class="form-create-ad__label_1 form_create_tog">Минимальная сумма <br>для открытия сделки:</label>
                            <div class="form-create-ad__control-wrap-inner-1 ">
                                <div class="form-create-ad__control-1 form_create_tog">
                                    <input type="checkbox" class="form-create-ad__checkbox jqstyler" 
                                    name="constraints-1" id="restriction-beginners-1" value="1">
                                </div>
                            </div>
                        </div>


                        <div class="form-create-ad_block">
                            <div class="form_create_caption_dn">

                                <!-- <label for="minimum_amount_bitcoin" class="form-create-ad__label">Минимальная сумма <br>для -->
                                    <!-- открытия сделки:</label> -->
                                    <div class="form-create-ad__control-wrap">
                                        <div class="form-create-ad__control">
                                            <input type="text" value="{{ $advert->min_crypto }}" 
                                            class="form-create-ad__text form-create-ad__text_minimum_amount_bitcoin"
                                            name="min_crypto" id="minimum_amount_bitcoin"
                                            placeholder="0.00000000">
                                            <span class="form-create-ad__control-suffix"><span class="crypto-short">BTC</span></span>
                                        </div>
                                        <div class="form-create-ad__control">
                                            <input type="text" class="form-create-ad__text form-create-ad__text_amount-uah" 
                                            name="min_currency" id="amount-uah" placeholder="0.00000000" value="{{ $advert->min_currency }}">
                                            <span class="form-create-ad__control-suffix"><span class="currency-short">UAH</span></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-create-ad__hint">Данная функция заполняется по желанию и необходима для тех
                                    пользователей, которые не желают заключать сделки на мелкие суммы. Укажите минимальную сумму
                                    <span class="case-1">покупки</span> в <span class="crypto-short">BTC</span> или <span class="currency-short">UAH</span>, и ваше объявление сможет открыть только тот пользователь, у которого
                                    средства на балансе будуть превышать указанную вами сумму для открытия сделки.
                                </div>
                            </div>

                    </div>
                    <div class="form-create-ad__item form-create-ad__item_amount-relevance" id="keep-up-date-change" style="display: none;">
                        <label for="amount_relevance" class="form-create-ad__label">Следить за актуальностью
                            суммы:</label>
                        <div class="form-create-ad__control-wrap">
                            <div class="form-create-ad__control">
                                <input type="checkbox" class="form-create-ad__checkbox jqstyler" name="keep_up"
                                       id="amount_relevance" value="1"{{ $advert->keep_up ? ' checked' : '' }}>
                            </div>
                        </div>
                        <div class="form-create-ad__hint">Если включен этот параметр, то система автоматически будет
                            уменьшать необходимую вам сумму <span class="case-1">покупки</span> криптовалюты после каждой успешно завершенной
                            сделки. Например, вы хотите <span class="case-2">купить</span> в общей сложности 5 <span class="crypto-short">BTC</span>. После успешной сделки на 0.5 <span class="crypto-short">BTC</span>
                            сумма в объявлении изменится на 4.5 <span class="crypto-short">BTC</span>.
                        </div>
                    </div>
                    <div class="form-create-ad__item form-create-ad__item_time-pay-transaction" id="time-form">
                        <label for="time-pay-transaction" class="form-create-ad__label">Время на оплату сделки:</label>
                        <div class="form-create-ad__control-wrap">
                            <div class="form-create-ad__control">
                                <input type="number"
                                       class="form-create-ad__text form-create-ad__text_time-pay-transaction"
                                       name="time" id="time-pay-transaction" value="{{ $advert->time }}">
                                <span class="form-create-ad__control-suffix">Минут</span>
                            </div>
                        </div>
                        <div class="form-create-ad__hint">Время, за которое пользователь должен произвести оплату, иначе
                            сделка будет автоматически отменена.
                        </div>
                    </div>
                    <div class="form-create-ad__item" id="transaction-details" style="display: none;">
                        <label for="payment-requisites" class="form-create-ad__label">Реквизиты сделки:</label>
                        <div class="form-create-ad__control-wrap">
                            <div class="form-create-ad__control">
                                <textarea type="text"
                                          class="form-create-ad__textarea form-create-ad__textarea_payment-requisites"
                                          name="requisites" id="payment-requisites"
                                          placeholder="Банковская карта 4149897765008956. ФИО - Иванов Иван Иванович">{{ $advert->requisites }}</textarea>
                            </div>
                            <div class="form-create-ad__hint">Укажите ваши личные реквизиты, на которые второй участник
                                сделки должен будет перевести средства. Если потребуется, дополнительно укажите ваше ФИО
                                и другую информацию, которая ускорит процесс оплаты. Эти данные будут выводится только
                                на этапе заключении сделки и не будут показаны при просмотре объявления.
                            </div>
                        </div>
                    </div>
                    <div class="form-create-ad__item">
                        <label for="transaction-description" class="form-create-ad__label">Описание сделки:</label>
                        <div class="form-create-ad__control-wrap">
                            <div class="form-create-ad__control">
                                <textarea type="text"
                                          class="form-create-ad__textarea form-create-ad__textarea_transaction-description"
                                          name="description" id="transaction-description"
                                          placeholder="Введите описание сделки...">{{ $advert->description }}</textarea>
                            </div>
                            <div class="form-create-ad__hint">Информация, указанная в этом поле, будет видна всем
                                пользователям, которые откроют ваше объявления для более подробного ознакомления.
                                Опишите здесь порядок оплаты комиссий или любые другие важные нюансы, которые посчитаете
                                нужными. Так же можете указать свои контактные данные для связи с вами. Если не знаете
                                что написать - напишите, что вы сейчас онлайн и можете быстро провести сделку.
                            </div>
                        </div>
                    </div>
                    <div class="form-create-ad__item form-create-ad__item_amount-relevance-result" id="keep-up-date-nochange" style="display: none;">
                        <label for="amount_relevance_result" class="form-create-ad__label">Следить за актуальностью
                            суммы:</label>
                        <div class="form-create-ad__control-wrap">
                            <div class="form-create-ad__content">Включено</div>
                        </div>
                        <div class="form-create-ad__hint">Если включен этот параметр, то система автоматически будет
                            уменьшать необходимую вам сумму <span class="case-1">покупки</span> криптовалюты после каждой успешно завершенной
                            сделки. Например, вы хотите <span class="case-2">купить</span> в общей сложности 5 <span class="crypto-short">BTC</span>. После успешной сделки на 0.5 <span class="crypto-short">BTC</span>
                            сумма в объявлении изменится на 4.5 <span class="crypto-short">BTC</span>.
                        </div>
                    </div>
                    {{--<div class="form-create-ad__item form-create-ad__item_restriction-beginners" id="constraints-form">--}}
                        {{--<label for="restriction-beginners" class="form-create-ad__label">Ограничение для--}}
                            {{--новичков:</label>--}}
                        {{--<div class="form-create-ad__control-wrap">--}}
                            {{--<div class="form-create-ad__control-wrap-inner">--}}
                                {{--<div class="form-create-ad__control">--}}
                                    {{--<input type="checkbox" class="form-create-ad__checkbox jqstyler"--}}
                                           {{--name="constraints" id="restriction-beginners" value="1"{{ $advert->constraints ? ' checked' : '' }}>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="form-create-ad__control-wrap-inner">--}}
                                {{--<div class="form-create-ad__control" id="constraints-min-form" style="display: {{ $advert->constraints ? '' : 'none' }};">--}}
                                    {{--<span class="form-create-ad__control-prefix">Пользователи с объемом сделок менее:</span>--}}
                                    {{--<input type="text"--}}
                                           {{--class="form-create-ad__text form-create-ad__text_volume-transactions-less"--}}
                                           {{--name="constraints_min" id="volume-transactions-less"--}}
                                           {{--placeholder="0.00000000" value="{{ $advert->constraints_min }}">--}}
                                    {{--<span class="form-create-ad__control-suffix"><span class="crypto-short">BTC</span></span>--}}
                                {{--</div>--}}
                                {{--<div class="form-create-ad__control" id="constraints-max-form" style="display: {{ $advert->constraints ? '' : 'none' }};">--}}
                                    {{--<span class="form-create-ad__control-prefix">Могут открывать сделки на сумму не более чем:</span>--}}
                                    {{--<input type="text" value="{{ $advert->constraints_max }}"--}}
                                           {{--class="form-create-ad__text form-create-ad__text_transactions-amount-not-exceeding"--}}
                                           {{--name="constraints_max"--}}
                                           {{--id="transactions-amount-not-exceeding" placeholder="0.00000000">--}}
                                    {{--<span class="form-create-ad__control-suffix"><span class="crypto-short">BTC</span></span>--}}
                                {{--</div>--}}
                                {{--<div class="form-create-ad__hint">Очень часто новые пользователи открывают сделки, чтобы--}}
                                    {{--разобраться, как работает сервис. Если этот параметр будет включен - сервис позволит--}}
                                    {{--новым пользователям совершать сделки на указанную вами максимальную сумму. Если--}}
                                    {{--новый участник действительно захочет совершить с вами сделку на большую сумму - вы--}}
                                    {{--всегда сможете снять это ограничение, либо заключить с ним две сделки.--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    <div class="form-create-ad__item" id="who-can-make-deal">
                        <label for="who-can-make-deal" class="form-create-ad__label">Кто может заключить сделку:</label>
                        <div class="form-create-ad__control-wrap">
                            <select name="achievement_id"
                                    class="form-create-ad__select form-create-ad__select_who-can-make-deal jqstyler">
                                <option value="">Все пользователи</option>
                                @foreach($achievements as $achievement)
                                    <option value="{{ $achievement->id }}"{{ $advert->achievement_id == $achievement->id ? ' selected' : '' }}>Только пользователи с отметкой «{{ $achievement->name }}»</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-create-ad__item" id="error-block" style="display: none;">
                    <div class="form-error"></div>
                </div>
                <div class="form-create-ad__actions">
                    <input type="submit" class="btn-green2 form-create-ad__submit" value="Сохранить изменения">
                    <input type="button" class="btn-blue form-create-ad__submit form-create-ad__submit_cancel" value="Отменить" onclick="window.history.back();">
                </div>
            </form>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $('.form_create_tog').on('click',function() {
            $('.form_create_caption_dn').toggle();
        });

        $('.jqstyler').styler({
            onFormStyled: function () {
                $('.jq-selectbox').each(function (index, el) {
                    let bgImg = $(this).find('.jq-selectbox__dropdown ul li.selected').css('background-image');
                    $(this).find('.jq-selectbox__select-text').css('background-image', bgImg);
                });
            },
            onSelectClosed: function () {
                let bgImg = $(this).find('.jq-selectbox__dropdown ul li.selected').css('background-image');
                $(this).find('.jq-selectbox__select-text').css('background-image', bgImg);

                if ($(this).hasClass('form-create-ad__select_operation-type')) {
                    if ($(this).find('.jq-selectbox__dropdown ul li.selected').index() == 4) {
                        $('.form-create-ad__item_cryptoexchange').addClass('form-create-ad__item_no-icons');
                    } else {
                        $('.form-create-ad__item_cryptoexchange').removeClass('form-create-ad__item_no-icons');
                    }
                }
            }
        });
        @if ($advert->min_crypto != 0 && $advert->min_currency != 0)
            $('#restriction-beginners-1-styler').trigger('click');
        @endif
        $('#restriction-beginners-1-styler').click(function () {
            if (!$(this).hasClass('.checked')) {
                $('#amount-uah').val('');
                $('#minimum_amount_bitcoin').val('');
            }
        });
    </script>
    <script src="/js/publication.js"></script>
@endsection