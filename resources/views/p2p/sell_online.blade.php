<div class="action-table-wrapper">
  <h2 class="action-table-wrapper__title">Купить {{ $crypto->name }}
    @if (($paymentType->id ?? 0) != 0)
    с помощью: {{ $paymentType->name }} ({{ $selected_currency->name }})
    @else
    с помощью всех способов оплаты
    @endif
  </h2>
  <div class="table-responsive">
    <table class="action-table">
      <thead class="action-table__thead">
        <tr class="action-table__tr">
          <td class="action-table__seller">Продавец</td>
          <td class="action-table__buy-with">Купить с помощью</td>
          <td class="action-table__price">Цена за 1 {{ $crypto->short }}</td>
          <td class="action-table__sum">Мин. - макс. сумма</td>
          <td class="action-table__action-type">Тип действия</td>
        </tr>
      </thead>
      <tbody class="action-table__tbody">
        <tr class="action-table__place-ad">
          <td colspan="5">
            <div class="place-ad">
              <div class="place-ad__text">Совершенно бесплатно разместите здесь ваше объявление и заключайте любые сделки с комиссией 0%</div>
              @if (!auth()->check())
              <a href="#" class="btn-green4 place-ad__btn" data-toggle="modal" data-target="#modal-error">Разместить бесплатно</a>
              @else
              <a href="/publications/create" class="btn-green4 place-ad__btn">Разместить бесплатно</a>
              @endif
            </div>
          </td>
        </tr>
        @forelse($adverts as $advert)
        <tr class="action-table__tr">
          <td class="action-table__seller">
            <div class="action-table__seller-photo action-table__seller-photo_{{ $advert->user->is_online ? 'online' : 'offline' }}" data-toggle="tooltip" title="{{ $advert->user->is_online ? 'Сейчас онлайн' : 'Заходил ' . $advert->user->last_activity_format }}">
              <img src="{{ $advert->user->image }}" alt="">
              <div class="action-table__seller-status"></div>
            </div>
            <!-- <div class="action-table__seller-info action-table__seller-info_{{ $advert->user->is_online ? 'online' : 'offline' }}"> -->
            <div class="action-table__seller-info">
              <div class="action-table__seller-info-block_left">
                <a href="/profile/{{ $advert->user->login }}" class="action-table__seller-name">{{ $advert->user->login }}</a>
                <div class="action-table__response-rate" data-toggle="tooltip" title="Обычно отвечает в течение 5 минут"></div>
              </div>
              <div class="action-table__seller-rating">
                {{ $advert->user->rating }}
              </div>
              <span class="action-table__seller-icons">
                @foreach($advert->user->achievements as $achievement)
                @if ($achievement->image == '/storage/achievements/2.png')
                <span data-toggle="tooltip" title="Пройдена полная верификация личности">
                  <img src="{{ $achievement->image }}" alt="">
                </span>
                @elseif($achievement->image == '/storage/achievements/1.png')
                <span data-toggle="tooltip" title="Верифицирован телефонный номер">
                  <img src="{{ $achievement->image }}" alt="">
                </span>
                @else
                <img src="{{ $achievement->image }}" alt="">
                @endif
                @endforeach
              </span>
            </div>
          </td>
          <td class="action-table__buy-with">
            @if ($advert->type == 'buy_online' || $advert->type == 'sell_online')
            <a href="#" class="action-table__link" type_id="{{ $advert->payment_type_id }}">
              {{ $advert->payment_type->name }}
            </a>
            @if ($advert->payment_type_id == 2)
            : {{ $advert->banks }}
            @endif
            @endif
          </td>
          <td class="action-table__price">{{ $advert->rate() }} {{ $advert->currency_name }}</td>
          <td class="action-table__sum">{{ $advert->minCurrency() }} - {{ $advert->sumCurrency() }} {{ $advert->currency_name }}</td>
          <td class="action-table__action-type table__action_sell">
            @if(auth()->user()&&$advert->user_id == auth()->user()->id)
            <!-- Випадаючий список -->
            <a class="btn-lime action-type_button">Изменить</a>
            <div class="jq-selectbox__dropdown action-type_dropdown" style="height: auto; bottom: auto; top: 42px;">
              <ul style="max-height: 300px;">
                <li class="selected sel" style=""><a href="/publications/{{ $advert->id }}/edit" >Изменить</a></li>
                <li style=""><a href="/publications/{{ $advert->id }}" >Посмотреть</a></li>
              </ul>
            </div>
            @else
            <a href="/publications/{{ $advert->id }}" class="btn-lime">Купить</a>
            @endif
          </td>
        </tr>
        @empty
        <tr class="action-table__empty">
          <td colspan="5">Объявления отсутствуют. Попробуйте изменить критерии поиска или создайте нужное вам объявление прямо сейчас.</td>
        </tr>
        @endforelse
      </tbody>
    </table>
  </div>
  @if ($adverts->total() > 5 && request()->path() == 'p2p')
  {{--<div class="action-table-wrapper__all-offers">--}}
    {{--<a id="sell-online-more" href="#" class="action-table-wrapper__offers-link">Показать все предложения</a>--}}
  {{--</div>--}}
  @endif
</div>