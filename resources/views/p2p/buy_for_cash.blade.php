<div class="action-table-wrapper">
  <h2 class="action-table-wrapper__title">Продать {{ $crypto->name }} за наличные{{-- в {{ $selectedCountry->name }} --}}</h2>
  <table class="action-table">
    <thead class="action-table__thead">
      <tr class="action-table__tr">
        <td class="action-table__seller">Продавец</td>
        <td class="action-table__transaction-place">Место проведения сделки</td>
        <td class="action-table__price">Цена за 1 {{ $crypto->short }}</td>
        <td class="action-table__sum">Мин. - макс. сумма</td>
        <td class="action-table__action-type">Тип действия</td>
      </tr>
    </thead>
    <tbody class="action-table__tbody">
      @forelse($adverts as $advert)
      <tr class="action-table__tr">
        <td class="action-table__seller">
          <div class="action-table__seller-photo"><img src="{{ $advert->user->image }}" alt=""></div>
          <div class="action-table__seller-info action-table__seller-info_{{ $advert->user->is_online ? 'online' : 'offline' }}">
            <div class="action-table__seller-info-block_left">
              <a href="/profile/{{ $advert->user->login }}" class="action-table__seller-name">{{ $advert->user->login }}</a>
              <div class="action-table__seller-status" data-toggle="tooltip" title="{{ $advert->user->is_online ? 'Сейчас онлайн' : 'Заходил ' . $advert->user->last_activity_format }}"></div>
            </div>
            
            <div class="action-table__seller-rating">
              {{ $advert->user->rating }}
            </div>
            <span class="action-table__seller-icons">
              @foreach($advert->user->achievements as $achievement)
              @if ($achievement->image == '/storage/achievements/2.png')
              <span data-toggle="tooltip" title="Пройдена полная верификация личности">
                <img src="{{ $achievement->image }}" alt="">
              </span>
              @elseif($achievement->image == '/storage/achievements/1.png')
              <span data-toggle="tooltip" title="Верифицирован телефонный номер">
                <img src="{{ $achievement->image }}" alt="">
              </span>
              @else
              <img src="{{ $achievement->image }}" alt="">
              @endif
              @endforeach
            </span>
          </div>
        </td>
        <td class="action-table__buy-with">
          {{ $advert->city_id ? $advert->city->name . ',' : '' }}
          {{ $advert->city_id && $advert->city->area ? $advert->city->area . ',' : '' }}
          {{ $advert->city_id && $advert->city->region ? $advert->city->region . ',' : '' }}
          {{ $advert->country->name }}
        </td>
        <td class="action-table__price">{{ $advert->rate() }} {{ $advert->currency_name }}</td>
        <td class="action-table__sum">{{ $advert->minCurrency() }} - {{ $advert->sumCurrency() }} {{ $advert->currency_name }}</td>
        <td class="action-table__action-type table__action_buy">
          @if(auth()->user()&&$advert->user_id == auth()->user()->id)
          <!-- Випадаючий список -->
          <a class="btn-lime action-type_button">Изменить</a>
          <div class="jq-selectbox__dropdown action-type_dropdown" style="height: auto; bottom: auto; top: 42px;">
            <ul style="max-height: 300px;">
              <li class="selected sel" style=""><a href="/publications/{{ $advert->id }}/edit" >Изменить</a></li>
              <li style=""><a href="/publications/{{ $advert->id }}" >Посмотреть</a></li>
            </ul>
          </div>
          @else
          <a href="/publications/{{ $advert->id }}" class="btn-lime">Продать</a>
          @endif
        </td>
      </tr>
      @empty
      <tr class="action-table__empty">
        <td colspan="5">Объявления отсутствуют. Попробуйте изменить критерии поиска или создайте нужное вам объявление прямо сейчас.</td>
      </tr>
      @endforelse
    </tbody>
  </table>

  @if ($adverts->total() > 5 && request()->path() == 'cryptosell')
  <div class="action-table-wrapper__all-offers">
    <a id="buy-for-cash-more" href="#" class="action-table-wrapper__offers-link">Показать все предложения</a>
  </div>
  @endif
</div>