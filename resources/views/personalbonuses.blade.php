@extends('layouts.app')

@title('Персональные бонусы')

@className('main')

@section('content')
	<div class="content">
		<div class="infopage infopage_bonuses">
			<div class="title-wrap title-wrap_mybonuses">
		    <div class="container">
		      <div class="row">
		        <h1 class="page-title title-wrap__title">Мои акции и бонусы</h1>
		        <a href="/" class="btn-back title-wrap__back">назад</a>
		      </div>
		    </div>
		  </div>
			<div class="container">
				<div class="row">
					<div class="bonuses bonuses_page-mybonuses">
						<h2 class="bonuses__title">Благодаря системе аналитики действий, мы подберём и бесплатно подарим вам именно те бонусы и акционные <br>предложения, которые действительно будут вам полезны.</h2>
						<div class="bonuses__text"><p>К примеру, если вы любите торговать - система подберёт для вас бонусы, акции или расширенные инструменты, которые значительно улучшат торговлю и увеличат взаимодействие с другими участниками сервиса. Для пользователей, которые занимаются привлечением партнёров в команду, умный алгоритм сформирует временное подарочное увеличение партнёрской ставки, сертификаты на зачисление в команду тех пользователей, которые были зарегистрированы без пригласителя или другие интересные бонусы.</p></div>
					</div>
					<div class="shares-blocks">
						<h2 class="shares-blocks__title"><img src="{{ auth()->user()->image }}" alt="Фотография" class="img-small shares-blocks__avatar"> <span class="shares-blocks__username">{{ auth()->user()->login }},</span> на основе анализа ваших действий мы дарим вам:</h2>
						<div class="shares-blocks__blocks">
							<div class="shares-block">
								<div class="shares-block__left">
									<div class="shares-block__type">Акция</div>
									<div class="shares-block__number">#1</div>
								</div>
								<div class="shares-block__right">
									<h2 class="shares-block__title">Дополнительное отключение комиссии на все операции в торговой платформе</h2>
									<div class="shares-block__text">
										<p>Каждый раз, когда объем ваших сделок суммарно будет достигать 0.50000000 BTC - отключение комиссии будет продлеваться на +7 календарных дней от последней установленной даты. К примеру, за суммарный объем в 5 BTC вы получите дополнительное отключение комиссии на 70 календарных дней после окончания общего акционного периода. Тип завершенных сделок не имеет значения. Любые ограничения отсутствуют. Текущая дата окончания вашего персонального акционного периода указана в выпадающем меню профиля.</p>
									</div>
									<div class="shares-block__validity">Акция действует до: 1.08.2018</div>
								</div>
							</div>
							<div class="shares-block shares-block_bonus">
								<div class="shares-block__left">
									<div class="shares-block__type">Бонус</div>
									<div class="shares-block__number">#2</div>
								</div>
								<div class="shares-block__right">
									<h2 class="shares-block__title">Отключение комиссии на 5 дней календарных дней</h2>
									<div class="shares-block__text">
										<p>В любое удобное для вас время - активируйте этот бонус и мгновенно получите отключение комиссии на все операции в торговой платформе сроком на 5 календарных дней. Бонус можно активировать в любой момент, когда значение комиссии будет больше нуля. Текущая дата окончания вашего персонального акционного периода указана в выпадающем меню профиля.</p>
									</div>
									<div class="shares-block__validity">Срок действия бонуса: неограниченно</div>
									<div class="shares-block__activate-offer">
										<div class="btn-activate">Активировать бонус #2</div>
									</div>
								</div>
							</div>
							<div class="shares-block">
								<div class="shares-block__left">
									<div class="shares-block__type">Акция</div>
									<div class="shares-block__number">#3</div>
								</div>
								<div class="shares-block__right">
									<h2 class="shares-block__title">+2 месяца отсутствия комиссии на любые операции с вашими партнёрами</h2>
									<div class="shares-block__text">
										<p>Все партнёры, которые зарегистрируются по вашей ссылке до 1 августа 2018 года, дополнительно получат +2 месяца полного отсутствия комиссий на все типы операций, которые будут проводиться с вами. Таким образом, вы можете пригласить в сервис своих друзей, знакомых или партнёров по бизнесу, и быть уверенными в том, что в любое удобное время с момента их регистрации и на протяжении 8 месяцев (6 месяцев по умолчанию + 2 дополнительных месяца в рамках этой акции), вы сможете заключить с ними неограниченное количество надёжно защищенных сделок, которые не будут облагаться комиссией. </p>
									</div>
									<div class="shares-block__validity">Акция действует до: 1.08.2018</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('scripts')
@endsection