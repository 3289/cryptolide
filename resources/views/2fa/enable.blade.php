@extends('layouts.app')

@title('Подключение двухфакторной аутентификации (2FA)')

@section('content')
	<div class="content">
		<div class="title-wrap title-wrap_my-wallet">
			<div class="container">
				<div class="row">
					<h1 class="page-title title-wrap__title">Подключение двухфакторной аутентификации (2FA)</h1>
					<a href="/" class="btn-back title-wrap__back">назад</a>
				</div>
			</div>
		</div>
		<div class="block-2fa-enable">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<!-- <img src="{{ $image }}">
						<p>Secret: {{ $secret }}</p>
						@if ($errors->count())
							<p style="color: red">{{ $errors->first() }}</p>
						@endif
						<form method="POST">
							{{ csrf_field() }}
							<input type="text" name="confirmation_code" required placeholder="Код подтверждения">
							<button href="/2fa/confirm_enable" class="btn-green2 form-withdrawal-bitcoin__form-submit">
								Включить 2FA
							</button>
						</form> -->
						<div class="block-2fa-enable__box block-2fa-enable__box_about">
							<h2 class="block-2fa-enable__subtitle">Что такое двухфакторная аутентификация (2FA)?</h2>
							<p>2FA - это метод идентификации пользователя, при котором используются два различных типа <br>аутентификационных данных. Двухфакторная аутентификация в сервисе CRYPTOLIDE работает с <br>мобильными приложениями ОС Android, iOS и Windows Mobile.</p>
							<img src="/images/content/2fa.png" alt="2FA">
							<p>С помощью 2FA можно предотвратить около 99% атак и, в отличие от классических паролей, коды <br>двухфакторной аутентификации очень сложно украсть или подобрать. Если вы хотите максимально <br>защитить ваш аккаунт - обязательно подключите 2FA. Это быстро, надежно, и совершенно бесплатно.</p>
						</div>
						<div class="block-2fa-enable__box block-2fa-enable__box_enable-2fa">
							<h2 class="block-2fa-enable__subtitle">Подключение двухфакторной аутентификации</h2>
							<p>1. Скачайте официальное приложение Google Authenticator для своего смартфона или планшета для ОС:</p>
							<ul>
								<li><a href="https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2" target="_blank">- Android</a></li>
								<li><a href="https://itunes.apple.com/us/app/google-authenticator/id388497605?mt=8" target="_blank">- iPhone, iPad, iPod</a></li>
								<li><a href="https://www.microsoft.com/uk-ua/store/p/authenticator/9wzdncrfj3rj?rtc=1" target="_blank">- Windows Phone.</a></li>
							</ul>
							<p>2. Запишите этот код: <span class="block-2fa-enable__code">{{ $secret }}</span> на бумаге и храните его в надёжном месте. Он поможет вам восстановить доступ к учетной записи, если вы потеряете устройство, на котором была подключена 2FA, или по любым другим причинам не сможете попасть в аккаунт.</p>
							<p>3. Установите скачанное приложение Google Authenticator и запустите его на своем мобильном устройстве. Найдите в приложении функцию "сканировать штрихкод" и отсканируйте штрихкод ниже:</p>
						</div>
						<div class="block-2fa-enable__form-wrapper">
							<div class="block-2fa-enable__qr-code">
								<img src="{{ $image }}">
							</div>
							<form method="POST" class="block-2fa-enable__form">
								{{ csrf_field() }}
								<label for="" class="block-2fa-enable__form-label">4. Введите полученный код в это поле:</label>
								<input type="text" name="confirmation_code" required placeholder="Код подтверждения" class="block-2fa-enable__form-text">
								<div class="block-2fa-enable__form-actions">
									<button href="/2fa/confirm_enable" class="btn-green2 block-2fa-enable__form-submit">
										Включить 2FA
									</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection