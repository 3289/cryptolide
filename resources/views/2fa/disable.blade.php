@extends('layouts.app')

@title('Google 2FA')

@section('content')
<div class="content">
	<div class="title-wrap title-wrap_my-wallet">
		<div class="container">
			<div class="row">
				<h1 class="page-title title-wrap__title">Отключение двухфакторной аутентификации (2FA)</h1>
				<a href="/" class="btn-back title-wrap__back">назад</a>
			</div>
		</div>
	</div>
	<div class="block-2fa-disable">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					@if ($errors->count())
					<p style="color: red">{{ $errors->first() }}</p>
					@endif
					<div class="block-2fa-disable__description">Для <span style="color: #e05505">отключения</span> двухфакторной аутентификации введите код подтверждения в поле ниже:</div>
					<div class="block-2fa-disable__note"><span style="color: #e05505">Обратите внимание!</span> Отключая 2FA, вы значительно снижаете защиту вашего аккаунта и подвергаете его риску взлома.</div>
					<form method="POST" class="block-2fa-disable__form">
						{{ csrf_field() }}
						<input type="text" name="confirmation_code" required placeholder="Код подтверждения" class="block-2fa-disable__form-text">
						<div class="block-2fa-disable__form-actions">
							<button href="/2fa/confirm_enable" class="btn-blue block-2fa-disable__form-submit">Отключить 2FA</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection