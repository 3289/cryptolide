@extends('layouts.app')
  
@title('2FA')

@section('content')
    <div class="content">
        <div class="container">
            <div class="row">
                <form action="/2fa/validate" method="POST" class="authorization-form authorization-form_login">
                    {{ csrf_field() }}
                    <h2 class="authorization-form__title">Двухфакторная аутентификация (2FA)</h2>
                    <div class="authorization-form__fieldset">
                        <div class="form-item{{ $errors->has('totp') ? ' form-item_has-error' : '' }}">
                            <input type="text" name="totp" id="totp" minlength="6" maxlength="6" placeholder="Код подтверждения" class="form-text">
                            @if ($errors->has('totp'))
                                <div class="form-error authorization-form__form-error">{{ $errors->first('totp') }}</div>
                            @endif
                        </div>
                    </div>
                    <span class="authorization-form-code-capt authorization-form-capt">Введите шестизначный номер с вашего мобильного приложения.</span>
                    <div class="authorization-form__actions">
                        <button type="submit" class="form-submit">Подтвердить вход</button>
                    </div>
                    <div class="authorization-form-problem-capt authorization-form-capt">Возникли проблемы с входом? <a style="cursor: pointer;" onclick="Chatra('openChat', true)">Напишите</a> нашим операторам.</div>
                </form>
            </div>
        </div>
    </div>
@endsection