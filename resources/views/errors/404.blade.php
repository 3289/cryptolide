@extends('layouts.app')

@section('content')
	<div class="content">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<div class="block-404">
						<div class="block-404__img"><img src="/images/content/404.png" alt=""></div>
						<h1 class="block-404__title">Ошибка 404: страница не найдена</h1>
						<div class="block-404__text">Такое иногда случается. Скорее всего, вы зашли по устаревшей ссылке или же запрашиваемая <br>страница была удалена. Попробуйте вернуться на предыдущую страницу или начать с главной.</div>
						<div class="block-404__buttons">
							<a href="#" class="btn-green2 btn-green2_large" onclick="window.history.back();">Вернуться назад</a>
							<a href="/" class="btn-blue btn-blue_large">На главную</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection