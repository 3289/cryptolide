@extends('layouts.app')

@section('content')
	<div class="content">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<div class="block-404">
						<div class="block-404__img"><img src="/images/content/404.png" alt=""></div>
						<h1 class="block-404__title">Увы, но у вас нет доступа к этой странице</h1>
						<div class="block-404__buttons">
							<a href="#" class="btn-green2 btn-green2_large" onclick="window.history.back();">Вернуться назад</a>
							<a href="/" class="btn-blue btn-blue_large">На главную</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection