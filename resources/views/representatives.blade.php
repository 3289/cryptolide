@extends('layouts.app')

@title('Представители Cryptolide')

@className('main_infopage')

@section('content')
	<div class="content">
		<div class="infopage infopage_representatives">
			<div class="infopage__title-wrap">
				<div class="container">
					<div class="row">
						<div class="col-xs-12">
							<h1 class="infopage-title">Представители CRYPTOLIDE</h1>
						</div>
					</div>
				</div>
			</div>
			<div class="container">
				<div class="row">
					<div class="infopage__content">
						<div class="infopage-block infopage-block_representatives">
							<h2 class="infopage-block__title">Представители CRYPTOLIDE - специальный раздел поощрений за <br>постоянные достижения в развитии и улучшении сервиса.</h2>
							<div class="infopage-block__text">
								<p>Пользователи, которые внесли существенный вклад в развитие сервиса, получают специальные статусы, обозначающие, что эти люди обладают дополнительными привилегиями и находятся на особом счету в администрации сервиса. У присваиваемых статусов нет временных ограничений, поэтому, однажды получив статус, вы всегда будете обладать дополнительными привилегиями, не зависимо от ваших дальнейших действий относительно сервиса. Обладатель любого из статусов всегда может повысить свой ранг или получить более высокий статус за новые заслуги, тем самым увеличить количество привилегий или увеличить их свойства.</p>
							</div>
							<div class="status-types">
								<h2 class="status-types__title">Виды статусов и их привилегии:</h2>
								<div class="status-type status-type_bronze status-types__type">
									<h2 class="status-type__name">Бронзовые статусы</h2>
									<div class="status-type__items">
										<div class="status-type__item clearfix">
											<div class="status-type__left">
												<div class="status-type__img"><img src="/images/content/bronze_status1.png" alt="Bronze status 1"></div>
											</div>
											<div class="status-type__right">
												<h3 class="status-type__item-name">Помощник 1 ранга</h3>
												<div class="status-type__item-text">Статус присваивается за постоянную помощь в улучшении или развитии сервиса. <br>Кроме таких стандартных привилегий как наивысший приоритет при обращении в службу поддержки и арбитраж, наличие персонального оператора - консультанта и открытие дополнительного функционала, пользователь на свое усмотрение может улучшить несколько комиссионных показателей, но не более чем на 25% от стандартных значений.</div>
											</div>
										</div>
										<div class="status-type__item clearfix">
											<div class="status-type__left">
												<div class="status-type__img"><img src="/images/content/bronze_status2.png" alt="Bronze status 2"></div>
											</div>
											<div class="status-type__right">
												<h3 class="status-type__item-name">Помощник 2 ранга</h3>
												<div class="status-type__item-text">Статус присваивается вместо предыдущего за новые заслуги в процессах улучшения или развития сервиса. Все предыдущие привилегии сохраняются и в дополнение, пользователь на свое усмотрение может улучшить несколько комиссионных показателей, но не более чем на 50% от стандартных значений.</div>
											</div>
										</div>
										<div class="status-type__item clearfix">
											<div class="status-type__left">
												<div class="status-type__img"><img src="/images/content/bronze_status3.png" alt="Bronze status 3"></div>
											</div>
											<div class="status-type__right">
												<h3 class="status-type__item-name">Помощник 3 ранга</h3>
												<div class="status-type__item-text">Статус присваивается вместо предыдущего за новые серьезные заслуги в процессах улучшения или развития сервиса. Все предыдущие привилегии сохраняются и в дополнение, пользователь на свое усмотрение может улучшить несколько комиссионных показателей, но не более чем на 75% от стандартных значений.</div>
											</div>
										</div>
									</div>
								</div>
								<div class="status-type status-type_gold status-types__type">
									<h2 class="status-type__name">Золотые статусы</h2>
									<div class="status-type__items">
										<div class="status-type__item clearfix">
											<div class="status-type__left">
												<div class="status-type__img"><img src="/images/content/gold_status1.png" alt="Bronze status 1"></div>
											</div>
											<div class="status-type__right">
												<h3 class="status-type__item-name">Советник 1 ранга</h3>
												<div class="status-type__item-text">Статус присваивается за постоянные советы, касающийся вопросов безопасности, быстродействия, оптимизации или любых других всевозможных разделов сайта, которые были использованы администрацией. Кроме таких стандартных привилегий как наивысший приоритет при обращении в службу поддержки и арбитраж, наличие персонального оператора - консультанта и открытие дополнительного функционала, пользователь на свое усмотрение может улучшить несколько комиссионных показателей, но не более чем на 35% от стандартных значений.</div>
											</div>
										</div>
										<div class="status-type__item clearfix">
											<div class="status-type__left">
												<div class="status-type__img"><img src="/images/content/gold_status2.png" alt="Bronze status 2"></div>
											</div>
											<div class="status-type__right">
												<h3 class="status-type__item-name">Советник 2 ранга</h3>
												<div class="status-type__item-text">Статус присваивается вместо предыдущего за новые заслуги в вопросах безопасности, быстродействия, оптимизации или любых других вопросах, касаемых улучшения сервиса. Все предыдущие привилегии сохраняются и в дополнение, пользователь на свое усмотрение может улучшить несколько комиссионных показателей, но не более чем на 75% от стандартных значений.</div>
											</div>
										</div>
										<div class="status-type__item clearfix">
											<div class="status-type__left">
												<div class="status-type__img"><img src="/images/content/gold_status3.png" alt="Bronze status 3"></div>
											</div>
											<div class="status-type__right">
												<h3 class="status-type__item-name">Советник 3 ранга</h3>
												<div class="status-type__item-text">Статус присваивается вместо предыдущего за новые серьезные заслуги в вопросах безопасности, быстродействия, оптимизации или любых других вопросах, касаемых улучшения сервиса. Все предыдущие привилегии сохраняются и в дополнение, пользователь на свое усмотрение может улучшить несколько комиссионных показателей, но не более чем на 95% от стандартных значений.</div>
											</div>
										</div>
									</div>
								</div>
								<div class="status-type status-type_representative status-types__type">
									<h2 class="status-type__name">Представительские статусы</h2>
									<div class="status-type__description">Статус «Представитель CRYPTOLIDE» - наиболее значимый и высокий, из всех имеющихся. Статус представителя присваивается за колоссальные достижения в улучшении и развитии сервиса, или за по-настоящему весомые советы в областях безопасности, быстродействия, оптимизации и других важных областях. Чаще всегда статус представителя получают либо те пользователи, которые не обладают особыми специфическими знаниями, но каждый день своими силами продвигали, развивали и улучшали сервис, или же пользователи, которые профессионально разбираются в тех или иных вопросах и давали советы, с помощью которых удалось значительно повысить защиту, оптимизацию или другие значимые технические разделы сервиса. У пользователей, которые обладают любым из представительских статусов, навсегда отключены все возможные комиссии сервиса, как по всему текущему функционалу, так и по будущему. При решении серьезных вопросов относительно дальнейшего развития сервиса или аналогичных, руководство компании советуется со всеми «Представителями» и только тогда принимает окончательное решение. Главное преимущество в присвоении «Представительского статуса» заключается в том, что <span style="color: #e24711">20%</span> от прибыли сервиса <span style="color: #e24711">ежедневно</span> делится между всеми пользователями со статусом «Представитель CRYPTOLIDE» не зависимо от того, посещают ли они сервис или нет. Это такая благодарность от лица компании за то, что эти люди сделали очень много для сервиса, и теперь руководство постоянно и с радостью выплачивает им щедрые дивиденды. Разделение прибыли между пользователями со статусом представителя происходит не по общему количеству человек, а по общему количеству единиц ранга. Это значит, что пользователь со статусом «Представитель CRYPTOLIDE 3 ранга» каждый день будет зарабатывать ровно в 3 раза больше, чем пользователь со статусом «Представитель CRYPTOLIDE 1 ранга», так как он обладает тремя ранговыми единицами.</div>
									<div class="status-type__items">
										<div class="status-type__item clearfix">
											<div class="status-type__left">
												<div class="status-type__img"><img src="/images/content/representative_status1.png" alt="Bronze status 1"></div>
											</div>
											<div class="status-type__right">
												<h3 class="status-type__item-name">Представитель CRYPTOLIDE 1 ранга</h3>
												<div class="status-type__item-text">Все возможные комиссии сервиса навсегда отключены. Все дополнительные функции и разделы активны и присутствуют. Пользователь ежедневно зарабатывает 1 ранговую единицу прибыли из общего дохода в размере 20%, который разделяется между всеми пользователями со статусом представителя.</div>
											</div>
										</div>
										<div class="status-type__item clearfix">
											<div class="status-type__left">
												<div class="status-type__img"><img src="/images/content/representative_status2.png" alt="Bronze status 2"></div>
											</div>
											<div class="status-type__right">
												<h3 class="status-type__item-name">Представитель CRYPTOLIDE 2 ранга</h3>
												<div class="status-type__item-text">Все возможные комиссии сервиса навсегда отключены. Все дополнительные функции и разделы активны и присутствуют. Пользователь ежедневно зарабатывает 2 ранговые единицы прибыли из общего дохода в размере 20%, который разделяется между всеми пользователями со статусом представителя.</div>
											</div>
										</div>
										<div class="status-type__item clearfix">
											<div class="status-type__left">
												<div class="status-type__img"><img src="/images/content/representative_status3.png" alt="Bronze status 3"></div>
											</div>
											<div class="status-type__right">
												<h3 class="status-type__item-name">Представитель CRYPTOLIDE 3 ранга</h3>
												<div class="status-type__item-text">Все возможные комиссии сервиса навсегда отключены. Все дополнительные функции и разделы активны и присутствуют. Пользователь ежедневно зарабатывает 3 ранговые единицы прибыли из общего дохода в размере 20%, который разделяется между всеми пользователями со статусом представителя.</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection