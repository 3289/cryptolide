@extends('layouts.app')

@title('Перевод денежных средств на ' . $crypto_currency->name . ' адрес')

@className('main_withdrawal-funds-bitcoin')

@section('content')
<div class="content">
	<div class="title-wrap title-wrap_withdrawal-funds-bitcoin">
		<div class="container">
			<div class="row">
				<h1 class="page-title title-wrap__title">Перевод денежных средств на {{ $crypto_currency->name }} адрес</h1>
				<a href="/" class="btn-back title-wrap__back">назад</a>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<form method="POST" class="form-withdrawal-bitcoin">
					{{ csrf_field() }}
					<input type="hidden" name="random_hash" value="{{ $random_hash }}">
					<h2 class="form-withdrawal-bitcoin__title">На вашем <img src="/images/content/icon_bitcoin.png" alt="{{ $crypto_currency->name }}"> {{ $crypto_currency->name }} кошельке: <span>{{ auth()->user()->balance[$crypto_currency->short] }} {{ $crypto_currency->short }}</span></h2>
					<div class="form-withdrawal-bitcoin__form-items">
						<div class="form-withdrawal-bitcoin__form-item form-withdrawal-bitcoin__form-item_btc-address">
							<label for="btc_address" class="form-withdrawal-bitcoin__form-label">{{ $crypto_currency->short }} адрес для отправки:</label>
							<div class="form-withdrawal-bitcoin__form-control">
								<input type="text" class="form-text form-withdrawal-bitcoin__form-text" name="address" placeholder="Введите {{ $crypto_currency->short }} адрес..." id="btc_address" autocomplete="off">
							</div>
							<div class="form-withdrawal-bitcoin__form-control-note" id="withdrawal-notice">Переводы на адреса других пользователей в CRYPTOLIDE мгновенны и полностью бесплатны, поэтому рекомендуем использовать именно их.</div>
						</div>
						<div class="form-withdrawal-bitcoin__form-item form-withdrawal-bitcoin__form-item_send-amount">
							<label for="send_amount" class="form-withdrawal-bitcoin__form-label">Отправляемая сумма в {{ $crypto_currency->short }}:</label>
							<div class="form-withdrawal-bitcoin__form-control">
								<div class="form-withdrawal-bitcoin__form-control-wrap">
									<input type="text" class="form-text form-withdrawal-bitcoin__form-text" name="amount" placeholder="{{ auth()->user()->balance[$crypto_currency->short] }}" id="send_amount">
									<span class="form-withdrawal-bitcoin__form-control-suffix">{{ $crypto_currency->short }}</span>
								</div>
								<span class="form-withdrawal-bitcoin__form-control-note">Максимальная сумма: <a class="max_amount"><span>{{ auth()->user()->balance[$crypto_currency->short] }}</span> {{ $crypto_currency->short }}</a></span>
							</div>
						</div>
						<div class="form-withdrawal-bitcoin__form-item" style="display: none;" id="amount-commission">
							<label for="current_commission" class="form-withdrawal-bitcoin__form-label">Текущая комиссия сети:</label>
							<div class="form-withdrawal-bitcoin__form-control"><span class="commission">{{ $crypto_currency->commission_output }}</span> {{ $crypto_currency->short }}</div>
						</div>
						<div class="form-withdrawal-bitcoin__form-item form-withdrawal-bitcoin__form-item_amount-receivable" style="display: none;" id="amount-with-commission">
							<label for="amount_receivable" class="form-withdrawal-bitcoin__form-label">Сумма {{ $crypto_currency->short }} к получению:</label>
							<div class="form-withdrawal-bitcoin__form-control">
								<div class="form-withdrawal-bitcoin__form-control-wrap">
									<input type="text" class="form-text form-withdrawal-bitcoin__form-text" name="amount_receivable" placeholder="{{ auth()->user()->balance[$crypto_currency->short] }}" id="amount_receivable">
									<span class="form-withdrawal-bitcoin__form-control-suffix">{{ $crypto_currency->short }}</span>
								</div>
							</div>
						</div>
					</div>
					<div class="transaction-error2 information-block__transaction-error2" id="error-block" style="display: none;">

					</div>

					<div class="form-withdrawal-bitcoin__form-actions" id="withdrawal-actions">
						<div class="form-withdrawal-bitcoin__form-items" id="confirmation-block" style="display: none;">
							<div class="form-withdrawal-bitcoin__form-item form-withdrawal-bitcoin__form-item_amount-receivable">
								<label class="form-withdrawal-bitcoin__form-label">
									@if (auth()->user()->google_fa)
									Код подтверждения 2FA:
									@else
									Код подтверждения из E-mail:
									@endif
								</label>
								<div class="form-withdrawal-bitcoin__form-control">
									<div class="form-withdrawal-bitcoin__form-control-wrap">
										<input type="text" class="form-text form-withdrawal-bitcoin__form-text" name="confirmation_code" placeholder="000000">
									</div>
								</div>
							</div>
						</div>
						<input type="submit" class="btn-green2 form-withdrawal-bitcoin__form-submit" id="confirm_button" value="Подтвердить вывод">
						<br>
						<a style="display: none;" id="cancel-withdrawal" href="/my-wallet">Отменить</a>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection

@section('scripts')
<script>
	$(function () {
		let _notice = $('#withdrawal-notice').text();
		let _confirm_button = $('#confirm_button').val();

		let commission = Number($('.commission').text());

		$('.max_amount').on('click', function() {
			let amount = $(this).find('span').text();

			$('[name="amount"]').val(amount).trigger('keyup');
		});

		$('[name="address"]').on('keyup change', function() {
			let address = $(this).val();
			if (address.length !== 34) {
				updateNotice(_notice);
				return;
			}

			$.post('/is-address-has-a-user', {
				address: address
			}).done(function(response) {
				if (!response.status) {
					updateNotice(_notice);
				} else {
					let notice = 'Введенный адрес принадлежит пользователю ' + response.username + '. Все переводы на внутренние адреса других пользователей полностью бесплатны, поэтому комиссия за перевод отсутствует.';
					updateNotice(notice, 'none');
				}
			}).fail(function () {
				updateNotice(_notice);
			});
		});

		function updateNotice(notice, display = '') {
			$('#withdrawal-notice').text(notice);
			$('#amount-with-commission').css('display', display);
			$('#amount-commission').css('display', display);
		}

		$('[name="amount"]').on('keyup change', function() {
			let amount_receivable;

			amount_receivable = Number($(this).val()) - commission;

			if (amount_receivable < 0) {
				$('[name="amount_receivable"]').val('0.00000000');
			} else {
				$('[name="amount_receivable"]').val(amount_receivable.toFixed(8));
			}
		});

		$('[name="amount_receivable"]').on('keyup', function() {
			let amount;

			amount = Number($(this).val()) + commission;

			if (amount < 0) {
				$('[name="amount"]').val('0.00000000');
			} else {
				$('[name="amount"]').val(amount.toFixed(8));
			}
		});
		let _error = $('#error-block');

		function showError(message) {
			_error.css('display', '');
			_error.text(message);
		}
		
		$('form').on('submit', function(event) {
			event.preventDefault();
			$('#cancel-withdrawal').css('display', 'none');
			_error.css('display', 'none');
			let confirmation_block = $('#confirmation-block');
			let confirmation_code = $('[name="confirmation_code"]');

			if (confirmation_block.css('display') === 'block' && confirmation_code.val() === '') {
				showError('Не указан код подтверждения.');
				$('#cancel-withdrawal').css('display', '');
				return;
			}

			let form = $(this);
			$('#confirm_button').val('Подождите...').prop('disabled', true);
			$.post(form.attr('action'), form.serialize()).done(function(response) {
				if (response.status === true) {
					if ($('#amount-commission').css('display') == 'none') {
						$('#withdrawal-actions').html('<h3 style="color: green;" class="alert-finished">Вы успешно отправили <span class="alert-amount">' + $('[name="amount"]').val() + '</span> {{ $crypto_currency->short }} на адрес: <span class="alert-address">' + $('[name="address"]').val() + '</span></h3>');
					} else {
						$('#withdrawal-actions').html('<h3 style="color: green;" class="alert-finished">Вы успешно отправили <span class="alert-amount">' + $('[name="amount_receivable"]').val() + '</span> {{ $crypto_currency->short }} на адрес: <span class="alert-address">' + $('[name="address"]').val() + '</span></h3>');
					}

					return;
				}

				showError(response.message);
				if (response.status === 'need_code') {
					confirmation_block.css('display', '');
					$('[name="amount"],[name="amount_receivable"],[name="address"]').prop('readonly', true);
					$('#cancel-withdrawal').css('display', '');
				}

				if (response.status === 'incorrect_code') {
					confirmation_code.val('');
					$('#cancel-withdrawal').css('display', '');
				}

				if (response.status === 'incorrect_amount') {
					$('#cancel-withdrawal').css('display', '');
				}
			}).fail(function(err) {
				showError(err.responseJSON.errors[Object.keys(err.responseJSON.errors)[0]][0]);
			}).always(function() {
				$('#confirm_button').val(_confirm_button).prop('disabled', false);
			});
		});
	});
</script>
@endsection