<div class="header-wrapper">
	<div class="header header_frontpage">
		<div class="container">
			<div class="row">
				<div class="header__left pull-left">
					<a href="/" title="На главную" class="header__logo">
						<svg id="Слой_1" width="146" data-name="Слой 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 125.34 20"><defs><style>.cls-1{fill:#fff;}</style></defs><title>Лго - вектор</title><g id="cry"><path class="cls-1" d="M96.91,18.49H88.57v-14h3.15V15.93h5.19Z" transform="translate(-2.93 -1.44)"/><path class="cls-1" d="M101.94,18.49H98.79v-14h3.15Z" transform="translate(-2.93 -1.44)"/><path class="cls-1" d="M105.13,18.49v-14h5q7.45,0,7.46,6.83a6.88,6.88,0,0,1-2,5.22,7.49,7.49,0,0,1-5.42,1.95Zm3.15-11.43v8.87h1.56a4.23,4.23,0,0,0,3.22-1.23,4.65,4.65,0,0,0,1.17-3.35,4.22,4.22,0,0,0-1.16-3.14,4.38,4.38,0,0,0-3.24-1.15Z" transform="translate(-2.93 -1.44)"/><path class="cls-1" d="M128.27,18.49h-8.4v-14H128V7.06H123v3.11h4.58v2.56H123v3.2h5.24Z" transform="translate(-2.93 -1.44)"/></g><path class="cls-1" d="M13.85,18a9.32,9.32,0,0,1-4,.74,6.72,6.72,0,0,1-5.06-1.9,6.92,6.92,0,0,1-1.85-5.05A7.39,7.39,0,0,1,5,6.34a7.25,7.25,0,0,1,5.38-2.09,10.21,10.21,0,0,1,3.46.52v3A6.14,6.14,0,0,0,10.65,7,4.25,4.25,0,0,0,7.46,8.21a4.6,4.6,0,0,0-1.21,3.36,4.54,4.54,0,0,0,1.14,3.24A4.05,4.05,0,0,0,10.47,16a6.55,6.55,0,0,0,3.38-.9Z" transform="translate(-2.93 -1.44)"/><path class="cls-1" d="M28,18.49H24.41l-2.17-3.6c-.17-.27-.32-.52-.47-.73a3.26,3.26,0,0,0-.46-.55,1.9,1.9,0,0,0-.47-.36,1.26,1.26,0,0,0-.55-.12h-.85v5.36H16.29v-14h5q5.1,0,5.1,3.81a3.9,3.9,0,0,1-.23,1.35,3.85,3.85,0,0,1-.63,1.12,4.32,4.32,0,0,1-1,.86,4.91,4.91,0,0,1-1.3.56v0a2.11,2.11,0,0,1,.61.32,5.27,5.27,0,0,1,.56.51,8,8,0,0,1,.53.63l.45.66ZM19.44,6.85v3.9h1.37a2.26,2.26,0,0,0,1.63-.59,1.94,1.94,0,0,0,.63-1.47c0-1.23-.74-1.84-2.2-1.84Z" transform="translate(-2.93 -1.44)"/><path class="cls-1" d="M39.9,4.49l-4.56,9v5H32.19V13.57L27.74,4.49h3.61L33.6,9.71c0,.1.15.48.33,1.15h0a4.8,4.8,0,0,1,.3-1.11l2.29-5.26Z" transform="translate(-2.93 -1.44)"/><path class="cls-1" d="M44.64,13.67v4.82H41.48v-14h5q5.3,0,5.29,4.46a4.3,4.3,0,0,1-1.52,3.42,6.05,6.05,0,0,1-4.06,1.3Zm0-6.76v4.37h1.24c1.68,0,2.52-.74,2.52-2.21s-.84-2.16-2.52-2.16Z" transform="translate(-2.93 -1.44)"/><path class="cls-1" d="M63.6,7.06h-4V18.49H56.45V7.06h-4V4.49H63.6Z" transform="translate(-2.93 -1.44)"/><path class="cls-1" d="M72.16,16.44h5.17v2H72.16a6,6,0,0,1-6-6v-5l2,2v3A4,4,0,0,0,72.16,16.44Z" transform="translate(-2.93 -1.44)"/><polygon class="cls-1" points="78 20 78 17 74.25 17 74.25 15 78 15 78 12 82 16 78 20"/><polygon class="cls-1" points="67.23 3 74.4 3 74.4 5 67.23 5 67.23 8 63.23 4 67.23 0 67.23 3"/><path class="cls-1" d="M85,10.44v5l-2-2v-3a4,4,0,0,0-4-4H77.25v-2H79A6,6,0,0,1,85,10.44Z" transform="translate(-2.93 -1.44)"/></svg>
					</a>
					<ul class="header__menu">
						<li class="header__menu-item"><a href="/p2p?crypto_id=1&sum_currency=&currency_id=155&payment_type_id=&country_id=206&city_id=" class="header__menu-link">Торговая платформа</a></li>
						<li class="header__menu-item"><a href="/wallet" class="header__menu-link">Кошелёк</a></li>
						<li class="header__menu-item"><a href="/affiliate-program" class="header__menu-link">Заработок</a></li>
						<li class="header__menu-item header__menu-item_other">
							<a href="#" class="header__menu-link">Другое</a>
							<ul class="header__submenu">
								<li class="header__submenu-item"><a href="/about-company" class="header__submenu-link">О компании</a></li>
								<li class="header__submenu-item"><a href="/news" class="header__submenu-link">Новости</a></li>
								<li class="header__submenu-item"><a href="/commissions" class="header__submenu-link">Комиссии и лимиты</a></li>
								<li class="header__submenu-item"><a href="/nocommission" class="header__submenu-link">Отключение комиссии</a></li>
	              <li class="header__submenu-item"><a href="/remote-work" class="header__submenu-link">Удалённая работа</a></li>
	              <li class="header__submenu-item"><a href="/personalbonuses" class="header__submenu-link">Персональные бонусы</a></li>
								<li class="header__submenu-item"><a href="/representatives" class="header__submenu-link">Представители</a></li>
								<li class="header__submenu-item"><a href="/security" class="header__submenu-link">Безопасность</a></li>
								<li class="header__submenu-item"><a href="/help" class="header__submenu-link">Помощь</a></li>
								<li class="header__submenu-item"><a href="/contacts" class="header__submenu-link">Контакты</a></li>
							</ul>
						</li>
					</ul>
				</div>
				<div class="header__right pull-right">
					<ul class="menu-login header__menu-login">
						@if (!auth()->check())
						<li class="menu-login__item"><a href="/login" class="menu-login__link">Войти</a></li>
						<li class="menu-login__item menu-login__item_register-frontpage"><a href="/register" class="menu-login__link">Зарегистрироваться</a></li>
						@else
						<li class="menu-login__item menu-login__item_register-frontpage"><a href="/index" class="menu-login__link">Личный кабинет</a></li>
						@endif
					</ul>
		      <button type="button" class="drawer-toggle drawer-hamburger">
			      <span class="sr-only">toggle navigation</span>
			      <span class="drawer-hamburger-icon"></span>
			    </button>
			    <nav class="drawer-nav" role="navigation">
			      <ul class="mobile-menu">
			        <li class="mobile-menu__item"><a class="mobile-menu__link" href="/login">Войти</a></li>
			        <li class="mobile-menu__item"><a class="mobile-menu__link" href="/register">Зарегистрироваться</a></li>
			      </ul>
			      <ul class="mobile-menu">
			        <li class="mobile-menu__item"><a class="mobile-menu__link" href="/p2p">Купить криптовалюту</a></li>
			        <li class="mobile-menu__item"><a class="mobile-menu__link" href="/cryptosell">Продать криптовалюту</a></li>
			        <li class="mobile-menu__item"><a class="mobile-menu__link" href="/cryptoexchange">Обменять криптовалюту</a></li>
			        <li class="mobile-menu__item"><a class="mobile-menu__link" href="/wallet">Открыть кошелёк</a></li>
			      </ul>
			      <ul class="mobile-menu">
			        <li class="mobile-menu__item"><a class="mobile-menu__link" href="/about-company">О компании</a></li>
			        <li class="mobile-menu__item"><a class="mobile-menu__link" href="/news">Новости</a></li>
			        <li class="mobile-menu__item"><a class="mobile-menu__link" href="/commissions">Комиссии и лимиты</a></li>
			        <li class="mobile-menu__item"><a class="mobile-menu__link" href="/nocommission">Отключение комиссии</a></li>
              <li class="mobile-menu__item"><a href="/remote-work" class="mobile-menu__link">Удалённая работа</a></li>
              <li class="mobile-menu__item"><a href="/affiliate-program" class="mobile-menu__link">Пассивная прибыль</a></li>
              <li class="mobile-menu__item"><a href="/personalbonuses" class="mobile-menu__link">Персональные бонусы</a></li>
			        <li class="mobile-menu__item"><a class="mobile-menu__link" href="/representatives">Представители</a></li>
			        <li class="mobile-menu__item"><a class="mobile-menu__link" href="/security">Безопасность</a></li>
			        <li class="mobile-menu__item"><a class="mobile-menu__link" href="/help">Помощь</a></li>
			        <li class="mobile-menu__item"><a class="mobile-menu__link" href="/contacts">Контакты</a></li>
			      </ul>
			    </nav>
				</div>
			</div>
		</div>
	</div>
	<div class="welcome">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<div class="welcome__content">
						<h1 class="welcome__title">Добро пожаловать в Криптолайд!</h1>
						<div class="welcome__text">CRYPTOLIDE - удобный международный сервис, с помощью которого пользователи продают и покупают друг у друга Bitcoin и другую криптовалюту в один клик, без посредников и комиссий.</div></div>
					</div>
				</div>
			</div>
		<!-- <div class="welcome__video">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<a href="#reviews" class="welcome__video-link ancor">Видео отзывы о нас</a>
					</div>
				</div>
			</div>
		</div> -->
	</div>
	<div class="your-preferences">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<h2 class="your-preferences__title">Что вам нравится больше?</h2>
					<div class="your-preferences__buttons">
						<a href="/p2p" class="btn-treaty your-preferences__btn">Покупать и продавать без комиссий</a>
						<span class="your-preferences__btn-separator">или</span>
						<a href="/wallet" class="btn-trade your-preferences__btn">Использовать защищённый кошелёк</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>