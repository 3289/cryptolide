<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> -->
    <meta name="description" content="">
    <meta name="author" content="skattex">
    <title>Админ панель</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="https://carlosroso.com/notyf/notyf.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="/css/admin.css">
    <style>
        body {
            padding-top: 5rem;
        }
        .notyf-container {
            z-index: 9999999;
        }
        .select2-container {
            display: block;
            width: 100%!important;
        }
        li.active a {
            color: white;
        }

        li a:hover {
            text-decoration: none;
            color: #17a2b8;
        }

        li.active a:hover {
            color: white;
        }

        li a {
            color: black;
        }

        .list-group-item.active {
            background-color: #17a2b8;
            border-color: #17a2b8;
        }
    </style>
</head>
<body>
<nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
    <a class="navbar-brand" href="/admin/users">CRYPTOLIDE</a>
</nav>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-2">
            <ul class="list-group" style="margin-bottom: 10px;">
                <li class="list-group-item{{ request()->path() == 'admin/users' ? ' active' : '' }}">
                    <a href="/admin/users">Пользователи</a>
                </li>
                <li class="list-group-item{{ request()->path() == 'admin/achievements' ? ' active' : '' }}">
                    <a href="/admin/achievements">Награды</a>
                </li>
                <li class="list-group-item{{ request()->path() == 'admin/countries_and_currencies' ? ' active' : '' }}">
                    <a href="/admin/countries_and_currencies">Страны и валюты</a>
                </li>
                <li class="list-group-item{{ request()->path() == 'admin/banks' ? ' active' : '' }}">
                    <a href="/admin/banks">Банки</a>
                </li>
                <li class="list-group-item{{ request()->path() == 'admin/payment_types_and_risks' ? ' active' : '' }}">
                    <a href="/admin/payment_types_and_risks">Способы оплаты и риски</a>
                </li>
                <li class="list-group-item{{ request()->path() == 'admin/addresses' ? ' active' : '' }}">
                    <a href="/admin/addresses">Адреса криптовалют</a>
                </li>
                <li class="list-group-item{{ request()->path() == 'admin/operations' ? ' active' : '' }}">
                    <a href="/admin/operations">История операций</a>
                </li>
                <li class="list-group-item{{ request()->path() == 'admin/replenishment' ? ' active' : '' }}">
                    <a href="/admin/replenishment">Ручное пополнение</a>
                </li>
                <li class="list-group-item{{ request()->path() == 'admin/withdrawals' ? ' active' : '' }}">
                    <a href="/admin/withdrawals">Заявки на вывод средств</a>
                </li>
                <li class="list-group-item{{ request()->path() == 'admin/additional_crypto_currencies' ? ' active' : '' }}">
                    <a href="/admin/additional_crypto_currencies">Дополнительные криптовалюты</a>
                </li>
                <li class="list-group-item{{ request()->path() == 'admin/commissions' ? ' active' : '' }}">
                    <a href="/admin/commissions">Комиссии</a>
                </li>
                <li class="list-group-item{{ request()->path() == 'admin/profit' ? ' active' : '' }}">
                    <a href="/admin/profit">Доход сайта</a>
                </li>
                <li class="list-group-item{{ request()->path() == 'admin/deals' ? ' active' : '' }}">
                    <a href="/admin/deals">Сделки</a>
                </li>
                <li class="list-group-item{{ request()->path() == 'admin/verification' ? ' active' : '' }}">
                    <a href="/admin/verification">Верификация телефона</a>
                </li>
                <li class="list-group-item{{ request()->path() == 'admin/notices' ? ' active' : '' }}">
                    <a href="/admin/notices">Замечания пользователям</a>
                </li>
            </ul>
        </div>
        <div class="col-md-10">
            @yield('content')
        </div>
    </div>
</div>
@yield('modals')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://carlosroso.com/notyf/notyf.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/js/select2.min.js"></script>
<script>
    let notyf = new Notyf();
    $(function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            }
        });
    });
</script>
@yield('scripts')
</body>
</html>
