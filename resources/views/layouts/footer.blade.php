<div class="footer">
  <div class="footer__top">
    <div class="container">
      <div class="row">
        <div class="footer__block footer__block_main-menu col-xs-2">
          <h3 class="footer__block-title">Главное</h3>
          <ul class="footer__menu">
            <li class="footer__menu-item footer__menu-item_active"><a href="/p2p"
              class="footer__menu-link">Торговая
            платформа</a></li>
            <li class="footer__menu-item"><a href="/wallet" class="footer__menu-link">Защищённый
            кошелёк</a></li>
          </ul>
        </div>
        <div class="footer__block footer__block_menu-company col-xs-2">
          <h3 class="footer__block-title">Компания</h3>
          <ul class="footer__menu">
            <li class="footer__menu-item"><a href="/about-company" class="footer__menu-link">О нас</a>
            </li>
            <li class="footer__menu-item"><a href="/news" class="footer__menu-link">Новости</a></li>
            <li class="footer__menu-item"><a href="/security" class="footer__menu-link">Безопасность</a>
            </li>
            <li class="footer__menu-item"><a href="/contacts" class="footer__menu-link">Контакты</a>
            </li>
          </ul>
        </div>
        <div class="footer__block footer__block_menu-user col-xs-2">
          <h3 class="footer__block-title">Пользователям</h3>
          <ul class="footer__menu">
            <li class="footer__menu-item"><a href="/commissions" class="footer__menu-link">Комиссии и
            лимиты</a></li>
            <li class="footer__menu-item"><a href="/nocommission" class="footer__menu-link">Отключение комиссии</a></li>
            <li class="footer__menu-item"><a href="/affiliate-program" class="footer__menu-link">Пассивная прибыль</a></li>
            <li class="footer__menu-item"><a href="/remote-work" class="footer__menu-link">Удалённая работа</a></li>
            <li class="footer__menu-item"><a href="/personalbonuses" class="footer__menu-link">Персональные бонусы</a></li>
            <li class="footer__menu-item"><a href="/representatives" class="footer__menu-link">Представители</a>
            </li>
            <li class="footer__menu-item"><a href="/help" class="footer__menu-link">Помощь</a></li>
          </ul>
        </div>
        <div class="footer__block footer__block_lastnews col-xs-3">
          <h3 class="footer__block-title">Последние новости</h3>
          <div class="footer__block-content">
            <a class="twitter-timeline" data-tweet-limit="1" data-width="240"
            data-chrome="noheader,nofooter" data-height="220" data-theme="light"
            data-link-color="#2B7BB9" href="https://twitter.com/CRYPTOLIDEcom"></a>
            <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
          </div>
        </div>
        <div class="footer__block footer__block_contacts col-xs-3">
          <h3 class="footer__block-title">Контакты</h3>
          <!-- Кнопка в любом месте страницы -->
          <!-- <button onclick="Chatra('openChat', true)">Открыть чат</button> -->
          <button onclick="Chatra('openChat', true)" class="chat-link footer__chat-link">Помощь онлайн
          </button>
          <!-- <a href="http://siteheart.com/webconsultation/885425" target="siteheart_window" class="chat-link footer__chat-link" onclick="o=window.open('http://siteheart.com/webconsultation/885425?', 'siteheart_window', 'width=550,height=400,top=30,left=30,resizable=yes'); return false;">Помощь онлайн</a> -->

          <ul class="footer__contacts">
            {{--<li class="footer__contacts-item footer__contacts-item_viber">+380509002282</li>--}}
            {{--<li class="footer__contacts-item footer__contacts-item_phone">+380509002282</li>--}}
            {{--<li class="footer__contacts-item footer__contacts-item_skype">live:cryptolide</li>--}}
            {{--<li class="footer__contacts-item footer__contacts-item_vk">vk.com/cryptolide</li>--}}
            <li class="footer__contacts-item footer__contacts-item_email">info@cryptolide.com</li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <div class="footer__bottom">
    <div class="container">
      <div class="row">
        <div class="footer__copyright">CRYPTOLIDE – Удобная торговая платформа и надёжный криптовалютный
          кошелёк! © 2017 - 2018. Все права защищены.
        </div>
      </div>
    </div>
  </div>
</div>