<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=1170px">
  {!! isset($viewport) ? '<meta name="viewport" content="width=device-width, initial-scale=1.0">' : '' !!}

  <title>CRYPTOLIDE {{ isset($title) ? '| ' . $title : '' }}</title>
  <link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="/js/jquery.formstyler/jquery.formstyler.css">
  <link rel="stylesheet" href="/js/jquery.formstyler/jquery.formstyler.theme.css">
  <link rel="stylesheet" href="/js/chosen/chosen.min.css">
  <link rel="stylesheet" href="/css/search.css">
  <link rel="stylesheet" href="/css/croppie.css">
  <link rel="stylesheet" href="/css/drawer.min.css">
  <link rel="stylesheet" href="/css/main.css">
  <link rel="stylesheet" href="/css/main3.css">
  <link rel="shortcut icon" href="/favicon.ico">
  <meta name="csrf-token" content="{{ csrf_token() }}">  
<!-- Chatra {literal} -->
<style>
  #chatra:not(.chatra--expanded) {
    visibility: hidden !important;
    opacity: 0 !important;
    pointer-events: none;
  }
</style>
<script>
    (function(d, w, c) {
        w.ChatraID = 'LCXCXxBxQhf47p7T4';
        var s = d.createElement('script');
        w[c] = w[c] || function() {
            (w[c].q = w[c].q || []).push(arguments);
        };
        s.async = true;
        s.src = (d.location.protocol === 'https:' ? 'https:': 'http:')
        + '//call.chatra.io/chatra.js';
        if (d.head) d.head.appendChild(s);
    })(document, window, 'Chatra');
</script>
<!-- /Chatra {/literal} -->
</head>
<body class="drawer drawer--right">
  <div class="wrapper">
    <div class="main {{ $className ?? '' }}">
      @includeWhen(request()->path() === '/', 'layouts.header_frontpage')
      @includeWhen(request()->path() !== '/', 'layouts.header')
      @if (auth()->check() && auth()->user()->notices->isNotEmpty())
      <div class="block-confirmation-email">
        <div class="container">
          <div class="row">
            <div class="col-xs-12">
              @foreach(auth()->user()->notices as $notice)
              <div class="block-confirmation-email__inner">
                <span class="confirmation-email-caption">{{ $notice->text }}</span>
              </div>
              @endforeach
            </div>
          </div>
        </div>
      </div>
      @endif
      @if (auth()->check() && !auth()->user()->activated)
      <div class="block-confirmation-email">
        <div class="container">
          <div class="row">
            <div class="col-xs-12">
              <div class="block-confirmation-email__inner">
                <span class="confirmation-email-caption">У вас <strong>не подтвержден</strong> E-mail. Пожалуйста, подтвердите его!</span>
                @if (session('email-status'))
                {{ session('email-status') }}
                @elseif (auth()->user()->activations_count >= 3 && auth()->user()->last_activation_time > (time()-60*5))
                Повторная отправка письма будет доступна через 5 минут.
                @elseif (auth()->user()->activations_count > 0 && auth()->user()->last_activation_time > (time()-60))
                Повторная отправка письма будет доступна через минуту.
                @else
                <a href="/send-email-confirmation" class="btn-white">Отправить письмо снова.</a>
                @endif
              </div>
            </div>
          </div>
        </div>
      </div>
      @endif
      @if(session()->has('admin_id') && session()->has('user_id'))
      <div class="block-confirmation-email">
        <div class="container">
          <div class="row">
            <div class="col-xs-12">
              <span>Вы вошли в аккаунт пользователя <strong>как админ</strong>. Нажмите <a
                href="{{ url('/user/'.session('admin_id').'/auth') }}">сюда</a> чтобы вернуться обратно.</span>
              </div>
            </div>
          </div>
        </div>
        @endif
        @yield('content')
      </div>
      @includeWhen(request()->path() === '/', 'layouts.footer_frontpage')
      @includeWhen(request()->path() !== '/', 'layouts.footer')
    </div>
    @yield('modals')

    <div class="modal fade" id="modal-error" tabindex="-1">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <span class="close" data-dismiss="modal"></span>
            <h4>Создание объявления</h4>
          </div>
          <div class="modal-body">
            <div class="login-error">Чтобы разместить объявление Вам <br>необходимо <a href="/login">войти</a> или
              <a href="/register">зарегистрироваться</a>. <br> Это очень быстро и совершенно бесплатно!
            </div>
            <a href="#" data-dismiss="modal" class="btn-close">Закрыть окно</a>
          </div>
        </div>
      </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"
    integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/iScroll/5.2.0/iscroll.js"></script>

    <script src="/js/review.js"></script>

    <script src="/bootstrap/js/bootstrap.min.js"></script>
    <script src="/js/jquery.formstyler/jquery.formstyler.min.js"></script>
    <script src="/js/chosen/chosen.jquery.min.js"></script>
    <!--script src="/js/search/search.js"></script-->
    <!--script-- src="/js/search/search2.js"></script-->
    <script src="/js/operationTypeChange.js"></script>
    <script src="/js/custom.js"></script>
    <script src="/js/x-search.js"></script>
    <script src="/js/drawer.min.js"></script>
    {{--<script src="/js/numberFields.js"></script>--}}
    @yield('scripts')
    @if(auth()->check())
    <script src="{{ asset('/js/notifications.js') }}"></script>

    <script>
      $(".action-table__action-type").on('click', function () {
        $(this).children(".action-type_dropdown").toggle();

      });
      $(".action-table__action-type").mouseleave(function () {
        $(".action-type_dropdown").hide();

      })
    </script>
    @endif
  </body>
  </html>