<div class="header {{ auth()->check()?'header_profile':'header_login' }}">
  <div class="container">
    <div class="row">
      <div class="header__left left">
        <a href="/" title="На главную" class="header__logo"><img src="/images/content/logo.png"
         alt="Логотип"></a>
         <ul class="header__menu">
          <li class="header__menu-item header__menu-item_trading-platform"><a href="/p2p?crypto_id=1&sum_currency=&currency_id=155&payment_type_id=&country_id=206&city_id="
            class="header__menu-link">Торговая
          платформа</a></li>
          <li class="header__menu-item header__menu-item_other">
            <a href="#" class="header__menu-link">Другое</a>
            <ul class="header__submenu">
              <li class="header__submenu-item"><a href="/wallet" class="header__submenu-link">Защищённый кошелёк</a></li>
              <li class="header__submenu-item"><a href="/about-company" class="header__submenu-link">О
              компании</a></li>
              <li class="header__submenu-item"><a href="/news" class="header__submenu-link">Новости</a>
              </li>
              <li class="header__submenu-item"><a href="/affiliate-program" class="header__submenu-link">Заработок</a>
              </li>
              <li class="header__submenu-item"><a href="/commissions" class="header__submenu-link">Комиссии
              и лимиты</a></li>
              <li class="header__submenu-item"><a href="/nocommission" class="header__submenu-link">Отключение комиссии</a></li>
              <li class="header__submenu-item"><a href="/remote-work" class="header__submenu-link">Удалённая работа</a></li>
              <li class="header__submenu-item"><a href="/personalbonuses" class="header__submenu-link">Персональные бонусы</a></li>
              <li class="header__submenu-item"><a href="/representatives" class="header__submenu-link">Представители</a>
              </li>
              <li class="header__submenu-item"><a href="/security" class="header__submenu-link">Безопасность</a>
              </li>
              <li class="header__submenu-item"><a href="/help" class="header__submenu-link">Помощь</a>
              </li>
              <li class="header__submenu-item"><a href="/contacts"
                class="header__submenu-link">Контакты</a></li>
              </ul>
            </li>
            @if(!auth()->check())
            <li class="header__menu-item"><a href="/contacts" class="header__menu-link">Контакты</a></li>
            @endif
          </ul>
          <div id="nav-icon4">
            <span></span>
            <span></span>
            <span></span>
          </div>
        </div>
        @if(!auth()->check())
        <div class="header__right right">
          <ul class="menu-login header__menu-login">
            <li class="menu-login__item"><a href="/login" class="menu-login__link">Войти</a></li>
            <li class="menu-login__item menu-login__item_register"><a href="/register"
              class="menu-login__link">Зарегистрироваться</a>
            </li>
          </ul>
        </div>
        @else
        <div class="header__right right">
          <div class="profile-block header__profile-block">
            <div class="profile-block__balance">
              <div class="profile-block__balance-top">
                <div class="profile-block__bitcoins-numbers">{{ auth()->user()->balance['BTC'] }} BTC
                </div>
                <div class="profile-block__bitcoins-application">
                  + {{ auth()->user()->reserved_balance['BTC'] }} в сделках
                </div>
              </div>
              <div class="profile-block__other">
                <div class="profile-block__other-wrapper">
                  <ul class="profile-block__currencies-list">
                    <li class="profile-block__currencies-item">
                      <div class="profile-block__bitcoins-numbers">{{ auth()->user()->balance['LTC'] }}
                        LTC
                      </div>
                      <div class="profile-block__bitcoins-application">
                        + {{ auth()->user()->reserved_balance['LTC'] }} в сделках
                      </div>
                    </li>
                    <li class="profile-block__currencies-item">
                      <div class="profile-block__bitcoins-numbers">{{ auth()->user()->balance['DASH'] }}
                        DASH
                      </div>
                      <div class="profile-block__bitcoins-application">
                        + {{ auth()->user()->reserved_balance['DASH'] }} в сделках
                      </div>
                    </li>
                  </ul>
                  <div class="profile-block__balance-bottom">
                    <a href="/my-wallet" class="profile-block__wallet-link">Перейти в кошелёк</a>
                  </div>
                </div>
              </div>
            </div>
            <ul class="action-icons">
              <li class="action-icons__item action-icons__item_wallet">
                <a href="#" class="action-icons__link"><img src="/images/icons/icon_wallet.png" alt=""/></a>
                <div class="user-notifications user-notifications_wallet">
                  <div class="user-notifications__wrapper">
                    <div class="user-notifications__header clearfix">
                      <h3 class="user-notifications__title">Мой кошелёк</h3>
                      <a href="/my-wallet" class="user-notifications__section-link">Перейти в
                      раздел</a>
                    </div>
                    <div class="user-notifications__body stylized-scroll">
                      @if (auth()->user()->operations()->count() > 0)
                      @foreach(auth()->user()->operations()->limit(20)->get() as $operation)
                      @if ($operation->amount > 0)
                      <div class="notification-message notification-message_wallet clearfix">
                        <div class="notification-message__img"><img
                          src="/images/icons/icon_income_transaction.png"
                          alt="Аватар"></div>
                          <div class="notification-message__content">
                            <div class="notification-message__date">{{ $operation->created_at->format('d.m.Y в H:i') }}</div>
                            <div class="notification-message__text">Приход
                              <span class="notification-message__income">+{{ $operation->amount . ' ' . $operation->crypto_currency->short }}
                              .</span>
                              <br>
                              {!! $operation->notification ? $operation->notification : $operation->description !!}
                            </div>
                          </div>
                        </div>
                        @else
                        <div class="notification-message notification-message_wallet clearfix">
                          <div class="notification-message__img"><img
                            src="/images/icons/icon_cost_transaction.png"
                            alt="Аватар"></div>
                            <div class="notification-message__content">
                              <div class="notification-message__date">{{ $operation->created_at->format('d.m.Y в H:i') }}</div>
                              <div class="notification-message__text">Расход
                                <span class="notification-message__cost">{{ $operation->amount . ' ' . $operation->crypto_currency->short }}
                                .</span>
                                <br>
                                {!! $operation->notification ? $operation->notification : $operation->description !!}
                              </div>
                            </div>
                          </div>
                          @endif
                          @endforeach
                          @else
                          <div class="user-notifications__empty">У вас еще не было операций.</div>
                          @endif
                        </div>
                        <div class="user-notifications__footer"><a href="/my-wallet"
                         class="btn-blue btn-blue_block">Перейти
                       в мой кошелёк</a></div>
                     </div>
                   </div>
                   <span class="action-icons__new-notifications"
                   style="@if (($countOperations = auth()->user()->operations()->where('read', 0)->count()) <= 0) display: none @endif"
                   id="count-operations">{{$countOperations}}</span>
                 </li>
                 <li class="action-icons__item action-icons__item_partner">
                  <a href="#" class="action-icons__link"><img src="/images/icons/icon_partner.png" alt=""></a>
                  <div class="user-notifications">
                    <div class="user-notifications__wrapper">
                      <div class="user-notifications__header clearfix">
                        <h3 class="user-notifications__title">Мои партнёры</h3>
                        <a href="/my-partners" class="user-notifications__section-link">Перейти в
                        раздел</a>
                      </div>
                      <div class="user-notifications__body stylized-scroll">
                        @if (auth()->user()->partners->count() > 0)
                        @foreach(auth()->user()->partners as $partner)
                        <div class="notification-message clearfix">
                          <div class="notification-message__img"><img
                            src="{{ $partner->image }}" alt="Аватар"></div>
                            <div class="notification-message__content">
                              <div class="notification-message__date">{{ $partner->created_at->format('d.m.Y в H:i') }}</div>
                              <div class="notification-message__text">Пользователь <a
                                href="/profile/{{ $partner->login }}">{{ $partner->login }}</a> присоединился в <a href="/my-partners">вашу команду.</a>
                              </div>
                            </div>
                          </div>
                          @endforeach
                          @else
                          <div class="user-notifications__empty">Вы еще никого не пригласили.
                          </div>
                          @endif
                        </div>
                        <div class="user-notifications__footer"><a href="/my-partners"
                         class="btn-blue btn-blue_block">Перейти
                       к моим партнёрам</a></div>
                     </div>
                   </div>
                   <span class="action-icons__new-notifications"
                   style="@if (($countPartnerHistories = auth()->user()->partner_histories()->where('read', 0)->count()) <= 0) display: none @endif"
                   id="count-partner-history">{{ $countPartnerHistories }}</span>

                 </li>
                 <li class="action-icons__item action-icons__item_deals">
                  <a href="#" class="action-icons__link"><img src="/images/icons/icon_deals.png"
                    alt=""></a>
                    <div class="user-notifications user-notifications_deals">
                      <div class="user-notifications__wrapper">
                        <div class="user-notifications__header clearfix">
                          <h3 class="user-notifications__title">Мои сделки</h3>
                          <a href="/deals" class="user-notifications__section-link">Перейти в
                          раздел</a>
                        </div>
                        <div class="user-notifications__body stylized-scroll">
                          @if (auth()->user()->deal_histories->count() > 0)
                          @foreach(auth()->user()->deal_histories->take(10) as $deal_history)
                          <div class="notification-message notification-message_deals clearfix">
                            <div class="notification-message__img">
                              @if($deal_history->type == 'completed')
                              <img src="/images/icons/icon_successfull_transaction.png"
                              alt="Аватар">
                              @elseif($deal_history->type == 'paid')
                              <img src="/images/icons/icon_sending_money.png"
                              alt="Аватар">
                              @elseif($deal_history->type == 'new_message')
                              <img src="/images/icons/icon_new_message.png"
                              alt="Аватар">
                              @elseif($deal_history->type == 'opened')
                              <img src="{{ $deal_history->deal->partner->image }}"
                              alt="Аватар">
                              @elseif($deal_history->type == 'canceled_by_service')
                              <img src="/images/icons/icon_cancel_deal.png"
                              alt="Аватар">
                              @elseif($deal_history->type == 'canceled_by_user')
                              <img src="/images/icons/icon_cancel_deal_user.png"
                              alt="Аватар">
                              @endif
                            </div>
                            <div class="notification-message__content">
                              <div class="notification-message__date">{{ $deal_history->created_at->format('d.m.Y H:i') }}</div>
                              <div class="notification-message__text">
                                {!! $deal_history->notification ? $deal_history->notification : $deal_history->text !!}
                              </div>
                            </div>
                          </div>
                          @endforeach
                          @else
                          <div class="user-notifications__empty">Вы еще не заключили ни одной
                            сделки.
                          </div>
                          @endif
                        </div>
                        <div class="user-notifications__footer"><a href="/deals"
                         class="btn-blue btn-blue_block">Перейти
                       к моим сделкам</a></div>
                     </div>
                   </div>
                   <span class="action-icons__new-notifications"
                   style="@if (($countDealHistories = auth()->user()->deal_histories()->where('read', 0)->count()) <= 0) display: none @endif"
                   id="count-deal-history">{{ $countDealHistories }}</span>

                 </li>
               </ul>
               <div class="profile-block__photo-and-login">
                <div class="profile-block__photo"><img src="{{ auth()->user()->image }}" alt="Аватар"></div>
                <div class="profile-block__login">{{ auth()->user()->login }}</div>
                <div class="user-menu">
                  <div class="interest-rates">
                    <h4 class="interest-rates__title">Мои процентные ставки</h4>
                    <div class="interest-rates__items">
                      <div class="interest-rates__item">
                        <a href="/nocommission" class="interest-rates__percent-block interest-rates__percent-block_exchange">0%</a>
                        <div class="interest-rates__caption">{{ auth()->user()->exp_date }}</div>
                      </div>
                      <div class="interest-rates__item">
                        <a href="/my-partners" class="interest-rates__percent-block interest-rates__percent-block_partner">{{ auth()->user()->commission_partner }}%</a>
                        <div class="interest-rates__caption">неограниченно</div>
                      </div>
                    </div>
                  </div>
                  <ul class="user-menu__menu">
                    <li class="user-menu__item"><a href="/profile/{{ auth()->user()->login }}"
                     class="user-menu__link">Мой бизнес профиль</a></li>
                     <li class="user-menu__item user-menu__item_my-wallet"><a href="/my-wallet"
                       class="user-menu__link">Мой
                     кошелёк</a></li>
                     <li class="user-menu__item user-menu__item_my-partners"><a href="/my-partners"
                       class="user-menu__link">Мои
                     партнёры</a></li>
                     <li class="user-menu__item user-menu__item_deals"><a href="/deals"
                       class="user-menu__link">Мои сделки</a></li>
                     <li class="user-menu__item user-menu__item_publications"><a href="/publications"
                       class="user-menu__link">Мои объявления</a></li>
                     <li class="user-menu__item user-menu__item_bonuses"><a href="/mybonuses"
                       class="user-menu__link">Мои акции и бонусы <span class="user-menu__bonuses-number">3</span></a></li>
                     @if(auth()->user()->google_fa)
                     <li class="user-menu__item user-menu__item_security"><a href="/my-security"
                      class="user-menu__link">Безопасность:</a>
                      <span class="user-menu__security-level-high">Высокая</span></li>
                      @else
                      <li class="user-menu__item user-menu__item_security"><a href="/my-security"
                        class="user-menu__link">Безопасность:</a>
                        <span class="user-menu__security-level-low">Низкая</span></li>
                        @endif
                        <li class="user-menu__item user-menu__item_settings"><a href="/general-information"
                          class="user-menu__link">Мои
                        настройки</a></li>
                        <li class="user-menu__item user-menu__item_verification"><a href="/verification"
                          class="user-menu__link">Верификация</a>
                        </li>
                        <li class="user-menu__item user-menu__item_logout"><a href="/logout"
                          class="user-menu__link">Выйти</a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
              @endif
            </div>
          </div>
        </div>