@extends('layouts.app')

@title('О компании')

@className('main_infopage')

@section('content')

	<div class="content">
		<div class="infopage infopage_about-company">
			<div class="infopage__title-wrap">
				<div class="container">
					<div class="row">
						<div class="col-xs-12">
							<h1 class="infopage-title">О компании</h1>
						</div>
					</div>
				</div>
			</div>
			<div class="container">
				<div class="row">
					<div class="infopage__content">
						<div class="infopage-block infopage-block_about-company">
							<div class="infopage-block__text">
								<div class="infopage-block__logo"><img src="/images/content/logo_page_about.png" alt="Логотип"></div>
								<p>CRYPTOLIDE - Первый и единственный международный сервис, который объединил систему P2P сделок и защищённый <br>кошелёк, создав при этом, наверное, самую удобную площадку для покупки, продажи и обмена криптовалюты.</p>
								<p>Работа над проектом началась в 2015 году. Alpha запуск платформы состоялся в мае 2017 года, и в этом же месяце название компании было изменено на CRYPTOLIDE. После этого в августе был опубликован анонс официального релиза сервиса. В октябре 2017 года состоялся Beta запуск платформы, который позволял всем желающим присоединится к тестированию. В течение финального тестового периода, в котором приняло участие около 500 пользователей, были исправлены мелкие ошибки, а также учтены все просьбы и пожелания будущих клиентов. В марте 2018 года состоялся официальный запуск сервиса Криптолайд. В общей сложности, нам потребовалась около 3-х лет, чтобы предоставить вам действительно выгодный, удобный и современный сервис, который хотелось бы рекомендовать друзьям и знакомым.</p>
								<p>Концепция работы сервиса состоит из 5 важных факторов, которых постоянно и обязательно придерживается руководство, ведь они в сумме образуют именно ту идеальную систему, над разработкой которой команда CRYPTOLIDE так долго трудилась.</p>
								<p>Обязательные и неизменные факторы, которые заложены в основу CRYPTOLIDE:</p>
							</div>
							<div class="factors">
								<div class="factors__item clearfix">
									<div class="factors__left">
										<div class="factors__img"><img src="/images/content/factors_img1.png" alt="factors_1"></div>
									</div>
									<div class="factors__right">
										<h3 class="factors__title">Безопасность</h3>
										<div class="factors__text">Безопасность ваших аккаунтов и средств - основа и фундамент нашей компании, на котором построены все остальные функции. Мы придерживаемся мнения, что мощная защита - наиболее важная задача для любой уважающей себя компании. О том, какие алгоритмы защиты мы используем, подробно описано <a href="/security">здесь.</a></div>
									</div>
								</div>
								<div class="factors__item clearfix">
									<div class="factors__left">
										<div class="factors__img"><img src="/images/content/factors_img2.png" alt="factors_2"></div>
									</div>
									<div class="factors__right">
										<h3 class="factors__title">Идеология</h3>
										<div class="factors__text">Наша позиция очень проста: интересы сервиса всегда будут выше, чем вопросы заработка. Используя CRYPTOLIDE, вы всегда можете быть уверенными, что для каждого пользователя предоставляются действительно наиболее выгодные и интересные условия. <a href="/commissions">Проверьте</a>, и убедись в этом лично.</div>
									</div>
								</div>
								<div class="factors__item clearfix">
									<div class="factors__left">
										<div class="factors__img"><img src="/images/content/factors_img3.png" alt="factors_3"></div>
									</div>
									<div class="factors__right">
										<h3 class="factors__title">Искренняя любовь к клиентам</h3>
										<div class="factors__text">Наша цель - сделать все для того, чтобы каждый пользователь, не зависимо от оборота его средств и рейтинга, остался довольным и от всей души рекомендовал CRYPTOLIDE друзьям и знакомым. Мы отобрали наилучших сотрудников, которые разделяют наши взгляды, и круглосуточно будут готовы <a href="/contacts">ответить</a> на ваши вопросы.</div>
									</div>
								</div>
								<div class="factors__item clearfix">
									<div class="factors__left">
										<div class="factors__img"><img src="/images/content/factors_img4.png" alt="factors_4"></div>
									</div>
									<div class="factors__right">
										<h3 class="factors__title">Инновационность</h3>
										<div class="factors__text">Мы не из тех, кто, однажды создав продукт годами его не дорабатывает. Каждый день в мире происходят важные открытия, появляются новые способы взлома и защиты, разрабатываются алгоритмы, упрощаются задачи. Мы внимательно следим за всеми инновациями и постоянно улучшаем наш сервис.</div>
									</div>
								</div>
								<div class="factors__item clearfix">
									<div class="factors__left">
										<div class="factors__img"><img src="/images/content/factors_img5.png" alt="factors_5"></div>
									</div>
									<div class="factors__right">
										<h3 class="factors__title">Благодарность</h3>
										<div class="factors__text">Каждый пользователь, который любым образом способствовал развитию сервиса - будет вознаграждён. Для желающих продвигать сервис стандартными инструментами мы уже разработали уникальную <a href="/affiliate-program">партнёрскую программу</a>, в которой не зарегистрированные пользователи ищут и привлекают партнёров, а, наоборот, будущие пользователи сами ищут партнёрские ссылки, чтобы по них зарегистрироваться. Не стесняйтесь проявлять инициативу, ведь даже когда вы найдёте ошибку в тексте - вас уже ждет награда. Для профессионалов, отлично разбирающихся в криптовалютной тематике или просто настойчивых любителей, которые постоянно стремятся любыми возможными способами улучшить сервис - команда CRYPTOLIDE в виде награды приготовила пожизненное присвоение одного из <a href="/representatives">постоянных статусов</a>, или обсуждаемый размер прямого денежного перевода на ваши реквизиты. С нетерпением ждём любых ваших замечания и предложений.</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection