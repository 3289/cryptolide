@extends('layouts.app')

@title('Удалённая работа')

@className('main_infopage')

@section('content')
	<div class="content">
		<div class="infopage infopage_remote-work">
			<div class="infopage__title-wrap">
				<div class="container">
					<div class="row">
						<div class="col-xs-12">
							<h1 class="infopage-title">Удалённая работа в CRYPTOLIDE</h1>
						</div>
					</div>
				</div>
			</div>
			<div class="container">
				<div class="row">
					<div class="infopage__content">
						<!-- <div class="infopage-block infopage-block_tabs corner-bottom">
							<ul class="infopage-tabs">
								<li class="infopage-tabs__item infopage-tabs__item_affiliate-program"><a href="/affiliate-program" class="infopage-tabs__link">Партнёрская программа</a></li>
								<li class="infopage-tabs__item infopage-tabs__item_remote-work infopage-tabs__item_active"><a href="/remote-work" class="infopage-tabs__link">Удаленная работа</a></li>
							</ul>
						</div> -->
						<div class="infopage-block infopage-block_remote-work">
							<h2 class="infopage-block__title">Удалённая работа</h2>
							<div class="infopage-block__text">
								<p>CRYPTOLIDE предлагает каждому желающему работать удалённо и получать за это фиксированную оплату. Работа не требует постоянного присутствия на рабочем месте, но именно от того, насколько быстро вы будете справляться с поставленными задачами, будет зависеть ваша оплата за текущий рабочий день.</p>
								<p>Мы сможем предоставить постоянный стабильный заработок практически каждому желающему пользователю, даже без знаний криптовалютной тематики и без опыта работы. Всё что будет требоваться от вас - максимально оперативно и точно выполнять конкретно поставленные задачи и быть с интернетом на "ТЫ".</p>
								<p>Биржа работы - объемный портал, который будет интегрирован в CRYPTOLIDE и синхронизирован с <a href="/wallet">CRYPTOLIDE Wallet</a>. Это инновационная разработка, аналогов которой мы пока не встречали даже в зарубежных странах. Официальный запуск биржи работы запланирован на ноябрь 2018 года.</p>
								<p style="margin: 30px 0 45px; padding-right: 33px; text-align: center;"><img src="/images/content/remote_work.png" alt="Удалённая работа"></p>
								<p>Ожидайте, осталось ещё немного. Если у вас появились вопросы - с радостью на них <a href="/contacts">ответим</a>. <br><br>С уважением, команда CRYPTOLIDE.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection