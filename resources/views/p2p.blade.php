@extends('layouts.app')

@title('Купить криптовалюту')

@section('content')
<div class="content">
  <div class="operation-menu-container">
    <div class="container">
      <div class="row">
        <ul class="operation-menu">
          <li class="operation-menu__item operation-menu__item_buy{{ request()->is('p2p/sell*') ? ' operation-menu__item_active' : '' }}"><a href="/p2p" class="operation-menu__link">Купить криптовалюту</a></li>
          <li class="operation-menu__item operation-menu__item_sell{{ request()->is('p2p/buy*') ? ' operation-menu__item_active' : '' }}"><a href="/cryptosell" class="operation-menu__link">Продать криптовалюту</a></li>
          <li class="operation-menu__item operation-menu__item_exchange{{ request()->is('p2p/trade') ? ' operation-menu__item_active' : '' }}"><a href="/cryptoexchange" class="operation-menu__link">Обменять криптовалюту на другую</a></li>
          <li class="operation-menu__item operation-menu__item_create-ad">
            @if (!auth()->check())
            <a href="#" class="operation-menu__link" data-toggle="modal" data-target="#modal-error">Создать объявление</a>
            @else
            <a href="/publications/create" class="operation-menu__link">Создать объявление</a>
            @endif
          </li>
        </ul>
      </div>
    </div>
  </div>

  <div class="search-section">
    <div class="container">
      <div class="row">
        @if ($type != 'trade')
        <form action="" class="search-form">
          <div class="search-form__item search-form__item_payment-system">
            <select name="crypto_id" id="payment-system" class="search-form__select jqstyler">
              <option value="1" selected>Bitcoin</option>
            </select>
          </div>
          <div class="search-form__item search-form__item_sum">
            <input type="text" name="sum_currency" class="search-form__text" placeholder="Сумма: 0.00" value="{{ request('sum_currency') }}">
            <div class="select-wrapper">
              <div class="select-wrapper__input-block">
                <input type="text" class="search__input x-search" placeholder="UAH" data-target="#currency-list" autocomplete="off">
                <input type="hidden" name="currency_id" value="{{ request('currency_id', session('user-currency')) }}">
                <div class="arrow-block">
                  <i class="arrow down"></i>
                </div>
              </div>
              <ul class="search__list" style="display: none;" id="currency-list">
                @foreach($currencies as $currency)
                <li data-id="{{ $currency->id }}">{{ $currency->name }}</li>
                @endforeach
              </ul>
            </div>
          </div>
          <div class="search-form__item search-form__item_payment-method">
            <div class="select-wrapper">
              <div class="select-wrapper__input-block">
                <input id="payment_type_input" type="text" class="search__input x-search" placeholder="Все способы оплаты" data-target="#payment-list" autocomplete="off">
                <input type="hidden" name="payment_type_id" value="{{ request('payment_type_id') }}">
                <div class="arrow-block">
                  <i class="arrow down"></i>
                </div>
              </div>
              <ul class="search__list" style="display: none;" id="payment-list">
                <li data-id="">Все способы оплаты</li>
                @foreach($payment_types as $payment_type)
                <li data-id="{{ $payment_type->id }}">{{ $payment_type->name }}</li>
                @endforeach
              </ul>
            </div>
          </div>
          <div class="search-form__item search-form__item_country">
            <div class="select-wrapper">
              <div class="select-wrapper__input-block">
                <input type="text" class="search__input x-search" placeholder="Начните вводить название страны..." id="country-name" data-target="#country-list" autocomplete="off">
                <input type="hidden" name="country_id" value="{{ request('country_id', session('user-country')) }}">
                <div class="arrow-block">
                  <i class="arrow down"></i>
                </div>
              </div>
              <ul class="search__list" style="display: none;" id="country-list">
                @foreach($countries as $country)
                <li data-id="{{ $country->id }}"{{ request('country_id') == $country->id ? ' selected' : '' }}>{{ $country->name }}</li>
                @endforeach
              </ul>
            </div>
          </div>
          <div class="search-form__item search-form__item_cities-and-regions">
            <div class="select-wrapper">
              <div class="select-wrapper__input-block">
                <input type="text" placeholder="Начните вводить город..." id="city-name" class="city-search search__input" data-target="#city-list" autocomplete="off">
                <input type="hidden" name="city_id" value="{{ request('city_id') }}">
                <div class="arrow-block">
                  <i class="arrow down"></i>
                </div>
              </div>
              <ul class="search__list" style="display: none;" id="city-list">

              </ul>
            </div>
          </div>
          <div class="search-form__actions">
            <input type="submit" class="search-form__submit">
          </div>
        </form>
        @else
        <form action="" class="search-form search-form_exchange">

          <div class="search-form__item search-form__item_crypto-select">
            <label for="cryptocurrency-select" class="search-form__label">Отдаете:</label>
            <select name="crypto_trade_id" id="payment-system" class="search-form__select jqstyler">
              @foreach($crypto_trade_currencies as $crypto_trade)
              <option data-short="{{ $crypto_trade->short }}" value="{{ $crypto_trade->id }}"{{ request('crypto_trade_id') == $crypto_trade->id ? ' selected' : '' }}>{{ $crypto_trade->name }}</option>
              @endforeach
            </select>
          </div>
          <div class="search-form__item search-form__item_sum-exchange">
            <input type="text" name="sum_currency" class="search-form__text" style="width: 230px;" placeholder="Сумма: 0.00" value="{{ request('sum_currency') }}" autocomplete="off">
            <span class="search-form__suffix" id="crypto-amount-short"></span>
          </div>

          <div class="search-form__item search-form__item_payment-system-exchange">
            <label for="payment-system" class="search-form__label">Получаете:</label>
            <select name="crypto_id" id="payment-system" class="search-form__select jqstyler">
              @foreach($crypto_currencies as $crypto_currency)
              @if ($crypto_currency->name == 'Bitcoin')
              <option value="{{ $crypto_currency->id }}"{{ request('crypto_id') == $crypto_currency->id ? ' selected' : '' }}>{{ $crypto_currency->name }}</option>
              @endif
              @endforeach
            </select>
          </div>


          <div class="search-form__actions">
            <input type="submit" class="search-form__submit">
          </div>
        </form>
        @endif
      </div>
    </div>
  </div>

  <div class="action-tables">
    <div class="container">
      <div class="row">
        @includeWhen($type == 'buy_online', 'p2p.buy_online')
        {{--@includeWhen($type == 'buy_for_cash', 'p2p.buy_for_cash')--}}

        @includeWhen($type == 'sell_online', 'p2p.sell_online')
        {{--@includeWhen($type == 'sell_for_cash', 'p2p.sell_for_cash')--}}

        @includeWhen($type == 'trade', 'p2p.trade')

        {{ $adverts->appends(request()->all())->links('pagination.cryptolide') }}
      </div>
    </div>
  </div>
</div>
@endsection

@section('scripts')
<script>
  $(function() {
    $('.jqstyler').styler({
      onFormStyled: function () {
        $('.jq-selectbox').each(function (index, el) {
          var bgImg = $(this).find('.jq-selectbox__dropdown ul li.selected').css('background-image');
          $(this).find('.jq-selectbox__select-text').css('background-image', bgImg);
        });
      },
      onSelectClosed: function () {
        var bgImg = $(this).find('.jq-selectbox__dropdown ul li.selected').css('background-image');
        $(this).find('.jq-selectbox__select-text').css('background-image', bgImg);
      }
    });

    $('[name="sum_currency"]').on('keypress', function(e) {
      let th = $(this);
      setTimeout(function () {
        th.val(th.val().replace(',', '.'))
      }, 5);
      if(e.ctrlKey && k == 86) {
        return false;
      }

      if (e.shiftKey === true ) {
        if (e.which == 44 || e.which == 9) {
          if (th.val().indexOf('.') != -1) {
            return false;
          }
          return true;
        }
      }
      if (e.which == 46 || e.which == 44) {
        if (th.val().indexOf('.') != -1) {
          return false;
        }
        return true;
      }
      if (e.which > 57 || e.which < 48) {
        return false;
      }
      return true;
    });

    $('[name="sum_currency"]').on('keypress', function(e) {
      let th = $(this);
      setTimeout(function () {
        th.val(th.val().replace(',', '.'));
      }, 5);
    });

    $('[name="sum_currency"]').on('paste', function(event){
      event.preventDefault();
    });
  });
</script>
@endsection