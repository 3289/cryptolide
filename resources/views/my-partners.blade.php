@extends('layouts.app')

@title('Мои партнёры')

@className('main_my-partners')

@section('content')
	<div class="content">
		<div class="title-wrap title-wrap_my-partners">
			<div class="container">
				<div class="row">
					<h1 class="page-title title-wrap__title">Мои партнёры</h1>
					<a href="/" class="btn-back title-wrap__back">назад</a>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<div class="affiliate-program-terms affiliate-program-terms_my-partners">
						<p>Рекомендуй <span style="color: #1c9724;">CRYPTOLIDE</span> друзьям и постоянно получай от <span class="affiliate-program-terms__percent">20%</span> до <span class="affiliate-program-terms__percent">100%</span> прибыли с каждой партнёрской операции.</p>
						<p>Все, что должен сделать ваш друг или знакомый - перейти и зарегистрироваться по любой вашей партнёрской ссылке. </p>
					</div>
				</div>
			</div>
			<div class="affiliate-links affiliate-links_my-partners">
				<div class="row affiliate-links__items">
					<div class="col-sm-6 affiliate-links__item">
						<div class="affiliate-links__title">Партнёрская ссылка на главную страницу</div>
						<form action="" class="form-copy form-copy_frontpage-link">
							<input type="text" class="form-copy__text" name="text_to_copy" value="{{ url('/?r=' . auth()->user()->id) }}" id="text_to_copy_frontpage" readonly="readonly">
							<button class="form-copy__button btn-clipboard" type="button" title="Нажмите, чтобы скопировать текст в буфер обмена"  data-clipboard-target="#text_to_copy_frontpage">Скопировать</button>
						</form>
					</div>
					<div class="col-sm-6 affiliate-links__item">
						<div class="affiliate-links__title">Партнёрская ссылка на ваш профиль</div>
						<form action="" class="form-copy form-copy_profile-link">
							<input type="text" class="form-copy__text" name="text_to_copy" value="{{ url('/profile/' . auth()->user()->login) }}" id="text_to_copy_profile" readonly="readonly">
							<button class="form-copy__button btn-clipboard" type="button" title="Нажмите, чтобы скопировать текст в буфер обмена" data-clipboard-target="#text_to_copy_profile">Скопировать</button>
						</form>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 affiliate-links__note">
						Обратите внимание, что вы очень просто и быстро можете превратить любую существующую ссылку в партнёрскую. <a href="/help#help_partners_4">Подробнее.</a> <br>Информация о партнёрской программе и всевозможных способах привлечения партнёров опубликована <a href="/affiliate-program">здесь.</a> <br>Так же вы можете бесплатно повысить партнёрскую ставку и получить другие <a href="/representatives">приятные бонусы.</a>
					</div>
				</div>
			</div>
			<div class="affiliate-income affiliate-income_my-partners">
				<h2 class="affiliate-income__title">Ваш партнёрский доход</h2>
				<div class="affiliate-income__items">
					<div class="affiliate-income__box affiliate-income__box_bitcoin">
						<div class="affiliate-income__content">
							<div class="affiliate-income__img"><img src="/images/content/icon_bitcoin.png" alt=""></div>
							<div class="affiliate-income__description">
								<div class="affiliate-income__currency-name">Bitcoin</div>
								<div class="affiliate-income__number">{{ $balance['BTC'] }} BTC</div>
							</div>
						</div>
                        <a href="#" data-href="/transfer-income/1" class="btn-blue affiliate-income__button transfer-money">Перевести на баланс</a>
					</div>
					<div class="affiliate-income__box affiliate-income__box_litecoin">
						<div class="affiliate-income__content">
							<div class="affiliate-income__img"><img src="/images/content/icon_litecoin.png" alt=""></div>
							<div class="affiliate-income__description">
								<div class="affiliate-income__currency-name">Litecoin</div>
								<div class="affiliate-income__number">{{ $balance['LTC'] }} LTC</div>
							</div>
						</div>
						<a href="#" data-href="/transfer-income/2" class="btn-blue affiliate-income__button transfer-money">Перевести на баланс</a>
					</div>
					<div class="affiliate-income__box affiliate-income__box_dash">
						<div class="affiliate-income__content">
							<div class="affiliate-income__img"><img src="/images/content/icon_dash.png" alt=""></div>
							<div class="affiliate-income__description">
								<div class="affiliate-income__currency-name">DASH</div>
								<div class="affiliate-income__number">{{ $balance['DASH'] }} DASH</div>
							</div>
						</div>
						<a href="#" data-href="/transfer-income/3" class="btn-blue affiliate-income__button transfer-money">Перевести на баланс</a>
					</div>
				</div>
			</div>
		</div>
		<div class="partners-list partners-list_invited">
			<div class="partners-list__header">
				<div class="container">
					<div class="row">
						<div class="col-xs-12">
							<h2 class="partners-list__title">Список приглашенных партнёров</h2>
						</div>
					</div>
				</div>
			</div>
			<div class="partners-table">
				<div class="partners-table__header">
					<div class="container">
						<div class="row">
							<div class="col-xs-12">
								<div class="table-responsive">
									<table class="partners-table__header-inner">
										<thead>
										<tr>
											<th class="partners-table__uid">UID</th>
											<th class="partners-table__date">Дата</th>
											<th class="partners-table__name-and-login">Имя и логин</th>
											<th class="partners-table__partner-contacts">Связь с партнёром</th>
											<th class="partners-table__revenue-from-partner">Доход от партнёра</th>
										</tr>
										</thead>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="partners-table__body">
					@if ($partners->count() > 0)
						@foreach ($partners as $partner)
							<div class="partners-table__row">
								<div class="container">
									<div class="row">
										<div class="col-xs-12">
											<div class="table-responsive">
												<table class="partners-table__row-inner">
													<tbody>
													<tr>
														<td class="partners-table__uid">{{ $partner->id }}</td>
														<td class="partners-table__date">{!! $partner->created_at->format('H:i <br> d.m.Y') !!}</td>
														<td class="partners-table__name-and-login">
															<div class="partners-table__partner-photo"><img src="{{ $partner->image }}" alt="Фото" class="img-small"></div>
															<div class="partners-table__name-and-login-wrap">
																<div class="partners-table__partner-name">{{ $partner->name }}</div>
																<div class="partners-table__partner-login">
																	<a href="/profile/{{ $partner->login }}">{{ $partner->login }}</a>
																</div>
															</div>
														</td>
														<td class="partners-table__partner-contacts">
															<ul class="partners-table__contacts-list">
																@foreach($partner->contacts()->type('partner')->get() as $contact)
																	<li class="partners-table__contacts-item partners-table__contacts-item_{{ $contact->name }}">
                                                                        {{ $contact->value }}
                                                                    </li>
																@endforeach
															</ul>
														</td>
														<td class="partners-table__revenue-from-partner">
															<ul class="partners-table__currencies">
																@foreach($partner->revert_partner_balance as $currency => $amount)
																	<li class="partners-table__currencies-item">{{ $amount . ' ' . $currency }}</li>
																@endforeach
															</ul>
														</td>
													</tr>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>
						@endforeach
					@else
						<div class="partners-table__no-partners">
							<div class="container">
								<div class="row">
									<div class="col-xs-12">Вы пока еще никого не пригласили</div>
								</div>
							</div>
						</div>
					@endif
				</div>
			</div>
			{{ $partners->links('pagination.cryptolide') }}
		</div>
		@if (($referrer = auth()->user()->referrer) != null)
			<div class="partners-list partners-list_inviter">
				<div class="partners-list__header">
					<div class="container">
						<div class="row">
							<div class="col-xs-12">
								<h2 class="partners-list__title">Ваш пригласитель</h2>
							</div>
						</div>
					</div>
				</div>
				<div class="partners-table">
					<div class="partners-table__header">
						<div class="container">
							<div class="row">
								<div class="col-xs-12">
									<div class="table-responsive">
										<table class="partners-table__header-inner">
											<thead>
											<tr>
												<th class="partners-table__uid">UID</th>
												<th class="partners-table__date">Дата</th>
												<th class="partners-table__name-and-login">Имя и логин</th>
												<th class="partners-table__inviter-contacts">Связь с пригласителем</th>
												<th class="partners-table__rating-and-awards">Рейтинг и награды</th>
											</tr>
											</thead>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="partners-table__body">
						<div class="partners-table__row">
							<div class="container">
								<div class="row">
									<div class="col-xs-12">
										<div class="table-responsive">
											<table class="partners-table__row-inner">
												<tbody>
												<tr>
													<td class="partners-table__uid">{{ $referrer->id }}</td>
													<td class="partners-table__date">{!! $referrer->created_at->format('H:i <br> d.m.Y') !!}</td>
													<td class="partners-table__name-and-login">
														<div class="partners-table__partner-photo"><img src="{{ $referrer->image }}" alt="Фото" class="img-small"></div>
														<div class="partners-table__name-and-login-wrap">
															<div class="partners-table__partner-name">{{ $referrer->name }}</div>
															<div class="partners-table__partner-login">
																<a href="/profile/{{ $referrer->login }}">{{ $referrer->login }}</a>
															</div>
														</div>
													</td>
													<td class="partners-table__inviter-contacts">
														<ul class="partners-table__contacts-list">
															@foreach($referrer->contacts()->type('partner')->get() as $contact)
																<li class="partners-table__contacts-item partners-table__contacts-item_{{ $contact->name }}">
																	{{ $contact->value }}
																</li>
															@endforeach
														</ul>
													</td>
													<td class="partners-table__rating-and-awards">
														<div class="rating">{{ $referrer->rating }}</div>
													</td>
												</tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		@endif
	</div>
@endsection

@section('scripts')
    <script src="https://cdn.jsdelivr.net/clipboard.js/1.5.3/clipboard.min.js"></script>
	<script>
        new Clipboard('.btn-clipboard');

        $(function() {
           	$('.transfer-money').on('click', function(event) {
           	   	event.preventDefault();
                let button = $(this);
                button.css('pointer-events', 'none');

                $.get(button.data('href')).done(function(response) {
                    button.text(response.message);

					if (response.status === true) {
					    button.css('background-color', 'green');
					} else {
                        button.css('background-color', 'red');
					}
                    window.location.reload();
				}).fail(function(err) {
				    alert(err.statusText);
                    button.css('pointer-events', 'auto');
				});
			});
		});
	</script>
@endsection