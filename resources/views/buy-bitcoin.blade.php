@extends('layouts.app')

@title(strip_tags($_title))

@className('main_buy-bitcoin')

@section('content')
<div class="content">
	@if (auth()->check())
	@if (auth()->user()->id == $advert->user_id)
	<div class="login-or-register login-or-register_top">
		<div class="login-or-register__text login-or-register__text_logged-in">Вы являетесь создателем этого объявления. Именно так, как показано ниже, его видят другие пользователи.</div>
	</div>
	@endif
	@endif
	<div class="title-wrap">
		<div class="container">
			<div class="row">
				<h1 class="page-title title-wrap__title">
					{!! $_title !!}
				</h1>
				<a href="/" class="btn-back title-wrap__back">назад</a>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="left-blocks">
				<div class="information-block information-block_general">
					<h2 class="information-block__title">Общая информация и оформление сделки</h2>
					<form action="/deals" method="POST" class="information-block__content">
						{{ csrf_field() }}
						<input type="hidden" name="advert_id" value="{{ $advert->id }}">
						<div class="information-block__payment">
							<div class="information-block__payment-item information-block__payment-item_user">
								<div class="information-block__payment-item-name">Пользователь:</div>
								<div class="information-block__payment-item-value">
									<div class="user">
										<div class="user__photo"><img src="{{ $advert->user->image }}" alt=""></div>
										<div class="user__info user__info_{{ $advert->user->is_online ? 'online' : 'offline' }}">
											<a href="/profile/{{ $advert->user->login }}" class="user__name">{{ $advert->user->login }}</a>
											<div class="user__rating">{{ $advert->user->rating }}</div>
											<span class="action-table__seller-icons">
												@foreach($advert->user->achievements as $achievement)
												@if ($achievement->image == '/storage/achievements/2.png')
				                <span data-toggle="tooltip" title="Пройдена полная верификация личности">
				                  <img src="{{ $achievement->image }}" alt="">
				                </span>
				                @elseif($achievement->image == '/storage/achievements/1.png')
				                <span data-toggle="tooltip" title="Верифицирован телефонный номер">
				                  <img src="{{ $achievement->image }}" alt="">
				                </span>
				                @else
				                <img src="{{ $achievement->image }}" alt="">
				                @endif
												@endforeach
											</span>
										</div>
									</div>
								</div>
							</div>
							<div class="information-block__payment-item information-block__payment-item_price">
								<div class="information-block__payment-item-name">Цена за 1 {{ $advert->crypto->short }}:</div>
								<div class="information-block__payment-item-value">{{ $advert->rate() }}
									{{ $advert->currency_name }}
								</div>
							</div>
							@if ($advert->type == 'buy_online' || $advert->type == 'sell_online')
							<div class="information-block__payment-item">
								<div class="information-block__payment-item-name">Способ оплаты:</div>
								<div class="information-block__payment-item-value">
									{{ $advert->payment_type->name }}
									@if ($advert->payment_type_id == 2)
									- {{ $advert->banks }}
									@endif
								</div>
							</div>
							@endif
							<div class="information-block__payment-item">
								<div class="information-block__payment-item-name">Мин. - макс. сумма {{ $advert->currency_name }}:</div>
								<div class="information-block__payment-item-value">{{  $advert->minCurrency() }} - {{ $advert->short_type != 'buy' ? $advert->sumCurrencyWithCommission() : $advert->sumCurrency() }} {{ $advert->currency_name }}</div>
							</div>
							<div class="information-block__payment-item">
								<div class="information-block__payment-item-name">Мин. - макс. сумма {{ $advert->crypto->short }}:</div>
								<div class="information-block__payment-item-value">{{ $advert->minCrypto() }} - {{ $advert->short_type != 'buy' ? $advert->sumCryptoWithCommission() : $advert->sumCrypto() }} {{ $advert->crypto->short }}</div>
							</div>
							@if ($advert->type != 'trade')
							<div class="information-block__payment-item">
								<div class="information-block__payment-item-name">Местоположение:</div>
								<div class="information-block__payment-item-value">										
									{{ $advert->city_id ? $advert->city->name . ',' : '' }} {{ $advert->city_id && $advert->city->area ? $advert->city->area . ',' : '' }} {{ $advert->country->name }}

								</div>
							</div>
							@endif
							<div class="information-block__payment-item information-block__payment-item_payment-time">
								<div class="information-block__payment-item-name">Время на оплату сделки:</div>
								<div class="information-block__payment-item-value">{{ $advert->time }} мин <span class="information-block__payment-item-value-tooltip" data-toggle="tooltip" title="" data-original-title="Время, за которое пользователь должен произвести оплату, иначе сделка будет автоматически отменена."></span></div>
							</div>
						</div>
						<div class="block-buy-bitcoin information-block__block-buy-bitcoin">
							<h3 class="block-buy-bitcoin__title">Сколько {{ $advert->crypto->short }} вы хотите
								@if ($advert->short_type == 'sell')
								купить
								@elseif ($advert->short_type == 'buy')
								продать
								@elseif ($advert->short_type == 'trade')
								обменять
								@endif
							?</h3>
							<div class="block-buy-bitcoin__content">
								<div class="block-buy-bitcoin__field-item block-buy-bitcoin__field-item_number">
									<div class="block-buy-bitcoin__field-wrap">
										<label for="btc-number" class="block-buy-bitcoin__field-label">{{ $advert->crypto->short }}</label>
										<input type="text" onkeyup="validate(this)" name="sum_crypto" id="btc-number" placeholder="{{ $advert->minCrypto() }}" class="block-buy-bitcoin__form-text">
									</div>
									<div class="block-buy-bitcoin__signature">укажите нужное количество</div>
								</div>
								<div class="block-buy-bitcoin__field-separator">или</div>
								<div class="block-buy-bitcoin__field-item block-buy-bitcoin__field-item_amount">
									<div class="block-buy-bitcoin__field-wrap">
										<input type="text" name="sum_currency" onkeyup="validate(this)" id="btc-amount" placeholder="{{ $advert->minCurrency() }}" class="block-buy-bitcoin__form-text">
										<label for="btc-amount" class="block-buy-bitcoin__field-label">
											{{ $advert->currency_name }}
										</label>
									</div>
									<div class="block-buy-bitcoin__signature">укажите на какую сумму</div>
								</div>
							</div>
						</div>
						@if (auth()->check())
						@if (auth()->user()->id == $advert->user_id)
						<div class="login-or-register block-buy-bitcoin__login-or-register">
							<div class="login-or-register__text login-or-register__text_logged-in">Вы являетесь создателем этого объявления.</div>
						</div>
						@elseif($deal)
								<div class="login-or-register block-buy-bitcoin__login-or-register">
									<div class="login-or-register__text login-or-register__text_logged-in">
										@if($advert->type == 'trade')
											У вас сейчас есть открытая сделка №{{ $deal->id.' '.$advert_type.' Bitcoin' }}
											<br>
											Чтобы заключить новую сделку по этим же параметрам, вам необходимо завершить или отменить текущую.
											<br>
											<a href="{{ url('/deals/'.$deal->id) }}">Перейти к сделке №{{ $deal->id }}</a>
										@else
											У вас сейчас есть открытая сделка №{{ $deal->id.' '.$advert_type.' Bitcoin с помощью: '.$advert->payment_type->name. ' (' . $advert->currency->name . ') в стране: '.$advert->country->name.'.' }}
											<br>
											Чтобы заключить новую сделку по этим же параметрам, вам необходимо завершить или отменить текущую.
											<br>
											<a href="{{ url('/deals/'.$deal->id) }}">Перейти к сделке №{{ $deal->id }}</a>
										@endif
									</div>
								</div>
						@elseif ($advert->status != 'active')
						<div class="information-block__open-deal">
							Это объявление сейчас не активно. Попробуйте открыть его немного позже или посмотрите другие объявления этого пользователя
						</div>
						@else
						@if($errors->count())
						<div class="transaction-error information-block__transaction-error">
							{{ $errors->first() }}
						</div>
						@endif
						<div class="information-block__open-deal">
							@if (!is_null($advert->achievement_id) && !in_array($advert->achievement_id, auth()->user()->achievements->pluck('id')->toArray()))
							Только пользователи с отметкой «{{ $advert->achievement->name }}» могут открыть это объявление.
							@else
							<button class="btn-action" type="submit">Открыть сделку</button>
							@endif
						</div>
						@endif
						@else
						<div class="login-or-register block-buy-bitcoin__login-or-register">
							<div class="login-or-register__text">Для открытия сделки нужно войти или зарегистрироваться</div>
							<div class="login-or-register__buttons">
								<a href="/register" class="btn-action btn-action_register">Зарегистрироваться</a>
								<span class="login-or-register__btn-separator">или</span>
								<a href="/login" class="btn-blue btn-blue_login">Войти в кабинет</a>
							</div>
						</div>
						@endif
					</form>
				</div>
				<a href="/help#help_platform_1" class="transaction-info-link transaction-info-link_bitcoin-buy">Информация для новых пользователей о том, как совершать сделки</a>
			</div>
			<div class="right-blocks">
				<div class="information-block information-block_transaction-terms">
					<h2 class="information-block__title">Условия сделки с {{ $advert->user->login }}</h2>
					<div class="information-block__content">
						{{ $advert->description ?: 'Описание сделки отсутствует. Вы можете уточнить все необходимые детали через онлайн чат.' }}
						<br>
						<br>
						{{ $advert->requisites }}
					</div>
				</div>
				<div class="complain-about-ad"><a href="#" class="complain-about-ad__link">Пожаловаться на это объявление</a></div>
				<div class="ad-complaint">
					<h4 class="ad-complaint__title">Жалоба на объявление</h4>
					<div class="ad-complaint__text">Пожалуйста, расскажите одному из наших онлайн консультантов, что именно вам не понравилось в этом объявлении. На любые нарушения мы реагируем мгновенно.</div>
					<div class="ad-complaint__button">
						<a style="cursor: pointer;" target="siteheart_window" class="btn-write-consultant" onclick="Chatra('openChat', true)">Написать онлайн консультанту</a>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="your-safety">
				<p>Cryptolide.com полностью обеспечивает вашу безопасность!</p>
				<p>Cryptolide - escrow сервис, который использует депонирование платежей и выступает доверенной стороной в сделке между продавцом <br>и покупателем. Это значит, что как только открывается любая сделка – указанное пользователем значение криптовалюты мгновенно списывается с баланса продавца и резервируется на сервисе до того момента, пока сделка не будет успешно завершена. Таким образом, каждый пользователь может быть абсолютно уверенным, что его партнер по сделке гарантированно выполнит свои обязательства.</p>
				<p>Система депонирования платежей полностью защищает и покупателя, и продавца во всех сделках без исключения.</p>
			</div>
		</div>
	</div>
</div>
@endsection

@section('scripts')
<script>
	$(function() {
		let rate = '{{ $advert->rate }}';
		let type = '{{ $advert->type }}';

        $('[name="sum_currency"]').on('keyup change', function() {
            if ($(this).val() === '' || $(this).val() === '.') {
                return;
            }

            if (type === 'trade') {
                $('[name="sum_crypto"]').val(($(this).val()*rate).toFixed(8));
            } else {
                $('[name="sum_crypto"]').val(($(this).val()/rate).toFixed(8));
			}
        });

        $('[name="sum_crypto"]').on('keyup change', function() {
            if ($(this).val() === '' || $(this).val() === '.') {
                return;
            }

            if (type === 'trade') {
                $('[name="sum_currency"]').val(($(this).val()/rate).toFixed(8));
            } else {
                $('[name="sum_currency"]').val(($(this).val()*rate).toFixed(2));
            }
        });

        $('[name="sum_currency"],[name="sum_crypto"]').on('keypress', function(e) {
            let th = $(this);
            setTimeout(function () {
                th.val(th.val().replace(',', '.'))
            }, 5);
            if(e.ctrlKey && k == 86) {
                return false;
            }

            if (e.shiftKey === true ) {
                if (e.which == 44 || e.which == 9) {
                    if (th.val().indexOf('.') != -1) {
                        return false;
                    }
                    return true;
                }
            }
            if (e.which == 46 || e.which == 44) {
                if (th.val().indexOf('.') != -1) {
                    return false;
                }
                return true;
            }
            if (e.which > 57 || e.which < 48) {
                return false;
            }
            return true;
        });

        $('[name="sum_currency"],[name="sum_crypto"]').on('keypress', function(e) {
            let th = $(this);
            setTimeout(function () {
                th.val(th.val().replace(',', '.'));
            }, 5);
        });

        $('[name="sum_currency"],[name="sum_crypto"]').on('paste', function(event){
            event.preventDefault();
        });

		$('.complain-about-ad__link').on('click', function() {
			$('.ad-complaint').css('display', 'block');
		});

		$('.information-block__open-deal .btn-action').click(function () {
			setTimeout(function () {
				$('.information-block__open-deal .btn-action').prop('disabled', true);
			}, 1);
		});
	});
</script>
@endsection