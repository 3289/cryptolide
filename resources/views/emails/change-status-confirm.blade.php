<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Cryptolide | Письмо с подтверждением действия</title>
</head>
<body>
<div style="position: relative; left: 92px; width: 770px; margin: 103px auto 0; font-family: 'Arial', sans-serif;">
	<img src="{{ url('images/content/logo_letter.png') }}" alt="Логотип" style="padding-left: 16px;">
	<div style="margin-bottom: 18px; padding-left: 16px; font-size: 14px; color: #a2a2a2">Удобная торговая платформа и надёжный криптовалютный кошелёк!</div>
	<div style="width: 704px; height: 1px; margin: 18px 0 23px; background: #e8e8e8;"></div>
	<h3 style="margin-bottom: 22px; padding-left: 16px; font-size: 20px; color: #393939;">Подтверждение действия!</h3>
	<div style="width: 704px; height: 1px; margin: 18px 0 23px; background: #e8e8e8;"></div>
	<p style="margin-bottom: 19px; padding-left: 16px; font-size: 18px; letter-spacing: 0.004em;">Вы подтверждаете, что изменили статус сделке №{{ $deal->id }}.</p>
	<p style="padding-left: 16px; font-size: 18px;">Чтобы сохранить свои действия</strong> введите код подтверждения:</p>
	<div style="padding-left: 16px; font-weight: bold; font-size: 55px; color: #7db703;">{{ $confirmation_code }}</div>
	<div style="width: 704px; height: 1px; margin: 24px 0 17px; background: #e8e8e8;"></div>
	<div style="font-size: 14px; color: #a2a2a2">
		<p style="margin-bottom: -8px; padding-left: 16px;">- Если вы не совершали этих действий - обратитесь в службу безопасности: <a href="mailto:control@cryptolide.com" style="color: #376ea7;">control@cryptolide.com</a></p>
		<p style="padding-left: 16px;">- Отвечать на это письмо не нужно.</p>
		<div style="width: 704px; height: 1px; margin: 24px 0 21px; background: #e8e8e8;"></div>
		<p style="padding-left: 16px;">С уважением, команда CRYPTOLIDE.</p>
	</div>
</div>
</body>
</html>