<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Cryptolide | {{ $subject }}</title>
</head>
<body>
<div style="position: relative; left: 92px; width: 770px; margin: 103px auto 0; font-family: 'Arial', sans-serif;">
	<img src="images/content/logo_letter.png" alt="Логотип" style="padding-left: 16px;">
	<h3 style="margin-bottom: 22px; padding-left: 16px; font-size: 20px; color: #7db703;">{{ $subject }}</h3>
	<div style="width: 704px; height: 1px; margin: 18px 0 23px; background: #e8e8e8;"></div>
	<p style="margin-bottom: 19px; padding-left: 16px; font-size: 18px; letter-spacing: 0.004em;">Пользователь <strong>{{ $userLogin }}</strong> создал заявку на вывод <strong>{{ $amount }} {{ $currency }}</strong> на адрес:</p>
	<br><br>
	<p style="padding-left: 16px; font-size: 18px;"><strong>{{ $address }}</strong></p>
</div>
</body>
</html>