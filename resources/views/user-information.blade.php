@extends('layouts.app')

@title('Бизнес профиль пользователя ' . $user->login)

@className('main_business-profile')

@section('content')
	<div class="content">
		<div class="title-wrap title-wrap_business-profile">
			<div class="container">
				<div class="row">
					<h1 class="page-title title-wrap__title">Бизнес профиль пользователя <span style="color: #59ba41;">{{ $user->login }}</span></h1>
					<a href="/" class="btn-back title-wrap__back">назад</a>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<div class="user-profile user-profile_business">
						<div class="user-profile__photo"><img src="{{ $user->image }}" alt=""></div>
						<div class="user-profile__info user-profile__info_{{ $user->is_online ? 'online' : 'offline' }}">
							@if ($user->show_name)
								<div class="user-profile__realname"><span class="user-profile__realname-inner">{{ $user->last_name . ' ' . $user->name . ' ' . $user->middle_name }}</span></div>
							@endif
							<a href="#" class="user-profile__name">{{ $user->login }}</a>
							<div class="user-profile__rating">{{ $user->rating }}</div>

							@foreach($user->achievements as $achievement)
								<img src="{{ $achievement->image }}" alt="" class="user-profile__ok">
							@endforeach
						</div>
					</div>
					@if (!auth()->check())
						<div class="get-gift get-gift_business">
							<a href="/?r={{ $user->id }}" class="btn-green2 btn-green2_get-gift">Получить подарок от {{ $user->login }}</a>
						</div>
					@endif
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<ul class="settings-tabs settings-tabs_business-profile settings-tabs_userinfo">
						<li class="settings-tabs__item settings-tabs__item_active"><a href="/profile/{{ $user->login }}" class="settings-tabs__link">ИНФОРМАЦИЯ О ПОЛЬЗОВАТЕЛЕ</a></li>
						<li class="settings-tabs__item"><a href="/profile/{{ $user->login }}/adverts" class="settings-tabs__link">АКТИВНЫЕ ОБЪЯВЛЕНИЯ ПОЛЬЗОВАТЕЛЯ <span class="deals-number">{{ $user->adverts()->where('status', 'active')->count() }}</span></a></li>
					</ul>
					<div class="userinfo-block userinfo-block_business-profile">
						<div class="userinfo-block__item">
							<div class="userinfo-block__label">Рейтинг:</div>
							<div class="userinfo-block__value"><div class="rating">{{ $user->rating }}</div></div>
						</div>
						<div class="userinfo-block__item">
							<div class="userinfo-block__label">Верификация: </div>
							<div class="userinfo-block__value">
								@if ($user->verified)
									<img src="/images/icons/ok.png" alt="" class="user-profile__ok"> Пройдена полная верификация личности
								@elseif ($user->achievements->isNotEmpty())
									@foreach($user->achievements as $achievement)
										@if ($achievement->id == 1 || $achievement->id == 2)
											<img src="{{ $achievement->image }}" alt="" class="user-profile__ok"> {{ $achievement->name }}
										@endif
									@endforeach
								@else
									<span style="color: gray;">Отсутствует</span>
								@endif
							</div>
						</div>
						@if ($user->achievements->where('id', 3)->first())
							<div class="userinfo-block__item">
								<div class="userinfo-block__label">Особые награды и достижения: </div>
								<div class="userinfo-block__value">
									@foreach($user->achievements as $achievement)
										@if ($achievement->id == 3)
											<img src="{{ $achievement->image }}" alt="" class="user-profile__ok"> {{ $achievement->name }}
										@endif
									@endforeach
								</div>
							</div>
						@endif
						<div class="userinfo-block__item">
							<div class="userinfo-block__label">E-mail:</div>
							<div class="userinfo-block__value">
								@if ($user->activated)
									<span class="userinfo-block__confirmed">Подтвержден</span>
								@else
									<span class="userinfo-block__not-confirmed" style="color: red;">Не подтвержден</span>
								@endif
							</div>
						</div>
						<div class="userinfo-block__item">
							<div class="userinfo-block__label">Статус обслуживания:</div>
							<div class="userinfo-block__value"><span class="userinfo-block__served">Обслуживается</span></div>
						</div>
					</div>
					<div class="userinfo-block userinfo-block_business-profile">
						<div class="userinfo-block__item">
							<div class="userinfo-block__label">Последнее посещение:</div>
							<div class="userinfo-block__value">{{ $user->last_activity_format }}</div>
						</div>
						<div class="userinfo-block__item">
							<div class="userinfo-block__label">Дата регистрации:</div>
							<div class="userinfo-block__value">{{ $user->created_at->format('d.m.Y') }}</div>
						</div>
					</div>
					<div class="userinfo-block userinfo-block_business-profile">
						<div class="userinfo-block__item">
							<div class="userinfo-block__label">Заблокирован пользователями:</div>
							<div class="userinfo-block__value">0</div>
						</div>
						<div class="userinfo-block__item">
							<div class="userinfo-block__label">Участий в арбитражах:</div>
							<div class="userinfo-block__value">0</div>
						</div>
						<div class="userinfo-block__item">
							<div class="userinfo-block__label">Претензии / Иски:</div>
							<div class="userinfo-block__value">0</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('scripts')
	<script>
        $('.jqstyler').styler({
            onFormStyled: function () {
                $('.jq-selectbox').each(function (index, el) {
                    var bgImg = $(this).find('.jq-selectbox__dropdown ul li.selected').css('background-image');
                    $(this).find('.jq-selectbox__select-text').css('background-image', bgImg);
                });
            },
            onSelectClosed: function () {
                var bgImg = $(this).find('.jq-selectbox__dropdown ul li.selected').css('background-image');
                $(this).find('.jq-selectbox__select-text').css('background-image', bgImg);
            }
        });
	</script>
@endsection