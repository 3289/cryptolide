@if ($paginator->hasPages())
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <ul class="pagination">
                    @if ($paginator->onFirstPage())
                        <li class="pagination__item"><a href="#" class="pagination__link">&laquo;</a></li>
                    @else
                        <li class="pagination__item"><a href="{{ $paginator->previousPageUrl() }}" class="pagination__link">&laquo;</a></li>
                    @endif

                    @foreach ($elements as $element)
                        {{-- "Three Dots" Separator --}}
                        @if (is_string($element))
                            <li class="page-item disabled"><span class="page-link">{{ $element }}</span></li>
                        @endif

                        {{-- Array Of Links --}}
                        @if (is_array($element))
                            @foreach ($element as $page => $url)
                                @if ($page == $paginator->currentPage())
                                    <li class="pagination__item pagination__item_current"><a href="#" class="pagination__link">{{ $page }}</a></li>
                                @else
                                    <li class="pagination__item"><a href="{{ $url }}" class="pagination__link">{{ $page }}</a></li>
                                @endif
                            @endforeach
                        @endif
                    @endforeach

                    @if ($paginator->hasMorePages())
                        <li class="pagination__item"><a href="{{ $paginator->nextPageUrl() }}" class="pagination__link">&raquo;</a></li>
                    @else
                        <li class="pagination__item"><a href="#" class="pagination__link">&raquo;</a></li>
                    @endif
                </ul>
            </div>
        </div>
    </div>
@endif
