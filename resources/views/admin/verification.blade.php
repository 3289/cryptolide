@extends('layouts.admin')

@section('content')
    <table class="table">
        <thead class="thead-light">
        <tr>
            <th>ID</th>
            <th>Дата</th>
            <th>Логин</th>
            <th>Тип верификации</th>
            <th>Телефон</th>
            <th>Код</th>
        </tr>
        </thead>
        <tbody>
        @foreach($verifications as $verification)
            <tr data-id="{{ $verification->id }}">
                <td>{{ $verification->id }}</td>
                <td>{{ $verification->created_at }}</td>
                <td>{{ $verification->user->login }}</td>
                <td>{{ \App\Models\Verification::TYPES[$verification->type] }}</td>
                <td>{{ $verification->phone }}</td>
                <td>{{ $verification->code }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <div class="float-right">
        {{ $verifications->appends(request()->all())->links() }}
    </div>
@endsection