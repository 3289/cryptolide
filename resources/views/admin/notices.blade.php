@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <form style="margin-bottom: 10px;" method="POST" action="/admin/notices">
                {{ csrf_field() }}
                <div class="form-group">
                    <label>Пользователи</label>
                    <select name="user_id" class="form-control" required>
                        <option disabled selected value>Выберите пользователя...</option>
                        @foreach($users as $id => $login)
                            <option value="{{ $id }}">{{ $login }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label>Текст замечания</label>
                    <textarea name="text" class="form-control" rows="2" placeholder="Введите текст замечания..." required></textarea>
                </div>
                <div class="form-group">
                    <label>Внутренняя заметка</label>
                    <textarea name="comment" class="form-control" rows="2" placeholder="Введите внутреннюю заметку..." required></textarea>
                </div>
                <button class="btn btn-info">Добавить</button>
            </form>
            <table class="table">
                <thead class="thead-light">
                <tr>
                    <th>ID</th>
                    <th>Дата</th>
                    <th>Логин</th>
                    <th>Текст замечания</th>
                    <th>Внутренняя заметка</th>
                    <th></th>
                </tr>
                </thead>
                <tbody id="notices">
                @foreach($notices as $notice)
                    <tr data-id="{{ $notice->id }}">
                        <td>{{ $notice->id }}</td>
                        <td>{{ $notice->created_at }}</td>
                        <td>{{ $notice->user->login }}</td>
                        <td>{{ $notice->text }}</td>
                        <td>{{ $notice->comment }}</td>
                        <td align="right">
                            <button class="btn btn-sm btn-info">Изменить</button>
                            <button class="btn btn-sm btn-danger">Удалить</button>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('modals')
    <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myLargeModalLabel">Large modal</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label>Пользователи</label>
                                    <select name="user_id" class="form-control" required>
                                        @foreach($users as $id => $login)
                                            <option value="{{ $id }}">{{ $login }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Текст замечания</label>
                                    <textarea name="text" class="form-control" rows="2" placeholder="Введите текст замечания..." required></textarea>
                                </div>
                                <div class="form-group">
                                    <label>Внутренняя заметка</label>
                                    <textarea name="comment" class="form-control" rows="2" placeholder="Введите внутреннюю заметку..." required></textarea>
                                </div>
                            </div>
                        </div>
                        <button class="btn btn-info">Применить</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(function() {
            $('tbody#notices').on('click', '.btn-info', function() {
                let row = $(this).closest('tr');
                $.get('/admin/notices/' +  row.data('id')).done(function(response) {
                    let modal = $('.modal');
                    modal.find('form').attr('action', '/admin/notices/' + response.id);
                    modal.find('[name="user_id"]').val(response.user_id);
                    modal.find('[name="text"]').val(response.text);
                    modal.find('[name="comment"]').val(response.comment);
                    modal.find('.modal-title').text('Редактирование замечания №'+response.id);
                    modal.modal('show');
                });
            }).on('click', '.btn-danger', function() {
                if (!confirm('Действительно удалить?')) {
                    return;
                }

                let row = $(this).closest('tr');
                $.ajax('/admin/notices/' + row.data('id'), {
                    type: 'DELETE',
                    success: function() {
                        window.location.reload();
                    },
                    error: function(error) {
                        notyf.alert(error.statusText);
                    }
                });
            });

            $('.modal form').on('submit', function(event) {
                event.preventDefault();
                let form = $(this);
                $.ajax(form.attr('action'), {
                    type: 'PATCH',
                    data: form.serialize(),
                    success: function() {
                        window.location.reload();
                    },
                    error: function(error) {
                        notyf.alert(error.statusText);
                    }
                });
            });
        });
    </script>
@endsection