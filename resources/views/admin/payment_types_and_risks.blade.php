@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-md-6">
            <form style="margin-bottom: 10px;" method="POST" action="/admin/payment_types">
                {{ csrf_field() }}
                <div class="form-group">
                    <label>Название</label>
                    <input type="text" name="name" class="form-control" value="{{ old('name') }}" required>
                </div>
                <div class="form-group">
                    <label>Приоритет</label>
                    <input type="number" name="priority" class="form-control" value="{{ old('name') }}" required>
                </div>
                <button class="btn btn-info">Добавить</button>
            </form>
            <table class="table">
                <thead class="thead-light">
                <tr>
                    <th>Название</th>
                    <th>Приоритет</th>
                    <th></th>
                </tr>
                </thead>
                <tbody id="payment_types">
                @foreach($payment_types as $payment_type)
                    <tr data-id="{{ $payment_type->id }}">
                        <td>{{ $payment_type->name }}</td>
                        <td>{{ $payment_type->priority }}</td>
                        <td align="right">
                            <button class="btn btn-sm btn-info">Изменить</button>
                            <button class="btn btn-sm btn-danger">Удалить</button>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="col-md-6">
            <form style="margin-bottom: 10px;" method="POST" action="/admin/risks">
                {{ csrf_field() }}
                <div class="form-group">
                    <label>Способ оплаты</label>
                    <select name="payment_type_id" class="form-control" required>
                        @foreach($payment_types as $payment_type)
                            <option value="{{ $payment_type->id }}">{{ $payment_type->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label>Валюта</label>
                    <select name="currency_id" class="form-control" required>
                        @foreach($currencies as $currency)
                            <option value="{{ $currency->id }}">{{ $currency->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label>Риск</label>
                    <select name="name" class="form-control" required>
                        <option value="Неизвестно">Неизвестно</option>
                        <option value="Низкий">Низкий</option>
                        <option value="Средний">Средний</option>
                        <option value="Высокий">Высокий</option>
                    </select>
                </div>
                <button class="btn btn-info">Добавить</button>
            </form>
            <table class="table">
                <thead class="thead-light">
                <tr>
                    <th>Способ оплаты</th>
                    <th>Валюта</th>
                    <th>Риск</th>
                    <th></th>
                </tr>
                </thead>
                <tbody id="risks">
                @foreach($risks as $risk)
                    <tr data-id="{{ $risk->id }}">
                        <td>{{ $risk->payment_type->name }}</td>
                        <td>{{ $risk->currency->name }}</td>
                        <td>{{ $risk->name }}</td>
                        <td align="right">
                            <button class="btn btn-sm btn-danger">Удалить</button>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('modals')
    <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myLargeModalLabel">Large modal</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label>Название</label>
                                    <input type="text" name="name" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>Приоритет</label>
                                    <input type="text" name="priority" class="form-control">
                                </div>
                            </div>
                        </div>
                        <button class="btn btn-info">Применить</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(function() {
            $('tbody#payment_types').on('click', '.btn-info', function() {
                let row = $(this).closest('tr');
                $.get('/admin/payment_types/' +  row.data('id')).done(function(response) {
                    let modal = $('.modal');
                    modal.find('form').attr('action', '/admin/payment_types/' + response.id);
                    modal.find('[name="name"]').val(response['name']);
                    modal.find('[name="priority"]').val(response['priority']);
                    modal.find('.modal-title').text(response.name);
                    modal.modal('show');
                });
            }).on('click', '.btn-danger', function() {
                if (!confirm('Действительно удалить?')) {
                    return;
                }

                let row = $(this).closest('tr');
                $.ajax('/admin/payment_types/' + row.data('id'), {
                    type: 'DELETE',
                    success: function() {
                        window.location.reload();
                    },
                    error: function(error) {
                        notyf.alert(error.statusText);
                    }
                });
            });

            $('tbody#risks').on('click', '.btn-danger', function() {
                if (!confirm('Действительно удалить?')) {
                    return;
                }

                let row = $(this).closest('tr');
                $.ajax('/admin/risks/' + row.data('id'), {
                    type: 'DELETE',
                    success: function() {
                        window.location.reload();
                    },
                    error: function(error) {
                        notyf.alert(error.statusText);
                    }
                });
            });

            $('.modal form').on('submit', function(event) {
                event.preventDefault();
                let form = $(this);
                $.ajax(form.attr('action'), {
                    type: 'PATCH',
                    data: form.serialize(),
                    success: function() {
                        window.location.reload();
                    },
                    error: function(error) {
                        notyf.alert(error.statusText);
                    }
                });
            });
        });
    </script>
@endsection