@extends('layouts.admin')

@section('content')
    @if ($errors->count() > 0)
        @foreach($errors->all() as $error)
            <div class="alert alert-danger" style="margin-bottom: 10px;" role="alert">
                {{ $error }}
            </div>
        @endforeach
    @endif
<form style="margin-bottom: 10px;" method="post" action="/admin/operations">
    {{ csrf_field() }}
    <div class="form-group">
        <label>Криптовалюта</label>
        <select class="form-control" name="crypto_id" required>
            @foreach($crypto_currencies as $crypto_currency)
                <option value="{{ $crypto_currency->id }}">{{ $crypto_currency->name }}</option>
            @endforeach
        </select>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label>Адрес</label>
                <input class="form-control" name="address" required>
            </div>
            <div class="form-group">
                <label>TX</label>
                <input class="form-control" name="tx" required>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label>Сумма</label>
                <input class="form-control" name="amount" required>
            </div>
            <div class="form-group">
                <label>Комиссия</label>
                <input class="form-control" name="commission" value="{{ $crypto_currencies->first()->commission_input }}" required>
            </div>
        </div>
    </div>
    <button class="btn btn-info" >Пополнить</button>
</form>
@endsection

@section('scripts')
    <script>
        var crypto_currencies = JSON.parse('{!! json_encode($crypto_currencies) !!}');
        $('select[name="crypto_id"]').change(function () {
            var selected_crypto = crypto_currencies.find(function (crypto) {
                return crypto.id == $('select[name="crypto_id"]').val();
            });
            $('input[name="commission"]').val(selected_crypto.commission_input);
        });

        $('form').submit(function(event) {
            event.preventDefault(); //this will prevent the default submit
            $('button').attr('disabled','disabled');
            $(this).unbind('submit').submit(); // continue the submit unbind preventDefault
        });
    </script>
@endsection