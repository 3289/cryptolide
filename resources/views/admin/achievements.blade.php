@extends('layouts.admin')

@section('content')
    <form style="margin-bottom: 10px;" method="POST" action="/admin/achievements" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Название</label>
                    <input type="text" name="name" class="form-control" value="{{ old('name') }}" required>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Картинка</label>
                    <input type="file" name="image" class="form-control" accept="image/*" required>
                </div>
            </div>
        </div>
        <button class="btn btn-info">Добавить</button>
    </form>
    <table class="table">
        <thead class="thead-light">
        <tr>
            <th>Название</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
            @foreach($achievements as $achievement)
                <tr data-id="{{ $achievement->id }}">
                    <td><img style="height: 24px;" src="{{ $achievement->image }}"> {{ $achievement->name }}</td>
                    <td align="right">
                        <button class="btn btn-sm btn-info">Изменить</button>
                        <button class="btn btn-sm btn-danger">Удалить</button>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection

@section('modals')
    <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myLargeModalLabel">Large modal</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label>Название</label>
                                    <input type="text" name="name" class="form-control">
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label>Картинка (не трогать если не нужно изменять)</label>
                                    <input type="file" name="image" class="form-control" accept="image/*">
                                </div>
                            </div>
                        </div>
                        <button class="btn btn-info">Применить</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(function() {
            $('tbody').on('click', '.btn-info', function() {
                let row = $(this).closest('tr');
                $.get('/admin/achievements/' +  row.data('id')).done(function(response) {
                    let modal = $('.modal');
                    modal.find('form').attr('action', '/admin/achievements/' + response.id);
                    modal.find('[name="name"]').val(response.name);
                    modal.find('.modal-title').text(response.name);
                    modal.modal('show');
                });
            }).on('click', '.btn-danger', function() {
                if (!confirm('Действительно удалить?')) {
                    return;
                }

                let row = $(this).closest('tr');
                $.ajax('/admin/achievements/' + row.data('id'), {
                    type: 'DELETE',
                    success: function() {
                        row.remove();
                    },
                    error: function(error) {
                        notyf.alert(error.statusText);
                    }
                });
            });

            $('.modal form').on('submit', function(event) {
                event.preventDefault();
                let form = $(this);
                $.ajax(form.attr('action'), {
                    type: 'POST',
                    data: new FormData(this),
                    contentType: false,
                    processData: false,
                    success: function() {
                        window.location.reload();
                    },
                    error: function(error) {
                        notyf.alert(error.statusText);
                    }
                });
            });
        });
    </script>
@endsection