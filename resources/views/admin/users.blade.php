@extends('layouts.admin')

@section('content')
    <form style="margin-bottom: 10px;">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>E-mail</label>
                    <input type="text" name="email" class="form-control" value="{{ request('email') }}">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Логин</label>
                    <input type="text" name="login" class="form-control" value="{{ request('login') }}">
                </div>
            </div>
        </div>
        <button class="btn btn-info">Искать</button>
    </form>
    <table class="table">
        <thead class="thead-light">
        <tr>
            <th>#</th>
            <th>E-mail</th>
            <th>Логин</th>
            <th>Дата регистрации</th>
            <th>Баланс</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
            @foreach($users as $user)
                <tr data-id="{{ $user->id }}">
                    <td>{{ $user->id }}</td>
                    <td>{{ $user->email }} {!! $user->activated ? '<img src="'.url('/images/icons/ok.png').'">' : '' !!}</td>
                    <td>{{ $user->login }}</td>
                    <td>{{ $user->created_at->format('H:i d.m.Y') }}</td>
                    <td>
                        <div class="profile-block__bitcoins-numbers">{{ $user->balance['BTC'] }} BTC
                        </div>
                        <div class="profile-block__bitcoins-application">
                            + {{ $user->reserved_balance['BTC'] }} в сделках
                        </div>
                    </td>
                    <td align="right">
                        <a class="btn btn-sm btn-warning" href="{{ url('/admin/users/'.$user->id.'/auth') }}" target="_blank">Войти</a>
                        <button class="btn btn-sm btn-info">Изменить</button>
                        <button class="btn btn-sm btn-danger">Удалить</button>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <div class="float-right">
        {{ $users->appends(request()->all())->links() }}
    </div>
@endsection

@section('modals')
    <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" style="max-width: 1024px;">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myLargeModalLabel">Large modal</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label>E-mail</label>
                                    <input type="text" name="email" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>Логин</label>
                                    <input type="text" name="login" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>Партнёрская комиссия</label>
                                    <input type="text" name="commission_partner" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>Рейтинг</label>
                                    <input type="text" name="rating" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>Телефон</label>
                                    <input type="text" name="phone" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>Комиссия</label>
                                    <input type="text" name="commission_deal" class="form-control">
                                    <small id="commission_description" class="help-block">Комиссия будет активна через 7 дней со дня регистрации</small>
                                </div>
                                <div class="form-group">
                                    <label>Дата истечения</label>
                                    <input type="text" name="exp_date" class="form-control">
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label>Имя</label>
                                    <input type="text" name="name" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>Фамилия</label>
                                    <input type="text" name="last_name" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>Отчество</label>
                                    <input type="text" name="middle_name" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>Роль</label>
                                    <select class="form-control" name="role">
                                        <option value="user">Пользователь</option>
                                        <option value="admin">Админ</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Награды</label>
                                    <select class="form-control" name="achievements[]" multiple>
                                        @foreach($achievements as $achievement)
                                            <option value="{{ $achievement->id }}">{{ $achievement->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col">
                                <div id="block-contacts-admin">
                                    <h5>Контакты для админов:</h5>
                                    <ul class="list-unstyled" id="user-contacts-admin">

                                    </ul>
                                </div>
                                <div id="block-contacts-partner">
                                    <h5>Контакты для партнёров:</h5>
                                    <ul class="list-unstyled" id="user-contacts-partner">

                                    </ul>
                                </div>

                            </div>
                        </div>
                        <button class="btn btn-info">Применить</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(function() {
            $('[name="achievements[]"]').select2();

            $('tbody').on('click', '.btn-info', function() {
                let row = $(this).closest('tr');
                $.get('/admin/users/' +  row.data('id')).done(function(response) {
                    let modal = $('.modal');
                    modal.find('.modal-title').text(response.data.email);
                    modal.find('form').attr('action', '/admin/users/' + response.data.id);
                    for(let key in response.data) {
                        modal.find('[name="' + key + '"]').val(response.data[key]);
                    }
                    modal.find('[name="commission_deal"]').val(response.commission);

                    let achievements = [];
                    if (response.achievements.length > 0) {
                        response.achievements.forEach(function(achievement) {
                            achievements.push(achievement.id);
                        });
                    }

                    $('#user-contacts-admin').empty();
                    $('#user-contacts-partner').empty();

                    let counts = {
                        admin: 0,
                        partner: 0
                    };

                    if (response.contacts.length > 0) {
                        response.contacts.forEach(function(contact) {
                            counts[contact.type]++;
                            $('#user-contacts-' + contact.type).append('<li><strong>' + contact.name + ':</strong> ' + contact.value + '</li>');
                        });
                    }

                    $('#block-contacts-admin')
                        .css('display', counts.admin > 0 ? '' : 'none')
                        .closest('.col')
                        .css('display', counts.admin > 0 || counts.partner > 0 ? '' : 'none');
                    $('#block-contacts-partner').css('display', counts.partner > 0 ? '' : 'none');
                    $('#commission_description').css('display', !response.is_commission_active ? '' : 'none');

                    $('[name="achievements[]"]').val(achievements).trigger('change');
                    modal.find('.modal-title').text(response.email);
                    modal.modal('show');
                });
            }).on('click', '.btn-danger', function() {
                if (!confirm('Действительно удалить?')) {
                    return;
                }

                let row = $(this).closest('tr');
                $.ajax('/admin/users/' + row.data('id'), {
                    type: 'DELETE',
                    success: function() {
                        row.remove();
                    },
                    error: function(error) {
                        notyf.alert(error.statusText);
                    }
                });
            });

            $('.modal form').on('submit', function(event) {
                event.preventDefault();
                let form = $(this);
                $.ajax(form.attr('action'), {
                    type: 'PATCH',
                    data: form.serialize(),
                    success: function() {
                        window.location.reload();
                    },
                    error: function(error) {
                        notyf.alert(error.statusText);
                    }
                });
            });
        });
    </script>
@endsection