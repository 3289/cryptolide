@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-md-6">
            <form style="margin-bottom: 10px;" method="POST" action="/admin/countries">
                {{ csrf_field() }}
                <div class="form-group">
                    <label>Название</label>
                    <input type="text" name="name" class="form-control" value="{{ old('name') }}">
                </div>
                <button class="btn btn-info">Добавить</button>
            </form>
            <table class="table">
                <thead class="thead-light">
                <tr>
                    <th>Название</th>
                    <th>Валюта</th>
                    <th></th>
                </tr>
                </thead>
                <tbody id="countries">
                @foreach($countries as $country)
                    <tr data-id="{{ $country->id }}">
                        <td>{{ $country->name }}</td>
                        <td>{{ $country->currency->name }}</td>
                        <td align="right">
                            <button class="btn btn-sm btn-info">Изменить</button>
                            <button class="btn btn-sm btn-danger">Удалить</button>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="col-md-6">
            <form style="margin-bottom: 10px;" method="POST" action="/admin/currencies">
                {{ csrf_field() }}
                <div class="form-group">
                    <label>Название</label>
                    <input type="text" name="name" class="form-control" value="{{ old('name') }}">
                </div>
                <button class="btn btn-info">Добавить</button>
            </form>
            <table class="table">
                <thead class="thead-light">
                <tr>
                    <th>Название</th>
                    <th></th>
                </tr>
                </thead>
                <tbody id="currencies">
                @foreach($currencies as $currency)
                    <tr data-id="{{ $currency->id }}">
                        <td>{{ $currency->name }}</td>
                        <td align="right">
                            <button class="btn btn-sm btn-danger">Удалить</button>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('modals')
    <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myLargeModalLabel">Large modal</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label>Название</label>
                                    <input type="text" name="name" class="form-control">
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label>Валюта</label>
                                    <select name="currency_id" class="form-control">
                                        <option value=""></option>
                                        @foreach($currencies as $currency)
                                            <option value="{{ $currency->id }}">{{ $currency->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <button class="btn btn-info">Применить</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(function() {
            $('tbody#countries').on('click', '.btn-info', function() {
                let row = $(this).closest('tr');
                $.get('/admin/countries/' +  row.data('id')).done(function(response) {
                    let modal = $('.modal');
                    modal.find('form').attr('action', '/admin/countries/' + response.id);
                    modal.find('[name="name"]').val(response.name);
                    modal.find('[name="currency_id"]').val(response.currency_id);
                    modal.find('.modal-title').text(response.name);
                    modal.modal('show');
                });
            }).on('click', '.btn-danger', function() {
                if (!confirm('Действительно удалить?')) {
                    return;
                }

                let row = $(this).closest('tr');
                $.ajax('/admin/countries/' + row.data('id'), {
                    type: 'DELETE',
                    success: function() {
                        window.location.reload();
                    },
                    error: function(error) {
                        notyf.alert(error.statusText);
                    }
                });
            });

            $('tbody#currencies').on('click', '.btn-danger', function() {
                if (!confirm('Действительно удалить?')) {
                    return;
                }

                let row = $(this).closest('tr');
                $.ajax('/admin/currencies/' + row.data('id'), {
                    type: 'DELETE',
                    success: function() {
                        window.location.reload();
                    },
                    error: function(error) {
                        notyf.alert(error.statusText);
                    }
                });
            });

            $('.modal form').on('submit', function(event) {
                event.preventDefault();
                let form = $(this);
                $.ajax(form.attr('action'), {
                    type: 'PATCH',
                    data: form.serialize(),
                    success: function() {
                        window.location.reload();
                    },
                    error: function(error) {
                        notyf.alert(error.statusText);
                    }
                });
            });
        });
    </script>
@endsection