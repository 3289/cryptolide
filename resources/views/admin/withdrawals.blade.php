@extends('layouts.admin')
<?php
    use App\Models\Withdrawal;
?>
@section('content')
    <table class="table">
        <thead class="thead-light">
        <tr>
            <th>#</th>
            <th>Логин</th>
            <th>Сумма</th>
            <th>Адрес</th>
            <th>Создана</th>
            {{--<th></th>--}}
            <th>Статус операции</th>
            <th>Обработана</th>
        </tr>
        </thead>
        <tbody>
        @foreach($withdrawals as $withdrawal)
            <tr data-id="{{ $withdrawal->id }}">
                <td>{{ $withdrawal->id }}</td>
                <td>
                    <a href="/admin/users/?login={{ $withdrawal->operation->user->login }}" target="_blank" class="text-info" style="text-decoration: none;">
                        {{ $withdrawal->operation->user->login }}
                    </a>
                </td>
                <td>{{ abs($withdrawal->operation->amount)-$withdrawal->operation->commission }} {{ $withdrawal->operation->crypto_currency->short }}</td>
                <td>{{ $withdrawal->address }}</td>
                <td>{{ $withdrawal->created_at->format('H:i d.m.Y') }}</td>
                {{--<td align="right">
                    <button class="btn btn-sm btn-danger">Удалить</button>
                </td>--}}
                <td>
                    @if($withdrawal->status == 'in_process')
                        <div class="withdrawals-status-img in-process withdrawals-status-cursor">
                            <img src="/images/icons/icon_time2.png" alt="Ожидает обработки">
                        </div>
                        <div class="withdrawals-status-text">
                            Ожидает обработки
                        </div>
                    @elseif($withdrawal->status == 'completed')
                        <div class="withdrawals-status-img">
                            <img src="/images/icons/icon_ok.png" alt="Обработана">
                        </div>
                        <div class="withdrawals-status-text">
                            Обработана
                        </div>
                    @elseif($withdrawal->status == 'cancelled')
                        <div class="withdrawals-status-img">
                            <img src="/images/icons/icon_cancel_deal_user.png" style="width: 20px" alt="Отменена">
                        </div>
                        <div class="withdrawals-status-text">
                            Отменена
                        </div>
                    @endif
                </td>
                <td>
                    @if ($withdrawal->status == 'completed')
                        {{ $withdrawal->updated_at->format('H:i d.m.Y') }} /
                        <a href="https://www.blocktrail.com/BTC/tx/{{ $withdrawal->txid }}" target="_blank">TXID</a>
                    @endif
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <div class="float-right">
        {{ $withdrawals->links() }}
    </div>
@endsection

@section('modals')
    <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" style="max-width: 1024px;">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myLargeModalLabel">Large modal</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <input type="hidden" value="" name="id">
                        <input type="hidden" value="" name="status">
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label for="">TXID</label>
                                    <input type="text" name="txid" placeholder="" class="form-control" >
                                </div>
                            </div>
                        </div>
                        <button type="submit" id="submit" class="btn btn-info">Обработать</button>
                        <button id="cancel" class="btn btn-danger">Отменить</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(function() {
            $('tbody').on('click', '.btn-danger', function() {
                if (!confirm('Действительно удалить?')) {
                    return;
                }

                let row = $(this).closest('tr');
                $.ajax('/admin/withdrawals/' + row.data('id'), {
                    type: 'DELETE',
                    success: function() {
                        row.remove();
                    },
                    error: function(error) {
                        notyf.alert(error.statusText);
                    }
                });
            });
        });

        $(function() {
            var confirm_msg;
            $('tbody').on('click', '.in-process', function() {
                let modal =  $('.modal');
                let row = $(this).closest('tr');
                modal.find('input[name="id"]').val(row.data('id'));
                modal.find('form').attr('action', '/admin/withdrawals/' + row.data('id'));
                modal.find('.modal-title').text('Заявка №'+row.data('id'));
                modal.modal('show');
            });

            $('.modal form').on('click', '#cancel', function(event) {
                $('.modal').find('input[name="txid"]').removeAttr('required');
                $('.modal').find('input[name="status"]').val('cancelled');
                confirm_msg = 'Действительно отменить?';
            });

            $('.modal form').on('click', '#submit', function(event) {
                $('.modal').find('input[name="txid"]').attr('required','1');
                $('.modal').find('input[name="status"]').val('completed');
                confirm_msg = 'Действительно обработать?';
            });

            $('.modal form').on('submit', function(event) {
                event.preventDefault();
                if (!confirm(confirm_msg)) {
                    return;
                }
                let form = $(this);
                $.ajax(form.attr('action'), {
                    type: 'PATCH',
                    data: form.serialize(),
                    success: function() {
                        window.location.reload();
                    },
                    error: function(error) {
                        notyf.alert(error.statusText);
                    }
                });
            });
        });
    </script>
@endsection