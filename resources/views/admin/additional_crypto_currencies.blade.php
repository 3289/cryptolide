@extends('layouts.admin')

@section('content')
    <form style="margin-bottom: 10px;" method="POST" action="/admin/additional_crypto_currencies">
        {{ csrf_field() }}
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Полное название</label>
                    <input type="text" name="name" class="form-control" value="{{ old('name') }}">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Сокращенное название</label>
                    <input type="text" name="short" class="form-control" value="{{ old('short') }}">
                </div>
            </div>
        </div>
        <button class="btn btn-info">Добавить</button>
    </form>
    <table class="table">
        <thead class="thead-light">
        <tr>
            <th>Полное название</th>
            <th>Сокращенное название</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @foreach($additionalCryptoCurrencies as $additionalCryptoCurrency)
            <tr data-id="{{ $additionalCryptoCurrency->id }}">
                <td>{{ $additionalCryptoCurrency->name }}</td>
                <td>{{ $additionalCryptoCurrency->short }}</td>
                <td align="right">
                    <button class="btn btn-sm btn-danger">Удалить</button>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection

@section('scripts')
    <script>
        $(function() {
            $('tbody').on('click', '.btn-danger', function() {
                if (!confirm('Действительно удалить?')) {
                    return;
                }

                let row = $(this).closest('tr');
                $.ajax('/admin/additional_crypto_currencies/' + row.data('id'), {
                    type: 'DELETE',
                    success: function() {
                        row.remove();
                    },
                    error: function(error) {
                        notyf.alert(error.statusText);
                    }
                });
            });
        });
    </script>
@endsection