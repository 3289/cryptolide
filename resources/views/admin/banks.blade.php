@extends('layouts.admin')

@section('content')
    <form style="margin-bottom: 10px;" method="POST" action="/admin/banks">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Название</label>
                    <input type="text" name="name" class="form-control" value="{{ old('name') }}">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Страна</label>
                    <select name="country_id" class="form-control">
                        @foreach($countries as $country)
                            <option value="{{ $country->id }}"{{ $country->id == old('country_id') ? ' selected' : '' }}>
                                {{ $country->name }}
                            </option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        {{ csrf_field() }}
        <button class="btn btn-info">Добавить</button>
    </form>
    <table class="table">
        <thead class="thead-light">
        <tr>
            <th>Название</th>
            <th>Страна</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @foreach($banks as $bank)
            <tr data-id="{{ $bank->id }}">
                <td>{{ $bank->name }}</td>
                <td>{{ $bank->country->name }}</td>
                <td align="right">
                    <button class="btn btn-sm btn-info">Изменить</button>
                    <button class="btn btn-sm btn-danger">Удалить</button>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection

@section('modals')
    <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myLargeModalLabel">Large modal</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label>Название</label>
                                    <input type="text" name="name" class="form-control">
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label>Страна</label>
                                    <select name="country_id" class="form-control">
                                        @foreach($countries as $country)
                                            <option value="{{ $country->id }}"{{ $country->id == old('country_id') ? ' selected' : '' }}>
                                                {{ $country->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <button class="btn btn-info">Применить</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(function() {
            $('tbody').on('click', '.btn-info', function() {
                let row = $(this).closest('tr');
                $.get('/admin/banks/' +  row.data('id')).done(function(response) {
                    let modal = $('.modal');
                    modal.find('form').attr('action', '/admin/banks/' + response.id);
                    modal.find('[name="country_id"]').val(response['country_id']);
                    modal.find('[name="name"]').val(response['name']);
                    modal.find('.modal-title').text(response.name);
                    modal.modal('show');
                });
            }).on('click', '.btn-danger', function() {
                if (!confirm('Действительно удалить?')) {
                    return;
                }

                let row = $(this).closest('tr');
                $.ajax('/admin/banks/' + row.data('id'), {
                    type: 'DELETE',
                    success: function() {
                        window.location.reload();
                    },
                    error: function(error) {
                        notyf.alert(error.statusText);
                    }
                });
            });


            $('.modal form').on('submit', function(event) {
                event.preventDefault();
                let form = $(this);
                $.ajax(form.attr('action'), {
                    type: 'PATCH',
                    data: form.serialize(),
                    success: function() {
                        window.location.reload();
                    },
                    error: function(error) {
                        notyf.alert(error.statusText);
                    }
                });
            });
        });
    </script>
@endsection