@extends('layouts.admin')

@section('content')
    <style>
        .action-table__seller-name {
            letter-spacing: 0.03em; }
        .action-table__seller-rating {
            display: inline;
            position: relative;
            width: 45px;
            margin-top: 3px;
            padding-left: 26px;
            padding-right: 12px;
            -webkit-border-radius: 1px;
            border-radius: 1px;
            background: #EDEFF2;
            font-size: 13px;
            line-height: 22px; }
        .action-table__seller-rating:before {
            content: "";
            position: absolute;
            top: 4px;
            left: 6px;
            width: 14px;
            height: 23px;
            background: url("../images/icons/icon_rating.png") 0 0 no-repeat; }
        .deal-history__transaction-status-img {
            display: inline;
            margin-bottom: 15px;
        }
        .deal-history__transaction-status-text {
            font-weight: 500;
            display: inline;
        }
    </style>
    <form style="margin-bottom: 10px;">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Сделки</label>
                    <select class="form-control" name="status">
                        <option value="">Выберите...</option>
                        <option value="active"{{ request('status') == 'active' ? ' selected' : '' }}>Активные</option>
                        <option value="archive"{{ request('status') == 'archive' ? ' selected' : '' }}>Архив</option>
                    </select>
                </div>
            </div>
        </div>
        <button class="btn btn-info" type="submit">Применить</button>
        <a class="btn btn-warning" href="/admin/deals">Сбросить</a>
    </form>

    <table class="table">
        <thead class="thead-light">
        <tr>
            <th>ID</th>
            <th>Дата</th>
            <th>Финансовые детали сделки</th>
            <th>Комиссия</th>
            <th>Статус сделки</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @foreach($deals as $deal)
            <tr data-id="{{ $deal->id }}">
                <td>{{ $deal->id }}</td>
                <td>{{ $deal->created_at }}</td>
                <td>
                    <div style="display: inline-block; float: left; margin-right: 15px">
                        <a href="/profile/{{ $deal->seller->login }}" class="action-table__seller-name">{{ $deal->seller->login }}</a>
                        <div class="action-table__seller-rating">
                            {{ $deal->seller->rating }}
                        </div>
                        <span> отдает: {{ $deal->sumCrypto() . ' ' . $deal->crypto->short }} </span>
                        <br>
                        <a href="/profile/{{ $deal->buyer->login }}" class="action-table__seller-name">{{ $deal->buyer->login }}</a>
                        <div class="action-table__seller-rating">
                            {{ $deal->buyer->rating }}
                        </div>
                        <span> отдает: {{ $deal->sumCurrency() . ' ' . $deal->currency_name }} </span>
                        </div>
                    <div style="display: inline-block">
                        <span>Курс: 1 {{ $deal->crypto->short }} = {{ $deal->rate() }} {{ $deal->short_type == 'trade' ? $deal->crypto_trade->short : $deal->currency_name }}</span>
                        <br>
                        <span>Закрытите: {{ $deal->time }} мин</span>
                    </div>
                </td>
                <td>
                    @if ($deal->status == 'completed')
                        {{ number_format($deal->sumCrypto() / 100 * ($deal->operations->first() ? $deal->operations->first()->commission : $deal->creator->commission), 8, '.', '') }} {{ $deal->crypto->short }}
                    @else
                        {{ number_format($deal->sumCrypto() / 100 * $deal->creator->commission, 8, '.', '') }} {{ $deal->crypto->short }}
                    @endif
                    <br>
                    <a href="/profile/{{ $deal->creator->login }}" class="action-table__seller-name">{{ $deal->creator->login }}</a>
                </td>
                <td class="status">
                    @if ($deal->status == 'pending')
                        <div class="deal-history__transaction-status-img">
                            <img src="/images/icons/icon_faq_clock.png" alt="">
                        </div>
                        <div class="deal-history__transaction-status-text">Ожидает оплаты</div>
                    @elseif ($deal->status == 'cancelled')
                        <div class="deal-history__transaction-status-img">
                            <img src="/images/icons/icon_cancel_deal_user.png" alt="">
                        </div>
                        <div class="deal-history__transaction-status-text">Отменена</div>
                        @if ($deal->admin_change)
                            <p style="font-size: 10px">Изменено пользователем admin в {{ $deal->updated_at }}.</p>
                        @endif
                    @elseif ($deal->status == 'paid')
                        <div class="deal-history__transaction-status-img">
                            <img src="/images/icons/icon_sending_money.png" alt="">
                        </div>
                        <div class="deal-history__transaction-status-text">Оплачена</div>
                    @elseif ($deal->status == 'completed')
                        <div class="deal-history__transaction-status-img">
                            <img src="/images/icons/icon_successfull_transaction.png" alt="">
                        </div>
                        <div class="deal-history__transaction-status-text">Успешно завершена</div>
                        @if ($deal->admin_change)
                            <p style="font-size: 10px">Изменено пользователем admin в {{ $deal->updated_at }}.</p>
                        @endif
                    @endif
                </td>
                <td>
                    @if (request('status') == 'active')
                        <a class="btn btn-sm btn-warning" href="{{ url('/admin/deals/'.$deal->id.'/auth') }}" target="_blank">Посмотреть сделку</a>
                    @endif
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <div class="float-right">
        {{ $deals->appends(request()->all())->links() }}
    </div>
@endsection

@section('modals')
    <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" style="max-width: 1024px;">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myLargeModalLabel">Large modal</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <input type="hidden" value="" name="id">
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label>Статус</label>
                                    <select id="status-select" class="form-control" name="status">
                                        <option value=""></option>
                                        <option value="completed">Успешно завершена</option>
                                        <option value="cancelled">Отменена</option>
                                    </select>
                                    @if (!auth()->user()->google_fa)
                                        <small class="help-block">Код подтверждения отправляется при изменении статуса</small>
                                    @endif
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    @if (auth()->user()->google_fa)
                                        <div>
                                            Для подтверждения перевода введите код подтверждения с
                                            приложения Google Authenticator.
                                        </div>
                                    @else
                                        <div>
                                            Для подтверждения перевода введите код подтверждения, <br>который
                                            был отправлен на ваш E-mail.
                                        </div>
                                    @endif
                                    <input type="text" name="confirmation_code" placeholder="000000" class="form-control">
                                </div>
                            </div>
                        </div>
                        <button class="btn btn-info">Применить</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(function() {
            @if (request('status') == 'active')
                $('tbody').on('click', '.status', function() {
                    let modal =  $('.modal');
                    let row = $(this).closest('tr');
                    modal.find('#status-select option:first-child').text($(this).find('.deal-history__transaction-status-text').text());
                    modal.find('input[name="id"]').val(row.data('id'));
                    modal.find('form').attr('action', '/admin/deals/' + row.data('id'));
                    modal.find('.modal-title').text('Сделка №'+row.data('id'));
                    modal.modal('show');
                });
            @endif

            @if (!auth()->user()->google_fa)
                $('.modal #status-select').change(function () {
                    $.get('/send-confirmation-code/'+$('input[name="id"]').val()+'?change=1')
                });
            @endif

            $('.modal form').on('submit', function(event) {
                event.preventDefault();
                let form = $(this);
                $.ajax(form.attr('action'), {
                    type: 'PATCH',
                    data: form.serialize(),
                    success: function() {
                        window.location.reload();
                    },
                    error: function(error) {
                        notyf.alert(error.statusText);
                    }
                });
            });
        });
    </script>
@endsection