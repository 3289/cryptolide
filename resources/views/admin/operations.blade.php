@extends('layouts.admin')

@section('content')
    @if (session('status'))
        <div class="alert alert-success" style="margin-bottom: 10px;" role="alert">
            {{ session('status') }}
        </div>
    @endif
    <table class="table">
        <thead class="thead-light">
        <tr>
            <th>Логин пользователя</th>
            <th>Сумма</th>
            <th>Комиссия</th>
            <th>Описание</th>
            <th>Дата</th>
        </tr>
        </thead>
        <tbody>
            @foreach($operations as $operation)
                <tr data-id="{{ $operation->id }}">
                    <td>
                        <a href="/admin/users?login={{ $operation->user->login }}" target="_blank" class="text-info" style="text-decoration: none;">
                            {{ $operation->user->login }}
                        </a>
                    </td>
                    <td class="text-{{ $operation->amount > 0 ? 'success' : 'danger' }}">{{ $operation->amount . ' ' . $operation->crypto_currency->short}}</td>
                    <td class="text-info">{{ $operation->commission . ' ' . $operation->crypto_currency->short}}</td>
                    <td>{{ strip_tags($operation->description) }}</td>
                    <td>{{ $operation->created_at->format('H:i d.m.Y') }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <div class="float-right">
        {{ $operations->appends(request()->all())->links() }}
    </div>
@endsection