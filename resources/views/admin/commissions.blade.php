@extends('layouts.admin')

@section('content')
    <table class="table">
        <thead class="thead-light">
        <tr>
            <th>Сокращенное название</th>
            <th>Комиссия для ввода</th>
            <th>Комиссия для вывода</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @foreach($crypto_currencies as $crypto_currency)
            <tr data-id="{{ $crypto_currency->id }}">
                <td>{{ $crypto_currency->short }}</td>
                <td>{{ $crypto_currency->commission_input }}</td>
                <td>{{ $crypto_currency->commission_output }}</td>
                <td align="right">
                    <button class="btn btn-sm btn-info">Изменить</button>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection

@section('modals')
    <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myLargeModalLabel">Large modal</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label>Название</label>
                                    <input type="text" name="short" class="form-control" disabled>
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label>Комиссия для ввода</label>
                                    <input type="text" name="commission_input" class="form-control">
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label>Комиссия для вывода</label>
                                    <input type="text" name="commission_output" class="form-control">
                                </div>
                            </div>
                        </div>
                        <button class="btn btn-info">Применить</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(function() {

            $('tbody').on('click', '.btn-info', function() {
                let row = $(this).closest('tr');
                $.get('/admin/commissions/' +  row.data('id')).done(function(response) {
                    let modal = $('.modal');
                    modal.find('form').attr('action', '/admin/commissions/' + response.id);
                    modal.find('[name="short"]').val(response.short);
                    modal.find('[name="commission_input"]').val(response.commission_input);
                    modal.find('[name="commission_output"]').val(response.commission_output);
                    modal.find('.modal-title').text(response.name);
                    modal.modal('show');
                });
            });

            $('.modal form').on('submit', function(event) {
                event.preventDefault();
                let form = $(this);
                $.ajax(form.attr('action'), {
                    type: 'PATCH',
                    data: form.serialize(),
                    success: function() {
                        window.location.reload();
                    },
                    error: function(error) {
                        notyf.alert(error.statusText);
                    }
                });
            });
        });
    </script>
@endsection