@extends('layouts.admin')

@section('content')
    <table class="table">
        <thead class="thead-light">
        <tr>
            <th>Дата</th>
            <th>Логин</th>
            <th>Тип комиссии</th>
            <th style="width: 180px;">Сумма</th>
            <th>Комиссия</th>
            <th>Партнёрские</th>
            <th>Прибыль</th>
            <th>Описание</th>
        </tr>
        </thead>
        <tbody>
        @foreach($operations as $operation)
            <tr data-id="{{ $operation->id }}">
                <td>{{ $operation->created_at->format('H:i d.m.Y') }}</td>
                <td>
                    @if ($operation->deal)
                        <a href="/admin/users?login={{ $operation->deal->creator->login }}" target="_blank" class="text-info" style="text-decoration: none;">
                            {{ $operation->deal->creator->login }}
                        </a>
                    @else
                        <a href="/admin/users?login={{ $operation->user->login }}" target="_blank" class="text-info" style="text-decoration: none;">
                            {{ $operation->user->login }}
                        </a>
                    @endif
                </td>
                <td>
                    @if (!is_null($operation->deal_id))
                        Успешная сделка
                    @elseif ($operation->amount > 0)
                        Пополнение баланса
                    @else
                        Вывод
                    @endif
                </td>
                <td class="text-{{ $operation->amount > 0 ? 'success' : 'danger' }}"><span style="color: #999;">{{ $operation->commission+$operation->amount. ' ' . $operation->crypto_currency->short}}</span><br>
                    {{ ($operation->amount < 0 ? number_format($operation->amount+$operation->commission_in_crypto, 8) : $operation->amount) . ' ' . $operation->crypto_currency->short}}
                </td>
                <td class="text-info">{{ $operation->commission . ' ' . $operation->crypto_currency->short . ((is_null($operation->deal_id) && $operation->amount > 0) ? '' : ' (' . number_format($operation->commission/abs($operation->amount)*100, 2) . '%)') }}</td>
                <td class="text-info">
                    @if (!is_null($operation->commission_partner))
                        @if($operation->commission!=0)
                            {{ number_format($operation->commission_partner, 8) . ' ' . $operation->crypto_currency->short . ' для ' . $operation->user->referrer->login . ' ('. number_format($operation->commission_partner/$operation->commission, 2)*100 .'%)'}}
                        @else
                            {{ '0 ' . $operation->crypto_currency->short . ' для ' . $operation->user->referrer->login . ' ('. number_format(0,2) .'%)'}}
                        @endif
                    @endif
                    {{--{{ $operation->user->referrer['login'] }}--}}
                </td>
                <td class="text-info">{{ number_format($operation->commission - (double)$operation->commission_partner, 8, '.', '') . ' ' . $operation->crypto_currency->short}}</td>
                <td>{{ strip_tags($operation->description) }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <div class="float-right">
        {{ $operations->appends(request()->all())->links() }}
    </div>
@endsection