@extends('layouts.admin')

@section('content')
    <form style="margin-bottom: 10px;" method="post" action="/admin/addresses">
        {{ csrf_field() }}
        <div class="form-group">
            <label>Криптовалюта</label>
            <select class="form-control" name="crypto_id" required>
                @foreach($crypto_currencies as $crypto_currency)
                    <option value="{{ $crypto_currency->id }}">{{ $crypto_currency->name }}</option>
                @endforeach
            </select>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Личные адреса</label>
                    <textarea name="private" class="form-control" rows="10"></textarea>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Адреса для всех пользователей</label>
                    <textarea name="public" class="form-control" rows="10"></textarea>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                @if (auth()->user()->google_fa)
                    <div>
                        Для подтверждения перевода введите код подтверждения с
                        приложения Google Authenticator.
                    </div>
                @else
                    <div>
                        Для подтверждения перевода введите код подтверждения, <br>который
                        был отправлен на ваш E-mail.
                    </div>
                @endif
            </div>
            <div class="col-md-2">
                <div class="form-group">

                    <input type="text" name="confirmation_code" placeholder="000000" class="form-control" required>
                </div>
            </div>
            <div class="col-md-2">
                @if (!auth()->user()->google_fa)
                    <button id="send_email" class="btn btn-info" type="button">Отправить код на E-mail</button>
                @endif
            </div>
        </div>
        <button class="btn btn-info">Добавить</button>
    </form>

    <form style="margin-bottom: 10px;">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Типы адресов</label>
                    <select class="form-control" name="type">
                        <option value="">Выберите...</option>
                        <option value="private"{{ request('type') == 'private' ? ' selected' : '' }}>Личные</option>
                        <option value="public"{{ request('type') == 'public' ? ' selected' : '' }}>Общие</option>
                    </select>
                </div>
            </div>
        </div>
        <button class="btn btn-info" type="submit">Применить</button>
        <a class="btn btn-warning" href="/admin/addresses">Сбросить</a>
    </form>

    <table class="table">
        <thead class="thead-light">
        <tr>
            <th>#</th>
            <th>Адрес</th>
            <th>Криптовалюта</th>
            <th>Тип</th>
            <th>Привязка к юзеру</th>
        </tr>
        </thead>
        <tbody>
        @foreach($addresses as $address)
            <tr data-id="{{ $address->id }}">
                <td>{{ $address->id }}</td>
                <td>{{ $address->address }}</td>
                <td>{{ $address->crypto_currency->name }}</td>
                <td>
                    @if ($address->type == 'public')
                        Для всех пользователей
                    @else
                        Личный
                    @endif
                </td>
                <td>
                    @if ($address->user != null)
                        <a href="/admin/users?login={{ $address->user->login }}" target="_blank" class="text-info"
                           style="text-decoration: none;">
                            {{ $address->user->login }}
                        </a>
                    @else
                        нет
                    @endif
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <div class="float-right">
        {{ $addresses->appends(request()->all())->links() }}
    </div>

@endsection


@section('scripts')
    <script>
        $(document).ready(function () {
            @if (!auth()->user()->google_fa)
                $('#send_email').click(function (e) {
                    e.preventDefault();
                    $.get('send-confirmation-code');
                    notyf.confirm('Код отправлен на Вашу почту');
                });
            @endif

            showInfo();
        });

        function showInfo() {
            status = {!! json_encode(Session::get('status')) !!};
            message = {!! json_encode(Session::get('message')) !!};
            if (status === 'error'){
                notyf.alert(message);
            }
            if (status === 'success'){
                notyf.confirm(message);
            }
        }
    </script>
@endsection