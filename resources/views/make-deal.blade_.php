@extends('layouts.app')

@title('Заключение сделки')

@className('main_make-deal')

@section('content')
	<div class="content">
		<div class="title-wrap title-wrap_make-deal">
			<div class="container">
				<div class="row">
					<h1 class="page-title title-wrap__title">
						@if ($deal->short_type == 'trade')
							Обмен
						@elseif ($deal->short_type == 'buy')
							Продажа
						@elseif ($deal->short_type == 'sell')
							Покупка
						@endif
						<span class="title-wrap__bitcoin-number">
							{{ $deal->sumCrypto() . ' ' . $deal->crypto->short }}</span>
							за
							<span class="title-wrap__bitcoin-amount">{{ $deal->sumCurrency() . ' ' . $deal->currency_name }}</span>
							@if ($deal->type == 'buy_online' || $deal->type == 'sell_online')
								через:
								{{ $deal->payment_type->name }}
								@if ($deal->payment_type_id == 2)
									- {{ $deal->banks }}
								@endif
							@endif
							<br>
							<span class="title-wrap__transaction-note">
								Сделка совершается по курсу: 1 BTC = {{ $deal->rate() . ' ' . $deal->currency_name }}
							</span>
					</h1>
					<a href="/" class="btn-back title-wrap__back">назад</a>
				</div>
			</div>
		</div>
		<div class="transaction-block">
			<div class="container">
				<div class="row">
					<div class="transaction-block__content">
						<div class="transaction-block__participant transaction-block__participant_first">
							<div class="transaction-block__participant-first-col">
								<div class="transaction-block__participant-photo">
									<img src="{{ $deal->seller->image }}" alt="Первый участник">
								</div>
							</div>
							<div class="transaction-block__participant-second-col">
								<div class="transaction-block__number transaction-block__number_green">
									{{ $deal->sumCrypto() . ' ' . $deal->crypto->short }}
								</div>
								<div class="user user_transaction-block user_participant-first">
									<div class="user__info user__info_{{ $deal->seller->is_online ? 'online' : 'offline' }}">
										<a href="/profile/{{ $deal->seller->login }}" class="user__name">{{ $deal->seller->login }}</a>
										<div class="user__rating">{{ $deal->seller->rating }} <span class="user__icons"></span></div>

									</div>
								</div>
							</div>
						</div>
						<div class="transaction-block__transaction-info">
							<div class="transaction-block__transaction-number">Сделка №{{ $deal->id }}</div>
							@if ($deal->time_left > 0)
								<div class="transaction-block__closing-time">закрытие через {{ $deal->time_left }} мин</div>
							@endif
						</div>
						<div class="transaction-block__participant transaction-block__participant_second">
							<div class="transaction-block__participant-first-col">
								<div class="transaction-block__number transaction-block__number_blue">
									{{ $deal->sumCurrency() . ' ' . $deal->currency_name }}
								</div>
								<div class="user user_transaction-block user_participant-second">
									<div class="user__info user__info_{{ $deal->buyer->is_online ? 'online' : 'offline' }}">
										<a href="/profile/{{ $deal->buyer->login }}" class="user__name">{{ $deal->buyer->login }}</a>
										<div class="user__rating">{{ $deal->buyer->rating }} <span class="user__icons"></span></div>
									</div>
								</div>
							</div>
							<div class="transaction-block__participant-second-col">
								<div class="transaction-block__participant-photo">
									<img src="{{ $deal->buyer->image }}" alt="Второй участник">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="transaction-status">
			<div class="container">
				<div class="row">
					<div class="transaction-status__text">
						Статус сделки:
						@if ($deal->status == 'pending')
						Ожидание оплаты от
							<a href="#">{{ $deal->buyer->login }}
							</a>
						@elseif ($deal->status == 'paid')
							Оплачена
						@elseif ($deal->status == 'cancelled')
							Отменена
						@elseif ($deal->status == 'completed')
							Завершена
						@endif
					</div>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row">
				<div class="blocks-wrapper">
					<div class="left-blocks">
						<div class="online-chat information-block">
							<h2 class="online-chat__title information-block__title">Онлайн чат с {{ $deal->partner->login }}</h2>
							<div class="information-block__content" id="chat">
								<form class="send-message online-chat__send-message">
                                    <input type="hidden" name="deal_id" value="{{ $deal->id }}">
									<div class="send-message__file">
										<label for="file" class="send-message__file-label" title="Прикрепить файл"></label>
										<input type="file" name="file" id="file" class="send-message__file-input">
									</div>
									<div class="send-message__form-item">
										<input type="text" name="text" placeholder="Введите текст сообщения..." class="send-message__form-text">
									</div>
									<div class="send-message__form-actions">
										<button class="send-message__form-submit" title="Отправить сообщение"></button>
									</div>
								</form>
								<div class="online-messages online-chat__messages stylized-scroll" id="message-list"></div>
							</div>
						</div>
					</div>
					<div class="right-blocks">
						<div class="information-block information-block_further-actions" {{ $deal->time_left < 0 ? ' hidden' : '' }}>
							<h2 class="information-block__title">Ваши дальнейшие действия</h2>
							<div class="information-block__content">
								<div class="information-block__steps">
									<div class="information-block__step">
										<h3 class="information-block__step-title">Шаг 1: Проверьте информацию о сделке</h3>
										<div class="information-block__step-content">
											Сумма в депонировании: <span style="font-weight: 500;">1.00003321</span><br>
											Сумма получаемого платежа: <span style="font-weight: 500;">50.00 UAH</span><br>
											Примечание к транзакции: <span style="font-weight: 500;">CL759</span><br>
										</div>
									</div>
									<div class="transaction-terms information-block__transaction-terms" style="background: #F2F3F6 url(../images/icons/icon_calendar_green.png) 30px 7px no-repeat; color:#3a9d41;">
										<div class="transaction-terms__title">Детали сделки, как показано покупателю:</div>
										<div class="transaction-terms__text" style="color: #393939;margin: 15px -54px 0px -53px; position: relative; left: -10px;">
												<div style="padding: 15px 63px; background: #FAFAF9;">Описание сделки отсутствует. Вы можете уточнить все необходимые детали через онлайн чат.</div>

									
												@if ($deal->type == 'buy_online' || $deal->type == 'sell_online')
												<div class="application__item" style="margin-left:30px; margin-top: 15px;">
													<div class="application__item-name">Способ:</div>
													<div class="application__item-value" style="max-width: 315px;">
														{{ $deal->payment_type->name }}
														@if ($deal->payment_type_id == 2)
															- {{ $deal->banks }}
														@endif
													</div>
												</div>
												@endif
												@if ($deal->type == 'sell_online')
																								<div class="application__item" style="margin-left:30px;">
																										<div class="application__item-name">Реквизиты:</div>
																										<div class="application__item-value" style="max-width: 315px;">{{ $deal->requisites }}</div>
																								</div>
												@endif
												<div class="application__item" style="margin-left:30px;">
													<div class="application__item-name">Сумма:</div>
													<div class="application__item-value" style="max-width: 315px;">{{ $deal->sumCurrency() . ' ' . $deal->currency_name }}</div>
												</div>
												<div class="application__item" style="margin-left:30px;">
													<div class="application__item-name">Примечание:</div>
													<div class="application__item-value" style="max-width: 315px;">CL{{ $deal->id }}</div>
												</div>
							
										</div>
									</div>
									<div class="information-block__step">
										<h3 class="information-block__step-title">Шаг 2: Подтвердите получение средств</h3>
										<div class="information-block__step-content">
											<p>Пользователь <strong>Sveta909900</strong> <strong style="color:#bd4625">еще не отметил</strong>, что перевел вам средства. Если оплата со стороны покупателя не будет совершена 
											в течение <strong style="color: #b88540;">90 минут</strong> - сделка автоматически закроется.</p>
											<br>
											<p>Если вам уже поступили средства по этой сделке - вы можете отправить пользователю <strong>0.00983899 BTC</strong> нажав на кнопку ниже.</p>
										</div>
									</div>
								</div>

                                @if ($deal->status == 'paid')
                                    @if ($deal->buyer->id == auth()->user()->id)
                                        <div class="funds-transferred information-block__funds-transferred">Вы перевели денежные средства</div>
                                    @else
                                        <div class="transaction-state transaction-state_confirm-transfer information-block__transaction-state" style="margin-bottom: 10px;">
                                            <div class="transaction-state__header">
                                                <h3 class="transaction-state__name">Подтвердите перевод криптовалюты</h3>
                                            </div>
                                            <div class="transaction-state__body">
                                                <div class="transaction-state__text">
                                                    <p>Я, <strong>{{ $deal->seller->login }}</strong>, подтверждаю что:</p>
                                                    <ul>
                                                        <li>- Получил <strong>{{ $deal->sumCurrency() }} {{ $deal->currency_name }}</strong> по текущей сделке №{{ $deal->id }}.</li>
                                                    </ul>
                                                </div>
                                                <div class="transaction-state__note">Обратите внимание! Существуют разные способы мошенничества <br>после перевода денежных средств на ваши реквизиты. Рекомендуем <br>предпринять все необходимые меры безопасности, чтобы не позволить <br>мошенникам нанести вред. Более подробно об этом <a href="#">написано здесь.</a></div>
                                            </div>
                                            <div class="transaction-state__confirmation-code">
                                                @if ($deal->seller->google_fa)
                                                    <div class="transaction-state__confirmation-text">
                                                        Для подтверждения перевода введите код подтверждения с приложения Google Authenticator.
                                                    </div>
                                                @else
                                                    <div class="transaction-state__confirmation-text">
                                                        Для подтверждения перевода введите код подтверждения, <br>который был отправлен на ваш E-mail. <a style="cursor: pointer" id="re-send-confirmation-code">Отправить повторно.</a>
                                                    </div>
                                                @endif
                                                <input type="text" name="confirmation_code" placeholder="000000" class="transaction-state__confirmation-input">
                                            </div>
                                            <div class="form-error transaction-state__form-error" style="display: none;" id="error-confirmation-code"></div>
                                            <div class="transaction-state__buttons">
                                                <button type="button" class="btn-green2 transaction-state__confirm-transfer" id="confirm-and-close">Подтвердить и отправить {{ $deal->sumCrypto() }} {{ $deal->crypto->short }}</button>
                                            </div>
                                        </div>
                                    @endif
                                @endif

                                @if ($deal->status == 'pending' && $deal->buyer->id == auth()->user()->id)
                                    <div class="information-block__confirm-payment" style="width: 100%; max-width:100%">
                                        <button class="btn-action" id="confirm-deal" style="width: 100%; max-width:100%">Подтвердить и отправить 0.00022200 BTC</button>
                                    </div>

                                    <div class="transaction-state information-block__transaction-state" id="confirm-transfer-block" style="display: none;margin-bottom: 10px;">
                                        <div class="transaction-state__header">
                                            <h3 class="transaction-state__name">Подтвердите оплату</h3>
                                        </div>
                                        <div class="transaction-state__body">
                                            <div class="transaction-state__text">
                                                <p>Я, <strong>{{ auth()->user()->login }}</strong>, подтверждаю что:</p>
                                                <ul>
                                                    <li>- Принимаю <a href="#">условия сделки</a> по объявлению №{{ $deal->id }}.</li>
                                                    <li>- Перевел деньги, следуя инструкциям продавца <strong>{{ $deal->seller->login }}</strong>.</li>
                                                </ul>
                                            </div>
                                            <div class="transaction-state__note">Если вы не уверены, что правильно совершили денежный перевод или <br>у вас не получается перевести средства - напишите в онлайн чат, который <br>находится левее, и попросите пользователя {{ $deal->seller->login }} вам помочь.</div>
                                        </div>
                                        <div class="transaction-state__buttons">
                                            <button type="button" class="btn-green2 transaction-state__confirm-payment" id="confirm-transfer">Подтвердить <br><span>я заплатил</span></button>
                                            <button type="button" class="btn-blue transaction-state__cancel-payment right" id="dont-confirm-transfer">Отменить <br><span>я еще не заплатил</span></button>
                                        </div>
                                    </div>
																		<hr style="max-width: 155px; margin: 20px auto; border-top: 2px solid #e3e3e3;">
																		<div class="information-block__step">
																			<div class="information-block__step-content">
																				Если покупатель не отвечает или не совершает платеж либо возникли разногласия по условиям сделки, вы можете оспорить эту сделку через 38 минут, <span style="color:#4183bc;">обратившись в арбитраж</span>.
																			</div>
																		</div>
                                @endif

                                @if ($deal->status == 'completed')
                                    Сделка завершена
                                @endif

                                @if ($deal->status == 'cancelled')
                                    <div class="deal-cancelled information-block__deal-cancelled">Сделка отменена</div>
                                @elseif ($deal->status != 'completed')
                                    
                                    @if ($deal->status == 'paid' && $deal->buyer->id == auth()->user()->id)
                                        <div class="transaction-state transaction-state_cancel-deal information-block__transaction-state" id="confirm-cancel-block" style="display: none;">
                                            <div class="transaction-state__header">
                                                <h3 class="transaction-state__name">Подтвердите отмену сделки</h3>
                                            </div>
                                            <div class="transaction-state__body">
                                                <div class="transaction-state__important">Обратите внимание!</div>
                                                <div class="transaction-state__text">
                                                    <p><strong>Вы указали</strong>, что перевели средства в размере <strong>{{ $deal->sumCurrency() }} {{ $deal->currency_name }}</strong> <br>пользователю <strong>{{ $deal->seller->login }}</strong>. После закрытия сделки сервис <br>CRYPTOLIDE освободит от депонирования средства продавца <br>и не сможет гарантировать вам успешное заключение сделки.</p>
                                                    <p>Вы действительно хотите отменить сделку №{{ $deal->id }}?</p>
                                                </div>
                                                <div class="transaction-state__description">Покупка {{ $deal->sumCrypto() }} {{ $deal->crypto->short }} за {{ $deal->sumCurrency() }} {{ $deal->currency_name }} через: <br>Банковский перевод (Украина) - Все банки страны</div>
                                            </div>
                                            <div class="transaction-state__buttons">
                                                <button type="button" class="btn-blue transaction-state__dont-cancel" id="dont-cancel">Не отменять сделку</button>
                                                <div class="transaction-state__confirm-cancellation">
                                                    <a class="transaction-state__confirm-link" id="confirm-cancel" style="cursor:pointer;">Подтвердить отмену сделки</a>
                                                </div>
                                            </div>
                                        </div>
                                    @else
                                        <div class="transaction-state information-block__transaction-state" id="confirm-cancel-block" style="display: none;">
                                            <div class="transaction-state__header">
                                                <h3 class="transaction-state__name">Подтвердите отмену сделки</h3>
                                            </div>
                                            <div class="transaction-state__body">
                                                <div class="transaction-state__answer">Вы действительно хотите отменить сделку №{{ $deal->id }}?</div>
                                                <div class="transaction-state__description">Покупка {{ $deal->sumCrypto() }} {{ $deal->crypto->short }} за {{ $deal->sumCurrency() }} {{ $deal->currency_name }} через: <br>Банковский перевод (Украина) - Все банки страны</div>
                                                @if ($deal->buyer->id == auth()->user()->id)
                                                    <div class="transaction-state__note">Ни в коем случае не отменяйте сделку, если вы перевели средства! После <br>закрытия сделки средства продавца освобождаются от депонирования.</div>
                                                @endif
                                            </div>
                                            <div class="transaction-state__buttons">
                                                <button type="button" class="btn-green2 transaction-state__cancel" id="confirm-cancel">Отменить сделку</button>
                                                <button type="button" class="btn-blue transaction-state__dont-cancel right" id="dont-cancel">Не отменять сделку</button>
                                            </div>
                                        </div>
                                    @endif
                                @endif
							</div>
						</div>
						<div class="about-transactions"><a href="#" class="transaction-info-link">Подробнее о сделках и арбитраже</a></div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('scripts')
    <script>
        $(function() {
            $('#confirm-deal').on('click', function() {
                $('#confirm-transfer-block').css('display', 'block');
            });

            $('#cancel-deal').on('click', function () {
                $('#confirm-cancel-block').css('display', 'block');
            });

            $('#dont-cancel').on('click', function() {
                $('#confirm-cancel-block').css('display', 'none');
            });

            $('#confirm-cancel').on('click', function() {
                updateStatus('cancelled');
            });

           

            function updateStatus(status) {
                $.ajax('/deals/{{ $deal->id }}', {
                    type: 'PATCH',
                    data: {
                        status: status,
                        confirmation_code: $('[name="confirmation_code"]').val()
                    },
                    success: function(response) {
                        if (response.status === true) {
                            window.location.reload();

                            return false;
                        }

                        if (status === 'completed') {
                            $('[name="confirmation_code"]').val('');

                            $('#error-confirmation-code').css('display', 'block').text(response.message);

                            return false;
                        }

                        alert('Error.');
                    },
                    error: function (err) {
                        alert(err.statusText);
                    }
                });
            }

            $('#confirm-transfer').on('click', function () {
                updateStatus('paid');
            });

            $('#dont-confirm-transfer').on('click', function () {
                $('#confirm-transfer-block').css('display', 'none');
            });

            $('#re-send-confirmation-code').on('click', function() {
                $.get('/send-confirmation-code/{{ $deal->id }}').done(function(response) {
                    if (response.status === true) {
                        alert('Код успешно отправлен.');
                    } else {
                        alert('Error.');
                    }
                }).fail(function(err) {
                    alert(err.statusText);
                });
            });

            $('#confirm-and-close').on('click', function () {
                updateStatus('completed');
            });

            let messages = [];

            function render(message)
			{
                    $('#message-list').prepend('<div class="message ' + (message.from_me ? 'message_your' : 'message_interlocator') + ' clearfix online-messages__message">\n' +
                        '<div class="message__first-col">\n' +
                        '<div class="message__avatar"><a href="#"><img src="' + message.user.image+'" alt=""></a></div>\n' +
                        '</div>\n' +
                        '<div class="message__second-col">\n' +
                        '<div class="message__information">\n' +
                        '<div class="message__author-name">'+message.user.login+'</div>\n' +
                        '<div class="message__datetime">'+message.created_at+'</div>\n' +
                        '</div>\n' +
                        '<div class="message__text">'+message.text+'</div>\n' +
                        '</div>\n' +
                        '</div>')
			}

            setInterval(function() {
                $.get('/messages/{{ $deal->id }}').done(function(response) {

                    if (response.data.length !== 0) {
                        if (messages.length < response.data.length) {
                            response.data.forEach(function(message) {
                               	if (messages.filter(function(element) {
                               	    	return message.id === element.id;
									}).length === 0)
                               	{
                                    messages.push(message);
                                    render(message);
								}
							});
                        }
					}
                });
            }, 1000);

            $('#chat').on('submit', 'form', function (event) {
                event.preventDefault();

                let form = $(this);
                $.post('/messages', form.serialize()).done(function(response) {
                    form.find('[name="text"]').val('');

                    messages.push(response.data);
                    render(response.data);
                });
            });



        });
    </script>
@endsection

    <script>
    		$(function() {
             $('#dont-confirm-transfer').on('click', function() {
                $('#confirm-transfer-block').css('display', 'none');
                $('.information-block__confirm-payment').show();
            });
           });
    </script>
