@extends('layouts.app')

@title('Восстановление пароля')

@section('content')
    <div class="content">
        <div class="container">
            <div class="row">
                <form action="{{ route('password.email') }}" method="POST" class="authorization-form authorization-form_password-recovery">
                    {{ csrf_field() }}
                    <h2 class="authorization-form__title">Восстановления пароля</h2>
                    <div class="authorization-form__fieldset">
                        <div class="form-warning authorization-form__form-warning">
                            @if (session('status'))
                                 На ваш E-mail адрес была отправлена ссылка с восстановлением пароля.
                            @else
                                 Чтобы изменить пароль к учётной записи - укажите E-mail адрес, который был закреплён за аккаунтом.
                            @endif
                        </div>
                        <div class="form-item{{ $errors->has('email') ? ' form-item_has-error' : '' }}">
                            <input type="text" name="email" id="email" placeholder="E-mail" class="form-text">
                            @if ($errors->has('email'))
                                <div class="form-error authorization-form__form-error">{{ $errors->first('email') }}</div>
                            @endif
                        </div>
                    </div>
                    <div class="authorization-form__captcha">
                        {!! app('captcha')->display(); !!}
                    </div>
                    <div class="authorization-form__actions">
                        <button type="submit" class="form-submit">Восстановить пароль</button>
                    </div>
                    <div class="authorization-form__login">
                        <a href="/login" class="authorization-form__login-link">Войти в кабинет</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection