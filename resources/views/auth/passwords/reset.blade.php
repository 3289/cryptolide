@extends('layouts.app')

@title('Восстановление пароля')

@section('content')
    <div class="content">
        <div class="container">
            <div class="row">
                <form method="POST" action="{{ route('password.request') }}" class="authorization-form authorization-form_password-recovery">
                    {{ csrf_field() }}
                    <div class="authorization-form__logo"><img src="/images/content/logo_recovery_password.png" alt="Логотип"></div>
                    <h2 class="authorization-form__title">Изменение пароля к учётной записи</h2>
                    <div class="authorization-form__fieldset">
                        <div class="form-warning authorization-form__form-warning">
                            @if (session('status'))
                                На ваш E-mail адрес была отправлена ссылка с восстановлением пароля.
                            @else
                               Чтобы изменить пароль к учётной записи укажите ваш E-mail адрес ещё раз и введите новый пароль.
                            @endif
                        </div>
                        <div class="form-item form-item_new-password">
                            <label for="new_password" class="authorization-form__label">E-mail</label>
                            <input type="text" name="email" value="{{ $email or old('email') }}" placeholder="Введите ваш e-mail" class="form-text"{!! $errors->has('email') ? ' style="border:1px solid red;"' : '' !!}>
                        </div>
                        <div class="form-item">
                            <label for="new_password" class="authorization-form__label">Пароль</label>
                            <input type="password" name="password" id="new_password" placeholder="Введите новый пароль" class="form-text"{!! $errors->has('password') ? ' style="border:1px solid red;"' : '' !!}>
                        </div>
                        <div class="form-item">
                            <label for="repeat_new_password" class="authorization-form__label">Пароль</label>
                            <input type="password" name="password_confirmation" id="repeat_new_password" placeholder="Введите новый пароль ещё раз" class="form-text">
                        </div>
                    </div>
                    <div class="authorization-form__actions">
                        <button class="form-submit">Изменить пароль</button>
                    </div>
                    <div class="authorization-form__frontpage">
                        <a href="/login" class="authorization-form__frontpage-link">Вернуться на главную страницу</a>
                    </div>
                    <input type="hidden" name="token" value="{{ $token }}">
                </form>
            </div>
        </div>
    </div>
@endsection