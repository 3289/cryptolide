@extends('layouts.app')

@title('Вход')

@section('content')
	<div class="content">
		<div class="container">
			<div class="row">
				<form action="/login" method="POST" class="authorization-form authorization-form_login">
					{{ csrf_field() }}
					<h2 class="authorization-form__title">Вход в личный кабинет</h2>
					<div class="authorization-form__fieldset">
						<div class="form-item{{ $errors->has('email') ? ' form-item_has-error' : '' }}">
							<input type="text" name="email" id="email" placeholder="E-mail" class="form-text" value="{{ old('email') }}">
							@if ($errors->has('email'))
								<div class="form-error authorization-form__form-error">{{ $errors->first('email') }}</div>
							@endif
						</div>
						<div class="form-item{{ $errors->has('password') ? ' form-item_has-error' : '' }}">
							<input type="password" name="password" id="password" placeholder="Пароль" class="form-text">
							@if ($errors->has('password'))
								<div class="form-error authorization-form__form-error">{{ $errors->first('password') }}</div>
							@endif
						</div>
					</div>
					<div class="authorization-form__captcha">
						{!! app('captcha')->display(); !!}
						@if ($errors->has('g-recaptcha-response'))
							<div class="form-error authorization-form__form-error">
								{{ $errors->first('g-recaptcha-response') }}
							</div>
						@endif
					</div>
					<div class="authorization-form__actions">
						<button type="submit" class="form-submit">Войти в кабинет</button>
					</div>
					<div class="authorization-form__login">
						<a href="/password/reset" class="authorization-form__login-link">Забыли пароль?</a>
					</div>
				</form>
			</div>
		</div>
	</div>
@endsection