@extends('layouts.app')

@title('Биржа')

@className('main_exchange')

@section('content')
	<div class="content">
		<div class="title-wrap title-wrap_exchange">
			<div class="container">
				<div class="row">
					<h1 class="page-title title-wrap__title">Биржа</h1>
					<a href="/" class="btn-back title-wrap__back">назад</a>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<div class="exchange-block">
						<div class="exchange-block__message">CRYPTOLIDE БИРЖА НАХОДИТСЯ НА СТАДИИ <span class="exchange-block__beta">BETA</span> ТЕСТИРОВАНИЯ <br>И БУДЕТ ЗАПУЩЕНА В БЛИЖАЙШЕЕ ВРЕМЯ.</div>
						<div class="exchange-block__name">CRYPTOLIDE EXCHANGE <span class="exchange-block__version">ver. 1.0</span></div>
						<div class="exchange-block__progress">
							<img src="/images/content/progressbar.png" alt="Progressbar">
						</div>
						<div class="exchange-block__text">
							<p>Если вы хотите внести свой вклад и принять участие в закрытом тестировании - пришлите заявку на control@cryptolide.com</p>
							<p>Следите за новостями. С уважением, команда CRYPTOLIDE.</p>
						</div>
						<a href="/p2p" class="btn-blue exchange-block__button">Перейти к торговой платформе</a>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('scripts')
	<script>
        $('.jqstyler').styler({
            onFormStyled: function () {
                $('.jq-selectbox').each(function (index, el) {
                    var bgImg = $(this).find('.jq-selectbox__dropdown ul li.selected').css('background-image');
                    $(this).find('.jq-selectbox__select-text').css('background-image', bgImg);
                });
            },
            onSelectClosed: function () {
                var bgImg = $(this).find('.jq-selectbox__dropdown ul li.selected').css('background-image');
                $(this).find('.jq-selectbox__select-text').css('background-image', bgImg);
            }
        });
	</script>
@endsection