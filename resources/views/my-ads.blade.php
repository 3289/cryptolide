@extends('layouts.app')

@title('Мои объявления')

@className('main_my-ads')

@section('content')
<div class="content">
  <div class="title-wrap title-wrap_my-ads">
    <div class="container">
      <div class="row">
        <h1 class="page-title title-wrap__title">Мои сделки и объявления</h1>
        <a href="/" class="btn-back title-wrap__back">назад</a>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <ul class="settings-tabs settings-tabs_my-ads">
          <li class="settings-tabs__item"><a href="/deals" class="settings-tabs__link">МОИ СДЕЛКИ</a></li>
          <li class="settings-tabs__item settings-tabs__item_active">
            <a href="/publications" class="settings-tabs__link">МОИ ОБЪЯВЛЕНИЯ</a>
          </li>
        </ul>
      </div>
    </div>
  </div>
  <div class="ads-list-block">
    <div class="ads-list-block__header">
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <div class="ads-list-block__action">
              @if ($adverts->count() > 0)
                <input type="checkbox" class="ads-list-block__checkbox jqstyler" name="select_all" id="select_all" value="yes">
                <select name="select-action-for-all" class="ads-list-block__select jqstyler">
                  <option value="">Выберите действие</option>
                  <option value="active">Активировать выбранные</option>
                  <option value="hidden">Отключить выбранные</option>
                  <option value="remove">Удалить выбранные</option>
                </select>
                @endif
            </div>
            <h2 class="ads-list-block__title">Список моих объявлений</h2>
            <a href="/publications/create" class="btn-create-ad ads-list-block__create-ad">Создать объявление</a>
          </div>
        </div>
      </div>
    </div>
    <div class="ads-list">
      <div class="ads-list__header">
        <div class="container">
          <div class="row">
            <div class="col-xs-12">
              <div class="table-responsive">
                <table class="ads-list__header-inner">
                  <thead>
                    <tr>
                      <th class="ads-list__check"></th>
                      <th class="ads-list__id">ID &nbsp;&nbsp;&nbsp;&nbsp;Дата &nbsp;&nbsp;&nbsp;&nbsp;Статус</th>
                      <th class="ads-list__general-info">Общая информация</th>
                      <th class="ads-list__financial-info">Финансовая информация</th>
                      <th class="ads-list__control">Управление</th>
                    </tr>
                  </thead>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="ads-list__body">
        @if ($adverts->count() > 0)
        @foreach($adverts as $advert)
        <div class="ads-list__row" data-id="{{ $advert->id }}">
          <div class="container">
            <div class="row">
              <div class="col-xs-12">
                <table class="ads-list__row-inner">
                  <tbody>
                    <tr>
                      <td class="ads-list__check">
                        <input type="checkbox" class="ads-list__checkbox jqstyler" name="select_this" value="yes">
                      </td>
                      <td class="ads-list__id">
                        <a href="#" class="ads-list__ad-id">{{ $advert->id }}</a>
                        <div class="ads-list__datetime">{{ $advert->created_at->format('H:i d.m.Y') }}</div>
                        <select name="select-action" id="select-action" class="ads-list__select jqstyler">
                          <option value="active"{{ $advert->status == 'active' ? ' selected' : '' }}>Активно</option>
                          <option value="hidden"{{ $advert->status == 'hidden' ? ' selected' : '' }}>Отключено</option>
                          <option value="remove">Удалить</option>
                        </select>
                      </td>
                      <td class="ads-list__general-info">
                        <ul class="ads-list__general-info-list">
                          <li>Я хочу: <strong>
                            @if ($advert->type == 'buy_online')
                            Купить криптовалюту онлайн
                            @elseif ($advert->type == 'buy_for_cash')
                            Купить криптовалюту за наличные
                            @elseif ($advert->type == 'sell_online')
                            Продать криптовалюту онлайн
                            @elseif ($advert->type == 'sell_for_cash')
                            Продать криптовалюту за наличные
                            @elseif ($advert->type == 'trade')
                            Обменять криптовалюту
                            @endif
                          </strong></li>
                          @if ($advert->type == 'trade')
                            <li>Отдаете: <strong>{{ $advert->crypto->name . ' - ' . $advert->crypto->short }}</strong></li>
                            <li>Получаете: <strong>{{ $advert->crypto_trade->name . ' - ' . $advert->crypto_trade->short }}</strong></li>
                          @else
                            <li>Криптовалюта: <strong>{{ $advert->crypto->name . ' - ' . $advert->crypto->short }}</strong></li>
                          @endif
                          @if ($advert->country_id)
                          <li>Страна: <strong>{{ $advert->country->name }}</strong></li>
                            @if ($advert->type == 'sell_for_cash' || $advert->type == 'buy_for_cash')
                              <li>Населённый пункт: <strong>{{ $advert->city->name.', '.$advert->city->region }}</strong></li>
                            @endif
                          @endif
                          @if ($advert->type == 'buy_online' || $advert->type == 'sell_online')
                          <li>
                            <strong>
                              {{ $advert->payment_type->name }}
                              @if ($advert->payment_type_id == 2)
                              : {{ $advert->banks }}
                              @endif
                            </strong>
                          </li>
                          @endif
                          @if ($advert->keep_up || $advert->constraints)
                          <li>
                           Опции:
                           <span style="color: #a4926a;">
                             @if ($advert->keep_up)
                             Актуальность суммы;
                             @endif
                             @if ($advert->constraints)
                             Лимит новичкам;
                             @endif
                           </span>
                          </li>
                          @endif
                         @if (!is_null($advert->achievement_id))
                         <li>
                          Кому:
                          Пользователям с отметкой «{{ $advert->achievement->name }}»;
                        </li>
                        @endif
                        <li>Время на оплату сделки: <strong>{{ $advert->time }} мин</strong></li>
                      </ul>
                    </td>
                    <td class="ads-list__financial-info">
                      <ul class="ads-list__financial-info-list">
                        @if ($advert->short_type == 'sell' || $advert->short_type == 'trade')
                        <li>Я отдаю:<span style="font-weight: 500; color: #1c9724;"> {{ $advert->sumCrypto() }} {{ $advert->crypto->short }}</span></li>
                        <li>Я получаю: <strong>{{ $advert->sumCurrency() }} {{ $advert->currency_name }}</strong></li>
                        @elseif ($advert->short_type == 'buy')
                        <li>Я отдаю: <strong>{{ $advert->sumCurrency() }} {{ $advert->currency_name }}</strong></li>
                        <li>Я получаю:<span style="font-weight: 500; color: #1c9724;"> {{ $advert->sumCrypto() }} {{ $advert->crypto->short }}</span></li>
                        @endif
                        <li>Комиссия:
                          {{ number_format($advert->sumCrypto() / 100 * $advert->user->commission, 8, '.', '') }} {{ $advert->crypto->short }}
                        </li>
                      </ul>
                      <div class="ads-list__rate">Курс: <span style="color: #669ac9;">{{ $advert->rate() }} {{ $advert->currency_name }}</span></div>
                    </td>
                    <td class="ads-list__control">
                      <div class="ads-list__edit"><a href="/publications/{{ $advert->id }}/edit" class="btn-blue">Редактировать</a></div>
                      <a href="/publications/{{ $advert->id }}" class="ads-list__view">Посмотреть</a>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      @endforeach
      @else
      <div class="wallet-operations__no-operations">
        <div class="container">
          <div class="row">
            <div class="col-xs-12">
              Вы пока еще не создали ни одного объявления. Сделайте это прямо сейчас!
            </div>
          </div>
        </div>
      </div>
      @endif
    </div>
  </div>
  {{ $adverts->links('pagination.cryptolide') }}
</div>
</div>
@endsection

@section('scripts')
<script>
  $('.jqstyler').styler({
    onFormStyled: function () {
      $('.jq-selectbox').each(function (index, el) {
        var bgImg = $(this).find('.jq-selectbox__dropdown ul li.selected').css('background-image');
        $(this).find('.jq-selectbox__select-text').css('background-image', bgImg);

        var liIndex = $(this).find('.jq-selectbox__dropdown ul li.selected').index();
        switch (liIndex) {
          case 0:
            $(this).find('.jq-selectbox__select-text').css('color', '#1C9724');
            break;
          case 1:
            $(this).find('.jq-selectbox__select-text').css('color', '#f00');
            break;
          case 2:
            $(this).find('.jq-selectbox__select-text').css('color', '#467EB0');
            break;
        }
      });
    },
    onSelectClosed: function () {
      var bgImg = $(this).find('.jq-selectbox__dropdown ul li.selected').css('background-image');
      $(this).find('.jq-selectbox__select-text').css('background-image', bgImg);

      var liIndex = $(this).find('.jq-selectbox__dropdown ul li.selected').index();
      switch (liIndex) {
        case 0:
          $(this).find('.jq-selectbox__select-text').css('color', '#1C9724');
          break;
        case 1:
          $(this).find('.jq-selectbox__select-text').css('color', '#f00');
          break;
        case 2:
          $(this).find('.jq-selectbox__select-text').css('color', '#467EB0');
          break;
      }
    }
  });

  $('[name="select_all"]').on('change', function() {
    $('[name="select_this"]').prop('checked', !$('[name="select_this"]').prop('checked')).closest('div').toggleClass('checked');

    if($('[name="select_this"]:checked').length == 0) {
        $('.ads-list-block__select').css('visibility', 'hidden');
    } else {
        $('.ads-list-block__select').css('visibility', 'visible');
    }
  });

  $('[name="select_this"]').on('change', function() {
    if($('[name="select_this"]:checked').length == 0) {
        $('.ads-list-block__select').css('visibility', 'hidden');
    } else {
        $('.ads-list-block__select').css('visibility', 'visible');
    }
  });

  $('[name="select-action-for-all"]').on('change', function() {
    let select = $(this);

    if (select.val() === '') {
      return;
    }

    if (select.val() === 'remove') {
      if (!confirm('Вы действительно хотите удалить выбранные объявления?')) {
        $(this).val('').trigger('change');

        return;
      }
    }

    $('[name="select_this"]:checked').each(function(index, element) {
      if (select.val() === 'remove') {
        destroy($(element).closest('.ads-list__row'));
      } else {
        $(element).closest('.ads-list__row').find('[name="select-action"]').val(select.val()).trigger('change');
      }
    });

    $(this).val('').trigger('change');
  });

  function destroy(row) {
    $.ajax('/publications/' + row.data('id'), {
      type: 'DELETE',
      success: function(response) {
        row.remove();
      },
      error: function (err) {
        if (err.status === 404) {
          row.remove();
        } else {
          alert(err.statusText);
        }
      }
    });
  }

  $('[name="select-action"]').on('change', function() {
    let row = $(this).closest('.ads-list__row');
    if ($(this).val() === 'remove') {
      if (!confirm('Вы действительно хотите удалить объявление?')) {
        return;
      }

      destroy(row);
    } else if ($(this).val() != '') {
      $.ajax('/change-visibility/' + row.data('id'), {
        type: 'PATCH',
        data: {
          status: $(this).val()
        },
        success: function(response) {
                        //
                      },
                      error: function (err) {
                        if (err.status === 404) {
                          row.remove();
                        } else {
                          alert(err.statusText);
                        }
                      }
                    });
    }
  });

  if($('[name="select_this"]:checked').length == 0) {
    $('.ads-list-block__select').css('visibility', 'hidden');
  } else {
    $('.ads-list-block__select').css('visibility', 'visible');
  }
</script>
@endsection