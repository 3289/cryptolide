@extends('layouts.app')

@title('Новости')

@className('main_infopage')

@section('content')
	<div class="content">
		<div class="infopage">
			<div class="infopage__title-wrap">
				<div class="container">
					<div class="row">
						<div class="col-xs-12">
							<h1 class="infopage-title">Новости</h1>
						</div>
					</div>
				</div>
			</div>
			<div class="container">
				<div class="row">
					<div class="infopage__content">
						<div class="news">
						<div class="news__item infopage-block">
								<div class="news__left">
									<div class="news__date">02.05.2018</div>
								</div>
								<div class="news__right">
									<h2 class="news__title">Зарабатывать на партнёрской программе ещё не было так просто!</h2>
									<div class="news__text">
										<p>Друзья! Напоминаем вам, что 30 мая будет открыт медиа раздел на главной странице нашего сайта, в котором мы разместим видеоролики пользователей. Под каждым видеороликом будет размещаться кнопка регистрации с реферальной ссылкой автора видеоролика. Все что от вас нужно - один раз записать небольшое видео (обзор, инструкцию и т.д.) о сервисе CRYPTOLIDE и отправить его нам. Затем, просто наблюдайте, как растет ваша команда и прибыль по партнерской программе (от 20% до 100% с каждой операции каждого партнера в торговой платформе). В случае возникновения каких-либо вопросов – обращайтесь к нам в онлайн чат. Наши консультанты работают для вас 24/7.
										<p style="text-align: right;">С уважением, команда CRYPTOLIDE.</p>
									</div>
								</div>
							</div>
						<div class="news__item infopage-block">
								<div class="news__left">
									<div class="news__date">25.03.2018</div>
								</div>
								<div class="news__right">
									<h2 class="news__title">Официальный запуск сервиса!</h2>
									<div class="news__text">
										<p>Друзья! С неимоверной радостью сообщаем, что сегодня состоялся официальный релиз сервиса CRYPTOLIDE! Долгих 3 года мы работали не покладая рук, чтобы предоставить вам действительно выгодный, удобный и современный сервис, который хотелось бы рекомендовать друзьям и знакомым. Спасибо за ваше терпение и за то, что вы всегда были и остаётесь с нами!
										<p style="text-align: right;">С уважением, команда CRYPTOLIDE.</p>
									</div>
								</div>
							</div>
							<div class="news__item infopage-block">
								<div class="news__left">
									<div class="news__date">08.03.2018</div>
								</div>
								<div class="news__right">
									<h2 class="news__title">С 8 марта!</h2>
									<div class="news__text">
										<p>Дорогие девушки! От всего сердца поздравляем вас с Международным женским днем! Желаем новых свершений, большого счастья и теплого весеннего солнца в небе. Нежные, сильные,серьёзные, общительные, самодостаточные - вы прекрасны! Будьте собой и наслаждайтесь жизнью!</p>
										<p style="text-align: right;">С любовью, команда CRYPTOLIDE.</p>
									</div>
								</div>
							</div>
						<div class="news__item infopage-block">
								<div class="news__left">
									<div class="news__date">01.01.2018</div>
								</div>
								<div class="news__right">
									<h2 class="news__title">С Новым годом!</h2>
									<div class="news__text">
										<p>Друзья! Команда сервиса CRYPTOLIDE искренне поздравляет вас с наступившим Новым годом! Желаем финансовых успехов и стремительного роста всех ваших активов.</p>
										<p style="text-align: right;">С уважением, команда CRYPTOLIDE.</p>
									</div>
								</div>
							</div>
							<div class="news__item infopage-block">
								<div class="news__left">
									<div class="news__date">01.11.2017</div>
								</div>
								<div class="news__right">
									<h2 class="news__title">Запуск сервиса в режиме SAFE START!</h2>
									<div class="news__text">
										<p>Друзья! Сервис официально запущен и работает в режиме safe start. Проверьте наличие инвайт кода на вашем регистрационном E-mail адресе и присоединяйтесь к финальному тестированию. </p>
										<p style="text-align: right;">С уважением, команда CRYPTOLIDE.</p>
									</div>
								</div>
							</div>						
						<div class="news__item infopage-block">
								<div class="news__left">
									<div class="news__date">20.09.2017</div>
								</div>
								<div class="news__right">
									<h2 class="news__title">Ожидание запуска сервиса!</h2>
									<div class="news__text">
										<p>Друзья! Мы находимся в ожидании запуска Криптолайд! Максимально продумываем каждую мелочь. Не забываем ни о чем и особенно усердно работаем над безопасностью!</p>
										<p style="text-align: right;">С уважением, команда CRYPTOLIDE.</p>
									</div>
								</div>
							</div>
							<div class="news__item infopage-block">
								<div class="news__left">
									<div class="news__date">11.09.2017</div>
								</div>
								<div class="news__right">
									<h2 class="news__title">Открытое голосование по криптовалютам.</h2>
									<div class="news__text">
										<p>Друзья, какие ДВЕ криптовалюты (кроме BTC) вы хотите видеть после запуска сервиса? Можете ответить на форумах или написать на info@cryptolide.com</p>
										<p style="text-align: right;">С уважением, команда CRYPTOLIDE.</p>
									</div>
								</div>
							</div>
							<div class="news__item infopage-block">
								<div class="news__left">
									<div class="news__date">8.09.2017</div>
								</div>
								<div class="news__right">
									<h2 class="news__title">Успешное завершение настроек для защиты от DDOS.</h2>
									<div class="news__text">
										<p>Друзья! Серверные настройки для обеспечения защиты от DDOS завершены. В качестве сотрудничества был выбран всемирно известный сервис Cloudflare.</p>
										<p style="text-align: right;">С уважением, команда CRYPTOLIDE.</p>
									</div>
								</div>
							</div>
							<div class="news__item infopage-block">
								<div class="news__left">
									<div class="news__date">6.09.2017</div>
								</div>
								<div class="news__right">
									<h2 class="news__title">Анонс запуска сервиса.</h2>
									<div class="news__text">
										<p>Друзья! Дата запуска сервиса CRYPTOLIDE будет объявлена 20 сентября. Следите за новостями!</p>
										<p style="text-align: right;">С уважением, команда CRYPTOLIDE.</p>
									</div>
								</div>
							</div>
							<div class="news__item infopage-block">
								<div class="news__left">
									<div class="news__date">6.09.2017</div>
								</div>
								<div class="news__right">
									<h2 class="news__title">Серверные настройки для защиты от DDOS.</h2>
									<div class="news__text">
										<p>Друзья! Проводятся все необходимые серверные настройки для мощной защиты от DDOS - атак. Следите за новостями!</p>
										<p style="text-align: right;">С уважением, команда CRYPTOLIDE.</p>
									</div>
								</div>
							</div>
							<div class="news__item infopage-block">
								<div class="news__left">
									<div class="news__date">2.09.2017</div>
								</div>
								<div class="news__right">
									<h2 class="news__title">Улучшение партнёрской программы.</h2>
									<div class="news__text">
										<p>Уважаемые пользователи! Для тех пользователей, которые будут заходить на сайт через поисковые системы или другим способом, то есть без партнёрской ссылки, будет интересен раздел на главной странице сайта, который называется "Отзывы наших клиентов". В нем будут размещены исключительно видео отзывы о нашем ресурсе. Это не обязательно должна быть запись лица (хотя это приветствуется и за это награда будет значительней), достаточно будет сделать запись своего экрана и рассказать несколько слов о нашем сервисе. Видео должно быть от 2 до 5 минут. Расскажите свое мнение, что вы думаете, как сильно вы ждете запуска, будете ли вы советовать своим друзьям наш сервис и т.д. Нам важно услышать любые ваши мнения. Запишите такое небольшое видео, затем пришлите ссылку на него в youtube или на другой видеохостинг на info@cryptolide.com и мы не только опубликуем его в "Отзывах", но и разместим под вашим видео кнопку регистрации с вашей партнёрской ссылкой и уведомим, что при регистрации будет начислен бонус. Пользователи будут регистрироваться, попадать в вашу команду и приносить вам прибыль, а вы наблюдать за стремительным ростом пассивного дохода. Видеоролик нужно прислать нам не позднее, чем 15 сентября. </p>
										<p style="text-align: right;">С уважением, команда CRYPTOLIDE.</p>
									</div>
								</div>
							</div>
							<div class="news__item infopage-block">
								<div class="news__left">
									<div class="news__date">31.08.2017</div>
								</div>
								<div class="news__right">
									<h2 class="news__title">Завершение работ по холодному хранению денежных средств.</h2>
									<div class="news__text">
										<p>Уважаемые пользователи! С радостью сообщаем, что мы полностью закончили все работы по холодному хранению денежных средств. Разработанный нами алгоритм позволит всегда поддерживать около 0.5% - 1% средств на горячих кошельках, а весь остаток будет будет отправляться в хранилище, доступ к которому есть только в главных администраторов сервиса. Мы всегда заботимся о сохранности ваших средств. Спасибо, что вы с нами!</p>
										<p style="text-align: right;">С уважением, команда CRYPTOLIDE.</p>
									</div>
								</div>
							</div>
							<div class="news__item infopage-block">
								<div class="news__left">
									<div class="news__date">29.08.2017</div>
								</div>
								<div class="news__right">
									<h2 class="news__title">Начало тестирования второго этапа.</h2>
									<div class="news__text">
										<p>Друзья! В процессе 2 этап объемного тестирования. Команда проверяет торговую платформу, систему сделок и процессы ввода / вывода криптовалют и фиата</p>
										<p style="text-align: right;">С уважением, команда CRYPTOLIDE.</p>
									</div>
								</div>
							</div>
							<div class="news__item infopage-block">
								<div class="news__left">
									<div class="news__date">22.08.2017</div>
								</div>
								<div class="news__right">
									<h2 class="news__title">Успешное завершение первой части тестирования.</h2>
									<div class="news__text">
										<p>Уважаемые пользователи! Первая часть объемного тестирования сервиса CRYPTOLIDE успешно завершена. Уверенно двигаемся дальше. Следите за новостями.</p>
										<p style="text-align: right;">С уважением, команда CRYPTOLIDE.</p>
									</div>
								</div>
							</div>
							<div class="news__item infopage-block">
								<div class="news__left">
									<div class="news__date">16.08.2017</div>
								</div>
								<div class="news__right">
									<h2 class="news__title">Запуск тестовой версии проекта.</h2>
									<div class="news__text">
										<p>Здравствуйте, уважаемые пользователи. Это наша первая новость, и мы с радостью сообщаем, что скоро состоится запуск BETA версии сервиса! Следите за нашими новостями :)</p>
										<p style="text-align: right;">С уважением, команда CRYPTOLIDE.</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection