@extends('layouts.app')

@title('Комиссии и лимиты')

@className('main_infopage')

@section('content')

<div class="content">
  <div class="infopage infopage_commissions">
    <div class="infopage__title-wrap">
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <h1 class="infopage-title">Комиссии и лимиты</h1>
          </div>
        </div>
      </div>
    </div>
    <div class="container">
      <div class="row">
        <div class="infopage__content">
          <div class="info-message info-message_commissions">Каждый пользователь может навсегда (или на очень длительный срок) отключить себе комиссию на абсолютно все операции в торговой платформе, выполнив несколько простых, и совершено бесплатных действий. Подробнее об отключении комиссии <a href="/nocommission">написано здесь.</a></div>
          <div class="infopage-block infopage-block_internal-operations">
            <h2 class="infopage-block__title">Комиссии за операции внутри сервиса</h2>
            <div class="infopage-block__text">
              <table class="operations-table operations-table_internal operations-table_trading-platform">
                <caption>Торговая платформа</caption>
                <thead>
                  <tr>
                    <td>Тип операции</td>
                    <td>Комиссия</td>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>Комиссия за размещение объявления</td>
                    <td>ОТСУТСТВУЕТ</td>
                  </tr>
                  <tr>
                    <td>Комиссия за закрытие объявления</td>
                    <td>ОТСУТСТВУЕТ</td>
                  </tr>
                  <tr>
                    <td>Комиссия за успешно совершенную сделку (оплачивает тот, кто разместил
                      объявление)
                    </td>
                    <td>0%</td>
                  </tr>
                </tbody>
              </table>
              <table class="operations-table operations-table_internal">
                <caption>CRYPTOLIDE Кошелёк</caption>
                <thead>
                  <tr>
                    <td>Тип операции</td>
                    <td>Комиссия</td>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>Комиссия за перевод средств другому пользователю в CRYPTOLIDE</td>
                    <td>ОТСУТСТВУЕТ</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
          <div class="infopage-block infopage-block_input-output">
            <h2 class="infopage-block__title">Комиссии за операции ввода / вывода</h2>
            <div class="infopage-block__text">
              <table class="operations-table operations-table_input-output">
                <caption>CRYPTOLIDE Кошелёк</caption>
                <thead>
                  <tr>
                    <td>Криптовалюта</td>
                    <td>Комиссия за ввод</td>
                    <td>Комиссия за вывод</td>
                  </tr>
                </thead>
                <tbody>
                  @foreach($commissions as $commission)
                  <tr>
                    <td>{{$commission->short}}</td>
                    @if($commission->commission_input==0)
                    <td>ОТСУТСТВУЕТ</td>
                    @else
                    <td>{{$commission->commission_input}} {{$commission->short}}</td>
                    @endif

                    @if($commission->commission_output==0)
                    <td>ОТСУТСТВУЕТ</td>
                    @else
                    <td>{{$commission->commission_output}} {{$commission->short}}</td>
                    @endif
                  </tr>
                  @endforeach    
                </tbody>
              </table>
            </div>
          </div>
          <div class="infopage-block infopage-block_input-output">
            <h2 class="infopage-block__title">Лимиты</h2>
            <div class="infopage-block__text">
              <table class="operations-table operations-table_input-output">
                <caption>Торговая платформа и CRYPTOLIDE Кошелёк</caption>
                <thead>
                  <tr>
                    <td>Криптовалюта</td>
                    <td>Минимальное количество</td>
                    <td>Максимальное количество</td>
                  </tr>
                </thead>
                <tbody>
                  @foreach($commissions as $commission)
                  <tr>
                    <td>{{$commission->short}}</td>
                    @if($commission->commission_input==0)
                    <td>ЛИМИТЫ ОТСУТСТВУЮТ</td>
                    @else
                    <!-- <td>{{$commission->commission_input}} {{$commission->short}}</td> -->
                    <td>ЛИМИТЫ ОТСУТСТВУЮТ</td>
                    @endif

                    @if($commission->commission_output==0)
                    <td>ЛИМИТЫ ОТСУТСТВУЮТ</td>
                    @else
                    <!-- <td>{{$commission->commission_output}} {{$commission->short}}</td> -->
                    <td>ЛИМИТЫ ОТСУТСТВУЮТ</td>
                    @endif
                  </tr>
                  @endforeach    
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection