@extends('layouts.app')

@title('Мои сделки')

@className('main_my-deals')

@section('content')
<div class="content">
  <div class="title-wrap title-wrap_my-deals">
    <div class="container">
      <div class="row">
        <h1 class="page-title title-wrap__title">Мои сделки и объявления</h1>
        <a href="/" class="btn-back title-wrap__back">назад</a>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <ul class="settings-tabs settings-tabs_my-deals">
          <li class="settings-tabs__item settings-tabs__item_active"><a href="/deals" class="settings-tabs__link">МОИ СДЕЛКИ</a></li>
          <li class="settings-tabs__item"><a href="/publications" class="settings-tabs__link">МОИ ОБЪЯВЛЕНИЯ</a></li>
        </ul>
      </div>
    </div>
  </div>
  @if($activeDeals->isNotEmpty())
  <div class="deal-history-block">
    <div class="deal-history-block__header">
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <h2 class="deal-history-block__title">Активные сделки</h2>
          </div>
        </div>
      </div>
    </div>
    <div class="deal-history">
      <div class="deal-history__header">
        <div class="container">
          <div class="row">
            <div class="col-xs-12">
              <div class="table-responsive">
                <table class="deal-history__header-inner">
                  <thead>
                    <tr>
                      <th class="deal-history__id">ID &nbsp;&nbsp;&nbsp;&nbsp;Дата</th>
                      <th class="deal-history__transaction-type">Тип сделки и способ оплаты</th>
                      <th class="deal-history__transaction-partner">Партнёр по сделке</th>
                      <th class="deal-history__transaction-description">Описание сделки</th>
                      <th class="deal-history__transaction-status">Статус сделки</th>
                    </tr>
                  </thead>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="deal-history__body">
        @if ($activeDeals->count() > 0)
        @foreach($activeDeals as $activeDeal)
        @if ($activeDeal->status == 'completed' || $activeDeal->status == 'cancelled')
        @continue
        @endif
        <div class="deal-history__row">
          <div class="container">
            <div class="row">
              <div class="col-xs-12">
                <div class="table-responsive">
                  <table class="deal-history__row-inner">
                    <tbody>
                      <tr>
                        <td class="deal-history__id">
                          <a href="/deals/{{ $activeDeal->id }}" class="deal-history__ad-id">{{ $activeDeal->id }}</a>
                          <div class="deal-history__time">{{ $activeDeal->created_at->format('H:i') }}</div>
                          <div class="deal-history__date">{{ $activeDeal->created_at->format('d.m.Y') }}</div>
                        </td>
                        <td class="deal-history__transaction-type">
                          <ul class="deal-history__general-info-list">
                            <li>Я хочу:
                              <strong>
                                @if ($activeDeal->type == 'buy_online')
                                @if (auth()->user()->id != $activeDeal->creator_id)
                                Продать криптовалюту онлайн
                                @else
                                Купить криптовалюту онлайн
                                @endif
                                @endif

                                @if ($activeDeal->type == 'buy_for_cash')
                                @if (auth()->user()->id != $activeDeal->creator_id)
                                Продать криптовалюту за наличные
                                @else
                                Купить криптовалюту за наличные
                                @endif
                                @endif

                                @if ($activeDeal->type == 'sell_online')
                                @if (auth()->user()->id != $activeDeal->creator_id)
                                Купить криптовалюту онлайн
                                @else
                                Продать криптовалюту онлайн
                                @endif
                                @endif

                                @if ($activeDeal->type == 'sell_for_cash')
                                @if (auth()->user()->id != $activeDeal->creator_id)
                                Купить криптовалюту за наличные
                                @else
                                Продать криптовалюту за наличные
                                @endif
                                @endif

                                @if ($activeDeal->type == 'trade')
                                Обменять криптовалюту
                                @endif
                              </strong>
                            </li>
                            @if ($activeDeal->type == 'trade')
                            @if (auth()->user()->id != $activeDeal->creator_id)
                            <li>Отдаете: <strong>{{ $activeDeal->crypto_trade->name . ' - ' . $activeDeal->crypto_trade->short }}</strong></li>
                            <li>Получаете: <strong>{{ $activeDeal->crypto->name . ' - ' . $activeDeal->crypto->short }}</strong></li>
                            @else
                            <li>Отдаете: <strong>{{ $activeDeal->crypto->name . ' - ' . $activeDeal->crypto->short }}</strong></li>
                            <li>Получаете: <strong>{{ $activeDeal->crypto_trade->name . ' - ' . $activeDeal->crypto_trade->short }}</strong></li>
                            @endif
                            @else
                            <li>Криптовалюта: <strong>{{ $activeDeal->crypto->name . ' - ' . $activeDeal->crypto->short }}</strong></li>
                            @endif
                            @if ($activeDeal->country_id)
                            <li>Страна: <strong>{{ $activeDeal->country->name }}</strong></li>
                            @if ($activeDeal->type == 'sell_for_cash' || $activeDeal->type == 'buy_for_cash')
                            <li>Населённый пункт: <strong>{{ $activeDeal->city->name.', '.$activeDeal->city->region }}</strong></li>
                            @endif
                            @endif
                            @if ($activeDeal->type == 'buy_online' || $activeDeal->type == 'sell_online')
                            <li>
                              <strong>
                                {{ $activeDeal->payment_type->name }}
                                @if ($activeDeal->payment_type_id == 2)
                                : {{ $activeDeal->banks }}
                                @endif
                              </strong>
                            </li>
                            @endif
                          </ul>
                        </td>
                        <td class="deal-history__transaction-partner">
                          <div class="user-profile">
                            <div class="user-profile__photo"><img src="{{ $activeDeal->partner->image }}" alt=""></div>
                            <div class="user-profile__info user-profile__info_{{ $activeDeal->partner->is_online ? 'online' : 'offline' }}">
                              <div class="user-profile__realname"><span class="user-profile__realname-inner">{{ $activeDeal->partner->name }}</span></div>
                              <a href="/profile/{{ $activeDeal->partner->login }}" class="user-profile__name"><span class="user-profile__name-inner">{{ $activeDeal->partner->login }}</span></a>
                              <div class="user-profile__rating">{{ $activeDeal->partner->rating }}</div>
                            </div>
                          </div>
                        </td>
                        <td class="deal-history__transaction-description">
                          <ul class="deal-history__financial-info-list">

                            @if ($activeDeal->short_type == 'sell')
                            @if (auth()->user()->id != $activeDeal->creator_id)
                            <li>Я отдаю: <span style="font-weight: 500; color: #5D5D5E;">{{ $activeDeal->sumCurrency() }} {{ $activeDeal->currency_name }}</span></li>
                            <li>Я получаю:<span style="font-weight: 500; color: #1c9724;"> {{ $activeDeal->sumCrypto() }} {{ $activeDeal->crypto->short }}</span></li>
                            @else
                            <li>Я отдаю:<span style="font-weight: 500; color: #5D5D5E;"> {{ $activeDeal->sumCrypto() }} {{ $activeDeal->crypto->short }}</span></li>
                            <li>Я получаю: <span style="font-weight: 500; color: #1c9724;">{{ $activeDeal->sumCurrency() }} {{ $activeDeal->currency_name }}</span></li>
                            @endif
                            @elseif ($activeDeal->short_type == 'buy')
                            @if (auth()->user()->id != $activeDeal->creator_id)
                            <li>Я отдаю:<span style="font-weight: 500; color: #5D5D5E;"> {{ $activeDeal->sumCrypto() }} {{ $activeDeal->crypto->short }}</span></li>
                            <li>Я получаю: <span style="font-weight: 500; color: #1c9724;">{{ $activeDeal->sumCurrency() }} {{ $activeDeal->currency_name }}</span></li>
                            @else
                            <li>Я отдаю: <span style="font-weight: 500; color: #5D5D5E;">{{ $activeDeal->sumCurrency() }} {{ $activeDeal->currency_name }}</span></li>
                            <li>Я получаю:<span style="font-weight: 500; color: #1c9724;"> {{ $activeDeal->sumCrypto() }} {{ $activeDeal->crypto->short }}</span></li>
                            @endif
                            @elseif($activeDeal->short_type == 'trade')
                            @if (auth()->user()->id != $activeDeal->creator_id)
                            <li>Я отдаю: <span style="font-weight: 500; color: #5D5D5E;">{{ $activeDeal->sumCurrency() }} {{ $activeDeal->currency_name }}</span></li>
                            <li>Я получаю:<span style="font-weight: 500; color: #1c9724;"> {{ $activeDeal->sumCrypto() }} {{ $activeDeal->crypto->short }}</span></li>
                            @else
                            <li>Я отдаю:<span style="font-weight: 500; color: #5D5D5E;"> {{ $activeDeal->sumCrypto() }} {{ $activeDeal->crypto->short }}</span></li>
                            <li>Я получаю: <span style="font-weight: 500; color: #1c9724;">{{ $activeDeal->sumCurrency() }} {{ $activeDeal->currency_name }}</span></li>
                            @endif
                            @endif

                            <li>
                              Курс:
                              {{ $activeDeal->rate() }} {{ $activeDeal->currency_name }}
                            </li>
                            @if (number_format($activeDeal->creator->commission/100*$activeDeal->sumCrypto(), 8, '.', '') > 0 && auth()->user()->id == $activeDeal->creator_id)
                            <li>
                              Комиссия:
                              {{ number_format($activeDeal->creator->commission/100*$activeDeal->sumCrypto(), 8, '.', '') }} {{ $activeDeal->crypto->short }}
                            </li>
                            @endif
                          </ul>
                        </td>
                        <td class="deal-history__transaction-status">
                          @if ($activeDeal->status == 'pending')
                          <div class="deal-history__transaction-status-img">
                            <img src="/images/icons/icon_faq_clock.png" alt="">
                          </div>
                          <a href="/deals/{{ $activeDeal->id }}"><div class="deal-history__transaction-status-text">Ожидает оплаты</div></a>
                          @elseif ($activeDeal->status == 'cancelled')
                          <div class="deal-history__transaction-status-img">
                            <img src="/images/icons/icon_cancel_deal_user.png" alt="">
                          </div>
                          <a href="/deals/{{ $activeDeal->id }}"><div class="deal-history__transaction-status-text">Отменена</div></a>
                          @elseif ($activeDeal->status == 'paid')
                          <div class="deal-history__transaction-status-img">
                            <img src="/images/icons/icon_sending_money.png" alt="">
                          </div>
                          <a href="/deals/{{ $activeDeal->id }}"><div class="deal-history__transaction-status-text">Оплачена</div></a>
                          @elseif ($activeDeal->status == 'completed')
                          <div class="deal-history__transaction-status-img">
                            <img src="/images/icons/icon_successfull_transaction.png" alt="">
                          </div>
                          <a href="/deals/{{ $activeDeal->id }}"><div class="deal-history__transaction-status-text">Успешно завершена</div></a>
                          @endif
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
        @endforeach
        @else
        <div class="wallet-operations__no-operations">
          <div class="container">
            <div class="row">
              <div class="col-xs-12">Вы ещё не заключили ни одной сделки.</div>
            </div>
          </div>
        </div>
        @endif
      </div>
    </div>
    {{ $activeDeals->links('pagination.cryptolide') }}
  </div>
  @endif
  <div class="deal-history-block">
    <div class="deal-history-block__header">
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <h2 class="deal-history-block__title">История сделок</h2>
          </div>
        </div>
      </div>
    </div>
    <div class="deal-history">
      <div class="deal-history__header">
        <div class="container">
          <div class="row">
            <div class="col-xs-12">
              <div class="table-responsive">
                <table class="deal-history__header-inner">
                  <thead>
                    <tr>
                      <th class="deal-history__id">ID &nbsp;&nbsp;&nbsp;&nbsp;Дата</th>
                      <th class="deal-history__transaction-type">Тип сделки и способ оплаты</th>
                      <th class="deal-history__transaction-partner">Партнёр по сделке</th>
                      <th class="deal-history__transaction-description">Описание сделки</th>
                      <th class="deal-history__transaction-status">Статус сделки</th>
                    </tr>
                  </thead>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="deal-history__body">
        @if ($deals->count() > 0)
        @foreach($deals as $deal)
        @if ($deal->status == 'pending' || $deal->status == 'paid')
        @continue
        @endif
        <div class="deal-history__row">
          <div class="container">
            <div class="row">
              <div class="col-xs-12">
                <div class="table-responsive">
                  <table class="deal-history__row-inner">
                    <tbody>
                      <tr>
                        <td class="deal-history__id">
                          <a href="/deals/{{ $deal->id }}" class="deal-history__ad-id">{{ $deal->id }}</a>
                          <div class="deal-history__time">{{ $deal->created_at->format('H:i') }}</div>
                          <div class="deal-history__date">{{ $deal->created_at->format('d.m.Y') }}</div>
                        </td>
                        <td class="deal-history__transaction-type">
                          <ul class="deal-history__general-info-list">
                            <li>Я хочу:
                              <strong>
                                @if ($deal->type == 'buy_online')
                                @if (auth()->user()->id != $deal->creator_id)
                                Продать криптовалюту онлайн
                                @else
                                Купить криптовалюту онлайн
                                @endif
                                @endif

                                @if ($deal->type == 'buy_for_cash')
                                @if (auth()->user()->id != $deal->creator_id)
                                Продать криптовалюту за наличные
                                @else
                                Купить криптовалюту за наличные
                                @endif
                                @endif

                                @if ($deal->type == 'sell_online')
                                @if (auth()->user()->id != $deal->creator_id)
                                Купить криптовалюту онлайн
                                @else
                                Продать криптовалюту онлайн
                                @endif
                                @endif

                                @if ($deal->type == 'sell_for_cash')
                                @if (auth()->user()->id != $deal->creator_id)
                                Купить криптовалюту за наличные
                                @else
                                Продать криптовалюту за наличные
                                @endif
                                @endif

                                @if ($deal->type == 'trade')
                                Обменять криптовалюту
                                @endif
                              </strong>
                            </li>
                            @if ($deal->type == 'trade')
                            @if (auth()->user()->id != $deal->creator_id)
                            <li>Отдаете: <strong>{{ $deal->crypto_trade->name . ' - ' . $deal->crypto_trade->short }}</strong></li>
                            <li>Получаете: <strong>{{ $deal->crypto->name . ' - ' . $deal->crypto->short }}</strong></li>
                            @else
                            <li>Отдаете: <strong>{{ $deal->crypto->name . ' - ' . $deal->crypto->short }}</strong></li>
                            <li>Получаете: <strong>{{ $deal->crypto_trade->name . ' - ' . $deal->crypto_trade->short }}</strong></li>
                            @endif
                            @else
                            <li>Криптовалюта: <strong>{{ $deal->crypto->name . ' - ' . $deal->crypto->short }}</strong></li>
                            @endif
                            @if ($deal->country_id)
                            <li>Страна: <strong>{{ $deal->country->name }}</strong></li>
                            @if ($deal->type == 'sell_for_cash' || $deal->type == 'buy_for_cash')
                            <li>Населённый пункт: <strong>{{ $deal->city->name.', '.$deal->city->region }}</strong></li>
                            @endif
                            @endif
                            @if ($deal->type == 'buy_online' || $deal->type == 'sell_online')
                            <li>
                              <strong>
                                {{ $deal->payment_type->name }}
                                @if ($deal->payment_type_id == 2)
                                : {{ $deal->banks }}
                                @endif
                              </strong>
                            </li>
                            @endif
                          </ul>
                        </td>
                        <td class="deal-history__transaction-partner">
                          <div class="user-profile">
                            <div class="user-profile__photo"><img src="{{ $deal->partner->image }}" alt=""></div>
                            <div class="user-profile__info user-profile__info_{{ $deal->partner->is_online ? 'online' : 'offline' }}">
                              <div class="user-profile__realname"><span class="user-profile__realname-inner">{{ $deal->partner->name }}</span></div>
                              <a href="/profile/{{ $deal->partner->login }}" class="user-profile__name"><span class="user-profile__name-inner">{{ $deal->partner->login }}</span></a>
                              <div class="user-profile__rating">{{ $deal->partner->rating }}</div>
                              <span class="action-table__seller-icons">
                                @foreach($deal->partner->achievements as $achievement)
                                @if ($achievement->image == '/storage/achievements/2.png')
                                <span data-toggle="tooltip" title="Пройдена полная верификация личности">
                                  <img src="{{ $achievement->image }}" alt="">
                                </span>
                                @elseif($achievement->image == '/storage/achievements/1.png')
                                <span data-toggle="tooltip" title="Верифицирован телефонный номер">
                                  <img src="{{ $achievement->image }}" alt="">
                                </span>
                                @else
                                <img src="{{ $achievement->image }}" alt="">
                                @endif
                                @endforeach
                              </span>
                            </div>
                          </div>
                        </td>
                        <td class="deal-history__transaction-description">
                          <ul class="deal-history__financial-info-list">

                            @if ($deal->short_type == 'sell')
                            @if (auth()->user()->id != $deal->creator_id)
                            <li>Я отдаю: <span style="font-weight: 500; color: #5D5D5E;">{{ $deal->sumCurrency() }} {{ $deal->currency_name }}</span></li>
                            <li>Я получаю:<span style="font-weight: 500; color: #1c9724;"> {{ $deal->sumCrypto() }} {{ $deal->crypto->short }}</span></li>
                            @else
                            <li>Я отдаю:<span style="font-weight: 500; color: #5D5D5E;"> {{ $deal->sumCrypto() }} {{ $deal->crypto->short }}</span></li>
                            <li>Я получаю: <span style="font-weight: 500; color: #1c9724;">{{ $deal->sumCurrency() }} {{ $deal->currency_name }}</span></li>
                            @endif
                            @elseif ($deal->short_type == 'buy')
                            @if (auth()->user()->id != $deal->creator_id)
                            <li>Я отдаю:<span style="font-weight: 500; color: #5D5D5E;"> {{ $deal->sumCrypto() }} {{ $deal->crypto->short }}</span></li>
                            <li>Я получаю: <span style="font-weight: 500; color: #1c9724;">{{ $deal->sumCurrency() }} {{ $deal->currency_name }}</span></li>
                            @else
                            <li>Я отдаю: <span style="font-weight: 500; color: #5D5D5E;">{{ $deal->sumCurrency() }} {{ $deal->currency_name }}</span></li>
                            <li>Я получаю:<span style="font-weight: 500; color: #1c9724;"> {{ $deal->sumCrypto() }} {{ $deal->crypto->short }}</span></li>
                            @endif
                            @elseif($deal->short_type == 'trade')
                            @if (auth()->user()->id != $deal->creator_id)
                            <li>Я отдаю: <span style="font-weight: 500; color: #5D5D5E;">{{ $deal->sumCurrency() }} {{ $deal->currency_name }}</span></li>
                            <li>Я получаю:<span style="font-weight: 500; color: #1c9724;"> {{ $deal->sumCrypto() }} {{ $deal->crypto->short }}</span></li>
                            @else
                            <li>Я отдаю:<span style="font-weight: 500; color: #5D5D5E;"> {{ $deal->sumCrypto() }} {{ $deal->crypto->short }}</span></li>
                            <li>Я получаю: <span style="font-weight: 500; color: #1c9724;">{{ $deal->sumCurrency() }} {{ $deal->currency_name }}</span></li>
                            @endif
                            @endif

                            <li>
                              Курс:
                              {{ $deal->rate() }} {{ $deal->currency_name }}
                            </li>
                            @if (number_format(($deal->operations->first() ? $deal->operations->first()->commission : ($deal->creator->commission/100*$deal->sumCrypto())), 8, '.', '') > 0 && auth()->user()->id == $deal->creator_id)
                            <li>
                              Комиссия:
                              {{ number_format(($deal->operations->first() ? $deal->operations->first()->commission : ($deal->creator->commission/100*$deal->sumCrypto())), 8, '.', '') }} {{ $deal->crypto->short }}
                            </li>
                            @endif
                          </ul>
                        </td>
                        <td class="deal-history__transaction-status">
                          @if ($deal->status == 'pending')
                          <div class="deal-history__transaction-status-img">
                            <img src="/images/icons/icon_faq_clock.png" alt="">
                          </div>
                          <a href="/deals/{{ $deal->id }}"><div class="deal-history__transaction-status-text">Ожидает оплаты</div></a>
                          @elseif ($deal->status == 'cancelled')
                          <div class="deal-history__transaction-status-img">
                            <img src="/images/icons/icon_cancel_deal_user.png" alt="">
                          </div>
                          <a href="/deals/{{ $deal->id }}"><div class="deal-history__transaction-status-text">Отменена</div></a>
                          @elseif ($deal->status == 'paid')
                          <div class="deal-history__transaction-status-img">
                            <img src="/images/icons/icon_sending_money.png" alt="">
                          </div>
                          <a href="/deals/{{ $deal->id }}"><div class="deal-history__transaction-status-text">Оплачена</div></a>
                          @elseif ($deal->status == 'completed')
                          <div class="deal-history__transaction-status-img">
                            <img src="/images/icons/icon_successfull_transaction.png" alt="">
                          </div>
                          <a href="/deals/{{ $deal->id }}"><div class="deal-history__transaction-status-text">Успешно завершена</div></a>
                          @endif
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
        @endforeach
        @else
        <div class="wallet-operations__no-operations">
          <div class="container">
            <div class="row">
              <div class="col-xs-12">Вы ещё не заключили ни одной сделки.</div>
            </div>
          </div>
        </div>
        @endif
      </div>
    </div>
    {{ $deals->links('pagination.cryptolide') }}
  </div>
</div>
@endsection

@section('scripts')
<script>
  $('.jqstyler').styler({
    onFormStyled: function () {
      $('.jq-selectbox').each(function (index, el) {
        var bgImg = $(this).find('.jq-selectbox__dropdown ul li.selected').css('background-image');
        $(this).find('.jq-selectbox__select-text').css('background-image', bgImg);
      });
    },
    onSelectClosed: function () {
      var bgImg = $(this).find('.jq-selectbox__dropdown ul li.selected').css('background-image');
      $(this).find('.jq-selectbox__select-text').css('background-image', bgImg);
    }
  });
</script>
@endsection