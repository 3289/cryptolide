@extends('layouts.app')

@title('Комиссия и способы её отключения')

@className('main_infopage')

@section('content')

	<div class="content">
		<div class="infopage infopage_nocommission">
			<div class="infopage__title-wrap">
				<div class="container">
					<div class="row">
						<div class="col-xs-12">
							<h1 class="infopage-title">Комиссия и способы её отключения</h1>
						</div>
					</div>
				</div>
			</div>
			<div class="container">
				<div class="row">
					<div class="infopage__content">
						<div class="infopage-block infopage-block_about-commission corner-bottom">
							<h2 class="infopage-block__title">Несколько слов о комиссии</h2>
							<div class="infopage-block__text">								
								<p>Сейчас компания CRYPTOLIDE проводит акцию и дарит отключение комиссии абсолютно всем пользователям. <br>Если регистрация была по партнёрской ссылке (есть пригласитель) - комиссия у этого пользователя будет отключена до 15 ноября (15.11.2018). Если регистрация была осуществлена без предварительного перехода по партнёрской ссылке - комиссия будет отключена до 1 октября (01.10.2018). Таким образом, новичкам будет намного выгоднее зарегистрироваться по партнёрской ссылке, чем напрямую. После окончания акционного периода комиссия будет составлять всего 0.35% от суммы, и оплачивать её будет только создатель объявления, по которому была совершена успешная сделка. Это значит, что вы, абсолютно всегда, будете покупать, продавать и обменивать любые суммы криптовалюты по объявлениям других пользователей совершенно без комиссии! Если же вы хотите заключать сделки по собственным объявлениям и никогда не платить комиссию - используйте те способы отключения комиссии, которые описаны ниже. А ещё, только до 1 августа 2018 года, за выполнение любого действия (своего собственного или описанного ниже) - время отключения комиссии будет увеличено еще на <strong>+35%</strong> дополнительно. Согласитесь, это просто идеальные условия, чтобы начать рекомендовать CRYPTOLIDE прямо сейчас и без промедлений.</p>
							</div>
						</div>
						<div class="infopage-block infopage-block_explanation corner-top corner-bottom">
							<h2 class="infopage-block__title">Каждый пользователь может навсегда (или на длительный срок) отключить комиссию <br>абсолютно на все операции в торговой платформе.</h2>
							<div class="infopage-block__text">Для этого всего лишь необходимо:</div>
						</div>
						<div class="infopage-block infopage-block_commission-deactivate corner-top corner-bottom">
							<div class="infopage-block__icon"><img src="/images/content/write_review.png" alt=""></div>
							<h2 class="infopage-block__title">Написать отзыв <span class="infopage-block__caption">Отключение комиссии: до 72 часов</span></h2>
							<div class="infopage-block__text">
								<p>Напишите один или несколько отзывов о нашем сервисе на любых интернет ресурсах. Чем больше отзывов вы напишите и чем выше активность проявите - тем дольше у вас будет отключена комиссия. Никаких ограничений не существует. Креативность и находчивость приветствуются. Интернет ресурсы каждый пользователь выбирает для себя сам.</p>
							</div>
						</div>
						<div class="infopage-block infopage-block_commission-deactivate corner-top corner-bottom">
							<div class="infopage-block__icon"><img src="/images/content/create_topic.png" alt=""></div>
							<h2 class="infopage-block__title">Создать тему и поддерживать её <span class="infopage-block__caption">Отключение комиссии: индивидуально</span></h2>
							<div class="infopage-block__text">
								<p>Создайте тему о нашем сервисе на форуме, блоге или любом другом интернет ресурсе. Расскажите об уникальности и преимуществах сервиса перед биржами, а также о том, почему вы рекомендуете CRYPTOLIDE. Чем выше будет активность в созданной вами теме - тем дольше у вас будет отключена комиссия. При постоянной поддержке темы (ответы на вопросы пользователей, публикация новостей, помощь новичкам и т.д. ) - срок отключения комиссии будет значительно увеличен.</p>
							</div>
						</div>
						<div class="infopage-block infopage-block_commission-deactivate corner-top corner-bottom">
							<div class="infopage-block__icon"><img src="/images/content/write_video_review.png" alt=""></div>
							<h2 class="infopage-block__title">Записать видео отзыв или видео инструкцию <span class="infopage-block__caption">Отключение комиссии: до 10 дней</span></h2>
							<div class="infopage-block__text">
								<p>Запишите видео отзыв или видео урок о CRYPTOLIDE. В видео отзыве поделитесь собственным мнение и мыслями, расскажите об уникальности и преимуществах сервиса Криптолайд перед биржами, а также о том, почему вы рекомендуете CRYPTOLIDE. В видео инструкции продемонстрируйте, как именно работать с теми или иными разделами сервиса, покажите, как все происходит на конкретном примере. Чем выше будет популярность вашего видеоролика - тем дольше у вас будет отключена комиссия. Также вы можете бесплатно разместить ваши видеоролики на главной странице, соблюдая <a href="/affiliate-program#affiliate-info-video">правила.</a></p>
							</div>
						</div>
						<div class="infopage-block infopage-block_commission-deactivate corner-top corner-bottom">
							<div class="infopage-block__icon"><img src="/images/content/promotion_option.png" alt=""></div>
							<h2 class="infopage-block__title">Предложить свой вариант продвижения CRYPTOLIDE <span class="infopage-block__caption">Отключение: индивидуально</span></h2>
							<div class="infopage-block__text">
								<p>Команда CRYPTOLIDE очень ценит креатив и находчивость. В интернете и за его пределами существуют миллионы разных способов продвижения, и вы можете использовать любой из них. Мы с радостью выслушаем вашу идею и наградим за её реализацию. Вы можете использовать любой другой вариант, который не был описан выше. Никаких ограничений нет, и никогда не будет. Всё зависит только от вас! Также не забывайте, что за постоянные и усердные усилия в CRYPTOLIDE предусмотрены особые <a href="/representatives">награды.</a></p>
							</div>
						</div>
						<div class="infopage-block infopage-block_commission-deactivate corner-top corner-bottom">
							<div class="infopage-block__icon"><img src="/images/content/bonuses_1.png" alt=""></div>
							<h2 class="infopage-block__title">Использовать персональные бонусы и предложения<span class="infopage-block__caption">Отключение: индивидуально</span></h2>
							<div class="infopage-block__text">
								<p>В CRYPTOLIDE работает специальный алгоритм, который анализирует действия каждого участника сервиса и на основе полученных данных формирует и совершенно бесплатно предоставляет персональные бонусы и акционные предложения. Мы понимаем, что все люди разные, и поэтому стараемся создать такие условия взаимодействия с сервисом, при которых каждый пользователь будет получать именно то, что ему действительно нужно, и чувствовать себя особенным.</a></p>
							</div>
						</div>
						<div class="infopage-block infopage-block_buttons corner-top">
							<div class="buttons">
								<a href="/" class="buttons__btn btn-green3 btn-green3 btn-green3_frontpage" target="_blank">Вернуться на главную</a>
								<span class="buttons__sep">или</span>
								<a href="#" class="buttons__btn btn-blue2 buttons__write-consultant" onclick="Chatra('openChat', true); return false;">Написать онлайн консультанту</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection