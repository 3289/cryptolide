@extends('layouts.app')
<?php
	use App\Models\Operation;
?>
@title('Мой защищённый кошелёк')

@className('main_my-wallet')

@section('content')
	<div class="content">
		<div class="title-wrap title-wrap_my-wallet">
			<div class="container">
				<div class="row">
					<h1 class="page-title title-wrap__title">Мой защищённый кошелёк</h1>
					<a href="/" class="btn-back title-wrap__back">назад</a>
				</div>
			</div>
		</div>
		<div class="currency-blocks">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<div class="currency-blocks__item">
							<div class="currency-block currency-block_bitcoin">
								<div class="currency-block__name">Bitcoin</div>
								<div class="currency-block__img"><img src="/images/content/icon_bitcoin.png" alt=""></div>
								<div class="currency-block__bitcoins-number">{{ auth()->user()->balance['BTC'] }} BTC</div>
								<div class="currency-block__bitcoins-number-transactions">+ {{ auth()->user()->reserved_balance['BTC'] }} BTC в сделках</div>
								<div class="currency-block__buttons">
									<a href="/replenishment/1" class="btn-green2 currency-block__btn btn-replenishment-funds">Пополнить</a>
									<a href="/withdrawals/1" class="btn-withdrawal-funds btn-blue currency-block__btn">Отправить</a>
								</div>
							</div>
						</div>
						<div class="currency-blocks__item">
							<div class="currency-block currency-block_litecoin">
								<div class="currency-block__name">Litecoin</div>
								<div class="currency-block__img"><img src="/images/content/icon_litecoin.png" alt=""></div>
								<div class="currency-block__bitcoins-number">{{ auth()->user()->balance['LTC'] }} LTC</div>
								<div class="currency-block__bitcoins-number-transactions">+ {{ auth()->user()->reserved_balance['LTC'] }} LTC в сделках</div>
								<div class="currency-block__buttons">
									<a data-crypto="litecoin" style="cursor: pointer" class="btn-replenishment-funds btn-green2 currency-block__btn">Пополнить</a>
									<a data-crypto="litecoin" style="cursor: pointer" class="btn-withdrawal-funds btn-blue currency-block__btn">Отправить</a>
								</div>
							</div>
						</div>
						<div class="currency-blocks__item">
							<div class="currency-block currency-block_dash">
								<div class="currency-block__name">DASH</div>
								<div class="currency-block__img"><img src="/images/content/icon_dash.png" alt=""></div>
								<div class="currency-block__bitcoins-number">{{ auth()->user()->balance['DASH'] }} DASH</div>
								<div class="currency-block__bitcoins-number-transactions">+ {{ auth()->user()->reserved_balance['DASH'] }} DASH в сделках</div>
								<div class="currency-block__buttons">
									<a data-crypto="dash" style="cursor: pointer" class="btn-replenishment-funds btn-green2 currency-block__btn">Пополнить</a>
									<a data-crypto="dash" style="cursor: pointer" class="btn-withdrawal-funds btn-blue currency-block__btn">Отправить</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="wallet-operations-history">
			<div class="wallet-operations-history__header">
				<div class="container">
					<div class="row">
						<div class="col-xs-12">
							<h2 class="wallet-operations-history__title">История операций</h2>
							<ul class="currency-types">
								@foreach(\App\Models\CryptoCurrency::all() as $crypto_currency)
									<li onclick="window.location.href='/my-wallet/?id={{ $crypto_currency->id }}'" class="currency-types__item{{ $crypto->id == $crypto_currency->id ? ' currency-types__item_active' : '' }}">{{ $crypto_currency->short }}</li>
								@endforeach
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="wallet-operations">
				<div class="wallet-operations__header">
					<div class="container">
						<div class="row">
							<div class="col-xs-12">
								<div class="table-responsive">
									<table class="wallet-operations__header-inner">
										<thead>
										<tr>
											<th class="wallet-operations__tid">TID</th>
											<th class="wallet-operations__date">Дата</th>
											<th class="wallet-operations__type">Операция</th>
											<th class="wallet-operations__sum-and-commission">Сумма и комиссия</th>
											<th class="wallet-operations__balance">Баланс</th>
											<th class="wallet-operations__description">Статус и описание операции</th>
										</tr>
										</thead>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="wallet-operations__body">
					@if($operations->count() > 0)
						@foreach($operations as $operation)
							<div class="wallet-operations__row">
								<div class="container">
									<div class="row">
										<div class="col-xs-12">
											<div class="table-responsive">
												<table class="wallet-operations__row-inner">
													<tbody>
													<tr>
														<td class="wallet-operations__tid">{{ $operation->id }}</td>
														<td class="wallet-operations__date">{!! $operation->created_at->format('H:i <br> d.m.Y') !!}</td>
														<td class="wallet-operations__type">
															@if ($operation->amount > 0)
																<span class="wallet-operations__income-transaction">Приход</span>
															@else
																<span class="wallet-operations__expenditure-operation">Расход</span>
															@endif
														</td>
														<td class="wallet-operations__sum-and-commission wallet-operations__sum-and-commission_consumption">
															@if ($operation->amount > 0)
																<div class="wallet-operations__income-transaction">+ {{ $operation->amount }} {{ $operation->crypto_currency->short }}</div>
															@else
																<div class="wallet-operations__expenditure-operation">{{ $operation->type == \App\Models\Operation::TYPE_WITHDRAW ? number_format($operation->amount+$operation->commission, 8) : $operation->amount }} {{ $operation->crypto_currency->short }}</div>
																@if ($operation->commission != 0)
																	<div class="wallet-operations__difference">- {{ number_format($operation->commission, 8) }} {{ $operation->crypto_currency->short }}</div>
																@endif
															@endif
														</td>
														<td class="wallet-operations__balance">{{ $operation->balance_before . ' ' . $operation->crypto_currency->short }}</td>
														<td class="wallet-operations__description
															@if($operation->type==Operation::TYPE_WITHDRAW && $operation->withdrawal != null)
																@if($operation->withdrawal->status=='completed')
																	wallet-operations__description-inner
																@elseif($operation->withdrawal->status=='cancelled')
																	wallet-operations__description-inner-cancel
              													@else
																	wallet-operations__description-inner-time
																@endif
															@else
																wallet-operations__description-inner
															@endif
															">
															{!! $operation->notification ? $operation->notification : $operation->description !!}
														</td>
														{{--<td class="wallet-operations__description {{ ($operation->type==\App\Models\Operation::TYPE_WITHDRAW && ($operation->withdrawal->status!='completed')) ? 'wallet-operations__description-inner-time' : 'wallet-operations__description-inner'}}">
															{!! $operation->notification ? $operation->notification : $operation->description !!}
															{!! ($operation->type==Operation::TYPE_WITHDRAW && ($operation->withdrawal->status=='completed')) ? ' / <a href="https://www.blocktrail.com/BTC/tx/'.$operation->withdrawal->txid.'" target="_blank">TXID</a>' : ''!!}
														</td>--}}
													</tr>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>
						@endforeach
					@else
						<div class="wallet-operations__no-operations">
							<div class="container">
								<div class="row">
									<div class="col-xs-12">Вы еще не совершали {{ $crypto->short }} операций</div>
								</div>
							</div>
						</div>
					@endif
				</div>
			</div>
			{{ $operations->appends(request()->all())->links('pagination.cryptolide') }}
		</div>
	</div>
@endsection

@section('modals')
	<div class="modal modal-cryptocurrency fade" id="modal-crypto" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<span class="close" data-dismiss="modal"></span>
					<h4>Криптовалюта <span class="crypto-short"></span></h4>
				</div>
				<div class="modal-body">
					<div class="cryptocurrency-block">
						<div class="cryptocurrency-block__img"><img src="(empty)" alt=""></div>
						<div class="cryptocurrency-block__text">
							<p>Криптовалюта <span class="crypto-short"></span> находится на стадии <span class="cryptocurrency-block__beta">BETA</span><br> тестирования и будет добавлена в ближайшее время.</p>
							<p>Следите за новостями сервиса.</p>
						</div>
					</div>
					<a href="#" data-dismiss="modal" class="btn-close">Закрыть окно</a>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('scripts')
	<script>
		$(function () {
            $('.btn-replenishment-funds, .btn-withdrawal-funds').on('click', function () {
                if ($(this).attr('data-crypto')) {
                    let modal = $('#modal-crypto');
                    let crypto = $(this).data('crypto');
                    modal.find('.crypto-short').text(crypto.charAt(0).toUpperCase() + crypto.slice(1));
                    modal.find('img').attr('src', '/images/content/icon_' + crypto + '_mini.png');
                    modal.modal('show');
                }
            });
        });
	</script>
@endsection