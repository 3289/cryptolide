@extends('layouts.app')

@title('Общая информация')

@className('main_general-information')

@section('content')

    <div class="content">
        <div class="title-wrap title-wrap_general-information">
            <div class="container">
                <div class="row">
                    <h1 class="page-title title-wrap__title">Мои настройки</h1>
                    <a href="/" class="btn-back title-wrap__back">назад</a>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <ul class="settings-tabs">
                        <li class="settings-tabs__item settings-tabs__item_active"><a href="/general-information" class="settings-tabs__link">ОБЩАЯ ИНФОРМАЦИЯ</a></li>
                        <li class="settings-tabs__item"><a href="/my-security" class="settings-tabs__link">БЕЗОПАСНОСТЬ</a></li>
                        <li class="settings-tabs__item"><a href="/verification" class="settings-tabs__link">ВЕРИФИКАЦИЯ</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="form-settings-wrapper">
            <form action="" class="form-settings" method="POST">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="patch" />

                <div class="form-settings__item form-settings__item_photos">
                    <label class="form-settings__label">Фотография и миниатюры:</label>
                    <div class="form-settings__control-wrap">
                        <div class="form-settings__control">
                            <img src="{{ auth()->user()->image }}" alt="Фотография" class="img-large">
                            <img src="{{ auth()->user()->image }}" alt="Фотография" class="img-medium">
                            <img src="{{ auth()->user()->image }}" alt="Фотография" class="img-small">
                            <a data-toggle="modal" data-target="#modal-avatar" style="cursor: pointer;" class="btn-green2">Изменить фото</a>
                        </div>
                    </div>
                </div>

                <div class="form-settings__item form-settings__item_fio">
                    <div class="form-settings__item-field">
                        <label for="name" class="form-settings__label">Имя:</label>
                        <div class="form-settings__control-wrap">
                            <input type="text" name="name" id="name" placeholder="Укажите имя" class="form-settings__text" value="{{ auth()->user()->name }}">
                            <div class="form-settings__hint">Укажите ваше настоящее имя. Оно не будет <br>показываться никому, если вы этого не захотите.</div>
                            @if($errors->has('name'))
                                <div class="form-error">{{ $errors->first('name') }}</div>
                            @endif
                        </div>
                    </div>
                    <div class="form-settings__item-field">
                        <label for="last_name" class="form-settings__label">Фамилия:</label>
                        <div class="form-settings__control-wrap">
                            <input type="text" name="last_name" id="last_name" placeholder="Укажите фамилию" class="form-settings__text" value="{{ auth()->user()->last_name }}">
                            <div class="form-settings__hint">Укажите вашу настоящую фамилию. Она не будет <br>показываться никому, если вы этого не захотите.</div>
                            @if($errors->has('last_name'))
                                <div class="form-error">{{ $errors->first('last_name') }}</div>
                            @endif
                        </div>
                    </div>
                    <div class="form-settings__item-field">
                        <label for="middle_name" class="form-settings__label">Отчество:</label>
                        <div class="form-settings__control-wrap">
                            <input type="text" name="middle_name" id="middle_name" placeholder="Укажите отчество" class="form-settings__text" value="{{ auth()->user()->middle_name }}">
                            <div class="form-settings__hint">Укажите ваше настоящее отчество. Она не будет показываться <br>никому, если вы этого не захотите. Если в вашей стране не <br>присваивают отчества - оставьте поле пустым.</div>
                            @if($errors->has('middle_name'))
                                <div class="form-error">{{ $errors->first('middle_name') }}</div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="form-settings__item form-settings__item_show-name">
                    <label for="show_name" class="form-settings__label">Показывать имя и фамилию:</label>
                    <div class="form-settings__control-wrap">
                        <div class="form-settings__control">
                            <input type="checkbox" class="form-settings__checkbox jqstyler" name="show_name" id="show_name" value="1"{{ auth()->user()->show_name ? ' checked' : '' }}>
                        </div>
                    </div>
                    <div class="form-settings__hint">Если включен этот параметр, то имя и фамилия будут показываться в вашем личном бизнес профиле. <br>Наличие реального имени и фамилии значительно увеличивает доверие к вам среди потенциальных клиентов.</div>
                </div>
                <div class="form-settings__item form-settings__item_contacts">
                    <label class="form-settings__label">Контакты для партнёров:</label>
                    <div class="form-settings__control-wrap">
                        <div class="form-settings__control" id="contact-list">
                            <div class="form-settings__item-field" id="default-contact" style="display: none;">
                                <select name="contacts[]" id="administration_contacts_select" class="form-settings__select form-settings__select_contacts">
                                    @foreach(config('custom.contacts') as $key => $value)
                                        <option value="{{ $key }}">
                                            {{ $value }}
                                        </option>
                                    @endforeach
                                </select>
                                <input type="text" name="values[]" id="administration_contacts_input" placeholder="Укажите данные" class="form-settings__text">
                                <!-- <a href="#" class="btn-green2 remove-contact" style="background:#ba4741;">Убрать</a> -->
                                <a href="#" class="remove-contact" title="Удалить контакт"><img src="/images/icons/icon_delete_contact.png" alt=""></a>
                            </div>

                            @foreach(auth()->user()->contacts()->type('partner')->get() as $contact)
                                <div class="form-settings__item-field">
                                    <select name="contacts[]" id="administration_contacts_select" class="form-settings__select form-settings__select_contacts">
                                        @foreach(config('custom.contacts') as $key => $value)
                                            <option value="{{ $key }}"{{ $key == $contact->contact_id ? ' selected' : '' }}>
                                                {{ $value }}
                                            </option>
                                        @endforeach
                                    </select>
                                    <input type="text" name="values[]" id="administration_contacts_input" placeholder="Укажите данные" class="form-settings__text" value="{{ $contact->value }}">
                                    <!-- <a href="#" class="btn-green2 remove-contact" style="background:#ba4741;">Убрать</a> -->
                                    <a href="#" class="remove-contact" title="Удалить контакт"><img src="/images/icons/icon_delete_contact.png" alt=""></a>
                                </div>
                            @endforeach
                        </div>
                        <a href="#" class="btn-green2 add-contact">Добавить</a>

                        <div class="form-settings__hint">Эти поля заполняются по желанию. Вы можете указать один или несколько способов связи. Контакты необходимы для того, чтобы в экстренных ситуациях сотрудники сервиса смогли с вами оперативно связаться. Введенные контакты строго конфиденциальны и не будут показываться никому из пользователей.</div>
                        @if($errors->has('contacts') || $errors->has('contacts.*') || $errors->has('values.*'))
                            <div class="form-error">Не заполнены поля.</div>
                        @endif
                    </div>
                </div>
                <div class="form-settings__actions">
                    <input type="submit" class="btn-green2 form-settings__submit" value="Сохранить">
                </div>
            </form>
        </div>
    </div>
@endsection

@section('modals')
    <div class="modal fade" id="modal-avatar" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <span class="close" data-dismiss="modal"></span>
                    <h4>Загрузка аватара</h4>
                </div>
                <form class="modal-body" method="POST" enctype="multipart/form-data" action="/avatar">
                    {{ csrf_field() }}
                    <input type="hidden" value="patch" name="_method">

                    <input type="hidden" value="0" name="start[x]">
                    <input type="hidden" value="0" name="start[y]">

                    <input type="hidden" value="0" name="end[x]">
                    <input type="hidden" value="0" name="end[y]">

                    <div class="login-error">
                        <input type="file" name="image" accept="image/*" required>

                        <img class="new-avatar" style="max-width: 100%;padding: 10px;"/>

                        <!-- <button type="submit">Отправить</button> -->
                    </div>
                    <!-- <a href="#" data-dismiss="modal" class="btn-close">Отменить</a> -->
                    <button type="submit" class="btn-close">Отправить</button>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="/js/croppie.min.js"></script>
    <script>
        if ($('#contact-list').children().length == 3) {
            $('.add-contact').hide();
        } else {
            $('.add-contact').show();
        }
        $('.jqstyler').styler({
            onFormStyled: function () {
                $('.jq-selectbox').each(function (index, el) {
                    var bgImg = $(this).find('.jq-selectbox__dropdown ul li.selected').css('background-image');
                    $(this).find('.jq-selectbox__select-text').css('background-image', bgImg);
                });
            },
            onSelectClosed: function () {
                var bgImg = $(this).find('.jq-selectbox__dropdown ul li.selected').css('background-image');
                $(this).find('.jq-selectbox__select-text').css('background-image', bgImg);
            }
        });

        $('.new-avatar').croppie({
            viewport: {
                width: 150,
                height: 150,
                type: 'circle'
            },
            boundary: {
                width: 500,
                height: 500
            },
            enableExif: true
        });

        $('[name="image"]').change(function() {
            let input = this;

            if (input.files && input.files[0]) {
                let reader = new FileReader();

                reader.onload = function(e) {
                    $('.new-avatar').croppie('bind', {
                        url: e.target.result
                    });
                };

                reader.readAsDataURL(input.files[0]);
            }
        });

        $('[action="/avatar"]').on('submit', function(event) {
            let data = $('.new-avatar').croppie('get');
            let form = $(this);

            form.find('[name="start[x]"]').val(data.points[0]);
            form.find('[name="start[y]"]').val(data.points[1]);
            form.find('[name="end[x]"]').val(data.points[2]);
            form.find('[name="end[y]"]').val(data.points[3]);
        });

        $('form').on('submit', function (event) {
            $('#default-contact').remove();
        });

        $('.add-contact').on('click', function (event) {
            event.preventDefault();

            $('#contact-list').append($('#default-contact').clone().attr({style: '', id: ''}));
            if ($('#contact-list').children().length == 3) {
                $(this).hide();
            } else {
                $(this).show();
            }
        });

        $(document).on('click', '.remove-contact', function (event) {
            event.preventDefault();

            $(this).closest('div').remove();
            $('.add-contact').show();
        });
    </script>
@endsection