@extends('layouts.app')

@title('Пополнение ' . $crypto_currency->name . ' кошелька')

@className('main_replenishment-bitcoin')

@section('content')
	<div class="content">
		<div class="title-wrap title-wrap_replenishment-bitcoin">
			<div class="container">
				<div class="row">
					<h1 class="page-title title-wrap__title">Пополнение {{ $crypto_currency->short }} кошелька</h1>
					<a href="/" class="btn-back title-wrap__back">назад</a>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<div class="bitcoin-address bitcoin-address_replenishment-bitcoin">
						<div class="bitcoin-address__text">Используйте приведенный ниже {{ $crypto_currency->short }} адрес или QR коды для пополнения вашего баланса</div>
						<form action="" class="form-copy">
							<input type="text" class="form-copy__text" name="text_to_copy" value="{{ $address->address }}" id="text_to_copy" readonly="readonly">
							<button class="form-copy__button btn-clipboard" type="button" title="Нажмите, чтобы скопировать текст в буфер обмена" data-clipboard-target="#text_to_copy">Скопировать</button>
						</form>
					</div>
					<div class="qr-codes qr-codes_replenishment-bitcoin">
						<div class="qr-codes__item">
							<div class="qr-codes__img">
								<img src="{{ $image->output() }}" alt="">
							</div>
							<div class="qr-codes__text">{{ $address->address }}</div>
						</div>
						<div class="qr-codes__item">
							<div class="qr-codes__img">
								<img src="{{ $image_with_url->output() }}" alt="">
							</div>
							<div class="qr-codes__text">{{ strtolower($address->crypto_currency->name) }}:{{ $address->address }}</div>
						</div>
					</div>
					<div class="replenishment-note replenishment-note_replenishment-bitcoin">Зачисление средств происходит в автоматическом режиме после 3-х подтверждений. Текущая комиссия сети за пополнение: {{ $crypto_currency->commission_input }} BTC.</div>
				</div>
			</div>
		</div>
		<div class="history-bitcoin-addresses">
			<div class="history-bitcoin-addresses__header">
				<div class="container">
					<div class="row">
						<div class="col-xs-12">
							<h2 class="history-bitcoin-addresses__title">История сгенерированных {{ $crypto_currency->short }} адресов</h2>
							<div class="history-bitcoin-addresses__description">Для повышения вашей безопасности и анонимности, CRYPTOLIDE автоматически генерирует новый BTC адрес для каждого перевода. <br>Все адреса, которые были сгенерированы ранее, навсегда закрепляются за вами и всегда будут доступны только для вашего пользования.</div>
						</div>
					</div>
				</div>
			</div>
			<div class="table-history-bitcoin">
				<div class="table-history-bitcoin__header">
					<div class="container">
						<div class="row">
							<div class="col-xs-12">
								<div class="table-responsive">
									<table class="table-history-bitcoin__header-inner">
										<thead>
										<tr>
											<th class="table-history-bitcoin__date">Дата</th>
											<th class="table-history-bitcoin__btc-address">{{ $crypto_currency->short }} адрес</th>
										</tr>
										</thead>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="table-history-bitcoin__body">
					@foreach(auth()->user()->addresses as $address)
						<div class="table-history-bitcoin__row">
							<div class="container">
								<div class="row">
									<div class="col-xs-12">
										<div class="table-responsive">
											<table class="table-history-bitcoin__row-inner">
												<tbody>
												<tr>
													<td class="table-history-bitcoin__date">{{ $address->updated_at->format('H:i d.m.Y') }}</td>
													<td class="table-history-bitcoin__btc-address">{{ substr_replace($address->address, '......', -14) }}</td>
												</tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					@endforeach
			</div>
			</div>
			{{ $addresses->links('pagination.cryptolide') }}
		</div>
	</div>
@endsection

@section('scripts')
	<script src="https://cdn.jsdelivr.net/clipboard.js/1.5.3/clipboard.min.js"></script>
	<script>
  	new Clipboard('.btn-clipboard');
	</script>
@endsection