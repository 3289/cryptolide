@extends('layouts.app')

@title('Безопасность')

@className('main_my-security')

@section('content')
    <div class="content">
        <div class="title-wrap title-wrap_my-security">
            <div class="container">
                <div class="row">
                    <h1 class="page-title title-wrap__title">Мои настройки</h1>
                    <a href="/" class="btn-back title-wrap__back">назад</a>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <ul class="settings-tabs">
                        <li class="settings-tabs__item"><a href="/general-information" class="settings-tabs__link">ОБЩАЯ ИНФОРМАЦИЯ</a></li>
                        <li class="settings-tabs__item settings-tabs__item_active"><a href="/my-security" class="settings-tabs__link">БЕЗОПАСНОСТЬ</a></li>
                        <li class="settings-tabs__item"><a href="/verification" class="settings-tabs__link">ВЕРИФИКАЦИЯ</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="form-settings-wrapper">
            <div class="form-settings">
                <div class="form-settings__item form-settings__item_twofactor-authentication">
                    <label class="form-settings__label">Двухфакторная аутентификация:</label>
                    <div class="form-settings__control-wrap">
                        <div class="form-settings__control">
                            @if (auth()->user()->google_fa)
                                <span class="form-settings__enabled">2FA ВКЛЮЧЕНА</span>
                                <a href="/2fa/disable" class="form-settings__btn-disable btn-blue">Выключить</a>
                            @else
                                <span class="form-settings__disabled">2FA ВЫКЛЮЧЕНА</span>
                                <a href="/2fa/enable" class="form-settings__btn-enable btn-green">Включить</a>
                            @endif
                        </div>
                        <div class="form-settings__hint">Двухфакторная аутентификация, реализованная с помощью приложения Google Authenticator, обеспечит максимальную <br>защиту вашего аккаунта путем постоянного и непрерывного генерирования специальных кодовых значений. Функция <br>является полностью бесплатной в использовании и повышает безопасность вашего аккаунта в несколько раз.</div>
                    </div>
                </div>
                <div class="form-settings__item form-settings__item_email">
                    <div class="form-settings__item-field">
                        <label class="form-settings__label">E-mail:</label>
                        <div class="form-settings__control-wrap">
                            <span class="form-settings__control">{{ $email }}</span>
                        </div>
                    </div>
                    <div class="form-settings__item-field">
                        <label for="email" class="form-settings__label form-settings__label_new-email">Новый E-mail:</label>
                        <div class="form-settings__control-wrap">
                            <input type="text" name="email" id="email" placeholder="Укажите новый E-mail" class="form-settings__text">
                            <div class="form-error email_error" style="display: none;"></div>
                            <div class="form-settings__control-btn">
                                <a href="#" class="btn-green2 change_email">Изменить E-mail</a>
                            </div>
                        </div>
                    </div>
                    <div class="form-settings__item-field form-settings__item-field_confirmation-code" style="display: none;">
                        <label for="activation_code" class="form-settings__label form-settings__label_new-email">Код подтверждения:</label>
                        <div class="form-settings__control-wrap">
                            <input type="text" name="activation_code" id="activation_code" placeholder="000000" class="form-settings__text">
                            <div class="form-settings__hint">На ваш старый E-mail было отправлено письмо <br>с кодом подтверждения. Введите код в поле.</div>
                            <div class="form-error activation_code_error" style="display: none;"></div>
                            <div class="form-settings__control-btn">
                                <a href="#" class="btn-green2 confirm_change_email">Подтвердить</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-settings__item form-settings__item_password">
                    <div class="form-settings__item-field">
                        <label for="current_password" class="form-settings__label">Старый пароль:</label>
                        <div class="form-settings__control-wrap">
                            <input type="password" name="current_password" id="current_password" placeholder="*********************" class="form-settings__text">
                            <div class="form-settings__hint">Введите пароль, который используется сейчас.</div>
                        </div>
                    </div>
                    <div class="form-settings__item-field">
                        <label for="password" class="form-settings__label">Новый пароль:</label>
                        <div class="form-settings__control-wrap">
                            <input type="password" name="password" id="password" placeholder="*********************" class="form-settings__text">
                            <div class="form-settings__hint">Придумайте новый пароль. Чем сложнее - тем лучше.</div>
                        </div>
                    </div>
                    <div class="form-settings__item-field">
                        <label for="password_confirmation" class="form-settings__label">Повторите новый пароль:</label>
                        <div class="form-settings__control-wrap">
                            <input type="password" name="password_confirmation" id="password_confirmation" placeholder="*********************" class="form-settings__text">
                            <div class="form-settings__hint">Повторите новый пароль.</div>
                            <div class="form-error password_error" style="display: none;"></div>
                            <div class="form-settings__control-btn">
                                <a href="#" class="btn-green2 change_password">Изменить пароль</a>
                            </div>
                            <div class="form-success password_success" style="display: none;"></div>
                        </div>
                    </div>
                </div>
                <form method="POST" action="">
                    {{ csrf_field() }}
                    <input type="hidden" value="patch" name="_method" />
                <div class="form-settings__item form-settings__item_contact-number">
                    <label for="phone" class="form-settings__label">Контактный телефон:</label>
                    <div class="form-settings__control-wrap">
                        <div class="form-settings__control">
                            <input type="text" name="phone" id="phone" placeholder="Укажите контактный телефон" class="form-settings__text" value="{{ auth()->user()->phone }}">
                        </div>
                        <div class="form-settings__hint">Контактный номер необходим для того, чтобы в экстренных ситуациях <br>сотрудники сервиса смогли с вами оперативно связаться. Номер является <br>конфиденциальным и не будет показываться никому из пользователей.</div>
                        @if($errors->has('phone'))
                            <div class="form-error">{{ $errors->first('phone') }}</div>
                        @endif
                    </div>
                </div>
                <div class="form-settings__item form-settings__item_contacts">
                    <label class="form-settings__label">Контакты для администрации:</label>
                    <div class="form-settings__control-wrap">
                        <div class="form-settings__control" id="contact-list">

                            <div class="form-settings__item-field" id="default-contact" style="display: none;">
                                <select name="contacts[]" id="administration_contacts_select" class="form-settings__select form-settings__select_contacts jqstyler">
                                    @foreach(config('custom.contacts') as $key => $value)
                                        <option value="{{ $key }}">
                                            {{ $value }}
                                        </option>
                                    @endforeach
                                </select>
                                <input type="text" name="values[]" id="administration_contacts_input" placeholder="Укажите данные" class="form-settings__text">
                                <!-- <a href="#" class="btn-green2 remove-contact" style="background:#ba4741;">Убрать</a> -->
                                <a href="#" class="remove-contact" title="Удалить контакт"><img src="/images/icons/icon_delete_contact.png" alt=""></a>
                            </div>

                            @foreach(auth()->user()->contacts()->type('admin')->get() as $contact)
                                <div class="form-settings__item-field">
                                    <select name="contacts[]" id="administration_contacts_select" class="form-settings__select form-settings__select_contacts jqstyler">
                                        @foreach(config('custom.contacts') as $key => $value)
                                            <option value="{{ $key }}"{{ $key == $contact->contact_id ? ' selected' : '' }}>
                                                {{ $value }}
                                            </option>
                                        @endforeach
                                    </select>
                                    <input type="text" name="values[]" id="administration_contacts_input" placeholder="Укажите данные" class="form-settings__text" value="{{ $contact->value }}">
                                    <!-- <a href="#" class="btn-green2 remove-contact" style="background:#ba4741;">Убрать</a> -->
                                    <a href="#" class="remove-contact" title="Удалить контакт"><img src="/images/icons/icon_delete_contact.png" alt=""></a>
                                </div>
                            @endforeach
                        </div>
                        <a href="#" class="btn-green2 add-contact">Добавить</a>
                        <div class="form-settings__hint">Эти поля заполняются по желанию. Вы можете указать один или несколько способов связи. Контакты необходимы для того, чтобы в экстренных ситуациях сотрудники сервиса смогли с вами оперативно связаться. Введенные контакты строго конфиденциальны и не будут показываться никому из пользователей.</div>
                        @if($errors->has('contacts') || $errors->has('contacts.*') || $errors->has('values.*'))
                            <div class="form-error">Не заполнены поля.</div>
                        @endif
                    </div>
                </div>
                <div class="form-settings__actions">
                    <input type="submit" class="btn-green2 form-settings__submit" value="Сохранить">
                </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        /*
        function stylerInit() {
            $('.jqstyler').styler({
                onFormStyled: function () {
                    $('.jq-selectbox').each(function (index, el) {
                        var bgImg = $(this).find('.jq-selectbox__dropdown ul li.selected').css('background-image');
                        $(this).find('.jq-selectbox__select-text').css('background-image', bgImg);
                    });
                },

                onSelectClosed: function () {
                    var bgImg = $(this).find('.jq-selectbox__dropdown ul li.selected').css('background-image');
                    $(this).find('.jq-selectbox__select-text').css('background-image', bgImg);
                }
            });
        }*/

        if ($('#contact-list').children().length == 9) {
            $('.add-contact').hide();
        } else {
            $('.add-contact').show();
        }
        $('form').on('submit', function (event) {
            $('#default-contact').remove();
        });

        $('.add-contact').on('click', function (event) {
            event.preventDefault();

            $('#contact-list').append($('#default-contact').clone().attr({style: '', id: ''}));
            if ($('#contact-list').children().length == 9) {
                $(this).hide();
            } else {
                $(this).show();
            }
        });

        $(document).on('click', '.remove-contact', function (event) {
            event.preventDefault();

            $(this).closest('div').remove();
            $('.add-contact').show();
        });

        $('.change_email').on('click', function(event) {
            event.preventDefault();

            var button = $(this);
            var old_text = button.text();

            button.text('Подождите...');

            $.ajax('/change_email', {
                type: 'PATCH',
                data: {
                    email: $('[name="email"]').val()
                },
                success: function(response) {
                    if (response.status) {
                        $('.email_error').fadeOut();
                        $('.form-settings__item-field_confirmation-code').fadeIn();
                    }
                },
                error: function(err) {
                    $('.email_error').text(err.responseJSON.errors.email[0]).fadeIn();
                },
                complete: function () {
                    button.text(old_text);
                }
            });
        });

        let i = 0;

        $('.confirm_change_email').on('click', function(event) {
            event.preventDefault();

            if (i == 5) {
                $('.activation_code_error').text('Повторите ваши действия позже.').fadeIn();
                return;
            }

            $.ajax('/confirm_change_email', {
                type: 'PATCH',
                data: {
                    email: $('[name="email"]').val(),
                    activation_code: $('[name="activation_code"]').val()
                },
                success: function(response) {
                    if (response['status']) {
                        window.location.reload();
                    } else {
                        $('.activation_code_error').text(response['message']).fadeIn();
                        i++;
                    }
                },
                error: function(err) {
                    $('.email_error').text(err.responseJSON.errors.email[0]).fadeIn();
                }
            });
        });

        $('.change_password').on('click', function(event) {
            event.preventDefault();

            var button = $(this);
            var old_text = button.text();

            button.text('Подождите...');

            $('.password_success').fadeOut();
            $('.password_error').fadeOut();

            $.ajax('/change_password', {
                type: 'PATCH',
                data: {
                    current_password: $('[name="current_password"]').val(),
                    password: $('[name="password"]').val(),
                    password_confirmation: $('[name="password_confirmation"]').val(),
                },
                success: function(response) {
                    if (response['status']) {
                        $('.password_success').text(response['message']).fadeIn();
                    } else {
                        $('.password_error').text(response['message']).fadeIn();
                    }
                    $('.form-settings__item_password input').val('');
                },
                error: function(err) {
                    if (err.responseJSON.errors.current_password !== undefined) {
                        $('.password_error').text(err.responseJSON.errors.current_password[0]);
                    }

                    if (err.responseJSON.errors.password !== undefined) {
                        $('.password_error').text(err.responseJSON.errors.password[0]);
                    }

                    $('.password_error').fadeIn();
                },
                complete: function () {
                    button.text(old_text);
                }
            });
        });

        $('input#phone').on('click', function(event) {
            $(this).parent().addClass('phone-focus');
        });
    </script>
@endsection