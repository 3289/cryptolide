@extends('layouts.app')

@title('Заключение сделки')

@className('main_make-deal')

@section('content')
<div class="content">
  <div class="title-wrap title-wrap_make-deal">
    <div class="container">
      <div class="row">
        <h1 class="page-title title-wrap__title">
          @if ($deal->short_type == 'sell')
          @if ($deal->buyer->id == auth()->user()->id)
          Покупка
          @else
          Продажа
          @endif
          @elseif ($deal->short_type == 'buy')
          @if ($deal->buyer->id == auth()->user()->id)
          Покупка
          @else
          Продажа
          @endif
          @else
          Обмен
          @endif

          @if ($deal->type == 'trade')
          <span class="title-wrap__bitcoin-amount">
            {{ $deal->sumCurrency() . ' ' . $deal->currency_name.' ('.$deal->crypto_trade->name.')' }}
          </span>
          на
          <span class="title-wrap__bitcoin-number">
            {{ $deal->sumCrypto() . ' ' . $deal->crypto->short.' ('.$deal->crypto->name.')' }}
          </span>
          @else
          <span class="title-wrap__bitcoin-number">
            {{ $deal->sumCrypto() . ' ' . $deal->crypto->short }}
          </span>
          за
          <span class="title-wrap__bitcoin-amount">
            {{ $deal->sumCurrency() . ' ' . $deal->currency_name }}
          </span>
          @endif


          @if ($deal->type == 'trade')
          @elseif ($deal->type == 'buy_online' || $deal->type == 'sell_online')
          через: {{ $deal->payment_type->name }}
          @if ($deal->payment_type_id == 2)
          ({{ $deal->country->name }}) - {{ $deal->banks }}
          @endif
          @else
          с помощью наличных в: {{ $deal->city->name }}, {{ $deal->country->name }}
          @endif

          <br>
          <span class="title-wrap__transaction-note">
            Сделка совершается по курсу: 1 BTC = {{ $deal->rate() . ' ' . $deal->currency_name }}
          </span>
        </h1>
        <a href="/" class="btn-back title-wrap__back">назад</a>
      </div>
    </div>
  </div>
  <div class="transaction-block">
    <div class="container">
      <div class="row">
        <div class="transaction-block__content">
          <div class="transaction-block__participant transaction-block__participant_first">
            <div class="transaction-block__participant-first-col">
              <div class="transaction-block__participant-photo">
                <img src="{{ $deal->seller->image }}" alt="Первый участник">
              </div>
            </div>
            <div class="transaction-block__participant-second-col">
              <div class="transaction-block__number transaction-block__number_green">
                {{ $deal->sumCrypto() . ' ' . $deal->crypto->short }}
              </div>
              <div class="user user_transaction-block user_participant-first">
                <div class="user__info user__info_{{ $deal->seller->is_online ? 'online' : 'offline' }}">
                  <a href="/profile/{{ $deal->seller->login }}"
                   class="user__name">{{ $deal->seller->login }}</a>
                   <div class="user__rating">{{ $deal->seller->rating }} <span
                    class="user__icons"></span></div>
                    <span class="action-table__seller-icons">
                      @foreach($deal->seller->achievements as $achievement)
                      @if ($achievement->image == '/storage/achievements/2.png')
                      <span data-toggle="tooltip" title="Пройдена полная верификация личности">
                        <img src="{{ $achievement->image }}" alt="">
                      </span>
                      @elseif($achievement->image == '/storage/achievements/1.png')
                      <span data-toggle="tooltip" title="Верифицирован телефонный номер">
                        <img src="{{ $achievement->image }}" alt="">
                      </span>
                      @else
                      <img src="{{ $achievement->image }}" alt="">
                      @endif
                      @endforeach
                    </span>

                  </div>
                </div>
              </div>
            </div>
            <div class="transaction-block__transaction-info">
              <div class="transaction-block__transaction-number">Сделка №{{ $deal->id }}</div>
              @if ($deal->time_left > 0 && $deal->status == 'pending')
              <div class="transaction-block__closing-time">закрытие через {{ $deal->time_left }} мин
              </div>
              @endif
            </div>
            <div class="transaction-block__participant transaction-block__participant_second">
              <div class="transaction-block__participant-first-col">
                <div class="transaction-block__number transaction-block__number_blue">
                  {{ $deal->sumCurrency() . ' ' . $deal->currency_name }}
                </div>
                <div class="user user_transaction-block user_participant-second">
                  <div class="user__info user__info_{{ $deal->buyer->is_online ? 'online' : 'offline' }}">
                    <div class="user__rating">{{ $deal->buyer->rating }} <span
                      class="user__icons"></span></div>
                      <span class="action-table__seller-icons">
                        @foreach($deal->buyer->achievements as $achievement)
                        <img src="{{ $achievement->image }}" alt="">
                        @endforeach
                      </span>
                      <a href="/profile/{{ $deal->buyer->login }}"
                       class="user__name">{{ $deal->buyer->login }}</a>
                     </div>
                   </div>
                 </div>
                 <div class="transaction-block__participant-second-col">
                  <div class="transaction-block__participant-photo">
                    <img src="{{ $deal->buyer->image }}" alt="Второй участник">
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="transaction-status">
        <div class="container">
          <div class="row">
            <div class="transaction-status__text">
             <!-- Статус сделки: -->
             @if ($deal->status == 'pending')
             <img src="/images/icons/icon_time_1.png" alt="">Статус сделки: Ожидание оплаты от
             <a href="#">{{ $deal->buyer->login }}
             </a>
             @elseif ($deal->status == 'paid')
             <img src="/images/icons/icon_time_2.png" alt="">Статус сделки: Оплачена
             @elseif ($deal->status == 'cancelled')
             <img src="/images/icons/icon_time_3.png" alt="">Статус сделки: Отменена
             @elseif ($deal->status == 'completed')
             <img src="/images/icons/icon_time_4.png" alt="">Статус сделки: Завершена
             @endif
           </div>
         </div>
       </div>
     </div>
     <div class="container">
      <div class="row">
        <div class="blocks-wrapper">
          <div class="left-blocks">
            <div class="online-chat information-block">
              <h2 class="online-chat__title information-block__title">Онлайн чат
                с {{ $deal->partner->login }}</h2>
                <div class="information-block__content" id="chat">
                  <form class="send-message online-chat__send-message">
                    <input type="hidden" name="deal_id" value="{{ $deal->id }}">
                    <div class="send-message__file">
                      <label for="file" class="send-message__file-label"
                      title="Прикрепить файл"></label>
                      <input type="file" name="file" id="file" class="send-message__file-input">
                    </div>
                    <div class="send-message__form-item">
                      <input type="text" name="text"  id="message-input"  placeholder="Введите текст сообщения..."
                      class="send-message__form-text">
                    </div>
                    <div class="send-message__form-actions">
                      <button class="send-message__form-submit" title="Отправить сообщение"></button>
                    </div>
                  </form>
                  <div class="online-messages online-chat__messages stylized-scroll"
                  id="message-list"></div>
                </div>
              </div>
            </div>
            <div class="right-blocks">
              <div class="information-block information-block_further-actions">
                <h2 class="information-block__title">Ваши дальнейшие действия</h2>
                <div class="information-block__content">
                  <div class="information-block__steps">
                    <div class="information-block__step">
                      @if ($deal->short_type == 'sell')
                      @if ($deal->buyer->id == auth()->user()->id)
                      <h3 class="information-block__step-title">Шаг 1: Оплатите заявку</h3>

                      <div class="information-block__step-content">
                        <span style="font-weight: 500;">{{ $deal->sumCrypto() . ' ' . $deal->crypto->short }}</span>
                        успешно зарезервированы на балансе Cryptolide. Теперь вам нужно
                        перевести денежные средства пользователю на реквизиты, указанные
                        ниже.
                        Чтобы пользователь смог определить, от кого поступил платеж - при
                        переводе обязательно укажите сообщение или примечание: <span
                        style="font-weight: 500;">CL{{ $deal->id }}</span>. Это
                        позволит
                        провести сделку значительно быстрее, например, всего за 5 минут.
                        Чтобы
                        получить помощь в совершении платежа - напишите в онлайн чат,
                        который
                        находится с левой стороны.
                      </div>
                      @else
                      <h3 class="information-block__step-title">Шаг 1: Проверьте информацию о
                      сделке</h3>

                      <div class="information-block__step-content">
                        <span style="font-weight: 500;">Сумма в депонировании: {{ $deal->sumCrypto() . ' ' . $deal->crypto->short }}</span>
                        <br>
                        <span style="font-weight: 500;">Сумма получаемого платежа: {{ $deal->sumCurrency() . ' ' . $deal->currency_name }}</span>
                        <br>
                        <span style="font-weight: 500;">Примечание к транзакции: CL{{ $deal->id }}</span>
                      </div>
                      @endif
                      @elseif($deal->short_type == 'buy')
                      @if ($deal->buyer->id == auth()->user()->id)
                      <h3 class="information-block__step-title">Шаг 1: Оплатите заявку</h3>

                      <div class="information-block__step-content">
                        <span style="font-weight: 500;">{{ $deal->sumCrypto() . ' ' . $deal->crypto->short }}</span>
                        успешно зарезервированы на балансе Cryptolide. Теперь вам нужно
                        перевести денежные средства пользователю на реквизиты, указанные
                        ниже.
                        Чтобы пользователь смог определить, от кого поступил платеж - при
                        переводе обязательно укажите сообщение или примечание: <span
                        style="font-weight: 500;">CL{{ $deal->id }}</span>. Это
                        позволит
                        провести сделку значительно быстрее, например, всего за 5 минут.
                        Чтобы
                        получить помощь в совершении платежа - напишите в онлайн чат,
                        который
                        находится с левой стороны.
                      </div>
                      @else
                      <h3 class="information-block__step-title">Шаг 1: Проверьте информацию о
                      сделке</h3>

                      <div class="information-block__step-content">
                        <span style="font-weight: 500;">Сумма в депонировании: {{ $deal->sumCrypto() . ' ' . $deal->crypto->short }}</span>
                        <br>
                        <span style="font-weight: 500;">Сумма получаемого платежа: {{ $deal->sumCurrency() . ' ' . $deal->currency_name }}</span>
                        <br>
                        <span style="font-weight: 500;">Примечание к транзакции: CL{{ $deal->id }}</span>
                      </div>
                      @endif
                      @elseif($deal->short_type == 'trade')
                      @if ($deal->buyer->id == auth()->user()->id)
                      <h3 class="information-block__step-title">Шаг 1: Оплатите заявку</h3>

                      <div class="information-block__step-content">
                        <span style="font-weight: 500;">{{ $deal->sumCrypto() . ' ' . $deal->crypto->short }}</span>
                        успешно зарезервированы на балансе Cryptolide. Теперь вам нужно
                        перевести денежные средства пользователю на реквизиты, указанные
                        ниже.
                        Чтобы пользователь смог определить, от кого поступил платеж - при
                        переводе обязательно укажите сообщение или примечание: <span
                        style="font-weight: 500;">CL{{ $deal->id }}</span>. Это
                        позволит
                        провести сделку значительно быстрее, например, всего за 5 минут.
                        Чтобы
                        получить помощь в совершении платежа - напишите в онлайн чат,
                        который
                        находится с левой стороны.
                      </div>
                      @else
                      <h3 class="information-block__step-title">Шаг 1: Проверьте информацию о
                      сделке</h3>

                      <div class="information-block__step-content">
                        <span style="font-weight: 500;">Сумма в депонировании: {{ $deal->sumCrypto() . ' ' . $deal->crypto->short }}</span>
                        <br>
                        <span style="font-weight: 500;">Сумма получаемого платежа: {{ $deal->sumCurrency() . ' ' . $deal->crypto_trade->short }}</span>
                        <br>
                        <span style="font-weight: 500;">Примечание к транзакции: CL{{ $deal->id }}</span>
                      </div>
                      @endif
                      @endif
                    </div>

                    @if ($deal->buyer->id == auth()->user()->id)
                    <div class="application information-block__application">
                      @if ($deal->type == 'buy_online' || $deal->type == 'sell_online')
                      <div class="application__item">
                        <div class="application__item-name">Способ:</div>
                        <div class="application__item-value">
                          {{ $deal->payment_type->name }}
                          @if ($deal->payment_type_id == 2)
                          ({{ $deal->country->name }}) - {{ $deal->banks }}
                          @endif
                        </div>
                      </div>
                      @endif
                      @if ($deal->requisites)
                      <div class="application__item">
                        <div class="application__item-name">Реквизиты:</div>
                        <div class="application__item-value">{{ $deal->requisites }}</div>
                      </div>
                      @endif
                      <div class="application__item">
                        <div class="application__item-name">Сумма:</div>
                        <div class="application__item-value">{{ $deal->sumCurrency() . ' ' . $deal->currency_name }}</div>
                      </div>
                      <div class="application__item">
                        <div class="application__item-name">Примечание:</div>
                        <div class="application__item-value">CL{{ $deal->id }}</div>
                      </div>
                    </div>
                    @endif

                    @if ($deal->buyer->id == auth()->user()->id)
                    <div class="transaction-terms information-block__transaction-terms">
                      <div class="transaction-terms__title">Условия сделки
                        с {{ $deal->creator->login }}:
                      </div>
                      <div class="transaction-terms__text">{{ $deal->description }}</div>
                    </div>
                    @else
                    <div class="transaction-terms information-block__transaction-terms information-block__transaction-terms_buyer">
                      <div class="transaction-terms__title_buyer">Детали сделки, как показано
                        покупателю
                      </div>
                      <div class="transaction-terms__text transaction-terms__text_buyer">{{ $deal->description }}</div>
                    </div>
                    @endif

                    @if ($deal->buyer->id != auth()->user()->id)
                    <div class=" information-block__application_buyer">Детали перевода:</div>
                    <div class="application information-block__application">
                      @if ($deal->type == 'buy_online' || $deal->type == 'sell_online')
                      <div class="application__item">
                        <div class="application__item-name">Способ:</div>
                        <div class="application__item-value">
                          {{ $deal->payment_type->name }}
                          @if ($deal->payment_type_id == 2)
                          ({{ $deal->country->name }}) - {{ $deal->banks }}
                          @endif
                        </div>
                      </div>
                      @endif
                      @if ($deal->requisites)
                      <div class="application__item">
                        <div class="application__item-name">Реквизиты:</div>
                        <div class="application__item-value">{{ $deal->requisites }}</div>
                      </div>
                      @endif
                      <div class="application__item">
                        <div class="application__item-name">Сумма:</div>
                        <div class="application__item-value">{{ $deal->sumCurrency() . ' ' . $deal->currency_name }}</div>
                      </div>
                      <div class="application__item">
                        <div class="application__item-name">Примечание:</div>
                        <div class="application__item-value">CL{{ $deal->id }}</div>
                      </div>
                    </div>
                    @endif

                    @if ($deal->buyer->id == auth()->user()->id)
                    <div class="information-block__step">
                      <h3 class="information-block__step-title information-block__step-title_2">
                      Шаг 2: Подтвердите оплату</h3>
                      <div class="information-block__step-content">
                        <strong>{{ $deal->sumCrypto() . ' ' . $deal->crypto->short }}</strong>
                        удерживаются на балансе Cryptolide в течение <span
                        style="color: #b88540;">{{ $deal->time }} минут</span>. Это
                        значит, что вам нужно совершить оплату до {{ $deal->closing_time }}
                        (сейчас {{ \Carbon\Carbon::now()->format('H:i') }}). После оплаты вам
                        <span style="color: #b88540;">обязательно нужно отметить</span> платеж
                        как завершенный, нажав на зеленую кнопку ниже. В противном случае сделка
                        будет автоматически отменена.
                      </div>
                    </div>
                    @else
                    <div class="information-block__step">
                      <h3 class="information-block__step-title information-block__step-title_2">
                      Шаг 2: Подтвердите получение средств</h3>
                      <div class="information-block__step-content">
                        @if ($deal->status == 'paid' || $deal->status == 'completed')
                                                 <!--    <p>Пользователь <strong>{{ $deal->buyer->login }}</strong> <strong
                                                                style="color:#3A9D41">отметил</strong>, что перевел
                                                        вам средства. Если оплата со стороны покупателя не будет совершена
                                                        в течение <strong style="color: #b88540;">{{ $deal->time }}
                                                        минут</strong> - сделка автоматически закроется.</p> -->


                                                        <p>Пользователь <strong>{{ $deal->buyer->login }}</strong> <strong
                                                          style="color:#3A9D41">отметил</strong>, что перевел
                                                          вам средства в размере <strong>{{ $deal->sumCurrency() . ' ' . $deal->currency_name }}</strong>. Проверьте поступление средств и отправьте пользователю  <strong>{{ $deal->sumCrypto() }} {{ $deal->crypto->short }}</strong>, нажав на кнопку ниже.

                                                        <!--  Если оплата со стороны покупателя не будет совершена
                                                        в течение <strong style="color: #b88540;">{{ $deal->time }}
                                                        минут</strong> - сделка автоматически закроется.</p>    -->     
                                                        @else
                                                        <p>Пользователь <strong>{{ $deal->buyer->login }}</strong> <strong
                                                          style="color:#bd4625">еще не отметил</strong>, что перевел
                                                          вам средства. Если оплата со стороны покупателя не будет совершена
                                                          в течение <strong style="color: #b88540;">{{ $deal->time }}
                                                          минут</strong> - сделка автоматически закроется.</p>
                                                          @endif
                                                          <br>
                                                          @if ($deal->status == 'paid' || $deal->status == 'completed')
                                                          <p></p>
                                                          @else
                                                          <p>Если вам уже поступили средства по этой сделке - вы можете отправить
                                                            пользователю
                                                            <strong>{{ $deal->sumCrypto() }} {{ $deal->crypto->short }}</strong>
                                                          нажав на кнопку ниже.</p>
                                                          @endif

                                                        </div>
                                                      </div>
                                                      @endif
                                                    </div>

                                                    @if ($deal->status == 'paid')
                                                    @if ($deal->buyer->id == auth()->user()->id)
                                                    <div class="funds-transferred information-block__funds-transferred">Вы перевели
                                                      денежные средства
                                                    </div>
                                                    @endif
                                                    @endif

                                                    @if ($deal->status == 'pending' && $deal->buyer->id == auth()->user()->id)
                                                    <div class="information-block__confirm-payment">
                                                      <button class="btn-action" id="confirm-deal">Я заплатил</button>
                                                    </div>

                                                    <div class="transaction-state information-block__transaction-state"
                                                    id="confirm-transfer-block" style="display: none;margin-bottom: 10px;">
                                                    <div class="transaction-state__header">
                                                      <h3 class="transaction-state__name">Подтвердите оплату</h3>
                                                    </div>
                                                    <div class="transaction-state__body">
                                                      <div class="transaction-state__text">
                                                        <p>Я, <strong>{{ auth()->user()->login }}</strong>, подтверждаю что:</p>
                                                        <ul>
                                                          <li>- Принимаю условия сделки по объявлению
                                                            №{{ $deal->id }}.
                                                          </li>
                                                          <li>- Перевел деньги, следуя инструкциям продавца
                                                            <strong>{{ $deal->seller->login }}</strong>.
                                                          </li>
                                                        </ul>
                                                      </div>
                                                      <div class="transaction-state__note">Если вы не уверены, что правильно
                                                        совершили денежный перевод или <br>у вас не получается перевести
                                                        средства - напишите в онлайн чат, который <br>находится левее, и
                                                        попросите пользователя {{ $deal->seller->login }} вам помочь.
                                                      </div>
                                                    </div>
                                                    <div class="transaction-state__buttons">
                                                      <button type="button" class="btn-green2 transaction-state__confirm-payment"
                                                      id="confirm-transfer">Подтвердить <br><span>я заплатил</span>
                                                    </button>
                                                    <button type="button"
                                                    class="btn-blue transaction-state__cancel-payment right"
                                                    id="dont-confirm-transfer">Отменить
                                                    <br><span>я еще не заплатил</span></button>
                                                  </div>
                                                </div>
                                                @endif

                                                @if ($deal->status == 'completed')

                                                <div class="complete_block">
                                                  <img src="/images/icons/icon_deal_complete.png" alt="" class="complete_block_img">
                                                  <p class="complete_block_caption">Сделка успешно завершена!</p>
                                                  <p class="complete_block_text">Вы получили <span class="complete_block_text_bold">+ {{ $deal->rate_up }}</span> TS к вашему рейтингу.</p>
                                                </div>


                                                <!-- 1Сделка успешно завершена! Ваш рейтинг увеличен на {{ $deal->rate_up }} единиц. -->
                                                
                                                @endif

                                                @if ($deal->status == 'cancelled')
                                                <div class="deal-cancelled information-block__deal-cancelled">Сделка отменена</div>
                                                @elseif ($deal->status != 'completed')
                                                @if(auth()->user()->id == $deal->buyer->id)
                                                <div class="cancel-deal information-block__cancel-deal"
                                                style="margin-bottom: 10px;">
                                                <a class="cancel-deal__link" id="cancel-deal" style="cursor:pointer;">Отменить
                                                сделку</a>
                                              </div>
                                              @else
                                              <div class="information-block__confirm-payment"
                                              style="width: 100%; max-width:100%">
                                              <button class="btn-action" id="seller-confirm-deal"
                                              style="width: 100%; max-width:100%">Подтвердить и
                                              отправить {{ $deal->sumCrypto() }} {{ $deal->crypto->short }}</button>
                                            </div>




                                            <div class="transaction-state transaction-state_confirm-transfer information-block__transaction-state"
                                            id="seller-confirmation-block" style="margin-bottom: 10px;">
                                            <div class="transaction-state__header">
                                              <h3 class="transaction-state__name">Подтвердите перевод
                                              криптовалюты</h3>
                                            </div>
                                            <div class="transaction-state__body">
                                              <div class="transaction-state__text">
                                                <p>Я, <strong>{{ $deal->seller->login }}</strong>, подтверждаю что:
                                                </p>
                                                <ul>
                                                  <li>- Получил
                                                    <strong>{{ $deal->sumCurrency() }} {{ $deal->currency_name }}</strong>
                                                    по текущей сделке <strong>№{{ $deal->id }}.</strong>
                                                  </li>
                                                </ul>
                                              </div>
                                              <div class="transaction-state__note">Обратите внимание! Существует такой способ мошенничества, при котором после перевода денежных средств на ваши реквизиты, отправитель, спустя некоторое время, отзывает платёж.
                                                Если текущий способ оплаты не позволяет вернуть отправленный платёж - вам не о чём беспокоиться.
                                              В противном случае, рекомендуем открывать сделки только для верифицированных пользователей (эту опцию можно указать при создании и редактировании объявления).</div>                                                    
                                            </div>
                                            <div class="transaction-state__confirmation-code">
                                              @if ($deal->seller->google_fa)
                                              <div class="transaction-state__confirmation-text">
                                                Для подтверждения перевода введите код подтверждения с
                                                приложения Google Authenticator.
                                              </div>
                                              @else
                                              <div class="transaction-state__confirmation-text">
                                                Для подтверждения перевода введите код подтверждения, <br>который
                                                был отправлен на ваш E-mail. <a style="cursor: pointer"
                                                id="re-send-confirmation-code">Отправить
                                              повторно.</a>
                                            </div>
                                            @endif
                                            <input type="text" name="confirmation_code" placeholder="000000"
                                            class="transaction-state__confirmation-input">
                                          </div>
                                          <div class="form-error transaction-state__form-error" style="display: none;"
                                          id="error-confirmation-code"></div>
                                          <div class="transaction-state__buttons">
                                            <button type="button"
                                            class="btn-green2 transaction-state__confirm-transfer"
                                            id="confirm-and-close">Подтвердить и
                                            отправить {{ $deal->sumCrypto() }} {{ $deal->crypto->short }}</button>
                                          </div>
                                        </div>

                                        <hr class="information-block_hide"
                                        style="max-width: 155px; margin: 20px auto; border-top: 2px solid #e3e3e3;">
                                        <div class="information-block__step information-block_hide">
                                          <div class="information-block__step-content">
                                            Если покупатель не отвечает или не совершает платеж либо возникли
                                            разногласия по условиям сделки, вы можете оспорить эту сделку
                                            через {{ $deal->time_left }} минут, <span style="color:#4183bc;">обратившись в арбитраж</span>.
                                          </div>
                                        </div>

                                        @endif

                                        @if ($deal->status == 'paid' && $deal->buyer->id == auth()->user()->id)
                                        <div class="transaction-state transaction-state_cancel-deal information-block__transaction-state"
                                        id="confirm-cancel-block" style="display: none;">
                                        <div class="transaction-state__header">
                                          <h3 class="transaction-state__name">Подтвердите отмену сделки</h3>
                                        </div>
                                        <div class="transaction-state__body">
                                          <div class="transaction-state__important">Обратите внимание!</div>
                                          <div class="transaction-state__text">
                                            <p><strong>Вы указали</strong>, что перевели средства в размере
                                              <strong>{{ $deal->sumCurrency() }} {{ $deal->currency_name }}</strong>
                                              <br>пользователю <strong>{{ $deal->seller->login }}</strong>.
                                              После закрытия сделки сервис <br>CRYPTOLIDE освободит от
                                              депонирования средства продавца <br>и не сможет гарантировать
                                            вам успешное заключение сделки.</p>
                                            <p>Вы действительно хотите отменить сделку №{{ $deal->id }}?</p>
                                          </div>
                                          <div class="transaction-state__description">
                                            @if ($deal->short_type == 'sell')
                                            @if ($deal->buyer->id == auth()->user()->id)
                                            Покупка
                                            @else
                                            Продажа
                                            @endif
                                            @elseif ($deal->short_type == 'buy')
                                            @if ($deal->buyer->id == auth()->user()->id)
                                            Продажа
                                            @else
                                            Покупка
                                            @endif
                                            @else
                                            Обмен
                                            @endif

                                            {{ $deal->sumCrypto() }} {{ $deal->crypto->short }}
                                            за {{ $deal->sumCurrency() }} {{ $deal->currency_name }}

                                            @if ($deal->type == 'trade')
                                            на {{ $deal->currency_name }}
                                            @elseif ($deal->type == 'buy_online' || $deal->type == 'sell_online')
                                            через: <br> {{ $deal->payment_type->name }}
                                            @if ($deal->payment_type_id == 2)
                                            ({{ $deal->country->name }}) - {{ $deal->banks }}
                                            @endif
                                            @else
                                            с помощью наличных в: {{ $deal->city->name }}, {{ $deal->country->name }}
                                            @endif
                                          </div>
                                        </div>
                                        <div class="transaction-state__buttons">
                                          <button type="button" class="btn-blue transaction-state__dont-cancel"
                                          id="dont-cancel">Не отменять сделку
                                        </button>
                                        <div class="transaction-state__confirm-cancellation">
                                          <a class="transaction-state__confirm-link" id="confirm-cancel"
                                          style="cursor:pointer;">Подтвердить отмену сделки</a>
                                        </div>
                                      </div>
                                    </div>
                                    @else
                                    <div class="transaction-state information-block__transaction-state"
                                    id="confirm-cancel-block" style="display: none;">
                                    <div class="transaction-state__header">
                                      <h3 class="transaction-state__name">Подтвердите отмену сделки</h3>
                                    </div>
                                    <div class="transaction-state__body">
                                      <div class="transaction-state__answer">Вы действительно хотите отменить
                                        сделку №{{ $deal->id }}?
                                      </div>
                                      <div class="transaction-state__description">
                                        @if ($deal->short_type == 'sell')
                                        @if ($deal->buyer->id == auth()->user()->id)
                                        Покупка
                                        @else
                                        Продажа
                                        @endif
                                        @elseif ($deal->short_type == 'buy')
                                        @if ($deal->buyer->id == auth()->user()->id)
                                        Продажа
                                        @else
                                        Покупка
                                        @endif
                                        @else
                                        Обмен
                                        @endif

                                        {{ $deal->sumCrypto() }} {{ $deal->crypto->short }}
                                        за {{ $deal->sumCurrency() }} {{ $deal->currency_name }}

                                        @if ($deal->type == 'trade')
                                        на {{ $deal->currency_name }}
                                        @elseif ($deal->type == 'buy_online' || $deal->type == 'sell_online')
                                        через: <br> {{ $deal->payment_type->name }}
                                        @if ($deal->payment_type_id == 2)
                                        ({{ $deal->country->name }}) - {{ $deal->banks }}
                                        @endif
                                        @else
                                        с помощью наличных в: {{ $deal->city->name }}, {{ $deal->country->name }}
                                        @endif
                                      </div>
                                      @if ($deal->buyer->id == auth()->user()->id)
                                      <div class="transaction-state__note">Ни в коем случае не отменяйте
                                        сделку, если вы перевели средства! После <br>закрытия сделки
                                        криптовалюта продавца освобождается от депонирования и сервис не сможет гарантировать успешное заключение сделки.

                                      </div>
                                      @endif
                                    </div>
                                    <div class="transaction-state__buttons">
                                      <button type="button" class="btn-green2 transaction-state__cancel"
                                      id="confirm-cancel">Отменить сделку
                                    </button>
                                    <button type="button"
                                    class="btn-blue transaction-state__dont-cancel right"
                                    id="dont-cancel">Не отменять сделку
                                  </button>
                                </div>
                              </div>
                              @endif
                              @endif
                            </div>
                          </div>

                          <div class="about-transactions"><a href="/help#help_work_1" class="transaction-info-link">Подробнее о сделках и
                          арбитраже</a></div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                @endsection

                @section('scripts')
                <script>
                  $(function () {

                    $(document).ready(function () {
                      $('.transaction-state, .information-block_hide').hide();
                      $('#confirm-deal').on('click', function () {
                        $('.information-block__confirm-payment').hide();
                        $('.information-block_hide').show();
                        $('#confirm-cancel-block').hide();
                        $('.information-block__cancel-deal').hide();

                      });
                      $('.cancel-deal').on('click', function () {
                        $('#confirm-transfer-block').hide();
                        $('.information-block__cancel-deal').hide();
                        $('.information-block__confirm-payment').hide();
                        $('#confirm-deal').hide();

                      });
                      $('#dont-cancel').on('click', function () {
                        $('.information-block__confirm-payment').show();
                        $('#confirm-deal').show();
                        $('.information-block__cancel-deal').show();

                      });
                      $('#dont-confirm-transfer').on('click', function () {
                        $('.information-block__cancel-deal').show();
                        $('#confirm-deal').show();

                      });
                      $('#seller-confirm-deal').on('click', function () {
                        $('#seller-confirm-deal').hide();

                      });
                    });


                    $('#confirm-deal').on('click', function () {
                      $('#confirm-transfer-block').css('display', 'block');
                    });

                    $('#cancel-deal').on('click', function () {
                      $('#confirm-cancel-block').css('display', 'block');
                    });

                    $('#dont-cancel').on('click', function () {
                      $('#confirm-cancel-block').css('display', 'none');
                    });

                    $('#confirm-cancel').on('click', function () {
                      $(this).prop('disabled', true);
                      updateStatus('cancelled', this);
                    });

                    $('#dont-confirm-transfer').on('click', function() {
                      $('#confirm-transfer-block').css('display', 'none');
                      $('.information-block__confirm-payment').show();
                    });


                    function updateStatus(status, btn = null) {
                      $.ajax('/deals/{{ $deal->id }}', {
                        type: 'PATCH',
                        data: {
                          status: status,
                          confirmation_code: $('[name="confirmation_code"]').val()
                        },
                        success: function (response) {
                          if (response.status === true) {
                            window.location.reload();

                            return false;
                          }

                          if (status === 'completed') {
                            $('[name="confirmation_code"]').val('');

                            $('#error-confirmation-code').css('display', 'block').text(response.message);
                            $(btn).prop('disabled', false);
                            return false;
                          }
                          $(btn).prop('disabled', false);
                          alert('Error.');
                        },
                        error: function (err) {
                          $('[name="confirmation_code"]').val('');

                          $('#error-confirmation-code').css('display', 'block').text('Пожалуйста, введите код подтверждения.');
                          $(btn).prop('disabled', false);
                          return false;
                        }
                      });
                    }

                    $('#confirm-transfer').on('click', function () {
                      $(this).prop('disabled', true);
                      updateStatus('paid', this);
                    });

                    $('#dont-confirm-transfer').on('click', function () {
                      $('#confirm-transfer-block').css('display', 'none');
                    });

                    @if (!$deal->seller->google_fa)
                    $('#re-send-confirmation-code').on('click', function () {
                      $.get('/send-confirmation-code/{{ $deal->id }}').done(function (response) {
                        if (response.status === true) {
                          alert(response.message);
                        } else {
                          alert('Error.');
                        }
                      }).fail(function (err) {
                        alert(err.statusText);
                      });
                    });
                    @endif

                    $('#confirm-and-close').on('click', function () {
                      $(this).prop('disabled', true);
                      updateStatus('completed', this);
                    });

                    $('#seller-confirm-deal').on('click', function () {
                      @if (!$deal->seller->google_fa)
                      $.get('/send-confirmation-code/{{ $deal->id }}');
                      @endif
                      $('#seller-confirmation-block').css('display', '');
                    });

                    let messages = [];

                    function render(message) {
                      $('#message-list').prepend('<div class="message ' + (message.from_me ? 'message_your' : 'message_interlocator') + ' clearfix online-messages__message">\n' +
                        '<div class="message__first-col">\n' +
                        '<div class="message__avatar"><a href="#"><img src="' + message.user.image + '" alt=""></a></div>\n' +
                        '</div>\n' +
                        '<div class="message__second-col">\n' +
                        '<div class="message__information">\n' +
                        '<div class="message__author-name">' + message.user.login + '</div>\n' +
                        '<div class="message__datetime">' + message.created_at + '</div>\n' +
                        '</div>\n' +
                        '<div class="message__text">' + message.text + '</div>\n' +
                        '</div>\n' +
                        '</div>')
                    }

                    setInterval(function () {
                      $.get('/messages/{{ $deal->id }}').done(function (response) {

                        if (response.data.length !== 0) {
                          if (messages.length < response.data.length) {
                            response.data.forEach(function (message) {
                              if (messages.filter(function (element) {
                                return message.id === element.id;
                              }).length === 0) {
                                messages.push(message);
                                render(message);
                              }
                            });
                          }
                        }
                      });
                    }, 1000);

                    $('#chat').on('submit', 'form', function (event) {
                      $('.send-message__form-submit').attr('disabled', true);
                      event.preventDefault();

                      let form = $(this);
                      $.post('/messages', form.serialize()).done(function (response) {
                        form.find('[name="text"]').val('');
                        $('.send-message__form-submit').attr('disabled', false);
                        $("#message-input").val("");
                      }).fail(function (response) {
                        $('.send-message__form-submit').attr('disabled', false);
                        $("#message-input").val("");
                      });
                    });
                  });
                </script>
                @endsection