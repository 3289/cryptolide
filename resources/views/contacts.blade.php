@title('Контакты')

@className('main_infopage')

@extends('layouts.app')

@section('content')
	<div class="content">
	<div class="infopage infopage_contacts">
		<div class="infopage__title-wrap">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<h1 class="infopage-title">С радостью ответим на ваши вопросы!</h1>
						<div class="infopage__note">Работаем для вас 24 / 7 без выходных и праздников.</div>
					</div>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row">
				<div class="infopage__content">
					<div class="contacts">
						<div class="contacts__left">
							<div class="lastnews">
								<div class="lastnews__heading infopage-block corner-bottom">
									<h2 class="lastnews__title">Последние новости</h2>
								</div>
								<div class="lastnews__content infopage-block corner-top">
									<div class="lastnews__item">
										<a class="twitter-timeline" data-tweet-limit="2" data-width="220" data-chrome="noheader,nofooter" data-height="220" data-theme="light" data-link-color="#2B7BB9" href="https://twitter.com/CRYPTOLIDEcom"></a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
									</div>									
								</div>
							</div>
						</div>
						<div class="contacts__right">
							<div class="communication-ways infopage-block">
								<h2 class="communication-ways__title">Способы получения ответа</h2>
								<div class="communication-ways__note">Перед тем, как задать вопрос - посмотрите нет ли на него ответа в последних новостях, <br>которые размещены левее, или в нашей <a href="#">базе знаний</a>, где собраны часто задаваемые вопросы.</div>
								<ul class="communication-ways__contacts">
									<li class="communication-ways__contacts-item communication-ways__contacts-item_knowledge-base">
										<a href="/help">
											<span class="communication-ways__contacts-inner">Найти ответ на вопрос в базе знаний</span>
										</a>
									</li>
									<li class="communication-ways__contacts-item communication-ways__contacts-item_online-consultant">
										<a style="cursor: pointer;" onclick="Chatra('openChat', true)">
											<span class="communication-ways__contacts-inner"  >Получить быстрый ответ от одного из <br>квалифицированных онлайн консультантов</span>
										</a>
									</li>
									<li class="communication-ways__contacts-item communication-ways__contacts-item_email communication-ways__contacts-item_email2">
										<div class="communication-ways__contact-icon">
											<img src="images/icons/icon_email_contacts.png" alt="E-mail">
										</div>
										<div class="communication-ways__contact-data">
											info@cryptolide.com
											<div class="communication-ways__contact-type">По общим вопросам</div>
										</div>
									</li>
									<li class="communication-ways__contacts-item communication-ways__contacts-item_email communication-ways__contacts-item_email2">
										<div class="communication-ways__contact-icon">
											<img src="images/icons/icon_email_contacts.png" alt="E-mail">
										</div>
										<div class="communication-ways__contact-data">
											admin@cryptolide.com
											<div class="communication-ways__contact-type">Связь с руководством</div>
										</div>
									</li>
									<!-- <li class="communication-ways__contacts-item communication-ways__contacts-item_viber">
                                        <div class="communication-ways__contact-icon">
                                            <img src="images/icons/icon_viber_contacts.png" alt="Viber">
                                        </div>
                                        <div class="communication-ways__contact-data">+48790876524</div>
                                        <div class="communication-ways__contact-signature">Viber</div>
                                    </li>
                                    <li class="communication-ways__contacts-item communication-ways__contacts-item_vk">
                                        <div class="communication-ways__contact-icon">
                                            <img src="images/icons/icon_vk_contacts.png" alt="Vkontakte">
                                        </div>
                                        <div class="communication-ways__contact-data"><span class="communication-ways__site-url">vk.com</span>/cryptolide_help</div>
                                        <div class="communication-ways__contact-signature">Vkontakte</div>
                                    </li>
                                    <li class="communication-ways__contacts-item communication-ways__contacts-item_whatsapp">
                                        <div class="communication-ways__contact-icon">
                                            <img src="images/icons/icon_whatsapp_contacts.png" alt="WhatsApp">
                                        </div>
                                        <div class="communication-ways__contact-data">+48790876524</div>
                                        <div class="communication-ways__contact-signature">WhatsApp</div>
                                    </li>
                                    <li class="communication-ways__contacts-item communication-ways__contacts-item_email">
                                        <div class="communication-ways__contact-icon">
                                            <img src="images/icons/icon_email_contacts.png" alt="E-mail">
                                        </div>
                                        <div class="communication-ways__contact-data">info@cryptolide.com</div>
                                        <div class="communication-ways__contact-signature">E-mail</div>
                                    </li>
                                    <li class="communication-ways__contacts-item communication-ways__contacts-item_skype">
                                        <div class="communication-ways__contact-icon">
                                            <img src="images/icons/icon_skype_contacts.png" alt="Skype">
                                        </div>
                                        <div class="communication-ways__contact-data">live:cryptolide</div>
                                        <div class="communication-ways__contact-signature">Skype</div>
                                    </li> -->
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
