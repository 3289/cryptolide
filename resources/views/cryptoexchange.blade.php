@extends('layouts.app')

@title('Обменять криптовалюту')

@section('content')
	<div class="content">
		<div class="operation-menu-container">
			<div class="container">
				<div class="row">
					<ul class="operation-menu">
						<li class="operation-menu__item operation-menu__item_buy"><a href="/p2p?crypto_id=1&sum_currency=&currency_id=155&payment_type_id=&country_id=206&city_id=" class="operation-menu__link">Купить криптовалюту</a></li>
						<li class="operation-menu__item operation-menu__item_sell"><a href="/cryptosell?crypto_id=1&sum_currency=&currency_id=155&payment_type_id=&country_id=206&city_id=" class="operation-menu__link">Продать криптовалюту</a></li>
						<li class="operation-menu__item operation-menu__item_exchange operation-menu__item_active"><a href="/cryptoexchange?crypto_trade_id=3&sum_currency=&crypto_id=1" class="operation-menu__link">Обменять криптовалюту на другую</a></li>
						<li class="operation-menu__item operation-menu__item_create-ad">
							@if (!auth()->check())
								<a href="#" class="operation-menu__link" data-toggle="modal" data-target="#modal-error">Создать объявление</a>
							@else
								<a href="/publications/create" class="operation-menu__link">Создать объявление</a>
							@endif
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="search-section">
			<div class="container">
				<div class="row">
					<form action="" class="search-form search-form_exchange">

						<div class="search-form__item search-form__item_crypto-select">
							<label for="cryptocurrency-select" class="search-form__label">Отдаете:</label>
							<select name="crypto_trade_id" id="payment-system" class="search-form__select jqstyler">
								@foreach($crypto_trade_currencies as $crypto_trade)
									<option data-short="{{ $crypto_trade->short }}" value="{{ $crypto_trade->id }}"{{ request('crypto_trade_id') == $crypto_trade->id ? ' selected' : '' }}>{{ $crypto_trade->name }}</option>
								@endforeach
							</select>
						</div>
						<div class="search-form__item search-form__item_sum-exchange">
							<input type="text" name="sum_currency" class="search-form__text" style="width: 213px;" placeholder="Сумма: 0.00000000" value="{{ request('sum_currency') }}" autocomplete="off">
							<span class="search-form__suffix" id="crypto-amount-short">{{ $cryptoTrade->short }}</span>
						</div>
						
						<div class="search-form__item search-form__item_payment-system-exchange">
							<label for="payment-system" class="search-form__label">Получаете:</label>
							<select name="crypto_id" id="payment-system" class="search-form__select jqstyler">
								@foreach($crypto_currencies as $crypto_currency)
									@if ($crypto_currency->name == 'Bitcoin')
                    					<option value="{{ $crypto_currency->id }}"{{ request('crypto_id') == $crypto_currency->id ? ' selected' : '' }}>{{ $crypto_currency->name }}</option>
	                				@endif
								@endforeach
							</select>
						</div>
						
						<div class="search-form__actions">
							<input type="submit" class="search-form__submit">
						</div>
					</form>
				</div>
			</div>
		</div>

		<div class="action-tables">
			<div class="container">
				<div class="row">
					@include('p2p.trade', ['adverts' => $adverts])
				</div>
			</div>
		</div>
	</div>
@endsection

@section('scripts')
	<script>
        $('.jqstyler').styler({
            onFormStyled: function () {
                $('.jq-selectbox').each(function (index, el) {
                    var bgImg = $(this).find('.jq-selectbox__dropdown ul li.selected').css('background-image');
                    $(this).find('.jq-selectbox__select-text').css('background-image', bgImg);
                });
            },
            onSelectClosed: function () {
                var bgImg = $(this).find('.jq-selectbox__dropdown ul li.selected').css('background-image');
                $(this).find('.jq-selectbox__select-text').css('background-image', bgImg);
            }
        });
	</script>

	<script>
		$(document).ready(function () {
            var urlParams = new URLSearchParams(window.location.search);
			if (!urlParams.has('crypto_id')) {
                $('input.search-form__submit').trigger('click');
			}
        });
	</script>

	<script>
        $('[name="sum_currency"]').on('keypress', function(e) {
            let th = $(this);
            setTimeout(function () {
                th.val(th.val().replace(',', '.'))
            }, 5);
            if(e.ctrlKey && k == 86) {
                return false;
            }

            if (e.shiftKey === true ) {
                if (e.which == 44 || e.which == 9) {
                    if (th.val().indexOf('.') != -1) {
                        return false;
                    }
                    return true;
                }
            }
            if (e.which == 46 || e.which == 44) {
                if (th.val().indexOf('.') != -1) {
                    return false;
                }
                return true;
            }
            if (e.which > 57 || e.which < 48) {
                return false;
            }
            return true;
        });

        $('[name="sum_currency"]').on('keypress', function(e) {
            let th = $(this);
            setTimeout(function () {
                th.val(th.val().replace(',', '.'));
            }, 5);
        });

        $('[name="sum_currency"]').on('paste', function(event){
            event.preventDefault();
        });
	</script>
@endsection