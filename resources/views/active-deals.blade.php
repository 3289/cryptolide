@extends('layouts.app')

@title('Бизнес профиль пользователя ' . $user->login)

@className('main_business-profile')

@section('content')
<div class="content">
  <div class="title-wrap title-wrap_business-profile">
    <div class="container">
      <div class="row">
        <h1 class="page-title title-wrap__title">Бизнес профиль пользователя <span style="color: #59ba41;">{{ $user->login }}</span></h1>
        <a href="/" class="btn-back title-wrap__back">назад</a>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <div class="user-profile user-profile_business">
          <div class="user-profile__photo"><img src="{{ $user->image }}" alt=""></div>
          <div class="user-profile__info user-profile__info_{{ $user->is_online ? 'online' : 'offline' }}">
            @if ($user->show_name)
            <div class="user-profile__realname"><span class="user-profile__realname-inner">{{ $user->last_name . ' ' . $user->name . ' ' . $user->middle_name }}</span></div>
            @endif
            <a href="#" class="user-profile__name">{{ $user->login }}</a>
            <div class="user-profile__rating">{{ $user->rating }}</div>                          

            @foreach($user->achievements as $achievement)
            <img src="{{ $achievement->image }}" alt="" class="user-profile__ok">
            @endforeach
          </div>
        </div>
        @if (!auth()->check())
        <div class="get-gift get-gift_business">
          <a href="/?r={{ $user->id }}" class="btn-green2 btn-green2_get-gift">Получить подарок от {{ $user->login }}</a>
        </div>
        @endif
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <ul class="settings-tabs settings-tabs_business-profile settings-tabs_userinfo">
          <li class="settings-tabs__item"><a href="/profile/{{ $user->login }}" class="settings-tabs__link">ИНФОРМАЦИЯ О ПОЛЬЗОВАТЕЛЕ</a></li>
          <li class="settings-tabs__item settings-tabs__item_active"><a href="/profile/{{ $user->login }}/adverts" class="settings-tabs__link">АКТИВНЫЕ ОБЪЯВЛЕНИЯ ПОЛЬЗОВАТЕЛЯ <span class="deals-number">{{ $user->adverts()->where('status', 'active')->count() }}</span></a></li>
        </ul>
        @forelse($catalog as $name => $adverts)
        <div class="action-table-wrapper action-table-wrapper_business-profile">
          <h2 class="action-table-wrapper__title">
            {{ $name }}
          </h2>
          <div class="table-responsive">
            <table class="action-table">
              <thead class="action-table__thead">
                <tr class="action-table__tr">
                  <td class="action-table__seller">Продавец</td>
                  <td class="action-table__buy-with">
                    {{ explode(' ', $name)[0] }} BTC с помощью
                  </td>
                  <td class="action-table__price">Цена за 1 BTC</td>
                  <td class="action-table__sum">Мин. - макс. сумма</td>
                  <td class="action-table__action-type">Тип действия</td>
                </tr>
              </thead>
              <tbody class="action-table__tbody">
                @foreach($adverts as $advert)
                <tr class="action-table__tr">
                  <td class="action-table__seller">
                    <div class="action-table__seller-photo"><img src="{{ $advert->user->image }}" alt=""></div>
                    <div class="action-table__seller-info action-table__seller-info_{{ $advert->user->is_online ? 'online' : 'offline' }}">
                      <div class="action-table__seller-info-block_left">
                        <a href="/profile/{{ $advert->user->login }}" class="action-table__seller-name">{{ $advert->user->login }}</a>
                        <div class="action-table__seller-status" data-toggle="tooltip" title="{{ $advert->user->is_online ? 'Сейчас онлайн' : 'Заходил ' . $advert->user->last_activity_format }}"></div>
                      </div>

                      <div class="action-table__seller-rating">
                        {{ $advert->user->rating }}
                        <span class="action-table__seller-icons">
                          @if ($advert->user->verified)
                          <span data-toggle="tooltip" title="Пройдена полная верификация личности">
                            <img src="/images/icons/ok.png" alt="OK">
                          </span>
                          @endif

                          @foreach($user->achievements as $achievement)
                          <span data-toggle="tooltip" title="Верифицирован телефонный номер">
                            <img src="{{ $achievement->image }}" alt="" class="user-profile__ok">
                          </span>
                          @endforeach
                        </span>
                      </div>
                    </div>
                  </td>
                  <td class="action-table__buy-with">
                    <a href="#" class="action-table__link">
                      @if ($advert->type == 'sell_online' || $advert->type == 'buy_online')
                      {{ $advert->payment_type_id == 2 ? $advert->banks : '' }}
                      @endif

                      @if ($advert->type == 'sell_for_cash' || $advert->type == 'buy_for_cash')
                      {{ $advert->city->name }}
                      @endif

                      @if ($advert->type == 'trade')
                      {{ $advert->currency_name }}
                      @endif
                    </a>
                  </td>
                  <td class="action-table__price">{{ $advert->rate() }}</td>
                  <td class="action-table__sum">{{ $advert->minCurrency() }} - {{ $advert->sumCurrency() }} {{ $advert->currency_name }}</td>
                  <td class="action-table__action-type">
                    <a href="/publications/{{ $advert->id }}" class="btn-lime">
                      @if ($advert->short_type == 'buy')
                      Продать
                      @elseif ($advert->short_type == 'sell')
                      Купить
                      @elseif ($advert->short_type == 'trade')
                      Обменять
                      @endif
                    </a>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
        @empty
        <div class="action-table-wrapper">
          <table class="action-table">
            <tbody class="action-table__tbody">
              <tr class="action-table__empty">
                <td colspan="5">Объявления отсутствуют.</td>
              </tr>
            </tbody>
          </table>
        </div>
        @endforelse

      </div>
    </div>
  </div>
</div>
@endsection

@section('scripts')
<script>
  $('.jqstyler').styler({
    onFormStyled: function () {
      $('.jq-selectbox').each(function (index, el) {
        var bgImg = $(this).find('.jq-selectbox__dropdown ul li.selected').css('background-image');
        $(this).find('.jq-selectbox__select-text').css('background-image', bgImg);
      });
    },
    onSelectClosed: function () {
      var bgImg = $(this).find('.jq-selectbox__dropdown ul li.selected').css('background-image');
      $(this).find('.jq-selectbox__select-text').css('background-image', bgImg);
    }
  });
</script>
@endsection