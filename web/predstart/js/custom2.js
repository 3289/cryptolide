$(function () {
	
	// Opening and closing news
	$('.news__title').on('click', function(event) {
		$(this).parents('.news__item').siblings().removeClass('news__item_open').find('.news__text').slideUp();
		$(this).parents('.news__item').toggleClass('news__item_open').find('.news__text').slideToggle();
	});

	// Opening and closing answers
	$('.faq__question').on('click', function(event) {
		$(this).parents('.faq__item').siblings().removeClass('faq__item_open').find('.faq__answer').slideUp();
		$(this).parents('.faq__item').toggleClass('faq__item_open').find('.faq__answer').slideToggle();
	});

	// OWL Carousel
	if ($('.owl-carousel').length) {
		$('.owl-carousel').owlCarousel({
			items: 3,
			margin: 40,
			nav: true,
			navText: ["<img src='images/icons/slider_prev.png'>","<img src='images/icons/slider_next.png'>"],
			dots: false,
			loop: true,
			autoplay: true,

			responsive : {
				0 : {
					items: 1
				},
				480 : {
					items: 2
				},
				768 : {
					items: 3
				}
			}
		});
	}

	// User notifications
	$('.action-icons__link').on('click', function(event) {
		event.preventDefault();
		$(this).parent().siblings().find('.user-notifications').fadeOut(100);
		$(this).siblings('.user-notifications').fadeToggle(100);
	});

	$(document).mouseup(function (e) {
		var container = $(".user-notifications");
		if (container.has(e.target).length === 0 && !$(e.target).parent().hasClass('action-icons__link')){
			container.fadeOut(100);
		}
	});

	// Ad complaint
	$('.complain-about-ad__link').on('click', function(event) {
		event.preventDefault();
		$('.ad-complaint').slideToggle();
	});

	// Cancel deal
	$('.cancel-deal__link').on('click', function(event) {
		event.preventDefault();
		$(this).parent().fadeOut().siblings('.transaction-state').fadeIn().siblings('.information-block__confirm-payment').fadeOut();
	});

	// Don't cancel deal
	$('.transaction-state__dont-cancel').on('click', function(event) {
		$(this).parents('.transaction-state').fadeOut().siblings('.information-block__confirm-payment, .information-block__cancel-deal').fadeIn();
	});

	$('.invitation-code__form-submit').on('click', function(event) {
		event.preventDefault();
		$('.invitation-code__form-error').fadeIn('slow');
	});

});