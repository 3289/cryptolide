'use strict';
//dictionary - array of object with field name,area & region
let dictionary = [
    {
        name: 'Бакуты',
        area: 'Гадячский район',
        region: 'Полтавская область'
    },
    {
        name: 'Балясное',
        area: 'Гадячский район',
        region: 'Полтавская область'
    },
    {
        name: 'Великое',
        area: 'Гадячский район',
        region: 'Полтавская область'
    },
    {
        name: 'Веселое',
        area: 'Гадячский район',
        region: 'Полтавская область'
    },
    {
        name: 'Гадяч',
        area: 'Гадячский район',
        region: 'Полтавская область'
    },
    {
        name: 'Гречановка',
        area: 'Гадячский район',
        region: 'Полтавская область'
    },
    {
        name: 'Анновка',
        area: 'Глобинский район',
        region: 'Полтавская область'
    },
    {
        name: 'Багны',
        area: 'Глобинский район',
        region: 'Полтавская область'
    },
    {
        name: 'Васьковка',
        area: 'Глобинский район',
        region: 'Полтавская область'
    },
    {
        name: 'Глубокое',
        area: 'Гадячский район',
        region: 'Полтавская область'
    },
    {
        name: 'Демченки',
        area: 'Гадячский район',
        region: 'Полтавская область'
    },
    {
        name: 'Алексеевка',
        area: 'Гребёнковский район',
        region: 'Полтавская область'
    },
    {
        name: 'Высокое',
        area: 'Гребёнковский район',
        region: 'Полтавская область'
    },
    {
        name: 'Горбы',
        area: 'Гребёнковский район',
        region: 'Полтавская область'
    },
    {
        name: 'Гулаковка',
        area: 'Гребёнковский район',
        region: 'Полтавская область'
    },
    {
        name: 'Грушковка',
        area: 'Гребёнковский район',
        region: 'Полтавская область'
    },
    {
        name: 'Архиповка',
        area: 'Лохвицкий  район',
        region: 'Полтавская область'
    },
    {
        name: 'Архиповка',
        area: 'Лохвицкий  район',
        region: 'Полтавская область'
    },
    {
        name: 'Бербеницы',
        area: 'Лохвицкий  район',
        region: 'Полтавская область'
    },
    {
        name: 'Воля',
        area: 'Лохвицкий  район',
        region: 'Полтавская область'
    },
    {
        name: 'Вишневое',
        area: 'Валковский  район',
        region: 'Полтавская область'
    },
    {
        name: 'Бараново',
        area: 'Валковский  район',
        region: 'Полтавская область'
    },
    {
        name: 'Гвоздево',
        area: 'Валковский  район',
        region: 'Полтавская область'
    },
    {
        name: 'Войтенки',
        area: 'Валковский  район',
        region: 'Полтавская область'
    },
    {
        name: 'Журавли',
        area: 'Валковский  район',
        region: 'Полтавская область'
    },
    {
        name: 'Буда-Орловецкая',
        area: 'Городищенский  район',
        region: 'Полтавская область'
    },
    {
        name: 'Вербовка ',
        area: 'Городищенский район',
        region: 'Черкасская область'
    },
    {
        name: 'Журавка ',
        area: 'Городищенский район',
        region: 'Черкасская область'
    },
    {
        name: 'Млиев',
        area: 'Городищенский район',
        region: 'Черкасская область'
    },
    {
        name: 'Богачовка',
        area: 'Звенигородский район',
        region: 'Черкасская область'
    },
    {
        name: 'Будище',
        area: 'Звенигородский район',
        region: 'Черкасская область'
    },
    {
        name: 'Баштечки',
        area: 'Жашковский район',
        region: 'Черкасская область'
    },
    {
        name: 'Бузовка',
        area: 'Жашковский район',
        region: 'Черкасская область'
    },
    {
        name: 'Вороное',
        area: 'Жашковский район',
        region: 'Черкасская область'
    },
    {
        name: 'Бабаковка',
        area: 'Белопольский район',
        region: 'Сумская  область'
    },
    {
        name: 'Бакша',
        area: 'Белопольский район',
        region: 'Сумская  область'
    },
    {
        name: 'Аркавское',
        area: 'Белопольский район',
        region: 'Сумская  область'
    },
    {
        name: 'Алексенки',
        area: 'Белопольский район',
        region: 'Сумская  область'
    },
    {
        name: 'Василец',
        area: 'Ямпольский район',
        region: 'Сумская  область'
    },
    {
        name: 'Белица',
        area: 'Ямпольский район',
        region: 'Сумская  область'
    },
    {
        name: 'Говоруново',
        area: 'Ямпольский район',
        region: 'Сумская  область'
    },
    {
        name: 'Диброва',
        area: 'Ямпольский район',
        region: 'Сумская  область'
    },
    {
        name: 'Алтыновка',
        area: 'Кролевецкий район',
        region: 'Сумская  область'
    },
    {
        name: 'Боцманов',
        area: 'Кролевецкий район',
        region: 'Сумская  область'
    },
    {
        name: 'Воргол',
        area: 'Кролевецкий район',
        region: 'Сумская  область'
    },
    {
        name: 'Грузское',
        area: 'Кролевецкий район',
        region: 'Сумская  область'
    },
];
// find all <li> in ul.search__list & add event listener
function addEvListToLiT() {
    let listOfLiTag = document.getElementById('search__list').children;
    if (listOfLiTag.length > 0) {
        for (let i = 0; i < listOfLiTag.length; i++) {
            listOfLiTag[i].addEventListener('click', function () {
                // fix issue with represent result & dictionary
                if (this.firstChild.innerHTML !== undefined) {
                    if (this.classList.value.indexOf('error') == -1) {
                        let input = document.getElementById('search__input');
                        input.value = this.firstChild.textContent;
                    }
                }
                else {
                    if (this.classList.value.indexOf('error') == -1) {
                        let input = document.getElementById('search__input');
                        input.value = this.innerHTML;
                    }
                }
                let list = document.getElementById('search__list');
                list.style.display = 'none';
            });
        }
    }

}

function searchTown() {
    let list = document.getElementById('search__list');
    list.style.display = 'block';
    let text = document.getElementById('search__input').value;
    let result = [];
    let ul = document.getElementById('search__list');
    // render default dictionary,if search string is empty
    if (text == "") {
        insertFromListT(dictionary, ul);
        return;
    }
    //call compare func with all obj in dictionary
    for (let i = 0; i < dictionary.length; i++) {
        result.push({
            'city': dictionary[i],
            'value': compareNames(dictionary[i].name, text)
        });
    }
    //sort result array for max to min correlation coefficient
    result = result.sort(function (a, b) {
        return -(a.value - b.value);
    });
    //call render func
    insertFromListT(result, ul);
}

//render func - take dictionary & ul
function insertFromListT(list, elem) {
    while (elem.lastChild) {
        elem.removeChild(elem.lastChild);
    }
    for (let i = 0; i < list.length; i++) {
        let el = document.createElement('li');
        // fix issue with represent result & dictionary
        if (list[i].city === undefined) {
            el.innerHTML = list[i].name;
            elem.classList.remove('highest');
        }
        else {
            if (list[i].value < 0.1) {
                continue;
            }
            let text = document.getElementById('search__input').value,
                nameFill = '',
                areaFill = '';
            for (let j = 0; j < text.length; j++) {
                try {
                    if (text[j].toLowerCase() == list[i].city.name[j].toLowerCase()) {
                        nameFill += text[j];
                    }

                    else {
                        break;
                    }
                }
                catch (er) {
                }
            }
            for (let j = 0; j < text.length; j++) {
                try {

                    if (text[j].toLowerCase() == list[i].city.area[j].toLowerCase()) {
                        areaFill += text[j];
                    }
                    else {
                        break;
                    }
                }
                catch (er) {
                }
            }
            // add class to grow <li> & render result obj field to <p>
            elem.classList.add('highest');
            let name = document.createElement('p');
            let b = document.createElement('b');
            b.innerHTML = list[i].city.name.slice(0, nameFill.length);
            name.appendChild(b);
            name.innerHTML += list[i].city.name.slice(nameFill.length);
            el.appendChild(name);
            let area = document.createElement('p');
            let secondBTag = document.createElement('b');
            secondBTag.innerHTML = list[i].city.name.slice(0, areaFill.length);
            area.appendChild(secondBTag);
            area.innerHTML += list[i].city.area.slice(areaFill.length);
            el.appendChild(area);
            let region = document.createElement('p');
            region.innerHTML = list[i].city.region;
            el.appendChild(region);
        }
        elem.appendChild(el);
    }
    if (!elem.lastChild) {
        let el = document.createElement('li');
        el.classList.add('error');
        el.innerHTML = 'Совпадений не найдено.';
        elem.appendChild(el);
    }
    //call func to add event listener to new <li> list
    addEvListToLiT();
}

//func what choose all pair of string and create a correlation coefficient
function compareNames(s1, s2) {
    let longer = s1,
        shorter = s2;
    if (s1.length < s2.length) {
        longer = s2;
        shorter = s1;
    }
    if (longer.length == 0) {
        return 1.0;
    }
    return (longer.length - _editDistance(longer, shorter)) / parseFloat(longer.length);
}

//calc correlation coefficient
function _editDistance(s1, s2) {
    s1 = s1.toLowerCase();
    s2 = s2.toLowerCase();
    let costs = [];
    for (let i = 0; i <= s1.length; i++) {
        let lastValue = i;
        for (let j = 0; j <= s2.length; j++) {
            if (!i) {
                costs[j] = j;
                continue;
            }
            if (j > 0) {
                let newValue = costs[j - 1];
                if (s1[i - 1] != s2[j - 1]) {
                    newValue = Math.min(Math.min(newValue, lastValue), costs[j]) + 1;
                }
                costs[j - 1] = lastValue;
                lastValue = newValue;
            }
        }
        if (i > 0) {
            costs[s2.length] = lastValue;
        }
    }
    s2.split('').forEach((el, i) => {
        if (el == s1[i]) {
            costs[s2.length] -= 2;
        }
        else {
            costs[s2.length] += 2;
        }
    });
    return costs[s2.length];
}