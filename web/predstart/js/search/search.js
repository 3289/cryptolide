'use strict';
//dictionary - array of object with field name,area & region
window.onload = function () {
    //add show/hide to ul
    $('html body').click(function (e) {
        console.log(e);
        if (!(e.target.localName == 'input' || e.target.className == 'select-ul' || e.target.className == 'select-ul__li' ||  e.target.className == 'arrow down' || e.target.className == 'arrow-block')) {
            $('.select-wrapper ul').hide();

            if (e.target.className !== 'select-ul__li--prevent') {
                $('.multi__list').hide();
            }
        }
        if(e.target.innerText == 'Все банки страны' && e.target.classList.value == '') {
            let ul = e.target.parentElement.parentElement.parentElement.children[1];
            ul.style.display = ul.style.display !== 'none' ? 'none' : 'block';
            $('.select-wrapper ul').hide();
        }
    });
    $('.arrow-block').click(function (e) {
        let list = e.target.parentElement.parentElement.children[1];
        if (list !== undefined && list.classList.value.indexOf('arrow-block') == -1) {
            list.style.display = list.style.display !== 'none' ? 'none' : 'block';
            $('.multi__list').hide();
        }
    });
    $('.arrow').click(function (e) {
        let list = e.target.parentElement.parentElement.parentElement.children[1];
        list.style.display = list.style.display !== 'none' ? 'none' : 'block';
        $('.multi__list').hide();
    });
    $('.search__input').click(function (e) {
        $('.select-wrapper ul').hide();
        $('.multi__list').hide();
        let list = e.target.parentElement.parentElement.children[1];
        list.style.display = 'block';
    });
    $('.search__list').each(function (i, el) {
        addEvListToLi(el);
    });
    $('#arrow-block').click(function (e) {
        let list = e.target.parentElement.parentElement.children[1];
        list.style.display = list.style.display !== 'none' ? 'none' : 'block';
    });
    $('.select-ul').click(function (e) {
        let list = e.currentTarget.parentElement.children[1];
        list.style.display = list.style.display !== 'none' ? 'none' : 'block';
        $('.select-wrapper ul').hide();
    });

    $('.select-ul .select-ul__li--input input').keydown(function (e) {
        if (e.keyCode == 8 && e.target.parentElement.parentElement.children.length > 1 && e.target.value == '') {
            e.target.parentElement.parentElement.children[0].remove();
        }
        disableChecked(e.target.parentElement.parentElement, e.target.parentElement.parentElement.parentElement.children[1]);
    });
    $('.multi__list li').click(function (e) {
        if (e.target.className == 'grey') {
            return;
        }
        let list = e.target.parentElement.parentElement.children[0];
        let li = document.createElement('li');
        let p = document.createElement('p');
        p.innerHTML = e.target.innerText;
        let img = document.createElement('img');
        img.src = 'img/exit.png';
        li.appendChild(p);

        if (list.children[0].className == 'select-ul__li--prevent') {
            list.children[0].remove();
            list.innerHTML = '<li class="select-ul__li--input"><input type="text" class="multi__input form-create-ad__text" placeholder="." oninput="multiSearch(0)"></li>';
            $('.select-ul .select-ul__li--input input').keydown(function (e) {
                if (e.keyCode == 8 && e.target.parentElement.parentElement.children.length > 1 && e.target.value == '') {
                    e.target.parentElement.parentElement.children[0].remove();
                }
                disableChecked(e.target.parentElement.parentElement, e.target.parentElement.parentElement.parentElement.children[1]);
            });
        }
        if (p.innerText !== 'Все банки страны') {
            li.className = "select-ul__li";
            li.appendChild(img);
        }
        else {
            li.className = "select-ul__li--prevent";
            list.innerHTML = '';
                /*'<li class="select-ul__li--input"><input type="text" class="multi__input form-create-ad__text" placeholder="." oninput="multiSearch(0)"></li>';
            $('.select-ul .select-ul__li--input input').keydown(function (e) {
                if (e.keyCode == 8 && e.target.parentElement.parentElement.children.length > 1 && e.target.value == '') {
                    e.target.parentElement.parentElement.children[0].remove();
                }
                disableChecked(e.target.parentElement.parentElement, e.target.parentElement.parentElement.parentElement.children[1]);
            });*/
        }
        if (list.children.length > 3) {
            return;
        }
        li.style.order = e.target.parentElement.parentElement.children[0].children.length;
        li.after(e.target.parentElement.parentElement.children[0], null);
        let check = false;
        for (let i = 0; i < list.children.length; i++) {
            if (list.children[i].innerText == p.innerText) {
                check = true;
            }
        }
        if (!check && p.innerText !== 'Совпадений не найдено.') {
            list.prepend(li);
        }
        addAfterEvent();
        let child = e.target.parentElement.children;
        disableChecked(list, child);
        let input = list.children[list.children.length - 1].children[0];
        setTimeout(_ => {
            if (li.className !== 'select-ul__li--prevent') {
                input.focus();
                input.click();
            }
        }, 5);
    });
    document.getElementById('search__input').addEventListener('click', function () {
        let list = document.getElementById('search__list');
        list.style.display = 'block';
        $('.multi__list').hide();
    });
    let ul = document.getElementById('search__list');
    insertFromListT(dictionary, ul);
    addEvListToLiT();
};

function disableChecked(list, child) {
    let activeList = [];
    for (let i = 0; i < list.children.length; i++) {
        if (list.children[i].children[0]) {
            if (list.children[i].children[0].nodeName !== 'INPUT') {
                activeList.push(list.children[i].children[0].innerText);
            }
        }
    }
    if (activeList.length < 1) {
        for (let i = 0; i < child.children.length; i++) {
            child.children[i].className = '';
        }
        return;
    }
    for (let i = 0; i < child.length; i++) {
        if (child[i].className !== 'select-ul__li--input') {
            child[i].className = '';
        }
        for (let j = 0; j < activeList.length; j++) {
            if (child[i].innerHTML == activeList[j]) {
                child[i].className = 'grey';
            }
        }
    }
}

function addAfterEvent() {
    $('.select-ul__li img').click(function (e) {
        let chooseText = e.currentTarget.parentElement.innerText;
        chooseText = chooseText.split('');
        chooseText.pop();
        chooseText = chooseText.join('');
        let list = e.currentTarget.parentElement.parentElement.parentElement.children[1];
        for (let i = 0; i < list.children.length; i++) {
            if (list.children[i].innerText.toString() == chooseText.toString()) {
                list.children[i].classList.value = '';
            }
            if (list.children[i].innerText.toString() == 'Все банки страны') {
                list.children[i].classList.value = '';
            }
        }
        e.currentTarget.parentElement.remove();
    });
}

function multiSearch(i) {
    let ul = document.getElementsByClassName('multi__list')[i];
    if (ul === undefined) {
        return;
    }
    ul.style.display = 'block';
    let input = document.getElementsByClassName('multi__input')[i];
    let text = input.value;
    let result = [];
    let dictionary = [];
    for (let j = 0; j < ul.children.length; j++) {
        if (ul.children[j].innerText !== 'Совпадений не найдено.') {
            dictionary.push({name: ul.children[j].innerText});
        }
    }

    if (text == "") {
        for (let i = 0; i < dictionary.length; i++) {
            if (dictionary[i].name == 'Все банки страны') {
                dictionary.splice(i, 1);
                i--;
            }
        }
        dictionary.unshift({name: 'Все банки страны'});
        insertFromListM(dictionary, ul, i);
        return;
    }
    //call compare func with all obj in dictionary
    for (let i = 0; i < dictionary.length; i++) {
        if (dictionary[i].name !== 'Все банки страны')
            result.push({
                'city': dictionary[i],
                'value': compareNames(dictionary[i].name, text)
            });
    }
    //sort result array for max to min correlation coefficient
    result = result.sort(function (a, b) {
        return -(a.value - b.value);
    });
    //call render func
    insertFromListM(result, ul, i);
}

function insertFromListM(list, elem, i) {
    while (elem.lastChild) {
        elem.removeChild(elem.lastChild);
    }
    for (let i = 0; i < list.length; i++) {
        let el = document.createElement('li');
        // fix issue with represent result & dictionary
        if (list[i].city === undefined) {
            el.innerHTML = list[i].name;
        }
        else {
            if (list[i].value < 0.2) {
            }
            let text = elem.parentElement.children[0].children[0].value,
                nameFill = '';
            for (let j = 0; j < text.length; j++) {
                try {
                    if (text[j].toLowerCase() == list[i].city.name[j].toLowerCase()) {
                        nameFill += text[j];
                    }
                    else {
                        break;
                    }
                }
                catch (er) {
                }
            }
            let name = document.createElement('p');
            let b = document.createElement('b');
            b.innerHTML = list[i].city.name.slice(0, nameFill.length);
            name.appendChild(b);
            name.innerHTML += list[i].city.name.slice(nameFill.length);
            if (list[i].value < 0.2) {
                el.style.display = 'none';
            }
            el.appendChild(name);
        }
        elem.appendChild(el);
    }
    let visibleLiCount = 0;
    for (let j = 0; j < elem.children.length; j++) {
        if (elem.children[j].style.display !== 'none') {
            visibleLiCount++;
        }
    }
    if (visibleLiCount == 0) {
        let el = document.createElement('li');
        el.classList.add('error');
        el.innerHTML = 'Совпадений не найдено.';
        elem.appendChild(el);
    }
    addEvListToLiM(elem, i);
}

function addEvListToLiM(el, i) {
    let list = el.children;
    for (let i = 0; i < list.length; i++) {
        list[i].onclick = function (e) {
            let list;

            if (e.target.className == 'grey') {
                return;
            }
            if (e.target.nodeName == 'P') {
                list = e.target.parentElement.parentElement.parentElement.children[0];
            }
            else {
                list = e.target.parentElement.parentElement.children[0];
            }

            let li = document.createElement('li');
            let p = document.createElement('p');
            p.innerHTML = e.target.innerText;

            let img = document.createElement('img');
            img.src = 'img/exit.png';
            li.appendChild(p);

            if (list.children[0].className == 'select-ul__li--prevent') {
                list.children[0].remove();
                list.innerHTML = '<li class="select-ul__li--input"><input type="text" class="multi__input form-create-ad__text" placeholder="." oninput="multiSearch(0)"></li>';
                $('.select-ul .select-ul__li--input input').keydown(function (e) {
                    if (e.keyCode == 8 && e.target.parentElement.parentElement.children.length > 1 && e.target.value == '') {
                        e.target.parentElement.parentElement.children[0].remove();
                    }
                    disableChecked(e.target.parentElement.parentElement, e.target.parentElement.parentElement.parentElement.children[1]);
                });
            }
            if (p.innerText !== 'Все банки страны') {
                li.className = "select-ul__li";
                li.appendChild(img);
            }
            else {
                li.className = "select-ul__li--prevent";
                list.innerHTML = '';/*'<li class="select-ul__li--input"><input type="text" class="multi__input form-create-ad__text" placeholder="." oninput="multiSearch(0)"></li>';
                $('.select-ul .select-ul__li--input input').keydown(function (e) {
                    if (e.keyCode == 8 && e.target.parentElement.parentElement.children.length > 1 && e.target.value == '') {
                        e.target.parentElement.parentElement.children[0].remove();
                    }
                    disableChecked(e.target.parentElement.parentElement, e.target.parentElement.parentElement.parentElement.children[1]);
                });*/
            }
            if (list.children.length > 3) {
                return;
            }
            li.style.order = e.target.parentElement.parentElement.children[0].children.length;
            li.after(e.target.parentElement.parentElement.children[0], null);
            let check = false;
            for (let i = 0; i < list.children.length; i++) {
                if (list.children[i].innerText == p.innerText) {
                    check = true;
                }
            }
            if (!check && p.innerText !== 'Совпадений не найдено.') {
                list.prepend(li);
            }
            addAfterEvent();
            let child = e.target.parentElement.children;
            disableChecked(list, child);
            let input = list.children[list.children.length - 1].children[0];
            input.value = '';
            setTimeout(_ => {
                if (li.className !== 'select-ul__li--prevent') {
                    input.focus();
                    input.click();
                }
            }, 5);
            multiSearch(i);
        }
    }
    disableChecked(el, el.parentElement.children[1]);
}

// find all <li> in ul.search__list & add event listener
function addEvListToLi(el) {
    let listOfLiTag = el.children;
    if (listOfLiTag.length > 0) {
        for (let i = 0; i < listOfLiTag.length; i++) {
            listOfLiTag[i].onclick = function (e) {
                if (e.target.classList.value.indexOf('error') == -1) {
                    e.target.parentElement.parentElement.children[0].children[0].value = e.target.innerText;
                    el.style.display = 'none';
                }
            };
            if (listOfLiTag[i].children.length > 0) {
                listOfLiTag[i].children[0].onclick = function (e) {
                    if (e.target.classList.value.indexOf('error') == -1) {
                        e.target.parentElement.parentElement.parentElement.children[0].children[0].value = e.target.innerText;
                        el.style.display = 'none';
                    }
                };
                listOfLiTag[i].children[0].children[0].onclick = function (e) {
                    if (e.target.classList.value.indexOf('error') == -1) {
                        e.target.parentElement.parentElement.parentElement.parentElement.children[0].children[0].value = e.target.parentElement.innerText;
                        el.style.display = 'none';
                    }
                };
            }
        }
    }
}

function search(i) {
    let ul = document.getElementsByClassName('search__list')[i];
    ul.style.display = 'block';
    let input = document.getElementsByClassName('search__input')[i];
    let text = input.value;
    let result = [];

    let dictionary = [];
    for (let j = 0; j < ul.children.length; j++) {
        if (ul.children[j].innerText !== 'Совпадений не найдено.') {
            dictionary.push({name: ul.children[j].innerText});
        }
    }
    // render default dictionary,if search string is empty
    if (text == "") {
        insertFromList(dictionary, ul);
        return;
    }
    //call compare func with all obj in dictionary
    for (let i = 0; i < dictionary.length; i++) {
        result.push({
            'city': dictionary[i],
            'value': compareNames(dictionary[i].name, text)
        });
    }
    //sort result array for max to min correlation coefficient
    result = result.sort(function (a, b) {
        return -(a.value - b.value);
    });
    //call render func
    insertFromList(result, ul);
}

//render func - take dictionary & ul
function insertFromList(list, elem) {
    while (elem.lastChild) {
        elem.removeChild(elem.lastChild);
    }
    for (let i = 0; i < list.length; i++) {
        let el = document.createElement('li');
        // fix issue with represent result & dictionary
        if (list[i].city === undefined) {
            el.innerHTML = list[i].name;
        }
        else {
            if (list[i].value < 0.2) {
            }
            let text = elem.parentElement.children[0].children[0].value,
                nameFill = '';
            for (let j = 0; j < text.length; j++) {
                try {
                    if (text[j].toLowerCase() == list[i].city.name[j].toLowerCase()) {
                        nameFill += text[j];
                    }
                    else {
                        break;
                    }
                }
                catch (er) {
                }
            }
            let name = document.createElement('p');
            let b = document.createElement('b');
            b.innerHTML = list[i].city.name.slice(0, nameFill.length);
            name.appendChild(b);
            name.innerHTML += list[i].city.name.slice(nameFill.length);
            if (list[i].value < 0.2) {
                el.style.display = 'none';
            }
            el.appendChild(name);
        }
        elem.appendChild(el);
    }
    let visibleLiCount = 0;
    for (let j = 0; j < elem.children.length; j++) {
        if (elem.children[j].style.display !== 'none') {
            visibleLiCount++;
        }
    }
    if (visibleLiCount == 0) {
        let el = document.createElement('li');
        el.classList.add('error');
        el.innerHTML = 'Совпадений не найдено.';
        elem.appendChild(el);
    }
    addEvListToLi(elem);
}

//func what choose all pair of string and create a correlation coefficient
function compareNames(s1, s2) {
    let longer = s1,
        shorter = s2;
    if (s1.length < s2.length) {
        longer = s2;
        shorter = s1;
    }
    if (longer.length == 0) {
        return 1.0;
    }
    return (longer.length - _editDistance(longer, shorter)) / parseFloat(longer.length);
}

//calc correlation coefficient
function _editDistance(s1, s2) {
    s1 = s1.toLowerCase();
    s2 = s2.toLowerCase();
    let costs = [];
    for (let i = 0; i <= s1.length; i++) {
        let lastValue = i;
        for (let j = 0; j <= s2.length; j++) {
            if (!i) {
                costs[j] = j;
                continue;
            }
            if (j > 0) {
                let newValue = costs[j - 1];
                if (s1[i - 1] != s2[j - 1]) {
                    newValue = Math.min(Math.min(newValue, lastValue), costs[j]) + 1;
                }
                costs[j - 1] = lastValue;
                lastValue = newValue;
            }
        }
        if (i > 0) {
            costs[s2.length] = lastValue;
        }
    }
    s2.split('').forEach((el, i) => {
        if (el == s1[i]) {
            costs[s2.length] -= 2;
        }
        else {
            costs[s2.length] += 2;
        }
    });
    return costs[s2.length];
}