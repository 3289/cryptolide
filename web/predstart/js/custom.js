$(function () {

  // Адаптивное меню
  $('.s-top__menu').before('<a href="#" id="touch-menu">Меню</a>');
  var touch = $('#touch-menu');
  var menu = $('.s-top__menu');

  $(touch).on('click', function(e){
    e.preventDefault();
    menu.slideToggle();
  });

  $(window).resize(function(){
    var wid = $(window).width();
    if (wid > 768) {
      menu.removeAttr('style');
    };
  });

  /* Подсчет заработка за первый месяц */
  function finalEarnings () {
    var y = parseInt($('#number-polls__number').val());
    var x = parseInt($('#number-friends__number').val());
    var formulaPart = (0.04 + 0.005 * x);
    if (formulaPart > 0.25) {
     formulaPart = 0.25;
    }
    var result = 0.875*y*30 + 5.00*x + formulaPart*x*120;
    $('.final-earnings__result-number').text(result.toFixed(2));
  }

  finalEarnings();

  /* Ползунок */
  $( "#number-polls__slider" ).slider({
    range: "min",
    value: 4,
    min: 1,
    max: 10,
    slide: function( event, ui ) {
      $( "#number-polls__number" ).val( ui.value );
      finalEarnings();
    }
  });
  $( "#number-friends__slider" ).slider({
    range: "min",
    value: 5,
    min: 0,
    max: 100,
    slide: function( event, ui ) {
      $( "#number-friends__number" ).val( ui.value );
      finalEarnings();
    }
  });

  /* Изменение ползунка в зависимости от значения, введенного в поле; подсчет формулы */
  function changeSlider (numberElement, sliderElement) {
    var number = parseInt(numberElement.val());
    var min = parseInt(sliderElement.slider( "option", "min" ));
    var max = parseInt(sliderElement.slider( "option", "max" ));

    if (number > max) {
      number = max;
    }
    if (number < min) {
      number = min;
    }

    numberElement.val(number);
    sliderElement.slider( "option", "value", number );
    finalEarnings();
  }

  $('#number-polls__number').on('change', function(event) {
    changeSlider($(this), $( "#number-polls__slider" ));
  });

  $('#number-friends__number').on('change', function(event) {
    changeSlider($(this), $( "#number-friends__slider" ));
  });

  // Фильтрация ввода в поля
  $('#number-polls__number, #number-friends__number').keypress(function(event){
    var key, keyChar;
    if(!event) var event = window.event;
    
    if (event.keyCode) key = event.keyCode;
    else if(event.which) key = event.which;
  
    if(key==null || key==0 || key==8 || key==13 || key==9 || key==46 || key==37 || key==39 ) return true;
    keyChar=String.fromCharCode(key);
    
    if(!/\d/.test(keyChar)) return false;
  });

	/* Circular progress loader */


  /* Показать/скрыть меню контактов */
  // $('a.show-contacts').on('click', function(event) {
  //   event.preventDefault();
  //   if ($('.block-menu-contacts').css('right') == '-400px') {
  //     $('.block-menu-contacts').css('right', '0');
  //   }
  //   else {
  //     $('.block-menu-contacts').css('right', '-400px');
  //   }
  //   return false;
  // });

  /* Скрыть меню контактов по нажатию на иконку закрытия */
  $('.block-menu-contacts .close').on('click', function(event) {
    $('.block-menu-contacts').css('right', '-400px');
  });

  /* Скрыть меню контактов по нажатию на любой области вне контактов */
  $(document).on('click', function(event) {
    if( $(event.target).closest(".block-menu-contacts").length ) 
      return;
    $('.block-menu-contacts').css('right', '-400px');
    event.stopPropagation();
  });

  // Изменение фона кнопки по вводу текста
  function changeBtnBg() {
    var flag = true;
    var btn = $(arguments[arguments.length-1]);
    for (var i = 0; i < arguments.length - 1; i++) {
      if ($(arguments[i]).val().length == 0) {
        flag = false;
        break;
      }
    }
    if (flag) {
      btn.removeClass('btn-green_non-hovered');
      btn.addClass('btn-green_hovered');
    }
    else {
      btn.removeClass('btn-green_hovered');
      btn.addClass('btn-green_non-hovered');
    }
  }

  $('#e0, #p0').on('input', function(event) {
    changeBtnBg('#e0', '#p0', '#loginButton0 input');
  });

  $('#e, #p, #p2').on('input', function(event) {
    changeBtnBg('#e', '#p', '#p2', '#regButton2 input');
  });

  $('#e0').on('input', function(event) {
    changeBtnBg('#e0', '#loginButton0 input');
  });

  $('.modal-withdraw #number, .modal-withdraw #amount').on('input', function(event) {
    changeBtnBg('.modal-withdraw #number', '.modal-withdraw #amount', '.form-withdraw__form-submit');
  });

  $('#activation_code').on('input', function(event) {
    changeBtnBg('#activation_code', '.form-email-confirm__form-submit');
  });

  $('#code').on('input', function(event) {
    changeBtnBg('#code', '.form-email-confirm__form-submit');
  });
});