function getParameterByName(name, value = null) {
    let url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");

    let regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)");
    let results = regex.exec(url);

    if (!results)
        return value;

    if (!results[2])
        return value;

    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

let _cities = [];

$(function() {
    $('.x-search').each(function(i, parent) {
        parent = $(parent);
        let target = $(parent.data('target'));
        let arrow = parent.parent().find('.arrow-block');

        $('body').on('click', function(event) {
            if (!parent.is($(event.target)) && !target.is($(event.target)) && !arrow.is($(event.target))) {
                target.css('display', 'none');
            }
        });

        arrow.on('click', function() {
            parent.trigger('focus');
        });

        target.on('click', 'li', function() {
            if ($(this).css('color') === 'rgb(211, 211, 211)') {
                return;
            }

            target.find('li').removeAttr('style');
            $(this).css('color', 'rgb(211, 211, 211)');
            parent.val($(this).text());
            target.css('display', 'none');
        });

        parent.on('focus', function() {
            target.css('display', '');
        }).on('keyup', function() {
            let query = $(this).val();
            target.find('li[data-id=""]').remove();

            if (query === '') {
                target.find('li').each(function(index, element) {
                    $(element).text($(element).text());
                });

                target.find('li').css('display', '');
            } else {
                let found = 0;

                target.find('li').each(function(index, element) {
                    let text = $(element).text();
                    if (query.length > text.length || text.substring(0, query.length).toLowerCase() !== query.toLowerCase()) {
                        $(element).css('display', 'none');
                    } else {
                        found++;
                        $(element).css('display', '');
                        $(element).html('<p style="font-weight: 400;"><strong>' + text.substring(0, query.length) + '</strong>' + text.substring(query.length) + '</p>');
                    }
                });

                if (target.find('li').length !== 0 && found === 0) {
                    target.prepend('<li style="color: lightgray;" data-id="">Ничего не найдено</li>');
                }
            }
        });
    });

    $('.bank-search').each(function(i, parent) {
        parent = $(parent);
        parent.prop('readonly', true);

        let list = parent.closest('.select-ul');
        let target = $(parent.data('target'));
        let arrow = parent.parent().find('.arrow-block');

        $('body').on('click', function(event) {
            if (!parent.is($(event.target)) && !list.is($(event.target)) && !arrow.is($(event.target)) && !target.is($(event.target))) {
                target.css('display', 'none');
            }
        });

        arrow.on('click', function() {
            parent.trigger('focus');
        });

        parent.on('focus', function() {
            target.css('display', '');
        });

        list.on('click', 'img', function () {
            let li = $(this).closest('li');
            target.find('li[data-id="' + li.data('id') + '"]').removeClass('selected').css('color', 'black');
            li.remove();
            recalculation();
        });

        function recalculation()
        {
            let ids = [];
            target.find('.selected').each(function(a, b) {
                ids.push($(b).data('id'));
            });
            $('[name="bank_ids"]').val(JSON.stringify(ids));
            if (ids.length === 0) {
                parent.attr('placeholder', 'Выберите банк');
            } else {
                parent.attr('placeholder', '');
            }
        }

        target.on('click', 'li', function () {
            if ($(this).hasClass('selected')) {
                return false;
            }

            if ($(this).data('id') === 0) {
                parent.val('Все банки страны');
                target.find('li').css('color', 'black').removeClass('selected');
                list.find('.select-ul__li').remove();
                $(this).css('color', '#d5d5d5').addClass('selected');
            } else {
                if (target.find('.selected').length === 3) {
                    return false;
                }

                $('.select-ul__li--input input').val('');
                target.find('li[data-id="0"]').css('color', 'black').removeClass('selected');

                $(this).css('color', '#d5d5d5').addClass('selected');
                target.css('display', 'none');

                list.append('<li class="select-ul__li" data-id="' + $(this).data('id') + '" style="order: 1;"><p>' + $(this).text() + '</p><img src="/img/exit.png"></li>');
            }

            recalculation();
        });
    });

    $('.city-search').each(function(i, parent) {
        parent = $(parent);
        let target = $(parent.data('target'));
        let arrow = parent.parent().find('.arrow-block');

        $('body').on('click', function(event) {
            if (!parent.is($(event.target)) && !target.is($(event.target)) && !arrow.is($(event.target))) {
                target.css('display', 'none');
            }
        });

        arrow.on('click', function() {
            parent.trigger('focus');
        });

        target.on('click', 'li', function() {
            if ($(this).css('color') === 'rgb(211, 211, 211)') {
                return;
            }

            target.find('li').removeAttr('style');
            $(this).css('color', 'rgb(211, 211, 211)');
            $('[name="city_id"]').val($(this).data('id'));
            parent.val($(this).text());
            target.css('display', 'none');
        });

        parent.on('focus', function() {
            target.css('display', '');
        }).on('keyup', function() {
            let query = $(this).val();
            $('[name="city_id"]').val('');
            function isValid(query, name) {
                return !(query.length > name.length || name.substring(0, query.length).toLowerCase() !== query.toLowerCase());
            }

            target.find('li').remove();
            if (query !== '') {
                _cities.forEach(function(city) {
                    if (isValid(query, city.name)) {
                        target.append('<li data-id="'+city.id+'"><p style="font-weight: 400;"><strong>' + city.name.substring(0, query.length) + '</strong>' + city.name.substring(query.length)  + (city.area ? ', ' + city.area : '') + (city.region ? ', ' + city.region : '') +  '</p></li>');
                    }
                });

                if (target.find('li').length === 0) {
                    target.append('<li style="color: lightgray;">Такой город не найден.</li>');
                }
            }
        });
    });

    if (
        window.location.pathname === '/p2p' ||
        window.location.pathname === '/cryptosell' ||
        window.location.pathname === '/p2p/buy_online' ||
        window.location.pathname === '/p2p/buy_for_cash' ||
        window.location.pathname === '/p2p/sell_online' ||
        window.location.pathname === '/p2p/sell_for_cash'
    ) {
        let already_set = false;

        $('#country-list').on('click', 'li', function () {
            let id = $(this).data('id');
            if (id === '') {
                return;
            }

            $('#city-name').val('').attr('placeholder', 'Загружаются города...').prop('disabled', true);
            $('[name="city_id"]').val('');
            $('#city-list').empty();
            $('[name="country_id"]').val(id);

            $.get('/countries/' + id).done(function (country) {
                if (country.currency !== 0 && !already_set) {
                    $('#currency-list li[data-id="' + country.currency + '"]').trigger('click');
                }

                already_set = false;

                $.get('/countries/' + id + '/cities').done(function(cities) {
                    _cities = cities;
                    $('#city-name').prop('disabled', false).attr('placeholder', 'Начните вводить город...');
                });

                if (getParameterByName('city_id') !== null) {
                    $('#city-list li[data-id="' + getParameterByName('city_id') + '"]').trigger('click');
                }
            });
        });

        $("#payment_type_input").keyup(function() {
            if (!this.value) {
                $('[name="payment_type_id"]').val(0);
            }
        });

        $('#payment-list').on('click', 'li', function() {
            let id = Number($(this).data('id'));
            if (id === '') {
                return;
            }

            $('[name="payment_type_id"]').val(id);
        });

        //Поиск по типу оплаты при нажатии ссылки
        $('.action-table__buy-with').on('click', 'a', function() {
            let id =Number($(this).attr('type_id'));
            if (id === '') {
                return;
            }
            $('[name="payment_type_id"]').val(id);
            $('.search-form__submit').trigger('click');
        });

        $('#currency-list').on('click', 'li', function () {
            let id = $(this).data('id');
            if (id === '') {
                return;
            }

            $('[name="currency_id"]').val(Number(id));
            already_set = true;
        });

        $('#currency-list li[data-id="' + getParameterByName('currency_id', $('[name="currency_id"]').val()) + '"]').trigger('click');

        $('#country-list li[data-id="' + getParameterByName('country_id', $('[name="country_id"]').val()) + '"]').trigger('click');

        $('#payment-list li[data-id="' + getParameterByName('payment_type_id', $('[name="payment_type_id"]').val()) + '"]').trigger('click');
    }

    if (window.location.pathname === '/cryptoexchange' || window.location.pathname === '/p2p/trade') {
        if (getParameterByName('crypto_trade_id') !== null) {
            $('#crypto-amount-short').text($('[name="crypto_trade_id"] option[value="' + getParameterByName('crypto_trade_id') + '"]').data('short'));
        }

        $('[name="crypto_trade_id"]').on('change', function() {
            $('#crypto-amount-short').text($(this).find('[value="' + $(this).val() + '"]').data('short'));
        });
    }
});