$(function() {
    let error_block = $('#error-block');
    let error_text = error_block.find('.form-error');
    let error_block_2 = $('#error-block-2');
    let error_text_2 = error_block_2.find('.form-error');

    $('[name="rate"],[name="min_currency"],[name="min_crypto"],[name="sum_currency"],[name="sum_crypto"]').on('keypress', function(e) {
        let th = $(this);
        setTimeout(function () {
            th.val(th.val().replace(',', '.'))
        }, 5);
        if (e.shiftKey === true ) {
            if (e.which == 44 || e.which == 9) {
                if (th.val().indexOf('.') != -1) {
                    return false;
                }
                return true;
            }
        }
        if (e.which == 46 || e.which == 44) {
            if (th.val().indexOf('.') != -1) {
                return false;
            }
            return true;
        }
        if (e.which > 57 || e.which < 48) {
            return false;
        }
        return true;
    });

    $('[name="rate"],[name="min_currency"],[name="min_crypto"],[name="sum_currency"],[name="sum_crypto"]').on('keypress', function(e) {
        let th = $(this);
        setTimeout(function () {
            th.val(th.val().replace(',', '.'));
        }, 5);
    });

    $('#country-list').on('click', 'li', function () {
        let id = $(this).data('id');
        if (id === '') {
            return;
        }

        if (id == $('[name="country_id"]').val()) {
            return;
        }

        $('[name="country_id"]').val(id);
        $('#city-name').val('').attr('placeholder', 'Загружаются города...').prop('disabled', true);
        $('#currency-name').val('');
        $('[name="currency_id"]').val('');
        $('#city-list').empty();
        $('#bank-list').html('<li data-id="0">Все банки страны</li>');
        $('#bank-list li[data-id="0"]').trigger('click');

        $.get('/countries/' + id).done(function (country) {
            $('[name="currency_id"]').val(country.currency);

            if (country.currency !== 0) {
                $('#currency-name').val($('#currency-list').find('[data-id="'+country.currency+'"]').text());
                $('.currency-short').text($('#currency-name').val());
            }

            $.get('/countries/' + id + '/cities').done(function(cities) {
                _cities = cities;
                $('#city-name').prop('disabled', false).attr('placeholder', 'Начните вводить город...');
            });

            if (country.banks.length > 0) {
                country.banks.forEach(function (bank) {
                    $('#bank-list').append('<li data-id="' + bank.id + '">' + bank.name + '</li>');
                });
            }

            showAdditionalData();
        });
    });

    $('#payment-list').on('click', 'li', function() {
        let id = Number($(this).data('id'));
        if (id === '') {
            return;
        }

        $('[name="payment_type_id"]').val(id);

        if (id === 2) {
            $('#bank-form').css('display', '');
            $('#bank-list li[data-id="0"]').trigger('click');
        } else {
            $('#bank-form').css('display', 'none');
        }
        validateAdvert();
        showAdditionalData();
    });

    $('#currency-list').on('click', 'li', function () {
        let id = $(this).data('id');
        if (id === '') {
            return;
        }

        $('[name="currency_id"]').val(Number(id));

        $('.currency-short').text($(this).text());

        showAdditionalData();
    });

    function setCase(type) {
        if (type === 'trade') {
            $('.case-1').text('обмена');
            $('.case-2').text('обменять');
        }

        if (type === 'sell_for_cash' || type === 'sell_online') {
            $('.case-1').text('продажи');
            $('.case-2').text('продать');
        }

        if (type === 'buy_for_cash' || type === 'buy_online') {
            $('.case-1').text('покупки');
            $('.case-2').text('купить');
        }
    }

    function showAdditionalData() {
        let addition_data = $('#additional-data');
        addition_data.css('display', 'none');

        let type = $('[name="type"]').val();
        let currency_id = $('[name="currency_id"]').val();
        let crypto_id = $('[name="crypto_id"]').val();
        let crypto_trade_id = $('[name="crypto_trade_id"]').val();
        let payment_type_id = $('[name="payment_type_id"]').val();
        let country_id = $('[name="country_id"]').val();


        $('#form-sum').css('display', '');
        $('#keep-up-date-change').css('display', 'none');
        $('#rate-purchase').css('display', '');
        $('#city-form').css('display', 'none');
        $('#transaction-details').css('display', 'none');
        $('#keep-up-date-nochange').css('display', 'none');
        $('#form-sum-sell').css('display', 'none');
        $('#constraints-form').css('display', 'none');
        $('#payment-type-form').css('display', 'none');
        $('#country-form').css('display', '');
        $('#crypto-type-purchase').css('display', 'none');
        $('#form-sum-description').css('display', '');
        $('#payment-and-currency-form').css('display', '');
        $('[name="keep_up"]').prop('checked', false);

        setCase(type);

        if (type === 'sell_online') {
            $('#form-sum-sell').css('display', '');
            $('#keep-up-date-nochange').css('display', 'none');
            $('#keep-up-date-change').css('display', '');
            $('#transaction-details').css('display', '');
            $('#constraints-form').css('display', '');
            $('#payment-type-form').css('display', '');
            $('[name="keep_up"]').prop('checked', true);
        }

        if (type === 'buy_online') {
            $('#keep-up-date-change').css('display', '');
            $('#payment-type-form').css('display', '');
            $('#form-sum-description').css('display', 'none');
            $('[name="keep_up"]').prop('checked', true);
        }

        if (type === 'sell_for_cash') {
            $('#city-form').css('display', '');
        }

        if (type === 'buy_for_cash') {
            $('#keep-up-date-change').css('display', '');
            $('#city-form').css('display', '');
            $('#form-sum-description').css('display', 'none');
        }

        if (type === 'trade') {
            $('#keep-up-date-change').css('display', '');
            $('[name="keep_up"]').prop('checked', true);
            $('[for="bitcoins-number-purchase"]').text('Сумма обмена:');

            $('#country-form').css('display', 'none');
            $('#payment-type-form').css('display', 'none');
            $('#crypto-type-purchase').css('display', '');
            $('#payment-and-currency-form').css('display', 'none');

            $('.type-str-def').text('обмен');
            $('.type-str-case-a').text('обменять');
            $('.type-str-case-i').text('обменять');

            $('.currency-short').text($('[name="crypto_trade_id"]').find('option:checked').data('short'));

            $('[name="min_currency"]').attr('placeholder', '0.00000000').attr('step', '0.00000001').attr('min', '0.00000000');
            $('[name="sum_currency"]').attr('placeholder', '0.00000000').attr('step', '0.00000001').attr('min', '0.00000000');
        } else {
            $('.currency-short').text($('#currency-list').find('li[data-id="'+$('[name="currency_id"]').val()+'"]').text());

            $('[name="min_currency"]').attr('placeholder', '0.00').attr('step', '0.01').attr('min', '0.01');
            $('[name="sum_currency"]').attr('placeholder', '0.00').attr('step', '0.01').attr('min', '1');
        }

        if (type === 'buy_online' || type === 'buy_for_cash') {
            $('[for="bitcoins-number-purchase"]').text('Сумма покупки:');

            $('.type-str-def').text('купить');
            $('.type-str-case-a').text('покупка');
            $('.type-str-case-i').text('покупки');
        }

        if (type === 'sell_online' || type === 'sell_for_cash') {
            $('.type-str-def').text('продать');
            $('.type-str-case-a').text('продажа');
            $('.type-str-case-i').text('продажи');

            $('[for="bitcoins-number-purchase"]').text('Сумма продажи:');
        }

        if (type !== 'sell_online' && type !== 'buy_online') {
            $('#bank-list li[data-id="0"]').trigger('click');
            $('#bank-form').css('display', 'none');
            $('#payment-type-name').val('');
            $('[name="payment_type_id"]').val('');
        }

        if (type === 'sell_for_cash' || type === 'buy_for_cash') {
            $('#payment-label').text('Валюта получения:');
        } else {
            $('#payment-label').text('Система и валюта получения:');
        }

        if ((type === 'trade' && crypto_trade_id === '') || (type !== 'trade' && currency_id === '')) {
            return;
        }

        error_block.css('display', 'none');

        addition_data.css('display', '');

        $.post('/rate', {
            crypto_id: crypto_id,
            type: type,
            currency_id: currency_id,
            crypto_trade_id: crypto_trade_id,
            payment_type_id: payment_type_id,
            country_id: country_id
        }).done(function(response) {
            if (response.rate === '0.00' || response.rate === 0.000000) {
                $('#rate-notice').css('display', 'none');
            } else {
                $('#rate-notice').css('display', '');
                $('.rate-sum').text(response.rate);
            }

            if (type !== 'buy_online' && type !== 'buy_for_cash') {
                $('.balance-amount').text(response.balance);
                $('.commission-sum').text(response.commission);
                $('.sum-with-commission').text(response.total);
            }
        });
        validateAdvert();
    }

    $('[name="crypto_id"]').on('change', function() {
        let select = $(this);
        $('.crypto-short').text(select.find('option:checked').data('short'));

        showAdditionalData();
    });

    $('[name="crypto_trade_id"]').on('change', function() {
        $('.currency-short').text($(this).find('option:checked').data('short'));

        showAdditionalData();
    });

    $('[name="constraints"]').on('change', function() {
        if ($(this).prop('checked') === true) {
            $('#constraints-min-form').css('display', '');
            $('#constraints-max-form').css('display', '');
        } else {
            $('#constraints-min-form').css('display', 'none');
            $('#constraints-max-form').css('display', 'none');
        }
    });

    $('[name="type"]').on('change', function() {
        if ($('[name="type"]').val() == 'trade') {
            setTradeClasses();
            $('.crypto-short').text($('[name="crypto_id"]').find('option:checked').data('short'));
        } else {
            removeTradeClasses();
        }

        showAdditionalData();
    });

    $('.sum-with-commission').on('click', function(event) {
        $('[name="sum_crypto"]').val($(this).text()).trigger('change');
    });

    $('[name="rate"],[name="sum_crypto"],[name="sum_currency"]').on('change keyup', function() {
        let sum_crypto = $('[name="sum_crypto"]');
        let sum_currency = $('[name="sum_currency"]');
        let rate = $('[name="rate"]');

        if (rate.val() === '' || (sum_crypto.val() === '' && sum_currency.val() === '')) {
            return;
        }

        if ($(this).attr('name') === 'sum_crypto') {
            if ($('[name="type"]').val() === 'trade') {
                sum_currency.val((sum_crypto.val() / rate.val()).toFixed(8));
            } else {
                sum_currency.val((sum_crypto.val() * rate.val()).toFixed(2));
            }
        } else {
            if ($('[name="type"]').val() === 'trade') {
                sum_crypto.val((sum_currency.val() * rate.val()).toFixed(8));
            } else {
                sum_crypto.val((sum_currency.val() / rate.val()).toFixed(8));
            }
        }
    });

    $('[name="rate"],[name="min_crypto"],[name="min_currency"]').on('change keyup', function() {
        let sum_crypto = $('[name="min_crypto"]');
        let sum_currency = $('[name="min_currency"]');
        let rate = $('[name="rate"]');

        if (rate.val() === '' || (sum_crypto.val() === '' && sum_currency.val() === '')) {
            return;
        }

        if ($(this).attr('name') === 'min_crypto') {
            if ($('[name="type"]').val() === 'trade') {
                sum_currency.val((sum_crypto.val() / rate.val()).toFixed(8));
            } else {
                sum_currency.val((sum_crypto.val() * rate.val()).toFixed(2));
            }
        } else {
            if ($('[name="type"]').val() === 'trade') {
                sum_crypto.val((sum_currency.val() * rate.val()).toFixed(8));
            } else {
                sum_crypto.val((sum_currency.val() / rate.val()).toFixed(8));
            }
        }
    });

    let btn_create_ad = $('#button-create-ad');
    let btn_create_ad_text = btn_create_ad.val();

    $('form').on('submit', function (event) {
        event.preventDefault();
        btn_create_ad.val('Подождите...').prop('disabled', true);

        error_block.css('display', 'none');
        error_block_2.css('display', 'none');

        let form = $(this);

        let url = '/publications';
        if ($('[name="id"]').val()) {
            url += '/'+ $('[name="id"]').val();
        }

        $.post(url, form.serialize()).done(function(response) {
            window.location.href = '/publications/' + response.id;
        }).fail(function(err) {
            error_block.css('display', '');
            let errors = err.responseJSON.errors;

            if (err.status !== 422) {
                error_text.text(err.statusText);
            } else {
                error_text.html(errors[Object.keys(errors)[0]][0]);
            }
            btn_create_ad.val(btn_create_ad_text).prop('disabled', false);
        });
    });


    $('form input').on('change paste keyup', function(event){
        event.preventDefault();
        validateAdvert();
    });

    function validateAdvert() {
        if (window.location.pathname.indexOf("edit") !== -1) {
            return;
        }

        btn_create_ad.val('Подождите...').prop('disabled', true);

        error_block.css('display', 'none');
        error_block_2.css('display', 'none');

        let form = $('form');

        let url = '/validate-advert-copy';

        $.ajax({
            url: url,
            method: 'POST',
            data: form.serialize()
        }).done(function(response) {
            btn_create_ad.val(btn_create_ad_text).prop('disabled', false);
            btn_create_ad.show();
        }).fail(function(err) {
            error_block.css('display', '');
            error_block_2.css('display', '');
            let errors = err.responseJSON.errors;

            if (err.status !== 422) {
                // error_text.text(err.statusText);
                // error_text_2.text(err.statusText);
                error_block.css('display', 'none');
                error_block_2.css('display', 'none');
                btn_create_ad.val(btn_create_ad_text).prop('disabled', false);
                btn_create_ad.show();
            } else {
                error_text.html(errors[Object.keys(errors)[0]][0]);
                error_text_2.html(errors[Object.keys(errors)[0]][0]);
                btn_create_ad.val(btn_create_ad_text).prop('disabled', false);
                btn_create_ad.hide();
            }
        });
    }

    function setTradeClasses() {
        $('.form-create-ad__item_purchase-rate .form-create-ad__control-prefix span').removeClass('crypto-short');
        $('.form-create-ad__item_purchase-rate .form-create-ad__control-prefix span').addClass('currency-short');

        $('.form-create-ad__item_purchase-rate .form-create-ad__control-suffix span').removeClass('currency-short');
        $('.form-create-ad__item_purchase-rate .form-create-ad__control-suffix span').addClass('crypto-short');
    }

    function removeTradeClasses() {
        $('.form-create-ad__item_purchase-rate .form-create-ad__control-prefix span').removeClass('currency-short');
        $('.form-create-ad__item_purchase-rate .form-create-ad__control-prefix span').addClass('crypto-short');

        $('.form-create-ad__item_purchase-rate .form-create-ad__control-suffix span').removeClass('crypto-short');
        $('.form-create-ad__item_purchase-rate .form-create-ad__control-suffix span').addClass('currency-short');
    }

    showAdditionalData();
});