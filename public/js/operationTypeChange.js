function cleaner() {
    $('#error-block').css('display', 'none');
    $('#error-block-2').css('display', 'none');
    $('.form-create-ad  input:not(:last-child)').val('');
    $("#country-input").val('');
    $("#country-list li").css("color", "");
    $("#payment-input").val('');
    $("#payment-list li").css("color", "");
    $("#transaction-description").val('');
    $("#time-pay-transaction").val('60');
    $("#payment-requisites").val('');
    $('.form-create-ad__select_who-can-make-deal li:first-child').click();
    if ($('#restriction-beginners-1-styler').hasClass('checked')) {
        $('#restriction-beginners-1-styler').click();
    }
    if ($('#restriction-beginners-styler').hasClass('checked')) {
        $('#restriction-beginners-styler').click();
    }
    if (!$('#amount_relevance-styler').hasClass('checked')) {
        $('#amount_relevance-styler').click();
    }
    $('#amount_relevance').val(1);
}

function changeTypeLabel(select) {
    if ($(select).val() == "trade") {
        $("#type-label").text("Отдаёте криптовалюту:");
    } else {
        $("#type-label").text("Тип криптовалюты:");
    }
}