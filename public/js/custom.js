$(function () {
	
	// Opening and closing news
	$('.news__title').on('click', function(event) {
		$(this).parents('.news__item').siblings().removeClass('news__item_open').find('.news__text').slideUp();
		$(this).parents('.news__item').toggleClass('news__item_open').find('.news__text').slideToggle();
	});

	/* Изменение URL в браузере без перезагрузки страницы */
	function changeUrl(title, url) {
    if (typeof (history.pushState) != "undefined") {
      var obj = { Title: title, Url: url };
      history.pushState(obj, obj.Title, obj.Url);
    } else {
      alert("Браузер не поддерживает HTML5.");
    }
	}

	// Opening and closing answers
	$('.faq__question').on('click', function(event) {
		$(this).parents('.faq__item').siblings().removeClass('faq__item_open').find('.faq__answer').slideUp();
		$(this).parents('.faq__item').toggleClass('faq__item_open').find('.faq__answer').slideToggle();
		changeUrl("/help", "/help#" + $(this).parent().attr('id'));
	});

	// OWL Carousel
	if ($('.owl-carousel').length) {
		$('.owl-carousel').owlCarousel({
			items: 3,
			margin: 40,
			nav: true,
			navText: ["<img src='images/icons/slider_prev.png'>","<img src='images/icons/slider_next.png'>"],
			dots: false,
			loop: true,
			// autoplay: true,

			responsive : {
				0 : {
					items: 1,
					margin: 1
				},
				481 : {
					items: 2,
					margin: 40,
				},
				768 : {
					items: 3,
					margin: 40,
				}
			}
		});
	}

	// User notifications
	$('.action-icons__link').on('click', function(event) {
		event.preventDefault();
		$(this).parent().siblings().find('.user-notifications').fadeOut(100);
		$(this).siblings('.user-notifications').fadeToggle(100);
	});

	$(document).mouseup(function (e) {
		var container = $(".user-notifications");
		if (container.has(e.target).length === 0 && !$(e.target).parent().hasClass('action-icons__link')){
			container.fadeOut(100);
		}
	});

	$.ajaxSetup({
		'headers':{
			'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
		}
	});

	$('[data-toggle="tooltip"]').tooltip();

	// Добавление класса active к ссылкам меню
  var pgurl = window.location.href.substr(window.location.href.lastIndexOf("/"));
  if (pgurl == '/p2p' || pgurl == '/cryptosell' || pgurl == '/cryptoexchange') {
  	$('.header__menu-item_trading-platform').addClass('header__menu-item_active');
  }

  // Ad complaint
	$('.complain-about-ad__link').on('click', function(event) {
		event.preventDefault();
		$('.ad-complaint').slideToggle();
	});

	$('.information-block__transaction-terms').on('click', function(event) {
		$(this).toggleClass('transaction-terms_open').find('.transaction-terms__text').slideToggle();
	});

	/* Автоматическая генерация id для FAQ */
	var allPanes = $('.faq .tab-pane');
	$.each(allPanes, function(index, elem) {
		var faqItems = $(elem).find('.faq__item');
		var paneId = $(elem).attr('id');
		$.each(faqItems, function(index, elem) {
			$(elem).attr('id', paneId + '_' + (index + 1));
		});
	});

	/* Открытие конкретного ответа по ссылке с учётом вкладок */
	var pgurl_hash = window.location.href.substr(window.location.href.lastIndexOf("#"));
	if (pgurl_hash != '/' && pgurl_hash != '#') {
		$('.faq ' + pgurl_hash).addClass('faq__item_open').find('.faq__answer').slideDown();
		$.each($('.category-help__link'), function(index, elem) {
			if (pgurl_hash.indexOf($(elem).attr('href')) >= 0) {
				console.log(elem);
				$(elem).tab('show');
			}
		});
	}

	/* Add body class based on the url */
	var loc = window.location.pathname.match(/^\/?(\w+)\b/);
	if(loc) {
	  $(document.body).addClass("page-" + loc[1].toLowerCase());
	} else {
	  $(document.body).addClass("page-front");
	}

	// Adaptive menu
  var touch = $('.menu-toggle');
  // var menu = $('.header-menu__wrap');

  $(touch).on('click', function(e){
    e.preventDefault();
    // menu.slideToggle();
    $(this).toggleClass('open');
  });

  $(window).resize(function(){
    var wid = $(window).width();
    if (wid > 1060) {
      // menu.removeAttr('style');
      $('.menu-toggle').removeClass('open');
    };
  });

  $('.drawer').drawer();

});