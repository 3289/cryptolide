$(document).ready(function () {
    var operationsList = $('li.action-icons__item_wallet .user-notifications__body.stylized-scroll');
    var operationsCount = $('#count-operations');

    var partnerHistoryList = $('li.action-icons__item_partner .user-notifications__body.stylized-scroll');
    var partnerHistoryCount = $('#count-partner-history');

    var dealHistoryList = $('li.action-icons__item_deals .user-notifications__body.stylized-scroll');
    var dealHistoryCount = $('#count-deal-history');

    $('.action-icons__item_wallet').on('click', function () {
        $.ajax('/read-operations', {
            type: 'PATCH',
            success: function () {
                operationsCount.text(0);
                operationsCount.hide();
            }
        });
    });

    $('.action-icons__item_deals').on('click', function () {
        $.ajax('/read-deal-history', {
            type: 'PATCH',
            success: function () {
                dealHistoryCount.text(0);
                dealHistoryCount.hide();
            }
        });
    });

    $('.action-icons__item_partner').on('click', function () {
        $.ajax('/read-partner-history', {
            type: 'PATCH',
            success: function () {
                partnerHistoryCount.text(0);
                partnerHistoryCount.hide();
            }
        });
    });

    $('.btn-back').on('click', function (event) {
        event.preventDefault();

        window.history.back();
    });

    function setNewOperations(operations) {
        if ($(operations).length !== 0 && $(operations).length !== parseInt(operationsCount.text())) {
            for (operation in operations) {
                addOperation(operations[operation]);
            }
            operationsList.find('.user-notifications__empty').hide();
            operationsCount.text($(operations).length);
            operationsCount.show();
        }
    }

    function addOperation(operation) {
        operationsList.prepend('<div class="notification-message notification-message_wallet clearfix"> ' +
            '<div class="notification-message__img"><img src="'+operation.img+'" alt="Аватар"></div> ' +
            '<div class="notification-message__content"> ' +
            '<div class="notification-message__date">'+operation.date+'</div> ' +
            '<div class="notification-message__text">' +operation.text+ '</div></div></div>');
    }

    function setNewPartnerHistoies(partnerHistories) {
        if ($(partnerHistories).length !== 0 && $(partnerHistories).length !== parseInt(partnerHistoryCount.text())) {
            for (partnerHistory in partnerHistories) {
                addPartnerHistory(partnerHistories[partnerHistory]);
            }
            partnerHistoryList.find('.user-notifications__empty').hide();
            partnerHistoryCount.text($(partnerHistories).length);
            partnerHistoryCount.show();
        }
    }

    function addPartnerHistory(partnerHistory) {
        partnerHistoryList.prepend('<div class="notification-message clearfix"> ' +
            '<div class="notification-message__img"><img src="'+partnerHistory.img+'" alt="Аватар"></div> ' +
            '<div class="notification-message__content"> ' +
            '<div class="notification-message__date">'+partnerHistory.date+'</div> ' +
            '<div class="notification-message__text">Пользователь <a href="/profile/'+partnerHistory.login+'">'+partnerHistory.login+'</a>' +
            ' присоединился в <a href="/my-partners">вашу команду.</a></div></div></div>');
    }

    function setNewDealHistories(dealHistories) {
        if ($(dealHistories).length !== 0 && $(dealHistories).length !== parseInt(dealHistoryCount.text())) {
            for (dealHistory in dealHistories) {
                addDealHistory(dealHistories[dealHistory]);
            }
            dealHistoryList.find('.user-notifications__empty').hide();
            dealHistoryCount.text($(dealHistories).length);
            dealHistoryCount.show();
        }
    }

    function addDealHistory(dealHistory) {
        dealHistoryList.prepend('<div class="notification-message notification-message_deals clearfix"> ' +
            '<div class="notification-message__img"><img src="'+dealHistory.img+'" alt="Аватар"></div> ' +
            '<div class="notification-message__content"> ' +
            '<div class="notification-message__date">'+dealHistory.date+'</div> ' +
            '<div class="notification-message__text">'+dealHistory.text+'</div></div></div>');
    }

    function refreshNotifications() {
        $.ajax('/refresh-notifications', {
            type: 'GET',
            success: function (data) {
                setNewOperations(data.operations);
                setNewPartnerHistoies(data.partnerHistories);
                setNewDealHistories(data.dealHistories);
                setTimeout(refreshNotifications, 2000);
            }
        });
    }
    refreshNotifications();
});