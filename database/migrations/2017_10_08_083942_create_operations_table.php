<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOperationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('operations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('crypto_id');
            $table->integer('user_id');
            $table->integer('deal_id')->nullable();
            $table->double('amount');
            $table->double('commission')->default(0);
            $table->double('commission_partner')->nullable();
            $table->text('description');
            $table->timestamps();
        });

        DB::update("ALTER TABLE operations AUTO_INCREMENT = 3902;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('operations');
    }
}
