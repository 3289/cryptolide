<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdvertsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adverts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('crypto_id');
            $table->integer('crypto_trade_id')->nullable();
            $table->json('bank_ids')->nullable();
            $table->integer('city_id')->nullable();
            $table->integer('achievement_id')->nullable();
            $table->integer('country_id')->nullable();
            $table->integer('currency_id')->nullable();
            $table->integer('payment_type_id')->nullable();
            $table->boolean('keep_up')->default(false);
            $table->boolean('constraints')->default(false);
            $table->double('constraints_min')->nullable();
            $table->double('constraints_max')->nullable();
            $table->double('min_crypto')->nullable();
            $table->double('sum_crypto');
            $table->double('rate');
            $table->integer('time')->nullable();
            $table->text('requisites')->nullable();
            $table->text('description')->nullable();
            $table->enum('type', ['buy_for_cash', 'buy_online', 'sell_for_cash', 'sell_online', 'trade']);
            $table->enum('status', ['active', 'hidden'])->default('active');
            $table->timestamps();
        });

        DB::update("ALTER TABLE adverts AUTO_INCREMENT = 1023;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('adverts');
    }
}
