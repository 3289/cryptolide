<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOperationPartnersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('operation_partners', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('crypto_id');
            $table->integer('user_id');
            $table->integer('partner_id');
            $table->double('amount');
            $table->boolean('transferred')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('operation_partners');
    }
}
