<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('last_name')->nullable();
            $table->string('middle_name')->nullable();
            $table->boolean('show_name')->default(true);
            $table->string('login')->unique();
            $table->string('email')->unique();
            $table->string('password');
            $table->string('phone');
            $table->double('rating')->default(0);
            $table->string('google_fa')->nullable();
            $table->double('commission_deal')->default(0.35);
            $table->double('commission_partner')->default(20);
            $table->boolean('activated')->default(false);
            $table->boolean('verified')->default(false);
            $table->integer('referrer_id')->nullable();
            $table->enum('role', ['user', 'admin'])->default('user');
            $table->rememberToken();
            $table->timestamps();
        });

        DB::update("ALTER TABLE users AUTO_INCREMENT = 4377;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
