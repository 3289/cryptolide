<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserWithBigId2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::table('users')->insert([
            'id' => 47491,
            'name' => 'test',
            'show_name' => false,
            'login' => 'te3st',
            'email' => 'te3st@t.t',
            'password' => 'test',
            'phone' => '03',
            'rating' => 0,
            'commission_deal' => 0,
            'commission_partner' => 0,
            'activated' => false,
            'verified' => false,
            'role' => 'user',
            'change_commission' => false,
        ]);

        \DB::table('users')->delete(47491);

    }

}
