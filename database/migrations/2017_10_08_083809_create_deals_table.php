<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDealsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deals', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('advert_id');
            $table->integer('user_id');
            $table->integer('creator_id');
            $table->integer('country_id')->nullable();
            $table->integer('city_id')->nullable();
            $table->integer('crypto_id');
            $table->integer('crypto_trade_id')->nullable();
            $table->integer('currency_id')->nullable();
            $table->integer('payment_type_id')->nullable();
            $table->json('bank_ids')->nullable();
            $table->double('sum_crypto');
            $table->double('rate');
            $table->text('requisites')->nullable();
            $table->text('description');
            $table->enum('type', ['buy_for_cash', 'buy_online', 'sell_for_cash', 'sell_online', 'trade']);
            $table->integer('time')->nullable();
            $table->enum('status', ['pending', 'cancelled', 'paid', 'completed'])->default('pending');
            $table->timestamps();
        });

        DB::update("ALTER TABLE deals AUTO_INCREMENT = 1593;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deals');
    }
}
