<?php

use Illuminate\Database\Seeder;

class CryptoCurrenciesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('crypto_currencies')->insert(['name' => 'Bitcoin', 'short' => 'BTC']);
        DB::table('crypto_currencies')->insert(['name' => 'Litecoin', 'short' => 'LTC']);
        DB::table('crypto_currencies')->insert(['name' => 'DASH', 'short' => 'DASH']);
    }
}
