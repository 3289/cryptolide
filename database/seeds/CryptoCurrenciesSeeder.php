<?php

use App\Models\CryptoCurrency;
use Illuminate\Database\Seeder;

class CryptoCurrenciesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CryptoCurrency::create(['name' => 'Bitcoin', 'short' => 'BTC']);
        CryptoCurrency::create(['name' => 'Litecoin', 'short' => 'LTC']);
        CryptoCurrency::create(['name' => 'DASH', 'short' => 'DASH']);
    }
}
