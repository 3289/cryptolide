<?php

use Illuminate\Database\Seeder;

class PaymentTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('payment_types')->insert(['name' => 'Международный SWIFT перевод']);
        //DB::table('payment_types')->insert(['name' => 'Все способы онлайн']);
        DB::table('payment_types')->insert(['name' => 'Банковский перевод']);
    }
}
