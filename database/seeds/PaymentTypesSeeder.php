<?php

use App\Models\PaymentType;
use Illuminate\Database\Seeder;

class PaymentTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arr = [
            'Международный SWIFT перевод',
            'Банковский перевод',
            'WebMoney',
            'Яндекс Деньги',
            'Qiwi',
            'Perfect Money',
            'PayPal',
            'Advanced Cash',
            'Payeer',
            'Skrill',
            'Payza',
            'OKPay',
            'MoneyGram',
            'Western Union',
            'Ria Money Transfer',
            'W1',
            'Paymer',
            'Paxum',
            'Contact',
            'Payoneer',
            'Другой способ онлайн оплаты',
            'Наличные',
            'SolidTrustPay',
            'Neteller',
            'CashU',
            'Transferwise'
        ];

        foreach ($arr as $item) {
            PaymentType::create(['name' => $item]);
        }
        
    }
}
