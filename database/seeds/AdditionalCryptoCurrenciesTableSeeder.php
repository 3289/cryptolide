<?php

use Illuminate\Database\Seeder;

class AdditionalCryptoCurrenciesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('additional_crypto_currencies')->insert(['name' => 'Ethereum', 'short' => 'ETH']);
    }
}
