<?php

use Illuminate\Database\Seeder;

class AchievementsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('achievements')->insert([
            'name' => 'Награда №1',
        ]);

        DB::table('achievements')->insert([
            'name' => 'Награда №2',
        ]);

        DB::table('achievements')->insert([
            'name' => 'Награда №3',
        ]);

        DB::table('achievements')->insert([
            'name' => 'Награда №4',
        ]);

        DB::table('achievements')->insert([
            'name' => 'Награда №5',
        ]);
    }
}
