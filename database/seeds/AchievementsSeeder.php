<?php

use App\Models\Achievement;
use Illuminate\Database\Seeder;

class AchievementsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Achievement::create(['name' => 'Верифицирован телефонный номер', 'image' => '/images/icons/icon_mobile_verified']);
        Achievement::create(['name' => 'Пройдена полная верификация личности', 'image' => '/images/icons/ok.png']);
        Achievement::create(['name' => 'Рекомендован к сотрудничеству компанией CRYPTOLIDE', 'image' => '/images/icons/like.png']);
    }
}
