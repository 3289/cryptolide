<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AchievementsSeeder::class);
        $this->call(AdditionalCryptoCurrenciesSeeder::class);
        $this->call(BanksSeeder::class);
        $this->call(CountriesSeeder::class);
        $this->call(CryptoCurrenciesSeeder::class);
        $this->call(CurrenciesSeeder::class);
        $this->call(PaymentTypesSeeder::class);
        $this->call(RisksSeeder::class);
        $this->call(UsersSeeder::class);
    }
}
