<?php

use Illuminate\Database\Seeder;
use App\Models\AdditionalCryptoCurrency;

class AdditionalCryptoCurrenciesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arr = [
            ['Ethereum', 'ETH'],
            ['Bitcoin Cash', 'BCH'],
            ['Ripple', 'XRP'],
            ['Litecoin', 'LTC'],
            ['DASH', 'DASH'],
            ['NEO', 'NEO'],
            ['NEM', 'XEM'],
            ['Monero', 'XMR'],
            ['Ethereum Classic', 'ETC'],
            ['IOTA', 'MIOTA'],
            ['Qtum', 'QTUM'],
            ['OmiseGO', 'OMG'],
            ['Zcash', 'ZEC'],
            ['Lisk', 'LSK'],
            ['BitConnect', 'BCC'],
            ['Cardano', 'ADA'],
            ['Tether', 'USDT'],
            ['Stellar', 'XLM'],
            ['EOS', 'EOS'],
            ['Hshare', 'HSR'],
            ['Waves', 'WAVES'],
        ];

        foreach ($arr as $item) {
            AdditionalCryptoCurrency::create(['name' => $item[0], 'short' => $item[1]]);
        }
    }
}
