<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if (auth()->check()) {
        return redirect('/p2p?crypto_id=1&sum_currency=&currency_id=155&payment_type_id=&country_id=206&city_id=');
    }

    return view('welcome')->with('viewport', true);
});

Route::view('about-company', 'about-company');

Route::view('agreement', 'agreement');

Route::view('nocommission', 'nocommission');

Route::view('affiliate-program', 'affiliate-program');

Route::get('commissions', 'StaticController@commission');

Route::view('contacts', 'contacts');

Route::view('help', 'help');

Route::view('news', 'news');

Route::view('remote-work', 'remote-work');

Route::view('representatives', 'representatives');

Route::view('security', 'security');

Route::redirect('home', '/p2p')->name('home');

Route::view('exchange','exchange');
Route::view('typeofrisk','typeofrisk');
Route::view('wallet','wallet');
Route::view('personalbonuses','personalbonusesforguest');

Route::get('p2p', 'PagesController@buy');
Route::get('cryptoexchange', 'PagesController@trade');
Route::get('cryptosell', 'PagesController@sell');
Route::get('p2p/{type}', 'PagesController@paginate');

Route::get('profile/{login}', 'ProfileController@show');
Route::get('profile/{login}/adverts', 'ProfileController@adverts');

Route::get('countries/{country}', 'CountryController@show');
Route::get('countries/{country}/cities', 'CountryController@cities');

Route::get('user/activation/{user_activation}', 'ActivationController@handle')->name('user.activate');

Route::group(['middleware' => 'guest'], function() {
    Auth::routes();

    Route::get('2fa/validate', 'Auth\LoginController@showConfirmationLogin');
    Route::post('2fa/validate', ['middleware' => 'throttle:5', 'uses' => 'Auth\LoginController@confirmationLogin']);
});

Route::get('publications/create', 'AdvertController@create');
Route::get('publications/{advert}', 'AdvertController@show');

Route::group(['middleware' => ['auth', 'activity']], function() {
    Route::get('logout', 'Auth\LoginController@logout');

    Route::patch('change-visibility/{advert}', 'AdvertController@changeVisibility');

    Route::post('is-address-has-a-user', 'AddressController@isAddressHasAUser');

    Route::get('send-email-confirmation', 'EmailController@confirm');

    Route::patch('read-operations', 'OperationController@update');

    Route::patch('read-deal-history', 'DealHistoryController@update');

    Route::patch('read-partner-history', 'PartnerController@read_partner_history');

    Route::patch('avatar', 'AvatarController@update');

    Route::post('rate', 'RateController@calculation');

    Route::get('my-wallet', 'OperationController@index');

    Route::view('general-information', 'general-information');
    Route::patch('general-information', 'SettingController@update_general');

    Route::get('my-security', 'SettingController@security');
    Route::patch('my-security', 'SettingController@update_security');

    Route::view('verification', 'verification');
    Route::post('verification', 'VerificationController@index');

    Route::get('2fa/enable', 'GoogleFAController@enable');
    Route::post('2fa/enable', 'GoogleFAController@confirm_enable');
    Route::get('2fa/disable', 'GoogleFAController@disable');
    Route::post('2fa/disable', 'GoogleFAController@confirm_disable');

    Route::patch('change_email', 'SettingController@change_email');
    Route::patch('confirm_change_email', 'SettingController@confirm_change_email');
    Route::patch('change_password', 'SettingController@change_password');

    Route::get('publications', 'AdvertController@index');
    Route::post('publications', 'AdvertController@store');
    Route::post('validate-advert-copy', 'AdvertController@validateAdvertCopy');
    Route::get('publications/{advert}/edit', 'AdvertController@edit');
    Route::patch('publications/{advert}', 'AdvertController@update');
    Route::delete('publications/{advert}', 'AdvertController@destroy');

    Route::get('deals', 'DealController@index');
    Route::post('deals', 'DealController@store');
    Route::get('deals/{deal}', 'DealController@show');
    Route::patch('deals/{deal}', 'DealController@update');

    Route::get('send-confirmation-code/{deal}', 'DealController@send_confirmation_code');


    Route::post('messages', 'MessageController@store');
    Route::get('messages/{deal}', 'MessageController@show');

    Route::post('withdrawals/{crypto_currency}', 'WithdrawalController@store');
    Route::get('withdrawals/{crypto_currency}', 'WithdrawalController@show');

    Route::get('replenishment/{crypto_currency}', 'AddressController@index');

    Route::get('my-partners', 'PartnerController@index');	
    Route::get('transfer-income/{crypto_currency}', 'PartnerController@transfer_income');

    Route::get('user/{user}/auth','ProfileController@auth');

    Route::get('refresh-notifications', 'ProfileController@refreshNotifications');
	
	Route::get('mybonuses', 'BonusesController@index');
});

Route::group(['middleware' => 'admin', 'namespace' => 'Admin', 'prefix' => 'admin'], function() {
    Route::get('users','UserController@index');
    Route::get('achievements','AchievementController@index');
    Route::get('addresses','AddressController@index');
    Route::get('countries_and_currencies','CountryController@index');
    Route::get('payment_types_and_risks','PaymentTypeController@index');
    Route::get('operations','OperationController@index');
    Route::get('replenishment','OperationController@create');
    Route::get('banks','BankController@index');
    Route::get('withdrawals', 'WithdrawalController@index');
    Route::get('additional_crypto_currencies', 'AdditionalCryptoCurrencyController@index');
    Route::get('commissions', 'CryptoCurrencyController@index');
    Route::get('profit', 'ProfitController@index');
    Route::get('deals', 'DealController@index');
    Route::get('verification', 'VerificationController@index');
    Route::get('notices','NoticeController@index');

    Route::get('users/{user}','UserController@show');
    Route::get('users/{user}/auth','UserController@auth');
    Route::patch('users/{user}','UserController@update');
    Route::delete('users/{user}','UserController@destroy');

    Route::post('achievements', 'AchievementController@store');
    Route::get('achievements/{achievement}', 'AchievementController@show');
    Route::post('achievements/{achievement}','AchievementController@update');
    Route::delete('achievements/{achievement}','AchievementController@destroy');

    Route::post('addresses','AddressController@store');
    Route::get('send-confirmation-code', 'AddressController@sendConfirmationCode');

    Route::post('countries','CountryController@store');
    Route::get('countries/{country}','CountryController@show');
    Route::patch('countries/{country}','CountryController@update');
    Route::delete('countries/{country}','CountryController@destroy');

    Route::post('currencies','CurrencyController@store');
    Route::patch('currencies/{currency}','CurrencyController@update');
    Route::delete('currencies/{currency}','CurrencyController@destroy');

    Route::post('operations','OperationController@store');

    Route::post('banks','BankController@store');
    Route::get('banks/{bank}','BankController@show');
    Route::patch('banks/{bank}','BankController@update');
    Route::delete('banks/{bank}','BankController@destroy');

    Route::post('risks','RiskController@store');
    Route::delete('risks/{risk}','RiskController@destroy');

    Route::post('payment_types','PaymentTypeController@store');
    Route::get('payment_types/{payment_type}','PaymentTypeController@show');
    Route::patch('payment_types/{payment_type}','PaymentTypeController@update');
    Route::delete('payment_types/{payment_type}','PaymentTypeController@destroy');

    Route::delete('withdrawals/{withdrawal}', 'WithdrawalController@destroy');
    Route::patch('withdrawals/{withdrawal}', 'WithdrawalController@update');

    Route::post('additional_crypto_currencies', 'AdditionalCryptoCurrencyController@store');
    Route::delete('additional_crypto_currencies/{additional_crypto_currency}', 'AdditionalCryptoCurrencyController@destroy');

    Route::get('commissions/{crypto_currency}', 'CryptoCurrencyController@show');
    Route::patch('commissions/{crypto_currency}','CryptoCurrencyController@update');

    Route::patch('deals/{deal}', 'DealController@changeStatus');
    Route::get('deals/{deal}/auth', 'DealController@auth');

    Route::post('notices','NoticeController@store');
    Route::get('notices/{notice}','NoticeController@show');
    Route::patch('notices/{notice}','NoticeController@update');
    Route::delete('notices/{notice}','NoticeController@destroy');
});
